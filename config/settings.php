<?php

return [
    'site-name' => [
        'title' => 'Site Name',
        'description' => 'Website Name',
        'type' => 'suitText',
        'value' => 'Chattime Indonesia'
    ],
    'site-description' => [
        'title' => 'Site Description',
        'Description' => 'Website description',
        'type' => 'suitTextarea',
        'value' => ''
    ],
    'site-image' => [
        'title' => 'Site Image',
        'Description' => 'Website image for social media',
        'type' => 'suitFileBrowser',
        'value' => ''
    ],
    'logo-url' => [
        'title' => 'Admin Logo Url',
        'type' => 'suitFileBrowser',
        'value' => 'assets/admin/img/logo.png'
    ],
    'social-facebook-username' => [
        'title' => 'Facebook username',
        'type' => 'suitText',
        'value' => 'acehardware'
    ],
    'social-instagram-username' => [
        'title' => 'Instagram username',
        'type' => 'suitText',
        'value' => 'acehardware'
    ],
    'social-twitter-username' => [
        'title' => 'Twitter username',
        'type' => 'suitText',
        'value' => 'AceHardware'
    ],
    'inbox-email-receiver' => [
        'title' => 'Inbox email receiver',
        'type' => 'suitTokenField',
        'value' => 'admin@ace-hardware.co.id, customer.care@ace-hardware.co.id, info@acehardware.co.id'
    ],
    'inbox-email-receiver-default' => [
        'title' => 'Inbox email receiver',
        'type' => 'suitTokenField',
        'value' => 'info@acehardware.co.id'
    ],
    'social-wall-title' => [
        'title' => 'Social Wall Title',
        'type' => 'suitText',
        'value' => 'AceHardware Social Wall'
    ],
    'social-wall-desc' => [
        'title' => 'Social Wall Description',
        'type' => 'suitTextarea',
        'value' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste ipsa quaerat vitae at optio, error eveniet. Assumenda, ducimus, neque? Totam soluta eaque dolores voluptatum inventore aut velit magni aspernatur, enim sequi illo quaerat tempora, voluptas amet commodi. Ad nulla autem magni soluta iure, explicabo amet neque. Architecto libero, asperiores quisquam.'
    ],
    'social-wall-middle-image-url' => [
        'title' => 'Middle image in Social Wall',
        'type' => 'suitFileBrowser',
        'value' => 'assets/img/ig-thumb-big.jpg'
    ],
    'download-app-data-ios' => [
        'title' => 'Data IOS for Download App',
        'type' => 'suitText',
        'value' => 'https://itunes.apple.com/us/app/ace-hardware-indonesia/id1056027974?ls=1&mt=8'
    ],
    'download-app-data-android' => [
        'title' => 'Data Android for Download App',
        'type' => 'suitText',
        'value' => 'https://play.google.com/store/apps/details?id=id.co.acehardware.acerewards&hl=en'
    ],
    'membership-banner-title' => [
         'title' => 'Membership Banner Title',
         'type' => 'suitText',
         'value' => 'Ace Hardware Rewards'
    ],
    'membership-banner-description' => [
        'title' => 'Membership Banner Description',
        'type' => 'suitWysiwyg',
        'value' => '
            <h2>Get rewarded for shopping</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil voluptatibus voluptates placeat, debitis delectus perferendis cumque voluptatum facere optio ratione ab vero facilis ipsam reprehenderit illo a porro enim ut!</p>'
    ],
    'membership-banner-image-url' => [
        'title' => 'Membership Banner Image',
        'type' => 'suitFileBrowser',
        'value' => ''
    ],
    'membership-voucher-banner-title' => [
         'title' => 'Membership Voucher Banner Title',
         'type' => 'suitText',
         'value' => 'Ace Hardware Rewards'
    ],
    'membership-voucher-banner-description' => [
        'title' => 'Membership Voucher Banner Description',
        'type' => 'suitWysiwyg',
        'value' => '
            <h2>Get rewarded for shopping</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil voluptatibus voluptates placeat, debitis delectus perferendis cumque voluptatum facere optio ratione ab vero facilis ipsam reprehenderit illo a porro enim ut!</p>'
    ],
    'membership-voucher-banner-image-url' => [
        'title' => 'Membership Voucher Banner Image',
        'type' => 'suitFileBrowser',
        'value' => ''
    ],
    'membership-merchandise-banner-title' => [
         'title' => 'Membership Merchandise Banner Title',
         'type' => 'suitText',
         'value' => 'SPECIAL MERCHANDISE'
    ],
    'membership-merchandise-banner-description' => [
        'title' => 'Membership Merchandise Banner Description',
        'type' => 'suitWysiwyg',
        'value' => '
            <h2>Tukarkan poin yang anda miliki dengan merchandise kami</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil voluptatibus voluptates placeat, debitis delectus perferendis cumque voluptatum facere optio ratione ab vero facilis ipsam reprehenderit illo a porro enim ut!</p>'
    ],
    'membership-merchandise-banner-image-url' => [
        'title' => 'Membership Merchandise Banner Image',
        'type' => 'suitFileBrowser',
        'value' => ''
    ],
    'membership-become-member-title' => [
         'title' => 'Membership Become Member Title',
         'type' => 'suitText',
         'value' => 'How to Become Member'
    ],
    'membership-become-member-description' => [
        'title' => 'Membership come Member BeDescription',
        'type' => 'suitWysiwyg',
        'value' => "
            <ol class='custom-ol'>
                <li>Daftarkan diri anda secara online ataupun offline di toko kami</li>
                <li>Lengkapi biaya administrasi</li>
                <li>Anda telah menjadi member</li>
            </ol>"
    ],
    'online-registration-description' => [
        'title' => 'Membership come Member BeDescription',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>ONLINE REGISTRATION</h2>
            <p>Kode TAM akan dikirimkan ke email anda. Silahkan lanjutkan pembayaran melalui <a href='#''>http://www.ruparupa.com/</a>.
            </p>"
    ],
    'offline-registration-description' => [
        'title' => 'Membership come Member BeDescription',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>OFFLINE REGISTRATION</h2>
            <p>Kode TAM akan dikirimkan ke email anda. Silahkan lanjutkan pembayaran di toko ACE Hardware terdekat dengan menunjukan kode TAM tersebut.
            </p>"
    ],
    'free-membership-description' => [
        'title' => 'Membership come Member BeDescription',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>FREE MEMBERSHIP</h2>
            <p><b>Syarat dan Ketentuan</b></p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis distinctio sunt dolor blanditiis sequi doloremque, voluptas quas excepturi pariatur. At rem quae asperiores, maiores molestias nobis quia ea? Ipsum sit, ratione tenetur rem? At adipisci cum quis sint molestias quia, alias, a veritatis modi numquam eum omnis itaque tempore natus.</p>
            <p>Atque, obcaecati, doloremque? Mollitia repudiandae voluptas hic unde rem neque beatae consequuntur inventore illo quibusdam voluptatum deleniti, cumque, odit similique et porro deserunt nulla delectus ad, natus quas quae enim! Rem quasi ab, similique iusto, et quod temporibus alias excepturi debitis itaque distinctio tempore, adipisci a odio harum perspiciatis quo!
            </p>"
    ],
    'auto-renewal-yes-description' => [
        'title' => 'Auto Renewal Yes Description',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>Apa anda yakin?</h2>
            <p>You need to renew your membership every year. Would you like to do it automatically by deducting your points (20 points)?
            </p>"
    ],
    'auto-renewal-no-description' => [
        'title' => 'Auto renewal No Description',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>Apa anda yakin?</h2>
            <p>Apabila anda tidak memilih auto renewal anda harus membayar setiap tahun sebesar Rp50.000,00. Keterlambatan pembayaran menyebabkan akun dan poin tidak dapat digunakan kembali.
            </p>"
    ],
    'membership-register-shopping-purpose' => [
        'title' => 'Shopping purpose list',
        'type' => 'suitTokenField',
        'value' => 'Peralatan Kantor, Peralatan Rumah'
    ],
    'profile-auto-renewal-description' => [
        'title' => 'Auto renewal description in Profile',
        'type' => 'suitTextarea',
        'value' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis deserunt dicta autem accusantium, dignissimos fuga?'
    ],
    'data-warning-edit-profile' => [
        'title' => 'Data warning edit profile',
        'type' => 'suitTextarea',
        'value' => 'Maaf anda tidak dapat mengubah data ini. Silahkan hubungi customer service di toko terdekat untuk update data ini.'
    ],
    'event-testimony-description' => [
        'title' => 'Description for Event Testimony',
        'type' => 'suitWysiwyg',
        'value' => '
            <h2>Apa pendapat anda mengenai Krisbow?</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque mollitia ducimus laborum architecto, maiores quis recusandae molestiae ipsum, beatae, ipsa excepturi eius voluptatibus, placeat ut odit accusamus iste quae facilis.</p>'
    ],
    'event-free-membership-description' => [
        'title' => 'Description for Event Free Membership',
        'type' => 'suitWysiwyg',
        'value' => '
            <h2>Dapatkan free membership</h2>
            <p>Belanja Rp2.000.000,00 akan mendapatkan free membership untuk 6 bulan. Syarat dan ketentuan berlaku.</p>'
    ],
    'event-free-membership-minimum-spend' => [
        'title' => 'Free membership minimum spend',
        'type' => 'suitNumber',
        'value' => '2000000'
    ],
    'promotion-background-image' => [
        'title' => 'Promotion background image - Promotion Page',
        'type' => 'suitFileBrowser',
        'value' => 'assets/img/promotion-banner.jpg'
    ],
    'download-app-default-url' => [
        'title' => 'Download app default url',
        'type' => 'suitTextarea',
        'value' => 'https://itunes.apple.com/us/app/ace-hardware-indonesia/id1056027974?ls=1&mt=8'
    ],
    'product-promotion-btn-to-ruparupa-image' => [
       'title' => 'Image of Btn to Ruparupa Product Promotion',
       'type' => 'suitFileBrowser',
       'value' => 'assets/img/logo-rupa-rupa-positive.png'
    ],
    'social-youtube-username' => [
        'title' => 'Youtube username',
        'type' => 'suitText',
        'value' => 'acehardware'
    ],
    'success-subscribe-description' => [
        'title' => 'Success subscribe description',
        'type' => 'suitWysiwyg',
        'value' => "<h2 class='text-red'>Thank you!</h2>
            <p>Anda berhasil melakukan subscribe, thank you</p>"
    ],
    'join-event-reach-maximum-quota' => [
        'title' => 'Join event reach maximum quota',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>Sorry, You can't Register to This Event</h2>
            <p>Registered participant alredy reach maximum quota, thank you</p>"
    ],
    'join-event-already-registered' => [
        'title' => 'Join event already registered',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>You Already Registered</h2>
            <p>You already register into this event, thank you</p>"
    ],
    'join-event-email-sent' => [
        'title' => 'Join event email has been sent',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>Email for Event confirmation has been sent</h2>
            <p>Please, do event participation confirmation, thank you</p>"
    ],
    'join-event-succesful' => [
        'title' => 'Join event successfully registered',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>You're Successfully Registered</h2>
            <p>you're successfully registered for this event, thank you</p>"
    ],
    'join-event-invalid-link' => [
        'title' => 'Join event invalid link',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>Invalid Link</h2>
            <p>Sorry, Link confirmation invalid</p>"
    ],
    'submit-testimony-succesful' => [
        'title' => 'Testimony successfully submitted',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>Your Testimony Succesfully submited</h2>
            <p>Thank you for your feedback</p>"
    ],
    'already-submit-testimony' => [
        'title' => 'Testimony already submitted',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>You Already Submit Your Testimony</h2>
            <p>You already send your feedback, thank you</p>"
    ],
    'member-account-auto-renewal-not-enough-point-alert' => [
        'title' => 'Member Account Renewal Alert',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>Member account renewal</h2>
            <p>Akun member anda telah memasuki masa tenggang dalam 30 hari. Mohon melakukan renewal akun. Point anda tidak cukup untuk melakukan auto renewal. Anda dapat melakukan renewal dengan membayar di toko.</p>"
    ],
    'member-account-manual-renewal-enough-point-alert' => [
        'title' => 'Member Account Renewal Alert',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>Member account renewal</h2>
            <p>Akun member anda telah memasuki masa tenggang dalam 30 hari. Mohon melakukan renewal akun. Anda dapat melakukan renewal dengan membayar di toko atau menggunakan point. Gunakan point untuk melakukan renewal akun secara otomatis</p>"
    ],
    'member-account-manual-renewal-not-enough-point-alert' => [
        'title' => 'Member Account Renewal Alert',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>Member account renewal</h2>
            <p>Akun member telah memasuki masa tenggang dalam 30 hari. Mohon melakukan renewal akun. Anda dapat melakukan renewal dengan membayar di toko.</p>"
    ],
    'member-account-renewal-alert-for-free-membership' => [
        'title' => 'Member Account Renewal Alert',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>Member account renewal</h2>
            <p>Akun member telah memasuki masa tenggang dalam 30 hari. Lakukan minimum purchase sebesar Rp. 2.000.000 untuk memndapatkan free membership selama 6 bulan.</p>"
    ],
    'successfully-get-free-membership' => [
        'title' => 'Successfully get free membership',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>Congratulation!</h2>
            <p>Anda telah mencapai maksimum purchase. Untuk mendapatkan free membership, Silahkan cek email anda untuk info lebih lanjut</p>"
    ],
    'profile-total-points-description' => [
        'title' => 'Total points description in Profile',
        'type' => 'suitTextarea',
        'value' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis deserunt dicta autem accusantium, dignissimos fuga?'
    ],
    'total-point-for-renewal-account' => [
        'title' => 'Besar potongan point untuk renewal account',
        'type' => 'suitNumber',
        'value' => '20'
    ],
    'register-success-admin-fee-offline' => [
        'title' => 'For user admin fee offline',
        'type' => 'suitTextarea',
        'value' => 'Kode TAM akan dikirimkan ke email anda. Silahkan lanjutkan pembayaran di toko ACE Hardware terdekat dengan menunjukan kode TAM tersebut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis deserunt dicta autem accusantium, dignissimos fuga?'
    ],
    'register-success-admin-fee-online' => [
        'title' => 'For user admin fee online ',
        'type' => 'suitTextarea',
        'value' => 'Kode TAM akan dikirimkan ke email anda. Silahkan lanjutkan pembayaran melalui http://www.ruparupa.com/. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis deserunt dicta autem accusantium, dignissimos fuga?'
    ],
    'register-success-admin-fee-free-membership' => [
        'title' => 'For user admin fee free membership',
        'type' => 'suitTextarea',
        'value' => 'Purchase up to IDR 2.000.000. You will get free membership for 6 month. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis deserunt dicta autem accusantium, dignissimos fuga?'
    ],
    'register-success-auto-renewal' => [
        'title' => 'For user auto renewal',
        'type' => 'suitTextarea',
        'value' => 'Masa aktif akun anda akan otomatis diperpanjang dengan pemotongan sebesar 20 point. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis deserunt dicta autem accusantium, dignissimos fuga?'
    ],
    'register-success-manual-renewal' => [
        'title' => 'For user manual renewal',
        'type' => 'suitTextarea',
        'value' => 'Manual renewal harus membayar  setiap tahun sebesar Rp50.000,00. Keterlambatan pembayaran menyebabkan akun dan poin tidak dapat digunakan kembali. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis deserunt dicta autem accusantium, dignissimos fuga?'
    ],
    'point-expiry-alert' => [
        'title' => 'Point Expiry Allert',
        'type' => 'suitWysiwyg',
        'value' => "
            <h2 class='text-red'>Expired Point</h2>
            <p>Poin anda akan hangus. Silahkan gunakan poin anda segera</p>"
    ],
    'profile-point-expired-description' => [
        'title' => 'Point expired description in Profile',
        'type' => 'suitTextarea',
        'value' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis deserunt dicta autem accusantium, dignissimos fuga?'
    ],
    'profile-point-expired-date-description' => [
        'title' => 'Point expired date description in Profile',
        'type' => 'suitTextarea',
        'value' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis deserunt dicta autem accusantium, dignissimos fuga?'
    ],
    'show-hide-online-administration-option' => [
        'title' => "Show or Hide online option for registration",
        'type' => 'suitSelect',
        'description' => "Select option below to show or hide online administration option",
        'value' => 'Show'
    ],
    'google-analytic-script' => [
        'title' => "Google Analytic Script",
        'type' => 'suitTextarea',
        'description' => "Google analytic script",
        'value' => ''
    ],
    'copyright-footer' => [
        'title' => "Copyright Footer",
        'type' => 'suitTextarea',
        'description' => "Copyright footer",
        'value' => '&copy; 2018 Ace Hardware Corporation. Ace Hardware and Ace Hardware logo are registered trademarks of Ace Hardware Corporation. All rights reserved.'
    ],
    'contact-us-background-image' => [
       'title' => 'Background image of Contact Us page',
       'type' => 'suitFileBrowser',
       'value' => 'assets/img/contact-bg.jpg'
    ],
    'footer-call-center-image-icon' => [
        'title' => 'Image Icon for Call Center Menu',
        'type' => 'suitFileBrowser',
        'description' => 'Recommended size 16x20',
        'value' => 'assets/call-icon.png'
    ],
    'footer-call-center-number' => [
        'title' => 'Call center number value',
        'type' => 'suitText',
        'description' => 'Example input value: 021-5829100',
        'value' => '021-5829100'
    ],
    'register-step-2-administration-desc' => [
        'title' => 'Description for Administration',
        'type' => 'suitTextarea',
        'description' => '',
        'value' => 'Sebelum mendapatkan reward, Anda harus menyelesaikan biaya administrasi. Anda bisa melakukan secara online atau offline'
    ],
    'register-step-2-membership-desc' => [
        'title' => 'Description for membership',
        'type' => 'suitTextarea',
        'description' => '',
        'value' => 'Anda perlu memperpanjang keanggotaan Anda setiap tahun. Apakah Anda ingin memperpanjangnya secara otomatis dengan cara mengurangi perolehan point Anda sebesar 20 poin'
    ],
    'register-step-2-referal-code-desc' => [
        'title' => 'Description for referal code',
        'type' => 'suitTextarea',
        'description' => '',
        'value' => 'Silahkan masukkan kode referal jika ada'
    ]
];
