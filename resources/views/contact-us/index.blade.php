@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">Home</a></li>
                        <li>{{ trans('contact-us.title') }}</li>
                    </ul>

                    <div class="contact-layout" style="background-image: url(<?= asset(Setting::get('contact-us-background-image')); ?>);">
                        <div class="contact-layout-content">
                            @include('_includes.flash')
                            <h2 class="home-section-heading">{{ trans('contact-us.title') }}</h2>

                            <form class="js-validate" action="{{ route('frontend.contact-us.send-message') }}" method="post">
                                <div class="contact-form-row">
                                    {{ csrf_field() }}
                                    <div>
                                        <label class="sr-only" for="inputName">Name</label>
                                        <input class="form-input" id="inputName" name="name" type="text" placeholder="{{ trans('contact-us.inbox_form.name') }}" value="{{ old('name') }}" required>
                                        <span class="msg-error">{{ $errors->first('name') }}</span>
                                    </div>
                                    <div>
                                        <label class="sr-only" for="inputAddress">Address</label>
                                        <input class="form-input" id="inputAddress" name="address" type="text" placeholder="{{ trans('contact-us.inbox_form.address') }}" value="{{ old('address') }}" required>
                                        <span class="msg-error">{{ $errors->first('address') }}</span>
                                    </div>
                                </div>
                                <div class="contact-form-row">
                                    <div>
                                        <label class="sr-only" for="inputPhone">Phone</label>
                                        <input class="form-input" id="inputPhone" name="phone" type="text" placeholder="{{ trans('contact-us.inbox_form.phone') }}" value="{{ old('phone') }}" required>
                                        <span class="msg-error">{{ $errors->first('phone') }}</span>
                                    </div>
                                    <div>
                                        <label class="sr-only" for="inputEmail">Email</label>
                                        <input class="form-input" id="inputEmail" name="email" type="email" placeholder="{{ trans('contact-us.inbox_form.email') }}" value="{{ old('email') }}" required>
                                        <span class="msg-error">{{ $errors->first('email') }}</span>
                                    </div>
                                </div>
                                <div class="contact-form-row">
                                    <div>
                                        <label class="sr-only" for="inputMessage">Message</label>
                                        <textarea class="form-input" id="inputMessage" name="message" cols="30" rows="5" placeholder="{{ trans('contact-us.inbox_form.message_label') }}" required>{{ old('message') }}</textarea>
                                        <span class="msg-error">{{ $errors->first('message') }}</span>
                                    </div>
                                </div>
                                <button class="btn btn--wide btn--primary" onclick="this.disabled=true;this.form.submit();">
                                    {{ trans('contact-us.inbox_form.btn_send') }}
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
