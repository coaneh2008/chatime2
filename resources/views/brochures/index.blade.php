@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>{{ trans('brochures.title_breadcrumb') }}</li>
                    </ul>

                    <h1 class="home-section-heading">{{ trans('brochures.title') }}</h1>
                    <form class="brochure-filter" action="{{ route('frontend.brochure.index') }}">
                        <div>
                            <label for="filterShow">{{ trans('brochures.show') }}</label>
                            <select class="form-input" id="filterShow" name="show" onchange="this.form.submit()"">
                                @foreach($show as $key => $value)
                                    <option value="{{ $key }}" {{ $key == app('request')->input('show') ? 'selected' : ''}}>
                                        {{ $value }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div>
                            <label for="sort">{{ trans('brochures.sort') }}</label>
                            <select class="form-input" id="sort" name="sort" onchange="this.form.submit()"">
                                @foreach($sort as $key => $value)
                                    <option value="{{ $key }}" {{ $key == app('request')->input('sort') ? 'selected' : ''}}>
                                        {{ $value }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </form>

                    <ul class="brochure-list list-nostyle">
                        @foreach($brochures as $brochure)
                        <li>
                            <div class="brochure-item">
                                <img src="{{ $brochure->getThumbnail('thumb_image', 'medium')}}" alt="{{ $brochure->image?$brochure->title:'' }}">
                                <h4 class="text-caps">{{ $brochure->title }}</h4>
                                <time>{{ $brochure->start_date ? Carbon\Carbon::parse($brochure->start_date)->format(' d F Y') : ''}} {{ $brochure->end_date && $brochure->end_date != '0000-00-00' ? ' - ' . Carbon\Carbon::parse($brochure->end_date)->format(' d F Y') : ''}}  </time>
                                <div class="brochure-item-btns">
                                    <a class="btn btn--small btn--primary" href="{{ route('frontend.brochure.show', $brochure->slug) }}">{{ trans('brochures.view') }}</a>
                                    <a class="btn btn--small btn--primary" href="{{ route('frontend.brochure.download', $brochure->slug) }}" download>{{ trans('brochures.download') }}</a>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    @include('_includes.pagination', ['paginator' => $brochures])
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
