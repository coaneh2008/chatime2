@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>
                            <a href="{{ route('frontend.home.index') }}">
                                {{ trans('home.home.title') }}
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('frontend.brochure.index') }}">
                                {{ trans('brochures.title_breadcrumb')}}
                            </a>
                        </li>
                        <li>{{ $brochure->title }}</li>
                    </ul>

                    <h1 class="home-section-heading">{{ $brochure->title }}</h1>

                    <div class="brochure-slider slider-style">
                        @foreach(\App\Model\Brochure::getBrochureAttachments($brochure->id) as $brochureAttachment)
                        <div>
                            <img src="{{ $brochureAttachment->getThumbnail('image', 'huge') }}" alt="">
                        </div>
                        @endforeach
                    </div>

                    @include('_includes.share')
                    @include('_includes.back')
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
