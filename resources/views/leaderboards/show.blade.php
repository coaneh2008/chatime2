@include('_includes.head')

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">Home</a></li>
                        <li><a href="{{ route('frontend.membership.dashboard') }}">{{ trans('membership.title') }}</a></li>
                        <li><a href="{{ route('frontend.leaderboard.index') }}">Monthly leaderboard</a></li>
                        <li>{{ $name }}</li>
                    </ul>

                    <div class="membership-layout">
                        @include('memberships._includes.membership-sidebar')

                        <div class="membership-layout-body">
                            @include('memberships._includes.membership-nav')

                            <div class="membership-layout-content">
                                <h2 class="home-section-heading">{{ $name }}</h2>
                                @if($leaderboards)
                                    <div class="table-wrapper">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th align="center" width="15">Rank</th>
                                                    <th align="right">Card ID</th>
                                                    <th align="left">Name</th>
                                                    <th align="right">Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @for($i=0; $i<$leaderboards->total_row; $i++)
                                                <tr>
                                                    <td align="center">{{ $leaderboards->rows[$i]->rank }}</td>
                                                    <td align="right">{{ $leaderboards->rows[$i]->card_id }}</td>
                                                    <td align="left">{{ $leaderboards->rows[$i]->cust_name }}</td>
                                                    <td align="right">Rp<?= number_format($leaderboards->rows[$i]->amount, 0, ',', '.') ?></td>
                                                </tr>
                                                @endfor
                                            </tbody>
                                        </table>
                                    </div>
                                @else
                                No Leaderboard Data
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
