<?php $existInTopSpender = false; ?>
@include('_includes.head')

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">Home</a></li>
                        <li><a href="{{ route('frontend.membership.dashboard') }}">{{ trans('membership.title') }}</a></li>
                        <li>Monthly leaderboard</li>
                    </ul>

                    <div class="membership-layout">
                        @include('memberships._includes.membership-sidebar')

                        <div class="membership-layout-body">
                            @include('memberships._includes.membership-nav')

                            <div class="membership-layout-content">
                                <section class="mb-48">
                                    <h2 class="home-section-heading">Leaderboard</h2>
                                    <b>{!! Setting::get('leaderboard-word') !!}</b>
                                    <p>
                                        - Rank 1-200  : Data Calon Pemenang Daerah Jabodetabek<br/>
                                        - Rank 201-300    : Data Calon Pemenang Daerah Jawa (selain Jabodetabek)<br/>
                                        - Rank 301-340    : Data Calon Pemenang Daerah Sumatera<br/>
                                        - Rank 341-360    : Data Calon Pemenang Daerah Bali & Nusa Tenggara<br/>
                                        - Rank 361-400    : Data Calon Pemenang Daerah Kalimantan, Sulawesi, & Maluku 
                                    </p>
                                    <p>
                                        <b>
                                            @if($leaderboard)
                                                @if($leaderboard->rank <= 400)
                                                    Selamat! Anda berkesempatan mendapatkan Hadiah Member Special Rewards (Rank {{$leaderboard->rank}}). Tingkatkan terus transaksi dan belanja Anda hingga 25 Juni 2019 di ACE agar tetap berkesempatan mendapatkan Ratusan Hadiah Total Milyaran Rupiah.
                                                @else
                                                    Saat ini posisi Anda (Rank {{$leaderboard->rank}}) dan Anda belum masuk dalam daftar calon Pemenang Member Special Rewards (Total 400 orang). Tingkatkan terus transaksi dan belanja Anda hingga 25 Juni 2019 di ACE agar berkesempatan mendapatkan Ratusan Hadiah Total Milyaran Rupiah.
                                                @endif
                                            @else
                                                Saat ini Anda belum masuk dalam daftar Pemenang Member Special Rewards. Tingkatkan terus transaksi dan belanja Anda hingga 25 Juni 2019 di ACE agar berkesempatan mendapatkan Ratusan Hadiah Total Milyaran Rupiah.
                                            @endif
                                        </b>
                                    </p>
                                    @if($leaderboard)
                                        <div class="table-wrapper">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th align="center" width="15">Rank</th>
                                                        <th align="right">Member Card ID</th>
                                                        <th align="left">Name</th>
                                                        @if ($leaderboard->amount)
                                                            <th align="right">Amount</th>
                                                        @endif
                                                        @if ($leaderboard->transaction)
                                                            <th align="right">Transaction</th>
                                                        @endif
                                                        @if ($leaderboard->quantity)
                                                            <th align="right">Quantity</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="{{ Auth::user()->member_id == $leaderboard->member_card_id ? 'row-highlighted' : ''}}">
                                                        <th align="center" width="15">{{ $leaderboard->rank }}</th>
                                                        <td align="right">{{ $leaderboard->member_card_id }}</td>
                                                        <td align="left">{{ $leaderboard->name }}</td>
                                                        @if ($leaderboard->amount)
                                                            <td align="right">
                                                                Rp {!! number_format($leaderboard->amount, 0, ',', '.') !!}
                                                            </td>
                                                        @endif
                                                        @if ($leaderboard->transaction)
                                                            <td align="right">
                                                                {!! number_format($leaderboard->transaction, 0, ',', '.') !!}
                                                            </td>
                                                        @endif
                                                        @if ($leaderboard->quantity)
                                                            <td align="right">
                                                                {!! number_format($leaderboard->quantity, 0, ',', '.') !!}
                                                            </td>
                                                        @endif
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
