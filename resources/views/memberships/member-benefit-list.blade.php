@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li><a href="{{ route('frontend.membership.index') }}">{{ trans('membership.title') }}</a></li>
                        <li>{{ $benefit->title }}</li>
                    </ul>

                    <figure class="member-hero">
                        <img src="{{ $benefit->getThumbnail('banner_image', 'default') }}" alt="">
                        <figcaption class="member-hero-content">
                            {!! $benefit->description !!}
                        </figcaption>
                    </figure>
                    <section class="home-section">
                        <h2 class="home-section-heading">{{ $benefit->title }}</h2>
                        @if (isset($locations) && isset($categories))
                        <form class="merchant-filter">
                            <div class="merchant-filter-item">
                                <label for="selectCategory">Kategori</label>
                                <select class="form-input" id="selectCategoryMerchant">
                                    @foreach($categories as $key => $category)
                                    <option value="{{ $key }}" {{ $key == request()->get('category') ? 'selected' : '' }}>{{ $category }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="merchant-filter-item">
                                <label for="selectLocation">Lokasi</label>
                                <select class="form-input" id="selectLocationMerchant">
                                    @foreach($locations as $key => $location)
                                    <option value="{{ $key }}" {{ $key == request()->get('location') ? 'selected' : '' }}>{{ $location }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </form>
                        @endif
                        <div class="fg fg-400-2 fg-480-3 fg-640-3 fg-768-4 fg-1024-5">
                            @if ($benefit->type !== \App\Model\MemberBenefit::TYPE_MERCHANT_PROMO)
                                @foreach($promos as $promo)
                                <div>
                                    <a class="anchor" href="{{ route('frontend.membership.benefit-detail', ['slug' => $benefit->slug, 'detailSlug' => $promo->slug]) }}">
                                        <img class="full-width" src="{{ $promo->getThumbnail('thumb_image', 'small') }}" alt="">
                                        <div class="p-8">
                                            <div class="mb-8 lh-1">
                                                <small>
                                                Periode Promosi:
                                                <br>
                                                {{ $promo->start_date->format('j F Y') }} - {{ $promo->end_date->format('j F Y') }}
                                                </small>
                                            </div>
                                            <h4 class="mb-12">{{ $promo->title }}</h4>
                                        </div>
                                    </a>
                                </div>
                                @endforeach
                            @else
                                @foreach($promos as $promo)
                                <div>
                                    <div class="text-center">
                                        <a class="anchor" href="{{ route('frontend.membership.benefit-detail', ['slug' => $benefit->slug, 'detailSlug' => $promo->slug]) }}">
                                            <h4 class="text-caps mb-8">
                                                {{ $promo->merchant_name }}
                                            </h4>
                                            <img class="full-width" src="{{ $promo->getThumbnail('thumb_image', 'small') }}" alt="">
                                            <div class="p-8 lh-1 mb-8">
                                                <h4 class="text-caps mb-2">
                                                    {{ $promo->title }}
                                                </h4>
                                                <small>
                                                    {!! $promo->description !!}
                                                    <br>
                                                    <b>Kategori:</b> {{ $promo->category->title }}
                                                    <br>
                                                    @if ($locations = $promo->locations()->get())
                                                    <b>Lokasi: </b>
                                                        @foreach($locations as $key => $location)
                                                        <?php
                                                            $title =  $location->title;
                                                            if ($key < count($locations)-1) {
                                                                $title .= ', ';
                                                            }
                                                        ?>
                                                        {{ $title }}
                                                        @endforeach
                                                    @endif
                                                </small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                @endforeach
                            @endif
                        </div>

                        @include('_includes.share', ['title' => $benefit->title, 'description' => $benefit->description])
                        @include('_includes.back')
                    </section>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
