@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>{{ trans('membership.title') }}</li>
                    </ul>
                    @include('_includes.flash')
                    <div class="bzg">
                        <div class="bzg_c" data-col="l6" data-offset="l3">
                            <h1 class="h2 home-section-heading text-center">Member registration</h1>
                            <div class="member-registration-steps">
                                <div class="member-registration-step">1</div>
                                <div class="member-registration-step is-active">2</div>
                            </div>

                            <form class="form-membership-step-2" action="{{ route('frontend.membership.do-register-step-2') }}" method="post">
                                <fieldset class="block">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <legend><b>{{ trans('membership.step_2.title') }}</b></legend>

                                    <fieldset class="block">
                                        <legend>{{ Setting::get('register-step-2-administration-desc') }}</legend>
                                        @if(Setting::get('show-hide-online-administration-option') == 'Show')
                                            <label for="inputOnline">
                                                <input id="inputOnline" type="radio" name="administration_type" value="0" required>
                                                <span>Online</span>
                                            </label>
                                            &nbsp;
                                        @endif
                                        <label for="inputOffline">
                                            <input id="inputOffline" type="radio" name="administration_type" value="1" required>
                                            <span>Offline </span>
                                        </label>
                                        &nbsp;
                                        @if($mGetMEvent)
                                             <input type="hidden" name="event_id" value="{{ $mGetMEvent->id }}">
                                            <label for="inputFreeMembership">
                                                <input id="inputFreeMembership" type="radio" name="administration_type" value="2" required>
                                                <span>Ace Access</span>
                                            </label>
                                        @endif
                                        <div>
                                            <small id="inputAdminFeeMsg" class="text-red" style="display: none;">Pilihan tidak boleh kosong</small>
                                        </div>
                                    </fieldset>
                                    <fieldset class="block">
                                        <legend>{{ Setting::get('register-step-2-membership-desc') }}</legend>

                                        <label for="renewalYes">
                                            <input id="renewalYes" type="radio" value="1" name="is_renewal" checked="true">
                                            <span>Ya</span>
                                        </label>
                                        &nbsp;
                                        <label for="renewalNo">
                                            <input id="renewalNo" type="radio" value="0" name="is_renewal">
                                            <span>Tidak</span>
                                        </label>
                                        <div>
                                            <small id="inputRenewalMsg" class="text-red" style="display: none;">Pilihan tidak boleh kosong</small>
                                        </div>
                                    </fieldset>
                                    <div>
                                        <label for="inputReferal">{{ Setting::get('register-step-2-referal-code-desc') }}</label>
                                        <input class="form-input {{ $errors->first('referal_id')?'input-invalid':'' }}" id="inputReferal" name="referal_id" value="{{ old('referal_id') }}" type="text">
                                        <span class="msg-error">{{ $errors->first('referal_id') }}</span>
                                    </div>
                                </fieldset>
                                <div class="block">
                                    <label for="inputAgree">
                                        <input id="inputAgree" type="checkbox">
                                        <span>Saya telah membaca dan setuju dengan <a target="_blank" href="/membership-terms-and-condition"> syarat dan ketentuan </a>yang berlaku</span><br>
                                        <small id="inputAgreeMsg" class="text-red" style="display: none;">Silahkan terlebih dahulu membaca dan menyetujui syarat dan kondisi yang berlaku</small>
                                    </label>
                                </div>
                                <div class="text-center block">
                                    <button class="btn btn--primary" onclick="this.disabled=true;this.form.submit();">Daftar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

    <div class="modal" id="modalConfirmRenewal">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>

            <div class="text-center">
                <?= Setting::get('auto-renewal-no-description') ?>
            </div>
        </div>
    </div>

    <div class="modal" id="modalConfirmRenewalYes">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>

            <div class="text-center">
                <?= Setting::get('auto-renewal-yes-description') ?>
            </div>
        </div>
    </div>

    <div class="modal" id="modalFreeMembership">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>
            <?= Setting::get('free-membership-description') ?>
        </div>
    </div>

    <div class="modal" id="modalOnlineRegistration">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>
            <?= Setting::get('online-registration-description') ?>
        </div>
    </div>

    <div class="modal" id="modalOfflineRegistration">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>
            <?= Setting::get('offline-registration-description') ?>
        </div>
    </div>

@include('_includes.scripts')
