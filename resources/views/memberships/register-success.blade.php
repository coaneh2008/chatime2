@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>{{ trans('membership.title') }}</li>
                    </ul>

                    <div class="text-center">
                        <h1 class="h2 home-section-heading">TERIMA KASIH TELAH BERGABUNG DALAM ACE REWARDS</h1>

                        @if($adminType == 2)
                            <p>{{ Setting::get('register-success-admin-fee-free-membership') }}</p>
                        @else
                            @if($isRenewal == 1)
                                <p>{{ Setting::get('register-success-auto-renewal') }}</p>
                            @else 
                                <p>{{ Setting::get('register-success-manual-renewal') }}</p>
                            @endif
                        @endif
                        <?php $memberPromos = $benefit->memberPromos()
                            ->where('is_highlighted', 1)
                            ->where('start_date', '<=', \Carbon\Carbon::now())
                            ->where('end_date', '>=', \Carbon\Carbon::now())
                            ->asc()
                            ->published()
                            ->limit(5)
                            ->get()
                        ?>
                        @if(count($memberPromos))
                            <p>Sebagai Member ACE Rewards, nikmati berbagai benefit khusus untuk Anda. Berikut promo terbaru dari ACE :</p>
                        @endif
                        <div class="fg fg-480-2 fg-768-3 fg-1024-5">
                            @foreach($memberPromos as $memberPromo)
                                <div>
                                    <div>
                                        <a class="anchor" href="{{ route('frontend.membership.benefit-detail', ['slug' => $benefit->slug, 'detailSlug' => $memberPromo->slug]) }}">
                                        <img class="full-width" src="{{ $memberPromo->getThumbnail('thumb_image', 'small') }}" alt="">
                                            <div class="p-8 lh-1">
                                                <time>
                                                    <small>
                                                    Periode Promo:
                                                    <br>
                                                    {{ $memberPromo->start_date->format('j F Y') }} - {{ $memberPromo->end_date->format('j F Y') }}
                                                    </small>
                                                </time>
                                            </div>
                                            <div class="p-8 lh-1">
                                                <h4 class="mb-8">
                                                    {{ $memberPromo->title }}
                                                </h4>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        @if($adminType == 0)
                            <p> {{ Setting::get('register-success-admin-fee-online') }} </p>
                            <a class="btn btn--primary" href="{{ route('frontend.payment.pay-via-ace-online') }}">Bayar</a>
                        @elseif($adminType == 1)
                            <p> {{ Setting::get('register-success-admin-fee-offline') }} </p>
                            <a class="btn btn--primary" target="blank" href="{{ route('frontend.store-location.index') }}">Temukan Toko</a>
                        @endif
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
