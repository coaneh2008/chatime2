<nav class="membership-nav">
    <a class="{{ (URL::current() == route('frontend.membership.dashboard') || (str_contains(URL::current(), route('frontend.events.index')))) ? 'is-active' : '' }}" href="{{ route('frontend.membership.dashboard') }}">Event</a>
    <a class="{{ (str_contains(URL::current(), route('frontend.followed-events.index'))) ? 'is-active' : '' }}" href="{{ route('frontend.followed-events.index') }}">Followed event</a>
    @if ($isExistLeaderBoard)
    <a class="{{ (str_contains(URL::current(), route('frontend.leaderboard.index'))) ? 'is-active' : '' }}" href="{{ route('frontend.leaderboard.index') }}">Leaderboard</a>
    @endif
    <a class="{{ (str_contains(URL::current(), route('frontend.events.transaction'))) ? 'is-active' : '' }}" href="{{ route('frontend.events.transaction') }}">Transaction &amp; redeem</a>
</nav>
