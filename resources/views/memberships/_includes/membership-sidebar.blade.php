<div class="membership-layout-sidebar">
    <div class="text-center">
        <figure>
            <img class="img-rounded mb-8" src="{{ $user->profile_picture ? $user->getThumbnail('profile_picture', 'square') : '//placehold.it/120x120' }}" alt="">
            <figcaption>
                <h4 class="mb-8">{{ $user->name }}</h4>
                <div>Card No. : {{ $user->username }}</div>
                <div class="mb-8">exp date : {{ Carbon\Carbon::parse($user->expiry_date)->format('d/M/Y') }}</div>
                <a class="btn btn--small btn--outline btn--secondary" href="{{ route('frontend.membership.edit-profile') }}">Edit</a>
                <a class="btn btn--small btn--outline btn--secondary" href="{{ route('frontend.user.logout') }}">Logout</a>
                <div class="mb-8"></div>
                <div class="mb-8">
                    <a class="btn btn--small btn--outline btn--secondary" href="{{ route('frontend.membership.edit-passkey') }}">Change Passkey</a>
                </div>
            </figcaption>
        </figure>
    </div>
    @if(!$renewalBtnIsHide)
        <div class="text-center block">
            <label class="toggler" for="autoRenewal">
                <span>Auto renewal</span>
                <input class="toggler-input sr-only" id="autoRenewal" type="checkbox" {{ $renewalStatus?'checked':'' }}>
                <span class="toggler-icon fa"></span>
            </label>
            <span class="tooltip" data-tooltip="{{ Setting::get('profile-auto-renewal-description') }}">
                <span class="fa fa-lg fa-fw fa-question-circle"></span>
            </span>
        </div>
    @endif
    <table>
        <tr>
            <td rowspan="2" valign="top">Total points</td>
            <td class="text-red" align="left"> 
                <?php $points = explode(' ', $point->total_point, 2); ?>
                {{ $points[0] }} Point
            </td>
            <td>
                <span class="tooltip" data-tooltip="{{ Setting::get('profile-total-points-description') }}">
                    &nbsp;&nbsp;<span class="fa fa-lg fa-fw fa-question-circle"></span>
                </span>
            </td>
        </tr>
        <tr>
            <td class="text-red" align="left" colspan="2"> 
                <span style="font-size: 12px !important;">
                    {{ !empty($points[1]) ? $points[1] : '' }}
                </span>
            </td>
        </tr>
        <tr>
            <td>Point expired</td>
            <td class="text-red" align="left">
                {{ $point->expiry_point }} Point
            </td>
            <td>
                <span class="tooltip" data-tooltip="{{ Setting::get('profile-point-expired-description') }}">
                    &nbsp;&nbsp;<span class="fa fa-lg fa-fw fa-question-circle"></span>
                </span>
            </td>
        </tr>
        <tr>
            <td>Point exp. date</td>
            <td class="text-red" align="left">
                {{ Carbon\Carbon::parse($point->expiry_date)->format('d/M/Y') }}
            </td>
            <td>
                <span class="tooltip" data-tooltip="{{ Setting::get('profile-point-expired-date-description') }}">
                    &nbsp;&nbsp;<span class="fa fa-lg fa-fw fa-question-circle"></span>
                </span>
            </td>
        </tr>
    </table>
    @if(Setting::get('show-hide-online-administration-option') == 'Show')
        <div class="text-center">
            <a class="btn btn--block btn--outline btn--primary" href="{{ route('frontend.payment.pay-via-ace-online') }}">Pay via ACE Online</a>
        </div>
    @endif
</div>
