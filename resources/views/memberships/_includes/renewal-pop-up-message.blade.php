    <div class="modal" id="renewalYesStatus">
        <div class="modal-dialog">
            <div class="text-center">
                <?= Setting::get('auto-renewal-yes-description') ?>
            </div>
            <div class="text-center" id="messageYesTxt">
            </div>
        </div>
    </div>
    <div class="modal" id="renewalNOStatus">
        <div class="modal-dialog">
            <div class="text-center">
                <?= Setting::get('auto-renewal-no-description') ?>
            </div>
            <div class="text-center" id="messageNOTxt">
            </div>
        </div>
    </div>