@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li><a href="{{ route('frontend.membership.index') }}">{{ trans('membership.title') }}</a></li>
                        <li>Merchandise List</li>
                    </ul>

                    <figure class="member-hero">
                        <img src="{{ Setting::get('membership-merchandise-banner-image-url') ? asset(Setting::get('membership-merchandise-banner-image-url')) : '//placehold.it/1100x500' }}" alt="">
                        <figcaption class="member-hero-content">
                            <h1 class="text-caps">{{ Setting::get('membership-merchandise-banner-title') }}</h1>
                            {!! Setting::get('membership-merchandise-banner-description') !!}
                        </figcaption>
                    </figure>
                    <section class="home-section">
                        <h2 class="home-section-heading">Merchandise</h2>

                        <div class="fg fg-400-2 fg-480-3 fg-640-3 fg-768-4 fg-1024-5">
                            @foreach($merchandises as $merchandise)
                            <div class="text-center">
                                <div class="mb-16">
                                    <div class="mb-8"><b>{{ $merchandise->title }}</b></div>
                                    <img class="mb-8 full-width" src="{{ $merchandise->getThumbnail('thumb_image', 'small') }}" alt="">
                                    <b>{{ $merchandise->point }} Poin</b>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        @include('_includes.share')
                        @include('_includes.back')
                    </section>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
