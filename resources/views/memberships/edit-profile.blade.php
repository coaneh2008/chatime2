@include('_includes.head')

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">Home</a></li>
                        <li><a href="{{ route('frontend.membership.dashboard') }}">{{ trans('membership.title') }}</a></li>
                        <li>Edit Profile</li>
                    </ul>

                    <div class="membership-layout">
                        @include('memberships._includes.membership-sidebar')

                        <div class="membership-layout-body">
                            <div class="membership-layout-content">
                                <h2 class="home-section-heading">Edit Profile</h2>
                                @include('_includes.flash')
                                @if (Session::get('NOTIF_VERIFIED'))
                                    <div class="alert alert--success">
                                        Terima kasih, email anda berhasil di verifikasi, klik link berikut untuk melihat promo terbaru dari kami. <br/><a href="https://www.acehardware.co.id/thematic-promotion">Lihat Promo</a>
                                        <button class="alert-close">&times;</button>
                                    </div>
                                @endif
                                <div class="bzg">
                                    <div class="bzg_c" data-col="l9">
                                        <form class="js-validate" action="{{ route('frontend.membership.do-edit-profile') }}" method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="block-half">
                                                <div id="wrapper">       
                                                </div>
                                                <label for="inputAvatar">Avatar</label>
                                                <div id="previous-image-holder">
                                                    <figure>
                                                        <img class="img-rounded" src="{{ asset($user->getThumbnail('profile_picture', 'square')) }}" alt="">
                                                    </figure>
                                                </div>
                                                <figure>
                                                    <div id="image-holder">
                                                        <img class="img-rounded" src="//placehold.it/120x120" alt="">
                                                    </div>
                                                </figure>
                                                <input class="form-input" id="inputAvatar" name="profile_picture" type="file">
                                                <span class="msg-error">{{ $errors->first('profile_picture') }}</span>
                                            </div>
                                            <div class="block-half">
                                                <label for="inputName">Nama</label>
                                                <input class="form-input js-readonly-trigger-warning" data-warning="{{ Setting::get('data-warning-edit-profile') }}" id="inputName" name="name" type="text" value="{{ $user->name }}" readonly>
                                            </div>
                                            <div class="block-half">
                                                <label for="inputCardID">Nomor Kartu Member</label>
                                                <input class="form-input js-readonly-trigger-warning" data-warning="{{ Setting::get('data-warning-edit-profile') }}" id="inputCardID" name="username" type="text" value="{{ $user->username }}" readonly>
                                            </div>
                                            <div class="block-half">
                                                <label for="inputExpiry">Tanggal Expired</label>
                                                <input class="form-input js-readonly-trigger-warning" data-warning="{{ Setting::get('data-warning-edit-profile') }}" id="inputExpiry" name="expiry_date" type="text" value="{{ Carbon\Carbon::parse($user->expiry_date)->format('d/M/Y') }}" readonly>
                                            </div>
                                            <div class="block-half">
                                                <label for="inputEmail">
                                                Email<span style="color:#FE2A1A;">*</span>
                                                </label>
                                                <input class="form-input" id="inputEmail" name="email" type="email" value="{{ $user->email != 'nomail@gmail.com' ? $user->email :'' }}">
                                                <span class="msg-error">{{ $errors->first('email') }}</span>
                                                @if(Auth::user()->is_verified == 0)
                                                    <div class="msg-error">Email Not Verified</div>
                                                @else
                                                    <div class="msg-success">Email Verified</div>
                                                @endif
                                            </div>
                                            <div class="block-half">
                                                <label for="inputEmail">
                                                Email Verifikasi
                                                </label>
                                                <input class="form-input" id="inputEmail" name="email_verified" type="email">
                                                <span class="msg-error">{{ $errors->first('email_verified') }}</span>
                                            </div>
                                            <div class="block-half">
                                                <label for="inputIDNumber">{{ trans('membership.step_1.id_card') }}
                                                    <span style="color:#FE2A1A;">*</span>
                                                </label>
                                                <input class="form-input {{ $errors->first('identity_card_id')?'input-invalid':'' }}" id="inputIDNumber" name="identity_card_id" value="{{ ($user->identity_card_id == 1111) ? '' : $user->identity_card_id }}" type="text">
                                                <span class="msg-error">{{ $errors->first('identity_card_id') }}</span>
                                            </div>
                                            <div class="block-half">
                                                <label for="inputTel">
                                                    No. HP <span style="color:#FE2A1A;">*</span>
                                                </label>
                                                <input class="form-input" id="inputTel" name="cellphone" type="text" value="{{ ($user->cellphone == '0000') ? '' :  $user->cellphone }}">
                                                <span class="msg-error">{{ $errors->first('cellphone') }}</span>
                                            </div>
                                            <fieldset class="block-half">
                                                <legend>{{ trans('membership.step_1.marital_status') }}</legend>
                                                @foreach ($maritalStatus as $key => $value)
                                                    <label for="statusSingle">
                                                        <input type="radio" value="{{ $key }}" name="marital_status" {{ $key == $user->marital_status ? 'checked' : ''}}>
                                                        <span>{{ $value }}</span>
                                                    </label>
                                                    &nbsp;
                                                @endforeach
                                            </fieldset>
                                            <fieldset class="block-half">
                                                <legend>{{ trans('membership.step_1.sex') }}</legend>
                                                @foreach ($sexTypes as $key => $sex)
                                                    <label>
                                                        <input type="radio" value="{{ $key }}" name="gender" {{ $key == $user->gender ? 'checked' : ''}}>
                                                        <span>{{ $sex}}</span>
                                                    </label>
                                                    &nbsp;
                                                @endforeach
                                            </fieldset>
                                            
                                            <div class="block-half">
                                                <label for="inputBirthplace">{{ trans('membership.step_1.birth_place') }}</label>
                                                <input class="form-input" id="inputBirthplace" name="place_of_birth" value="{{ $user->place_of_birth }}" type="text">
                                                <span class="msg-error">{{ $errors->first('place_of_birth') }}</span>
                                            </div>
                                            <div class="block-half">
                                                <label for="inputBirthday">{{ trans('membership.step_1.birthday') }}
                                                    <span style="color:#FE2A1A;">*</span>
                                                </label>
                                                <input class="form-input js-datepicker" id="inputBirthday" name="date_of_birth" value="{{ Carbon\Carbon::parse($user->date_of_birth)->format('Y/m/d') }}" type="text">
                                                <span class="msg-error">{{ $errors->first('date_of_birth') }}</span>
                                            </div>
                                            <fieldset class="block">
                                                <legend><b>{{ trans('membership.step_1.current_address') }}</b></legend>

                                                <div class="block-half">
                                                    <label for="inputCurrentAddressStreet">{{ trans('membership.step_1.street_name') }}</label>
                                                    <textarea class="form-input {{ $errors->first('current_address')?'input-invalid':'' }}" id="inputCurrentAddressStreet" name="current_address" rows="3">{{ $user->current_address }}</textarea>
                                                    <span class="msg-error">{{ $errors->first('current_address') }}</span>
                                                </div>
                                                <div class="block-half">
                                                    <label for="inputCUrrentAddressCity">{{ trans('membership.step_1.city') }}</label>
                                                    <select class="form-input" id="inputCUrrentAddressCity" name="current_address_city_id">
                                                        <option value="" selected disabled>Pilih Kota</option>
                                                        @foreach($cities as $key => $city)
                                                            <option value="{{ $key }}" {{ $key == $cityCode ? 'selected' : ''}}>{{ $city }}{{ $user->city_id}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="block-half">
                                                    <label for="inputCurrentAddressZip">{{ trans('membership.step_1.zip_code') }}</label>
                                                    <input class="form-input" name="current_address_zip_code" value="{{ $user->current_address_zip_code }}" type="text">
                                                    <span class="msg-error">{{ $errors->first('current_address_zip_code') }}</span>
                                                </div>
                                            </fieldset>
                                            <button class="btn btn--primary">Simpan</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>
    @include('memberships._includes.renewal-pop-up-message')
@include('_includes.scripts')

