@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>{{ trans('membership.title') }}</li>
                    </ul>

                    <figure class="member-hero">
                        <img src="{{ Setting::get('membership-banner-image-url') ? Setting::get('membership-banner-image-url') : '//placehold.it/1100x500' }}" alt="">
                        <figcaption class="member-hero-content">
                            <h1 class="text-caps">{{ Setting::get('membership-banner-title') }}</h1>
                            {!! Setting::get('membership-banner-description') !!}
                            <br>
                            @if(\Auth::check())
                                @if(\Auth::user()->isUser())
                                    <h2>Hi, {{ \Auth::user()->name }}</h2>
                                    <h2>{{ \Auth::user()->username }}</h2>
                                @else
                                    <div class="block-half">
                                        <a class="btn btn--outline btn--white" href="{{ route('frontend.membership.login') }}">
                                            {{ trans('membership.login.title_2')}}
                                        </a>
                                    </div>
                                    <div><small>Not yet a member?</small></div>
                                    <div>
                                        <a class="btn btn--primary" href="{{ route('frontend.membership.register') }}">{{ trans('membership.register.title_2') }}</a>
                                    </div>
                                @endif
                            @else
                                <div class="block-half">
                                    <a class="btn btn--outline btn--white" href="{{ route('frontend.membership.login') }}">
                                        {{ trans('membership.login.title_2')}}
                                    </a>
                                </div>
                                <div><small>Not yet a member?</small></div>
                                <div>
                                    <a class="btn btn--primary" href="{{ route('frontend.membership.register') }}">{{ trans('membership.register.title_2') }}</a>
                                </div>
                            @endif
                        </figcaption>
                    </figure>

                    <section class="home-section">
                        <h2 class="home-section-heading">{{ Setting::get('membership-become-member-title') }}</h2>

                        {!! Setting::get('membership-become-member-description') !!}
                        @if(\Auth::check())
                            @if(!\Auth::user()->isUser())
                                <a class="btn btn--primary" href="{{ route('frontend.membership.register') }}">
                                    {{ trans('membership.register.become_member') }}
                                </a>
                            @endif
                        @endif
                        
                    </section>
                    <br>

                    <section class="home-section">
                        <h2 class="home-section-heading">Member benefits</h2>
                        <div class="member-benefits slider-style">
                            @foreach ($benefits as $key => $benefit)
                            <div>
                                <div class="member-benefit">
                                    <img class="block" src="{{ $benefit->getThumbnail('thumb_image', 'small') }}" alt="">
                                    <button class="btn btn--primary" data-benefit-panel="#panel{{ $key + 1 }}">
                                        {{ $benefit->title }}
                                    </button>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        @foreach ($benefits as $key => $benefit)
                        <div class="member-benefit-panel" id="panel{{ $key + 1 }}">
                            @if ($benefit->type == App\Model\MemberBenefit::TYPE_FREE_CONTENT)
                                {!! $benefit->content !!}
                            @elseif ($benefit->type == App\Model\MemberBenefit::TYPE_MEMBER_PROMO)
                                <h3 class="text-caps">{{ $benefit->title }}</h3>

                                <div class="fg fg-480-2 fg-768-3 fg-1024-5">
                                    @foreach($benefit->memberPromos()->where('is_highlighted', 1)->asc()->limit(5)->get() as $memberPromo)
                                    <div>
                                        <div>
                                            <a class="anchor" href="{{ route('frontend.membership.benefit-detail', ['slug' => $benefit->slug, 'detailSlug' => $memberPromo->slug]) }}">
                                                <img class="full-width" src="{{ $memberPromo->getThumbnail('thumb_image', 'small') }}" alt="">
                                                <div class="p-8 lh-1">
                                                    <time>
                                                        <small>
                                                        Periode Promosi:
                                                        <br>
                                                        {{ $memberPromo->start_date->format('j F Y') }} - {{ $memberPromo->end_date->format('j F Y') }}
                                                        </small>
                                                    </time>
                                                </div>
                                                <div class="p-8 lh-1">
                                                    <h4 class="mb-8">
                                                        {{ $memberPromo->title }}
                                                    </h4>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>

                                <div class="text-center">
                                    <a class="btn btn--primary" href="{{ route('frontend.membership.benefit-index', ['slug' => $benefit->slug]) }}">See more</a>
                                </div>
                            @elseif ($benefit->type == App\Model\MemberBenefit::TYPE_MERCHANT_PROMO)
                                <h3 class="text-caps">{{ $benefit->title }}</h3>

                                <div class="fg fg-480-2 fg-768-3 fg-1024-5">
                                    @foreach($benefit->merchantPromos()->where('is_highlighted', 1)->asc()->limit(5)->get() as $merchantPromo)
                                    
                                    <div>
                                        <div class="text-center">
                                            <a class="anchor" href="{{ route('frontend.membership.benefit-detail', ['slug' => $benefit->slug, 'detailSlug' => $merchantPromo->slug]) }}">
                                                <h4 class="text-caps mb-8">
                                                    {{ $merchantPromo->merchant_name }}
                                                </h4>
                                                <img class="full-width" src="{{ $merchantPromo->getThumbnail('thumb_image', 'small') }}" alt="">
                                                <div class="p-8 lh-1 mb-8">
                                                    <h4 class="text-caps mb-2">
                                                        {{ $merchantPromo->title }}
                                                    </h4>
                                                    <small>
                                                        {!! $merchantPromo->description !!}
                                                        <br>
                                                        <b>Kategori:</b> {{ $merchantPromo->category->title }}
                                                        <br>
                                                        @if ($locations = $merchantPromo->locations()->get())
                                                        <b>Lokasi: </b>
                                                            @foreach($locations as $key => $location)
                                                            <?php
                                                                $title =  $location->title;
                                                                if ($key < count($locations)-1) {
                                                                    $title .= ', ';
                                                                }
                                                            ?>
                                                            {{ $title }}
                                                            @endforeach
                                                        @endif
                                                    </small>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="text-center">
                                    <a class="btn btn--primary" href="{{ route('frontend.membership.benefit-index', ['slug' => $benefit->slug]) }}">See more</a>
                                </div>
                            @elseif ($benefit->type == App\Model\MemberBenefit::TYPE_NEWS_UPDATES)
                                <h3 class="text-caps">{{ $benefit->title }}</h3>

                                <div class="fg fg-480-2 fg-768-3 fg-1024-5">
                                    @foreach($benefit->newsUpdates()->where('is_highlighted', 1)->asc()->limit(5)->get() as $newsUpdate)
                                    <div>
                                        <div>
                                            <a class="anchor" href="{{ route('frontend.membership.benefit-detail', ['slug' => $benefit->slug, 'detailSlug' => $newsUpdate->slug]) }}">
                                                <img class="full-width" src="{{ $newsUpdate->getThumbnail('thumb_image', 'small') }}" alt="">
                                                <div class="p-8 lh-1">
                                                    <time>
                                                        <small>
                                                        Periode Promosi:
                                                        <br>
                                                        {{ $newsUpdate->start_date->format('j F Y') }} - {{ $newsUpdate->end_date->format('j F Y') }}
                                                        </small>
                                                    </time>
                                                </div>
                                                <div class="p-8 lh-1">
                                                    <h4 class="mb-8">
                                                        {{ $newsUpdate->title }}
                                                    </h4>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>

                                <div class="text-center">
                                    <a class="btn btn--primary" href="{{ route('frontend.membership.benefit-index', ['slug' => $benefit->slug]) }}">See more</a>
                                </div>
                            @endif
                        </div>
                        @endforeach
                        
                    </section>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
