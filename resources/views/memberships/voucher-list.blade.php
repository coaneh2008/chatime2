@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li><a href="{{ route('frontend.membership.index') }}">{{ trans('membership.title') }}</a></li>
                        <li>Voucher List</li>
                    </ul>

                    <figure class="member-hero">
                        <img src="{{ Setting::get('membership-voucher-banner-image-url') ? asset(Setting::get('membership-voucher-banner-image-url')) : '//placehold.it/1100x500' }}" alt="">
                        <figcaption class="member-hero-content">
                            <h1 class="text-caps">{{ Setting::get('membership-voucher-banner-title') }}</h1>
                            {!! Setting::get('membership-voucher-banner-description') !!}
                        </figcaption>
                    </figure>
                    <section class="home-section">
                        <h2 class="home-section-heading">Voucher List</h2>
                        <ul class="m-voucher-list">
                            @foreach($vouchers as $voucher)
                            <li class="mb-24">
                                <div class="voucher">
                                    <div class="voucher-left">
                                        <div class="voucher-value" data-prefix="{{ !str_contains($voucher->value, '%') ? 'Rp.' : '' }}" data-label="{{ $voucher->label }}">
                                            @if (!str_contains($voucher->value, '%'))
                                            {{ number_format($voucher->value, 0, ",", ".") }}
                                            @else
                                            {{ $voucher->value }}
                                            @endif
                                        </div>
                                        <div class="voucher-point">
                                        @if ($voucher->point)
                                        {{ $voucher->point }} point{{ $voucher->point > 1 ? 's' : '' }}
                                        @endif
                                        </div>
                                    </div>
                                    <div class="voucher-right">
                                        {!! $voucher->content !!}
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                        @include('_includes.share')
                        @include('_includes.back')    
                    </section>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
