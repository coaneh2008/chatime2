@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>{{ trans('membership.title') }}</li>
                    </ul>
                    <div class="bzg">
                        @include('_includes.flash')
                        <div class="bzg_c" data-col="l6" data-offset="l3">
                            <h1 class="h2 home-section-heading text-center">{{ trans('membership.register.title_1')}}</h1>

                            <div class="member-registration-steps">
                                <div class="member-registration-step is-active">1</div>
                                <div class="member-registration-step">2</div>
                            </div>

                            <form class="js-validate" action="{{ route('frontend.membership.do-register-step-1') }}" method="post">
                                <fieldset class="block">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <legend><b>{{ trans('membership.step_1.title') }}</b></legend>

                                    <div class="block-half">
                                        <label for="inputName">{{ trans('membership.step_1.name') }} 
                                            <span style="color:#FE2A1A;">*</span>
                                        </label>
                                        <input class="form-input {{ $errors->first('name')?'input-invalid':'' }}" id="inputName" name="name" value="{{ old('name') }}" type="text">
                                        <span class="msg-error">{{ $errors->first('name') }}</span>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputEmail">{{ trans('membership.step_1.email') }}
                                            <span style="color:#FE2A1A;">*</span>
                                        </label>
                                        <input class="form-input {{ $errors->first('email')?'input-invalid':'' }}" id="inputEmail" name="email" value="{{ old('email') }}" type="email">
                                         <span class="msg-error">{{ $errors->first('email') }}</span>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputEmail">Konfirmasi {{ trans('membership.step_1.email') }}
                                            <span style="color:#FE2A1A;">*</span>
                                        </label>
                                        <input class="form-input {{ $errors->first('email')?'input-invalid':'' }}" id="inputEmail" name="confirm_email" value="{{ old('confirm_email') }}" type="email">
                                         <span class="msg-error">{{ $errors->first('confirm_email') }}</span>
                                    </div>
                                    <div class="block-half" style="display:none">
                                        <label for="inputTelephone">{{ trans('membership.step_1.telephone') }}</label>
                                        <input class="form-input {{ $errors->first('telephone')?'input-invalid':'' }}" id="inputTelephone" name="telephone" value="{{ old('telephone') }}" type="text">
                                        <span class="msg-error">{{ $errors->first('telephone') }}</span>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputCellphone">{{ trans('membership.step_1.cellphone') }}
                                            <span style="color:#FE2A1A;">*</span>
                                        </label>
                                        <input class="form-input {{ $errors->first('cellphone')?'input-invalid':'' }}" id="inputCellphone" name="cellphone" value="{{ old('cellphone') }}"  type="text">
                                        <span class="msg-error">{{ $errors->first('cellphone') }}</span>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputIDNumber">{{ trans('membership.step_1.id_card') }}
                                            <span style="color:#FE2A1A;">*</span>
                                        </label>
                                        <input class="form-input {{ $errors->first('identity_card_id')?'input-invalid':'' }}" id="inputIDNumber" name="identity_card_id" value="{{ old('identity_card_id') }}" type="text">
                                        <span class="msg-error">{{ $errors->first('identity_card_id') }}</span>
                                    </div>
                                    <fieldset class="block-half" style="display:none">
                                        <legend>{{ trans('membership.step_1.sex') }}</legend>
                                        @foreach ($sexTypes as $key => $sex)
                                            <label>
                                                <input type="radio" value="{{ $key }}" name="gender">
                                                <span>{{ $sex}}</span>
                                            </label>
                                            &nbsp;
                                        @endforeach
                                    </fieldset>
                                    <div class="block-half" style="display:none">
                                        <label for="inputBirthplace">{{ trans('membership.step_1.birth_place') }}</label>
                                        <input class="form-input" id="inputBirthplace" name="place_of_birth" value="{{ old('place_of_birth') }}" type="text">
                                        <span class="msg-error">{{ $errors->first('place_of_birth') }}</span>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputBirthday">{{ trans('membership.step_1.birthday') }}
                                            <span style="color:#FE2A1A;">*</span>
                                        </label>
                                        <input class="form-input js-datepicker" id="inputBirthday" name="date_of_birth" value="{{ old('date_of_birth') }}" type="text">
                                        <span class="msg-error">{{ $errors->first('date_of_birth') }}</span>
                                    </div>
                                    <div class="block-half" style="display:none">
                                        <label for="inputReligion">{{ trans('membership.step_1.religion') }}</label>
                                        <select class="form-input" id="inputReligion" name="religion_id">
                                            <option value="" selected disabled>Pilih Agama</option>
                                            @foreach ($religions as $key => $religion)
                                                <option value="{{ $key }}">{{ $religion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <fieldset class="block-half" style="display:none">
                                        <legend>{{ trans('membership.step_1.marital_status') }}</legend>
                                        @foreach ($maritalStatus as $key => $value)
                                            <label for="statusSingle">
                                                <input type="radio" value="{{ $key }}" name="marital_status">
                                                <span>{{ $value }}</span>
                                            </label>
                                            &nbsp;
                                        @endforeach
                                    </fieldset>
                                    <fieldset class="block-half" style="display:none">
                                        <legend>{{ trans('membership.step_1.nationality') }}</legend>
                                        @foreach ($citizenships as $key => $citizenship)
                                            <label for="WNI">
                                                <input type="radio" value="{{ $key }}" name="citizenship_id">
                                                <span>{{ $citizenship }}</span>
                                            </label>
                                            &nbsp;
                                        @endforeach
                                    </fieldset>
                                    <div class="block-half" style="display:none">
                                        <label for="inputOccupation">{{ trans('membership.step_1.occupation') }}</label>
                                        <select class="form-input" id="inputOccupation" name="occupation_id">
                                            <option value="" selected disabled>Pilih Pekerjaan</option>
                                            @foreach($occupations as $key => $occupation)
                                                <option value="{{ $key }}">{{ $occupation }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="block-half" style="display:none">
                                        <label for="inputShoppingPurpose">{{ trans('membership.step_1.shopping_purpose') }}</label>
                                        <select class="form-input" id="inputShoppingPurpose" name="shopping_purpose">
                                            <option value="" selected disabled>Pilih Tujuan Berbelanja</option>
                                            @foreach ($shoppingPurpose as $value)
                                                <option value="{{ $value }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </fieldset>
                                <fieldset class="block" style="display:none">
                                    <legend><b>{{ trans('membership.step_1.current_address') }}</b></legend>

                                    <div class="block-half">
                                        <label for="inputCurrentAddressStreet">{{ trans('membership.step_1.street_name') }}</label>
                                        <textarea class="form-input {{ $errors->first('current_address')?'input-invalid':'' }}" id="inputCurrentAddressStreet" name="current_address" rows="3">{{ old('current_address') }}</textarea>
                                        <span class="msg-error">{{ $errors->first('current_address') }}</span>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputCUrrentAddressCity">{{ trans('membership.step_1.city') }}</label>
                                        <select class="form-input" id="inputCUrrentAddressCity" name="current_address_city_id">
                                            <option value="" selected disabled>Pilih Kota</option>
                                            @foreach($cities as $key => $city)
                                                <option value="{{ $key }}">{{ $city }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputCurrentAddressZip">{{ trans('membership.step_1.zip_code') }}</label>
                                        <input class="form-input {{ $errors->first('current_address_zip_code')?'input-invalid':'' }}" id="inputCurrentAddressZip" name="current_address_zip_code" value="{{ old('current_address_zip_code') }}" type="text">
                                        <span class="msg-error">{{ $errors->first('current_address_zip_code') }}</span>
                                    </div>
                                </fieldset>
                                <fieldset class="block" style="display:none">
                                    <legend><b>{{ trans('membership.step_1.id_card_address') }}</b></legend>

                                    <div class="block-half">
                                        <label for="inputIDAddressStreet">{{ trans('membership.step_1.street_name') }}</label>
                                        <textarea class="form-input {{ $errors->first('ktp_address')?'input-invalid':'' }}" id="inputIDAddressStreet" name="ktp_address" rows="3">{{ old('ktp_address') }}</textarea>
                                        <span class="msg-error">{{ $errors->first('ktp_address') }}</span>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputIDAddressCity">{{ trans('membership.step_1.city') }}</label>
                                        <select class="form-input" id="inputIDAddressCity" name="ktp_address_city_id">
                                            <option value="" selected disabled>Pilih Kota</option>
                                             @foreach($cities as $key => $city)
                                                <option value="{{ $key }}">{{ $city }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputIDAddressZip">{{ trans('membership.step_1.zip_code') }}</label>
                                        <input class="form-input {{ $errors->first('ktp_address_zip_code')?'input-invalid':'' }}" id="inputIDAddressZip" name="ktp_address_zip_code" value="{{ old('ktp_address_zip_code') }}" type="text">
                                        <span class="msg-error">{{ $errors->first('ktp_address_zip_code') }}</span>
                                    </div>
                                </fieldset>
                                <div class="text-right block">
                                    <button class="btn btn--primary">{{ trans('membership.step_1.next') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
