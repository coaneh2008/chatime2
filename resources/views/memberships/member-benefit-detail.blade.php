@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li><a href="{{ route('frontend.membership.index') }}">{{ trans('membership.title') }}</a></li>
                        <li>
                            <a href="{{ route('frontend.membership.benefit-index', ['slug' => $benefit->slug]) }}">
                                {{ $benefit->title }}
                            </a>
                        </li>
                        <li>{{ $promo->title }}</li>
                    </ul>

                    <figure>
                        <img src="{{ $promo->getThumbnail('banner_image', "default") }}" alt="">
                    </figure>

                    <section class="home-section">
                        {!! $promo->content !!}
                    </section>

                    @include('_includes.share', ['title' => $promo->title, 'description' => $promo->description])
                    @include('_includes.back')
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
