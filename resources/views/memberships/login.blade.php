@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>{{ trans('membership.title') }}</li>
                    </ul>
                    @include('_includes.flash')

                    <div class="bzg">
                        <div class="bzg_c" data-col="l4" data-offset="l4">
                            <h1 class="h2 home-section-heading text-center">
                                {{ trans('membership.login.title_1') }}
                            </h1>
                            
                            <form class="js-validate" action="{{ route('frontend.membership.do-login') }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="block-half">
                                    <label for="inputCardNumber">{{ trans('membership.login.card_number') }}</label>
                                    <input class="form-input" id="inputCardNumber" name="no_kartu_member" value="{{ old('username') }}" type="text">
                                    <span class="msg-error">{{ $errors->first('no_kartu_member') }}</span>
                                </div>
                                <div class="block-half">
                                    <label for="inputPassKey">Passkey</label>
                                    <input class="form-input" id="inputPassKey" name="passkey" value="{{ old('password') }}" type="password">
                                    <span class="msg-error">{{ $errors->first('passkey') }}</span>
                                </div>
                                <div class="block-half">
                                    <a href="{{ route('frontend.membership.forgot-passkey') }}">
                                        <small>Lupa Passkey?</small>
                                    </a>
                                </div>
                                <button class="btn btn--block btn--primary mb-16">{{ trans('membership.login.title_3') }}</button>

                                <p>
                                    <small>Ingin jadi member?
                                    <a href="{{ route('frontend.membership.register') }}">
                                        daftar
                                    </a>
                                    </small>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
