@include('_includes.head')

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">Home</a></li>
                        <li><a href="{{ route('frontend.membership.dashboard') }}">{{ trans('membership.title') }}</a></li>
                        <li>Update Passkey</li>
                    </ul>

                    <div class="membership-layout">
                        @include('memberships._includes.membership-sidebar')

                        <div class="membership-layout-body">
                            <div class="membership-layout-content">
                                <h2 class="home-section-heading">Update Passkey</h2>
                                @include('_includes.flash')
                                <div class="bzg">
                                    <div class="bzg_c" data-col="l9">
                                        <form class="js-validate" action="{{ route('frontend.membership.do-edit-passkey') }}" method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="block-half">
                                                <label for="inputOldPasskey">
                                                    Passkey Lama
                                                    <span style="color:#FE2A1A;">*</span>
                                                </label>
                                                <input class="form-input" name="old_passkey" value="{{ old('old_passkey') }}" type="password">
                                                <span class="msg-error">{{ $errors->first('old_passkey') }}</span>   
                                            </div>
                                            <div class="block-half">
                                                <label for="inputOldPasskey">
                                                    Passkey Baru
                                                    <span style="color:#FE2A1A;">*</span>
                                                </label>
                                                <input class="form-input" name="new_passkey" value="{{ old('new_passkey') }}" type="password">
                                                <span class="msg-error">{{ $errors->first('new_passkey') }}</span>   
                                            </div>
                                            <div class="block-half">
                                                <label for="inputOldPasskey">
                                                    Konfirmasi Passkey Baru
                                                    <span style="color:#FE2A1A;">*</span>
                                                </label>
                                                <input class="form-input" name="confirm_new_passkey" value="{{ old('confirm_new_passkey') }}" type="password">
                                                <span class="msg-error">{{ $errors->first('confirm_new_passkey') }}</span>   
                                            </div>
                                            
                                            <button class="btn btn--primary">Simpan</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>
    @include('memberships._includes.renewal-pop-up-message')
@include('_includes.scripts')

