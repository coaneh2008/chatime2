@include('_includes.head')

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>{{ trans('membership.title') }}</li>
                    </ul>

                    <div class="membership-layout">
                        @include('memberships._includes.membership-sidebar')

                        <div class="membership-layout-body">
                            @include('memberships._includes.membership-nav')

                            <div class="membership-layout-content">
                                <h2 class="home-section-heading">Ongoing event</h2>

                                <div class="fg fg-480-2 fg-1024-3">
                                    @foreach ($events as $event)
                                        @if($event->type == \App\Model\Event::TYPE_FREE_MEMBERSHIP)
                                            @if($followFreeMembership)
                                                <div>
                                                    <a class="anchor" href="{{ route('frontend.events.show', $event->slug) }}">
                                                    <img class="full-width mb-8" src="{{ $event->thumb_image ? $event->getThumbnail('thumb_image', 'medium') : '//placehold.it/250x200' }}" alt="">
                                                    <div class="p-8">
                                                        <h3 class="mb-8">{{ $event->title }}</h3>
                                                        <p>{{ $event->description }}</p>
                                                    </div>
                                                    </a>
                                                </div>
                                            @endif
                                        @elseif($event->type == \App\Model\Event::TYPE_MEMBER_GET_MEMBER)
                                            @if(!$tempAccount)
                                                <div>
                                                    <a class="anchor" href="{{ route('frontend.events.show', $event->slug) }}">
                                                    <img class="full-width mb-8" src="{{ $event->thumb_image ? $event->getThumbnail('thumb_image', 'medium') : '//placehold.it/250x200' }}" alt="">
                                                    <div class="p-8">
                                                        <h3 class="mb-8">{{ $event->title }}</h3>
                                                        <p>{{ $event->description }}</p>
                                                    </div>
                                                    </a>
                                                </div>
                                            @endif
                                        @else
                                            <div>
                                                <a class="anchor" href="{{ route('frontend.events.show', $event->slug) }}">
                                                <img class="full-width mb-8" src="{{ $event->thumb_image ? $event->getThumbnail('thumb_image', 'medium') : '//placehold.it/250x200' }}" alt="">
                                                <div class="p-8">
                                                    <h3 class="mb-8">{{ $event->title }}</h3>
                                                    <p>{{ $event->description }}</p>
                                                </div>
                                                </a>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>
    <div class="modal {{ Session::has(NOTIF_WARNING) ? 'is-active' : '' }}" id="renewalNoStatus">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>
            <div class="text-center">
                {!! Session::get(NOTIF_WARNING) !!}
                @if($btnPayIsShow)
                    @if(($adminType == 0) && (Setting::get('show-hide-online-administration-option') == 'Show'))
                        <a class="btn btn--primary" href="{{ route('frontend.payment.pay-via-ace-online') }}" target="blank">Bayar</a>
                    @elseif($adminType==1)
                        <a class="btn btn--primary" href="{{ route('frontend.store-location.index') }}" target="blank">Temukan Toko</a>
                    @else
                        @if(Setting::get('show-hide-online-administration-option') == 'Show')
                            <a class="btn btn--primary" href="{{ route('frontend.payment.pay-via-ace-online') }}" target="blank">Bayar</a>
                        @endif
                        <a class="btn btn--primary" href="{{ route('frontend.store-location.index') }}" target="blank">Temukan Toko</a>
                    @endif
                    @if($havePoint)
                        <a class="btn btn--outline btn--primary" href="{{ route('frontend.auto-renewal.yes', 1) }}" target="blank">Gunakan point</a>
                    @endif
                @endif
            </div>
        </div>
    </div>
    <div class="modal {{ Session::has('NOTIF_WARNING_POINT_EXPIRED') ? 'is-active' : '' }}" id="pointExpired">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>
            <div class="text-center">
                {!! Session::get('NOTIF_WARNING_POINT_EXPIRED') !!}
            </div>
        </div>
    </div>
    <div class="modal {{ (Session::has('NOTIF_SUCCESS_RENEW') || Session::has('NOTIF_DANGER_RENEW')) ? 'is-active' : '' }}">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>

            <div class="text-center">
                @if($notifSuccess = Session::get('NOTIF_SUCCESS_RENEW'))
                    {!! $notifSuccess !!}
                @elseif($notifWarning = Session::get('NOTIF_DANGER_RENEW'))
                    {!! $notifWarning !!}
                @endif
            </div>
        </div>
    </div>
    @include('memberships._includes.renewal-pop-up-message')
@include('_includes.scripts')
