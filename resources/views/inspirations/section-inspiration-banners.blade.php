<div class="home-section">
    <h2 class="home-section-heading">{{ trans('home.inspiration.title') }}</h2>
    <div class="section-inspirations">
        <div class="section-inspiration-big">
            @if(!empty($bigInspirationBanners[0]))
                @if($bigInspirationBanners[0]->banner_image)
                    <a href="{{ route('frontend.inspiration.show', $bigInspirationBanners[0]->slug)}}">
                            <img src="{{ asset($bigInspirationBanners[0]->getThumbnail('banner_image', 'big_square')) }}" alt="{{ $bigInspirationBanners[0]->title }}">
                    </a>
                @endif
            @endif
        </div>
        <div class="section-inspiration-small">
            @foreach($smallInspirationBanners as $banner)
                @if($banner->banner_image)
                    <a href="{{ route('frontend.inspiration.show', $banner->slug) }}">
                        <img src="{{ asset($banner->getThumbnail('banner_image', 'small_square')) }}" alt="{{ $banner->title }}">
                    </a>
                @endif
            @endforeach
        </div>
        <div class="section-inspiration-big">
            @if(!empty($bigInspirationBanners[1]))
                @if($bigInspirationBanners[1]->banner_image)
                    <a href="{{ route('frontend.inspiration.show', $bigInspirationBanners[1]->slug)}}">
                        <img src="{{ $bigInspirationBanners[1]->getThumbnail('banner_image', 'big_square')}}" alt="{{ $bigInspirationBanners[1]->title }}">
                    </a>
                @endif
            @endif
        </div>
    </div>
</div>
