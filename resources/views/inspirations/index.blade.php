@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>{{ trans('inspiration.title') }}</li>
                    </ul>

                    @include('inspirations.section-inspiration-banners')
                    <div class="inspiration-list-layout">
                        <div class="inspiration-list-layout-nav">
                            <form class="inspiration-list-mobile-nav block">
                                <label for="selectCategory">Category</label>
                                <select class="form-input" id="selectCategory" name="slug" onchange="this.form.submit()"">
                                    @foreach ($categories as $value)
                                        <option value="{{ $value->slug }}" {{ app('request')->input('slug') == $value->slug ? 'selected' : '' }}>{{ $value->title }}</option>
                                    @endforeach
                                </select>
                            </form>
                            <ul class="inspiration-list-nav list-nostyle">
                                @foreach ($categories as $key => $value)
                                    <li>
                                        <a class="{{ app('request')->input('slug') ? ($value->slug == app('request')->input('slug') ? 'is-active' : '') : ($key==0 ? 'is-active' : '')}}" href="?slug={{ $value->slug }}">{{ $value->title }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="inspiration-list-layout-content">
                            <ul class="inspiration-list list-nostyle">
                                @if(count($articles)<1)
                                No Article Yet
                                @else
                                @foreach($articles as $article)
                                    <li class="inspiration-list-item">
                                        <article class="inspiration-list-media">
                                            <figure class="inspiration-list-media-figure">
                                                @if($article->thumb_image)
                                                    <a href="{{ route('frontend.inspiration.show', $article->slug) }}">
                                                        <img src="{{ asset($article->getThumbnail('thumb_image', 'medium')) }}" alt="{{ $article->title }}">
                                                    </a>
                                                @endif
                                            </figure>
                                            <div class="inspiration-list-media-content">
                                                <time>
                                                    {{ Carbon\Carbon::parse($article->published_date ? $article->published_date : $article->published_at)->format('d F Y') }}
                                                </time>
                                                <h2>
                                                    <a href="{{ route('frontend.inspiration.show', $article->slug) }}">
                                                    {{ $article->title }}
                                                    </a>
                                                </h2>
                                                <p>{{ $article->description }}</p>
                                            </div>
                                        </article>
                                    </li>
                                @endforeach
                                @endif
                            </ul>
                            @include('_includes.pagination', ['paginator' => $articles])
                        </div>
                        <div class="inspiration-list-layout-archieve">
                            <h3 class="h4 text-caps text-red">Archive</h3>

                            <ul class="inspiration-list-archieve-list list-nostyle">
                                @foreach ($archivedYear as $key => $value)
                                <li>
                                    <button class="inspiration-list-archieve-btn">{{ $value->year }}</button>
                                    <ul class="inspiration-list-archieve {{ app('request')->input('year') ? $value->year == app('request')->input('year') ? 'is-active' : '' : $key==0 ? 'is-active' : ''}} list-nostyle">
                                        @foreach(\App\Model\InspirationArticle::getArchivedMonthByYear($value->year) as $val)
                                            <li class = "{{ $val->month == app('request')->input('month') ? 'is-active' : '' }}">
                                                <a href="{{ app('request')->input('slug') ? '?slug='. app('request')->input('slug') . '&' : '?' }}{{'month='. $val->month . '&year=' . $value->year}}">
                                                    {{ $val->month }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                <section class="home-section">
                        <h2 class="home-section-heading">Popular articles</h2>
                        <div class="inspiration-list-related">
                            @foreach ($popularArticles as $value)
                                <div class="inspiration-list-related-item">
                                    <a class="inspiration-list-related-anchors" href="{{ route('frontend.inspiration.show', $value->slug) }}">
                                        <img src="{{ asset($value->getThumbnail('thumb_image', 'medium')) }}" alt="{{ $value->title }}">
                                        <span>{{ $value->title }}</span>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </section>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
