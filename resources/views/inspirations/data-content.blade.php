<hr>
<article class="js-inspiration-article" data-slug="{{ route('frontend.inspiration.show', $article->slug) }}">
    <div class="label" style="background: #FEA827;">{{ $article->inspirationArticleCategory->title }}</div>

    <h1 class="text-caps">{{ $article->title }}</h1>
    <time>
        {{ Carbon\Carbon::parse($article->published_date ? $article->published_date : $article->published_at)->format('d F Y') }}
    </time>

    <figure>
        <img class="full-width" src="{{ $article->getThumbnail('thumb_image', 'huge') }}" alt="{{ $article->title }}">
    </figure>

    <p>Written by: <b>{{ $article->user->username }}</b></p>

    {!! $article->content !!}

    @include('_includes.share', ['title' => $article->title, 'description' => $article->description, 'shareUrl' => route('frontend.inspiration.show', ['slug' => $article->slug])])

    @include('_includes.back')
</article>
    <br>
    @if(count($relatedProducts)>0)
        <section class="block">
            <h2 class="home-section-heading">Related product</h2>

            <div class="product-detail-related-products">
                @foreach($relatedProducts as $articleAttachment)
                    <div class="product-detail-related-products-item">
                        <img src="{{ asset($articleAttachment->product->getThumbnail('image', 'medium-2')) }}" alt="">
                        <p>{{ $articleAttachment->product->title }}</p>
                        <a class="btn btn--block btn--primary" href="{{ $articleAttachment->product->url_to_ace_online }}" target="_blank">Buy at ACE Online</a>
                    </div>
                @endforeach
            </div>
        </section>
    @endif