@include('_includes.head')

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li><a href="{{ route('frontend.inspiration.index') }}">{{ trans('inspiration.title') }}</a></li>
                        <li><a href="{{ route('frontend.inspiration.index') }}?slug={{ $article->inspirationArticleCategory->slug }}">{{ $article->inspirationArticleCategory->title }}</a></li>
                        <li>{{ $article->title }}</a></li>
                    </ul>

                    <div class="product-detail">
                        <div class="product-detail-article">
                            <article class="js-inspiration-article" data-slug="{{ route('frontend.inspiration.show', $article->slug) }}">
                                <div class="label" style="background: #FEA827;">{{ $article->inspirationArticleCategory->title }}</div>

                                <h1 class="text-caps">{{ $article->title }}</h1>
                                <time>
                                    {{ Carbon\Carbon::parse($article->published_date ? $article->published_date : $article->published_at)->format('d F Y') }}
                                </time>

                                <figure>
                                    <img class="full-width" src="{{ $article->getThumbnail('thumb_image', 'huge') }}" alt="{{ $article->title }}">
                                </figure>

                                <p>Written by: <b>{{ $article->user->username }}</b></p>

                                {!! $article->content !!}

                                @include('_includes.share', ['title' => $article->title, 'description' => $article->description])

                                @include('_includes.back')
                            </article>
                            <?php
                                $relatedProducts = \App\Model\InspirationArticle::getInspirationArticleAttachments($article->id);
                            ?>
                            <br>
                            @if(count($relatedProducts)>0)
                                <section class="block">
                                    <h2 class="home-section-heading">Related product</h2>

                                    <div class="product-detail-related-products">
                                        @foreach($relatedProducts as $articleAttachment)
                                            <div class="product-detail-related-products-item">
                                                <img src="{{ asset($articleAttachment->product->getThumbnail('image', 'medium-2')) }}" alt="">
                                                <p>{{ $articleAttachment->product->title }}</p>
                                                <a class="btn btn--block btn--primary" href="{{ $articleAttachment->product->url_to_ace_online }}"" target="_blank">Buy at ACE Online</a>
                                            </div>
                                        @endforeach
                                    </div>
                                </section>
                            @endif
                            @if($nextArticle)
                                <div class="text-center" id="articleLoader" data-next-article="{{ route('frontend.inspiration.next-article-data', ['counter' => 0, 'slug' => $article->slug]) }}">
                                    loading...
                                </div>
                            @endif
                        </div>
                        <aside class="product-detail-aside">

                            @if (count($relatedArticles) > 0)
                            <h4 class="text-red text-caps">Related article</h4>
                            <div class="product-detail-related">
                                @foreach($relatedArticles as $relatedArticle)
                                <a href="{{ route('frontend.inspiration.show', $relatedArticle->slug) }}">
                                    <figure>
                                        <img src="{{ asset($relatedArticle->getThumbnail('thumb_image', 'medium')) }}" alt="{{ $relatedArticle->title }}">
                                        <figcaption>
                                            <time>
                                                {{ Carbon\Carbon::parse($relatedArticle->published_date ? $relatedArticle->published_date : $relatedArticle->published_at)->format('d F Y') }}
                                            </time>
                                            <b class="text-caps">{{ $relatedArticle->title }}</b>
                                        </figcaption>
                                    </figure>
                                </a>
                                @endforeach
                            </div>
                            @endif

                            @if ($mostReadArticles)

                            <h4 class="text-red text-caps">Most read</h4>

                            <div class="product-detail-related">
                                @foreach($mostReadArticles as $mostReadArticle)
                                <a href="{{ route('frontend.inspiration.show', $mostReadArticle->slug) }}">
                                    <figure>
                                        <img src="{{ asset($mostReadArticle->getThumbnail('thumb_image', 'medium')) }}" alt="{{ $mostReadArticle->title }}">
                                        <figcaption>
                                            <time>
                                                {{ Carbon\Carbon::parse($mostReadArticle->published_date ? $mostReadArticle->published_date : $mostReadArticle->published_at)->format('d F Y') }}
                                            </time>
                                            <b class="text-caps">{{ $mostReadArticle->title }}</b>
                                        </figcaption>
                                    </figure>
                                </a>
                                @endforeach
                            </div>
                            @endif
                        </aside>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
