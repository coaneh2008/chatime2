@include('_includes.head')

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">Home</a></li>
                        <li><a href="{{ route('frontend.membership.dashboard') }}">{{ trans('membership.title') }}</a></li>
                        <li>Transaction & Redeem </li>
                    </ul>

                    <div class="membership-layout">
                        @include('memberships._includes.membership-sidebar')

                        <div class="membership-layout-body">
                            @include('memberships._includes.membership-nav')

                            <div class="membership-layout-content">
                                <section class="mb-48">
                                    <h2 class="home-section-heading">Transaction History</h2>

                                    <table class="table js-table-enhanced">
                                        <thead>
                                           <tr>
                                                <th align="left">Receipt</th>
                                                <th align="left">Store</th>
                                                <th align="right">Date</th>
                                                <th align="right">Amount</th>
                                                <th align="right">Point</th>
                                                <th align="right">Coupon</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($transactions as $transaction)
                                                <tr>
                                                    <td align="left">{{ $transaction->receive_no }} </td>
                                                    <td align="left">{{ $transaction->company_name }}</td>
                                                    <td align="right">{{ \Carbon\Carbon::parse($transaction->date)->format('d/M/Y') }}</td>
                                                    <td align="right">Rp{{ number_format($transaction->amount, 2, ',', '.') }}</td>
                                                    <td align="right">{{ $transaction->point }}</td>
                                                    <td align="right">{{ $transaction->coupon_no?$transaction->coupon_no:'-' }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </section>

                                <section class="mb-48">
                                    <h2 class="home-section-heading">Redeem Point History</h2>

                                    <table class="table js-table-enhanced">
                                        <thead>
                                            <tr>
                                                <th align="left">Receipt</th>
                                                    <th align="left">Store</th>
                                                    <th align="right">Date</th>
                                                    <th align="right">Amount</th>
                                                    <th align="right">Point</th>
                                                </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($redeems as $redeem)
                                                <tr>
                                                    <td align="left">{{ $redeem->redeem_no }}</td>
                                                    <td align="left">{{ $redeem->company_name }}</td>
                                                    <td align="right">{{ \Carbon\Carbon::parse($redeem->date)->format('d/M/Y') }}</td>
                                                    <td align="right">Rp{{ number_format($redeem->amount, 2, ',', '.') }}</td>
                                                    <td align="right">{{ $redeem->point }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
