


   
    @foreach ($list_promo as $banner)    
    <div class="col-lg-4 col-xs-6 container_list_promo" style="font-family: LasiverMedium">
        <div class="thumbnail_promo" style="cursor: pointer;">
            <a href="{{ $banner->url }}">
                <img src="{{ asset($banner->banner_image) }}" alt="{{ $banner->title }}">
            </a>
        </div>
        <div class="col-lg-12 col-xs-12" style="color:#5C2D91;padding-top: 5%;padding-bottom: 5%;text-align: left;">
            <b>{{ $banner->title }}</b>
        </div>
        <div class="col-lg-12 col-xs-12" style="color:#5C2D91;margin-bottom: 5%;padding-left: 0px;">
            <div class="col-lg-2 col-xs-3">
                <img src="{{ asset('assets/img/time_icon.png') }}" alt="">
            </div>
            <div class="col-lg-10 col-xs-9" style="text-align: left">
                <b>Periode Promo</b><br/>
                {{ date('d',strtotime($banner->start_date)) }} - {{ date('d M Y',strtotime($banner->end_date)) }} 
            </div>
        </div>
        <div class="col-lg-12 col-xs-12" style="color:#5C2D91;padding-left: 0px;">
        
            <!-- <div class="col-lg-2">
                <img src="{{ asset('assets/img/voucher_icon.png') }}" alt="">
            </div>
            <div class="col-lg-10" style="text-align: left;">
                <b>Kode Promo</b><br/>
                <span style="color:#FF64D0;font-weight: bold;">{{ $banner->kode_promo }}</span>
            </div> -->
            <div class="row container_katering_service" >
                <div class="col-lg-6" style="margin-top: 5%;margin-bottom:5%;margin-left: auto;margin-right: auto;position: relative;">
                    <a href="{{ $banner->url }}" style="text-decoration: none">
                    <div style="color: #fff;font-size: 18px;border-radius: 20px;background-color: #5DD859;
                    padding-top: 6%;padding-bottom: 6%;font-weight: bold;text-align: center;">
                        More Info
                    </div>
                    </a>

                </div>
            </div>
        </div>
    </div>
    @endforeach




