<!-- <div class="main-slider slider-style slider-style--primary" data-slick='{
    "autoplay": true,
    "autoplaySpeed": 5000,
    "pauseOnHover": false
}'>
    @foreach ($banners2 as $banner)
        <div>
            <div style="position: absolute;top:20%;color: #fff;margin-left: 3%">
                <div class="row">
                    <div class="col-lg-7" style="font-size: 25px">
                        <h1><b>{{ $banner->judul }}</b></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6" style="font-size: 18px">
                        {{ $banner->deskripsi }}
                    </div>
                </div>
                
            </div>

            @if($banner->type != '')
                <a {{ $banner->type==1 ? 'data-fancybox' : '' }} href="{{ $banner->video_url }} {{ $banner->type==1 ? '&control=0&showinfo=0' : ''}}" target="_blank">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="">
                </a>
            @else
                <a href="{{ $banner->video_url }}">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="">
                </a>
            @endif
        </div>
    @endforeach
</div> -->
<!-- <div class="multiple-items2">
    @foreach ($banners2 as $banner)
        <div style="cursor: pointer;width: 100%">
            <div style="position: absolute;top:20%;color: #fff;margin-left: 3%">
                <a href="{{ $banner->video_url }}">
                <div class="row">
                    <div class="col-lg-10" style="font-size: 25px">
                        <h1><b>{{ $banner->judul }}</b></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6" style="font-size: 18px">
                        {{ $banner->deskripsi }}
                    </div>
                </div>
                </a>
                <div class="row">
                    <div class="col-lg-4" style="margin-top: 2%">
                        <a href="{{ $banner->video_url }}">
                            <div style="color: #fff;font-size: 18px;border-radius: 20px;background-color: #5DD859;
                            padding-top: 6%;padding-bottom: 6%;font-weight: bold;text-align: center;">
                                More Info
                            </div>
                        </a>

                    </div>
                </div>
            </div>

            @if($banner->type != '')
                <a {{ $banner->type==1 ? 'data-fancybox' : '' }} href="{{ $banner->video_url }} {{ $banner->type==1 ? '&control=0&showinfo=0' : ''}}" target="_blank">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="" width="100%">
                </a>
            @else
                <a href="{{ $banner->video_url }}">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="" width="100%">
                </a>
            @endif
        </div>
    @endforeach
</div> -->

<div class="multiple-items2">
    @foreach ($banners2 as $banner)
        <img src="{{ asset($banner->image) }}" alt="">
    @endforeach
</div>





<!-- <div class="multiple-items2-product">
    @foreach ($banners2 as $banner)
        <div style="cursor: pointer;width: 100%">


            @if($banner->type != '')
                <a {{ $banner->type==1 ? 'data-fancybox' : '' }} href="{{ $banner->video_url }} {{ $banner->type==1 ? '&control=0&showinfo=0' : ''}}" target="_blank">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="" width="100%">
                </a>
            @else
                <a href="{{ $banner->video_url }}">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="" width="100%">
                </a>
            @endif
        </div>
        
    @endforeach
</div> -->



<div class="container container_filter_promo" style="margin-top: 2%;margin-bottom: 2%">
    
    <div class="col-lg-8" style="padding-left: 0px;">
        
        <!-- <div class="col-lg-6" style="background-color: #5C2D91;color: #fff;padding-top: 1.5%;padding-bottom: 1.5%;border-radius: 20px;">
            <b>Cari Promo</b>
        </div> -->
    </div>
      
    <div class="col-lg-4" style="padding-left: 0px;font-family: LasiverMedium">
        <div class="col-lg-5" style="padding-top: 3%;padding-bottom: 3%;padding-left: 0px">
            <span style="color: #5C2D91;font-size: 18px;"><b>Cari Promo:</b></span>
        </div>
        <div class="col-lg-7 selectdiv" style="background-color: #5C2D91;color: #fff;border-radius: 20px;">
            <select class="form-control category_promo" style="background-color:#5C2D91;border:none;color: #fff;-webkit-box-shadow:none;font-size: 19px;font-family: LasiverMedium" onchange="load_filter_promotions(this,0)">
                <?php foreach ($promotionCategories as $row) { ?>
                    <option value="<?php echo $row->slug;?>" ><h1 style="font-family: LasiverMedium"><?php echo $row->title?></h1></option>
                <?php } ?>    
            </select>
        </div>

    </div>
</div>

<div class="container container_promo list_promo">
    
    @foreach ($list_promo as $banner)    
    <div class="col-lg-4 container_list_promo" style="font-family: LasiverMedium">
        <div class="thumbnail_promo" style="cursor: pointer;">
            <a href="{{ $banner->url }}">
                <img src="{{ asset($banner->banner_image) }}" alt="{{ $banner->title }}">
            </a>
        </div>

        <div class="col-lg-12" style="color:#5C2D91;padding-top: 5%;padding-bottom: 5%;text-align: left;font-size: 11px">
            <b>{{ $banner->title }}</b>
        </div>
        <div class="col-lg-12" style="color:#5C2D91;margin-bottom: 5%;padding-left: 0px;font-size: 11px">
            <div class="col-lg-2" >
                <img src="{{ asset('assets/img/time_icon.png') }}" alt="">
            </div>
            <div class="col-lg-10" style="text-align: left">
                <b>Periode Promo</b><br/>
                {{ date('d',strtotime($banner->start_date)) }} - {{ date('d M Y',strtotime($banner->end_date)) }}
            </div>
        </div>

        <div class="col-lg-12" style="color:#5C2D91;padding-left: 0px;">
        
            <!-- <div class="col-lg-2">
                <img src="{{ asset('assets/img/voucher_icon.png') }}" alt="">
            </div>
            <div class="col-lg-10" style="text-align: left;">
                <b>Kode Promo</b><br/>
                <span style="color:#FF64D0;font-weight: bold;">{{ $banner->kode_promo }}</span>
            </div> -->
            <div class="row container_katering_service" >
                <div class="col-lg-6" style="margin-top: 5%;margin-bottom:5%;margin-left: auto;margin-right: auto;position: relative;">
                    <a href="{{ $banner->url }}" style="text-decoration: none">
                    <div style="color: #fff;font-size: 18px;border-radius: 20px;background-color: #5DD859;
                    padding-top: 6%;padding-bottom: 6%;font-weight: bold;text-align: center;">
                        More Info
                    </div>
                    </a>

                </div>
            </div>
        </div>
    </div>
    @endforeach

    


</div>

 <link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick-theme.css') }}"/>

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/js/slick/slick.min.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/slack_custom.css') }}"/>


<script type="text/javascript">
    
    $('.multiple-items').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000
    });

    $('.multiple-items2').slick({
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      autoplay: true
    });
    
    /*$('.multiple-items2').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000
    });*/

    /*$('.multiple-items').slick();*/

    /*$('.carousel_group').flickity({
            // options
            groupCells: true,
            cellAlign: "left",
            pageDots: false,
            wrapAround: true,
            imagesLoaded: true
    });*/
</script>