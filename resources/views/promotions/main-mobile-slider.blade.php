

<!-- <div class="multiple-items_mobile">
    @foreach ($banners2 as $banner)
        
        <div style="cursor: pointer;width: 100%;position: relative;text-align: center;">
            <div style="position: absolute;
  top: 30%;
  left: 50%;
  transform: translate(-50%, -50%);color: #fff;font-family: LasiverMedium">
                <a href="{{ $banner->video_url }}">
                <div class="row">
                    <div class="col-xs-12" style="font-size: 30px;text-align: center;">
                        <b>{{ $banner->judul }}</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="font-size: 20px;text-align: center;">
                        {{ $banner->deskripsi }}
                    </div>
                </div>
                </a>
                
            </div>
            @if($banner->type != '')
                <a {{ $banner->type==1 ? 'data-fancybox' : '' }} href="{{ $banner->video_url }} {{ $banner->type==1 ? '&control=0&showinfo=0' : ''}}" target="_blank">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="" width="100%">
                </a>
            @else
                <a href="{{ $banner->video_url }}">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="" width="100%">
                </a>
            @endif
        </div>

    @endforeach
</div> -->

<div class="multiple-items2_mobile">
    @foreach ($banners2 as $banner)
        <img src="{{ asset($banner->image) }}" alt="">
    @endforeach
</div>


<div class="container container_filter_promo" style="margin-top: 2%;margin-bottom: 2%;width: 100%">
    
    <div class="col-xs-6">
        <!-- <div class="col-xs-12" style="background-color: #5C2D91;color: #fff;padding-top: 3%;padding-bottom: 3%;border-radius: 20px;">
            <b>Cari Promo</b>
        </div> -->
        
    </div>
      
    <div class="col-xs-6">
        
        <div class="col-xs-12 selectdiv" style="background-color: #5C2D91;color: #fff;border-radius: 20px;">
            <select class="form-control category_promo" style="background-color:#5C2D91;border:none;color: #fff;-webkit-box-shadow:none;font-family: LasiverMedium" onchange="load_filter_promotions(this,1)">
                <?php foreach ($promotionCategories as $row) { ?>
                    <option value="<?php echo $row->slug;?>"><span style="font-family: LasiverMedium"><?php echo $row->title?></span></option>
                <?php } ?>    
            </select>
        </div>
        
    </div>
</div>

<div class="container container_katering_service list_promo">
    
    @foreach ($list_promo as $banner)    


    <div class="col-xs-6 container_list_promo" style="font-family: LasiverMedium">
        <div class="thumbnail_promo" style="cursor: pointer;">
            <a href="{{ $banner->url }}">
                <img src="{{ asset($banner->banner_image) }}" alt="{{ $banner->title }}">
            </a>
        </div>
        <div class="col-xs-12" style="color:#5C2D91;padding-top: 5%;padding-bottom: 5%;text-align: left;font-size: 11px">
            <b>{{ $banner->title }}</b>
        </div>
        <div class="col-xs-12" style="color:#5C2D91;margin-bottom: 5%;padding-left: 0px;font-size: 11px">
            <div class="col-xs-3" style="padding-right: 0px">
                <img src="{{ asset('assets/img/time_icon.png') }}" alt="">
            </div>
            <div class="col-xs-9" style="text-align: left">
                <b>Periode Promo</b><br/>
                {{ date('d',strtotime($banner->start_date)) }} - {{ date('d M Y',strtotime($banner->end_date)) }}
            </div>
        </div>
        <div class="col-xs-12" style="color:#5C2D91;padding-left: 0px;font-size: 11px">
        
            <!-- <div class="col-xs-3" style="padding-right: 0px">
                <img src="{{ asset('assets/img/voucher_icon.png') }}" alt="">
            </div>
            <div class="col-xs-9" style="text-align: left;">
                <b>Kode Promo</b><br/>
                <span style="color:#FF64D0;font-weight: bold;">{{ $banner->kode_promo }}</span>
            </div> -->
            <div class="row container_katering_service" >
                <div class="col-xs-12" style="margin-top: 5%;margin-bottom:5%;margin-left: auto;margin-right: auto;position: relative;">
                    <a href="{{ $banner->url }}" style="text-decoration: none">
                    <div style="color: #fff;font-size: 18px;border-radius: 20px;background-color: #5DD859;
                    padding-top: 6%;padding-bottom: 6%;font-weight: bold;text-align: center;font-size: 11px">
                        More Info
                    </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    @endforeach


    

</div>

 <link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick-theme.css') }}"/>

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/js/slick/slick.min.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/slack_custom.css') }}"/>


<script type="text/javascript">
    
    $('.multiple-items_mobile').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000
    });
    
    $('.multiple-items').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000
    });

    /*$('.multiple-items').slick();*/

    /*$('.carousel_group').flickity({
            // options
            groupCells: true,
            cellAlign: "left",
            pageDots: false,
            wrapAround: true,
            imagesLoaded: true
    });*/
</script>