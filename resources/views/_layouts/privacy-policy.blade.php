@include('_includes.head')
<!-- @include('home.flash-message') -->
<style type="text/css">
    
    a {
    color: red;
    text-decoration: none;
    }

</style>
<div class="sticky-footer-container">
    <div class="sticky-footer-container-item">
        @include('_includes.header')
    </div>
    <div class="sticky-footer-container-item --pushed">
        <div class="site-cover"></div>
        <main class="site-main">
            <div class="container" style="padding-top: 1%">
                <ul class="breadcrumb">
                    <li><a href="{{ route('frontend.home.index') }}"><span style="color:#5C2D93;font-weight: bold;">{{ trans('home.home.title') }}</span></a></li>
                    <li style="color:#5C2D93;font-weight: bold;">{{ $page->title }}</li>
                </ul>

                
                
            </div>
            <div class="container" style="font-family: LasiverMedium">
                @yield('privacy-policy')
            </div>
        </main>
        


    </div>
    <div class="sticky-footer-container-item">
        @include('_includes.footer')
    </div>
</div>

@include('_includes.scripts')
