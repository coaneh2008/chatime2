@include('_includes.head')
<!-- @include('home.flash-message') -->
<div class="sticky-footer-container">
    <div class="sticky-footer-container-item">
        @include('_includes.header')
    </div>
    <div class="sticky-footer-container-item --pushed">
        <div class="site-cover"></div>
        <main class="site-main">
            <div class="container" style="padding-top: 1%">
                <ul class="breadcrumb" style="background-color: #5C2D93">
                    <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                    <li>Terms &amp; conditions</li>
                </ul>
                
            </div>
            <div class="container">
                @yield('term-and-condition')
            </div>
        </main>
        


    </div>
    <div class="sticky-footer-container-item">
        @include('_includes.footer')
    </div>
</div>

@include('_includes.scripts')
