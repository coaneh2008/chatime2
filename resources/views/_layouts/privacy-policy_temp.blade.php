@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>{{ $page->title }}</li>
                    </ul>

                    <div class="bzg">
                        <div class="bzg_c" data-col="l9">
                            @yield('privacy-policy')
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>


@include('_includes.head')
<!-- @include('home.flash-message') -->
<div class="sticky-footer-container">
    <div class="sticky-footer-container-item">
        @include('_includes.header')
    </div>
    <div class="sticky-footer-container-item --pushed">
        <div class="site-cover"></div>
        <div class="mobile-hide">
            <main class="site-main">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>{{ $page->title }}</li>
                    </ul>
                    <div class="bzg">
                        <div class="bzg_c" data-col="l9">
                            @yield('privacy-policy')
                        </div>
                    </div>
                </div>
                
            </main>
        </div>

    </div>
    <div class="sticky-footer-container-item">
        @include('_includes.ornament')
        @include('_includes.footer')
    </div>
</div>

@include('_includes.scripts')
