<!-- BEGIN FOOTER // -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter" style="text-align: center;">
    <tr>
        <td style="padding: 8px; padding-top: 16px;" align="center">
            @if($fbUsername = Setting::get('social-facebook-username'))
                <a href="https://www.facebook.com/{{ $fbUsername }}"><img src="{{ asset('assets/img/icon-fb.png') }}" width="16" alt="" /></a>
                &nbsp;
            @endif
            @if($twitterUsername = Setting::get('social-twitter-username'))
                <a href="https://twitter.com/{{ $twitterUsername }}"><img src="{{ asset('assets/img/icon-twitter.png') }}" width="16" alt="" /></a>
                &nbsp;
            @endif
            @if($igUsername = Setting::get('social-instagram-username'))
                <a href="https://www.instagram.com/{{ $igUsername }}"><img src="{{ asset('assets/img/icon-instagram.png') }}" width="16" alt="" /></a>
                &nbsp;
            @endif
            @if($youtubeUsername = Setting::get('social-youtube-username'))
                <a href="https://www.youtube.com/channel/{{ $youtubeUsername }}"><img src="{{ asset('assets/img/icon-youtube.png') }}" width="16" alt="" /></a>
            @endif
        </td>
    </tr>
    @if(isset($subscribe))
        <tr>
            <td style="padding: 8px;">You're reveiving this email because you opted in on <a href="{{ route('frontend.home.index') }}">Our Website</a>. If you never signed up for a newsletter, or don't want to receive anymore emails from us, <a href="#">Unsubscribe Here</a>.
            </td>
        </tr>
    @endif
    <tr>
        <td style="padding: 8px;">Copyright &copy; Ace Hardware All Rights Reserved.</td>
    </tr>
</table>
<!-- // END FOOTER -->