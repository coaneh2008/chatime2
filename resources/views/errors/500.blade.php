<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="{{ Setting::get('site-name') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" href="{{ asset('assets/img/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/img/favicon.png') }}">

    <title>{{ Setting::get('site-name')}}</title>

    <style>
        * {
            box-sizing: border-box;
        }

        html {
            font-family: -apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", "Roboto", "Helvetica Neue", Arial, sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        body {
            display: -webkit-flex;
            display: -moz-flex;
            display: -ms-flex;
            display: -o-flex;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
            text-align: center;
        }

        figure {
            margin: 0 0 16px;
        }

        figure img {
            width: 160px;
            margin-bottom: 16px;
        }

        figure figcaption {
            font-size: 24px;
            font-weight: 500;
        }

        a {
            color: #FE2A1A;
        }
    </style>
</head>
<body>
    <figure>
        <img src="{{ Setting::get('logo-url') }}" alt="">
        <figcaption>500 Internal Server Error</figcaption>
    </figure>
    <a href="{{ asset('/') }}">Kembali halaman depan</a>.</p>
</body>
</html>
