@include('_includes.head')

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <div class="text-center">
                        <h1 class="text-red mb-4">404</h1>
                        <h2>Maaf, halaman yang kamu tuju tidak ditemukan.</h2>
                        <p>Periksa kembali link yang ingin kamu tuju atau kembali ke <a href="{{ asset('/') }}">halaman depan</a>.</p>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
