@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>Promotion</li>
                    </ul>
                    <article class="product">
                        <figure>
                            <img src="{{ asset($homePromo->getThumbnail('banner_image', 'huge')) }}" alt="">
                        </figure>
                        <div class="bzg">
                            <div class="bzg_c" data-col="l8" data-offset="l2">
                                <?= $homePromo->content ?>
                            </div>
                        </div>
                        <br>
                    </article>
                    <?php
                        $relatedProducts = \App\Model\homePromotion::getHomePromotionAttachments($homePromo->id);
                    ?>
                    <br>
                    @if(count($relatedProducts)>0)
                        <section class="block">
                            <h2 class="home-section-heading">Related product</h2>

                            <div class="product-detail-related-products">
                                @foreach($relatedProducts as $articleAttachment)
                                    <div class="product-detail-related-products-item">
                                        <img src="{{ asset($articleAttachment->product->getThumbnail('image', 'medium-2')) }}" alt="">
                                        <p>{{ $articleAttachment->product->title }}</p>
                                        <a class="btn btn--block btn--primary" href="{{ $articleAttachment->product->url_to_ace_online }}"" target="_blank">Buy at ACE Online</a>
                                    </div>
                                @endforeach
                            </div>
                        </section>
                    @endif
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>
@include('_includes.scripts')