<header class="site-header">
    <div class="container">
        <button class="site-header-nav-trigger">
            <span class="fa fa-fw fa-bars"></span>
        </button>
        <button class="site-header-search-trigger js-search-trigger">
            <span class="fa fa-fw"></span>
        </button>

        <div class="site-header-tops">
            <a class="site-logo" href="{{ asset('/') }}">
                <img src="{{ asset('assets/img/logo.png') }}" alt="">
            </a>
            <div class="site-header-supports">
                <!-- <form class="site-search" role="search" action="{{ route('search') }}" method="get">
                    <input class="form-input" name="name" id="name" type="search" placeholder="{{ trans('header.search-placeholder') }}">
                    <button class="site-search-btn">
                        <span class="fa fa-fw fa-search"></span>
                    </button>
                    <button class="site-search-close js-search-trigger" type="button">
                        <span class="fa fa-fw fa-times"></span>
                    </button>
                </form> -->
                <!-- @foreach ($secondaryMenus as $secondaryMenu)
                    @if (strpos(strtolower($secondaryMenu->title), 'download') !== false)
                        <a class="site-header-support js-app-download" href="{{ $secondaryMenu->real_url }}" target="_blank" data-ios="{{ Setting::get('download-app-data-ios') }}" data-android="{{ Setting::get('download-app-data-android') }}">
                            <img src="{{ asset('assets/img/icon-nav-download.png') }}" alt="">
                            {{ $secondaryMenu->title }}
                        </a>
                    @else
                        <a class="site-header-support" href="{{ $secondaryMenu->real_url }}" target="{{ str_contains($secondaryMenu->real_url, env('APP_URL', '')) ? '' : '_blank' }}">
                            @if($secondaryMenu->icon_image)
                                <img src="{{ asset($secondaryMenu->getThumbnail('icon_image', 'full_height_small')) }}" alt="{{ $secondaryMenu->title }}">
                            @endif
                                {{ $secondaryMenu->title }}
                        </a>
                    @endif
                @endforeach -->

                <a class="site-header-support" href="">
                    HOME
                </a>
                <a class="site-header-support" href="store-locations">
                    OUTLETS
                </a>
                <a class="site-header-support js-app-download" href="https://itunes.apple.com/us/app/ace-hardware-indonesia/id1056027974?ls=1&mt=8" target="_blank" data-ios="https://itunes.apple.com/us/app/ace-hardware-indonesia/id1056027974?ls=1&mt=8" data-android="https://play.google.com/store/apps/details?id=id.co.acehardware.acerewards&hl=en">
                    ATEALIER
                </a>
                <a class="site-header-support" href="brochures.php">
                    SPEACIAL TEA
                </a>
                <a class="site-header-support" href="contact.php">
                    <div style="background-color: #FF22BF;border-radius: 15px;padding-top: 5%;padding-bottom: 5%;padding-left: 10px;padding-right: 10px;">MOBILE APP</div>
                </a>

            </div>
        </div>

        <div class="nav mobile-view">
            <button class="nav-close-btn">&times;</button>
            <a class="categories-trigger {{Auth::check() ? 'flag-login' : ''}}" target="_blank" href="{{ url('https://www.ruparupa.com/ace/?utm_source=acehardware.co.id&utm_medium=referral&utm_campaign=product+selection') }}">
                <span class="fa fa-fw fa-plus-circle"></span>
                Product Selection
            </a>
            <ul class="nav-list list-nostyle">
            @foreach ($mainMenus as $menu)
                <li class="nav-list-item">
                    <a class="nav-anchor <?php if ($menu->real_url == 'https://www.ruparupa.com/ace?utm_source=acehardware.co.id&utm_medium=referral&utm_content=buy+now+button&utm_campaign=button') echo 'buy-online'; ?>" href="{{ $menu->real_url }}" target="{{ str_contains($menu->real_url, env('APP_URL', '')) ? '' : '_blank' }}">
                    @if ($menu->icon_image)
                    <img src="{{ asset($menu->getThumbnail('icon_image', 'small_square')) }}">
                    @endif
                    {{ $menu->title }}</a>
                </li>
            @endforeach
            @if ($user = \Auth::user())
                @if ($user->isUser())
                <li class="nav-list-item">
                    <a class="nav-anchor" href="{{ route('frontend.membership.dashboard') }}">
                        My Profile
                    </a>
                </li>
                @endif
            @endif
            @foreach ($secondaryMenus as $secondaryMenu)
                @if (strpos(strtolower($secondaryMenu->title), 'download') !== false)
                    <li class="nav-list-item hidden-large">
                        <a class="nav-anchor js-app-download" href="{{ $secondaryMenu->real_url }}" target="_blank" data-ios="{{ Setting::get('download-app-data-ios') }}" data-android="{{ Setting::get('download-app-data-android') }}">
                            {{ $secondaryMenu->title }}
                        </a>
                    </li>
                @else
                    <li class="nav-list-item hidden-large">
                        <a class="nav-anchor" href="{{ $secondaryMenu->real_url }}" target="{{ str_contains($secondaryMenu->real_url, env('APP_URL', '')) ? '' : '_blank' }}">{{ $secondaryMenu->title }}</a>
                    </li>
                @endif
            @endforeach

            </ul>
        </div>

        <div class="category-dropdown">
            <ul class="category-list list-nostyle">
                 @foreach($productCategories as $productCategory)
                    <li class="category-list-item">
                        <a class="category-list-anchor category-list-anchor--level-1" href="{{ $productCategory->url_to_ace_online }}" target="_blank">
                            <img src="{{ asset($productCategory->getThumbnail('thumb_image', 'image_icon')) }}" alt="{{ $productCategory->title }}">
                            <span>{{ $productCategory->title }}</span>
                        </a>

                        {{-- <div class="category-list category-list--level-2">
                            <img class="category-list-img" src="{{ asset($productCategory->getThumbnail('sidebar_image', 'medium')) }}" alt="">
                        </div> --}}
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</header>
