@if ($notif_success = Session::get(NOTIF_SUCCESS))
	<div class="alert alert--success">
	    {{ $notif_success}}
	    <button class="alert-close">&times;</button>
	</div>
@endif
@if ($notif_danger = Session::get(NOTIF_DANGER))
	<div class="alert alert--alert">
        {{ $notif_danger}}
	    <button class="alert-close">&times;</button>
	</div>
@endif
