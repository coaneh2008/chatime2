    <div class="product-detail-share">
        <h4>Share</h4>
        <?php
            $title = isset($title) ? $title : \Setting::get('site-name');
            $description = isset($description) ? $description : \Setting::get('site-description');
            $shareUrl = isset($shareUrl) ? $shareUrl : URL::current();
        ?>
        <div class="social-medias">
            <a class="social-media --fb" onClick="window.open(this.href,'targetWindow','toolbar=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=800,height=350'); return false;" href="https://www.facebook.com/sharer/sharer.php?u={{  $shareUrl }}&title={{ $title }}&description={{ strip_tags($description) }}&quote={{ $title }}">
                <span class="fa fa-fw fa-facebook-official"></span>
            </a>
            <a class="social-media --tw" onClick="window.open(this.href,'targetWindow','toolbar=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=800,height=350'); return false;" href="https://twitter.com/intent/tweet?text={{ $title }} - Ace Hardware Indonesia {{ $shareUrl }}">
                <span class="fa fa-fw fa-twitter"></span>
            </a>
            <a class="social-media --gp" onClick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://plus.google.com/share?url={{ $shareUrl }}">
                <span class="fa fa-fw fa-google-plus"></span>
            </a>
            <a class="social-media --wa" href="whatsapp://send?text={{ $shareUrl }}">
                <span class="fa fa-fw fa-whatsapp"></span>
            </a>
        </div>
    </div>