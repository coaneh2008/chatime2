<!doctype html>
<html class="no-js" lang="en">
<head>
    @if (env('APP_ENV') == 'production')
        {!! Setting::get('head-script') !!}
    @endif
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta name="description" content="{{ isset($metaDescription) ? $metaDescription : Setting::get('site-description') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <meta name="format-detection" content="telephone=no"> -->
    <meta property="og:url" content="{{ URL::current() }}">
    <meta property="og:title" content="{{ isset($openGraphTitle) ? $openGraphTitle : Setting::get('site-name') }}">
    <meta property="og:site_name" content="{{ Setting::get('site-name') }}">
    <meta property="og:description" content="{{ isset($openGraphDescription) ? $openGraphDescription : Setting::get('site-name') }}">
    <meta property="og:image" content="{{ isset($metaImage) ? $metaImage : asset(Setting::get('site-image')) }}">
    <meta property="og:type" content="website">

    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="{{ '@' . Setting::get('social-twitter-username') }}">
    <meta name="twitter:creator" content="{{ '@' . Setting::get('social-twitter-username') }}" />
    <meta name="twitter:description" content="{{ isset($twitterDescription) ? $twitterDescription : Setting::get('site-name') }}">
    <meta name="twitter:title" content="{{ isset($twitterTitle) ? $twitterTitle : Setting::get('site-name') }}">
    <meta name="twitter:image" content="{{ isset($metaImage) ? $metaImage : asset(Setting::get('site-image')) }}">

    <link rel="apple-touch-icon" href="{{ asset('assets/img/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/img/favicon.png') }}">

    <title>{{ (isset($metaTitle)) ? $metaTitle : Setting::get('site-name')}}</title>

    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
</head>
<body>
    <!--[if lt IE 9]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
