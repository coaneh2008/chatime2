<section class="home-section">
    <h2 class="home-section-heading">Product category</h2>

    <ul class="section-category-list list-nostyle">
        
        @foreach($productCategories as $productCategory)
            <li>
                <a href="{{ route('frontend.product-category.show', $productCategory->slug) }}">
                    <img src="{{ asset($productCategory->getThumbnail('thumb_image', 'image_icon')) }}" alt="">
                    <span>{{ $productCategory->title }}</span>
                </a>
            </li>
        @endforeach
    </ul>
</section>
