<!doctype html>
<html class="no-js" lang="en">
<head>
    @if (env('APP_ENV') == 'production')
        {!! Setting::get('head-script') !!}
    @endif
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta name="description" content="{{ isset($metaDescription) ? $metaDescription : Setting::get('site-description') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <meta name="format-detection" content="telephone=no"> -->
    <meta property="og:url" content="{{ URL::current() }}">
    <meta property="og:title" content="{{ isset($openGraphTitle) ? $openGraphTitle : Setting::get('site-name') }}">
    <meta property="og:site_name" content="{{ Setting::get('site-name') }}">
    <meta property="og:description" content="{{ isset($openGraphDescription) ? $openGraphDescription : Setting::get('site-name') }}">
    <meta property="og:image" content="{{ isset($metaImage) ? $metaImage : asset(Setting::get('site-image')) }}">
    <meta property="og:type" content="website">

    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="{{ '@' . Setting::get('social-twitter-username') }}">
    <meta name="twitter:creator" content="{{ '@' . Setting::get('social-twitter-username') }}" />
    <meta name="twitter:description" content="{{ isset($twitterDescription) ? $twitterDescription : Setting::get('site-name') }}">
    <meta name="twitter:title" content="{{ isset($twitterTitle) ? $twitterTitle : Setting::get('site-name') }}">
    <meta name="twitter:image" content="{{ isset($metaImage) ? $metaImage : asset(Setting::get('site-image')) }}">

    <!-- <meta property="fb:app_id" content="" /> untuk dapat data analytic -->

    <link rel="apple-touch-icon" href="{{ asset('assets/img/chatime-favicon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/img/chatime-favicon.png') }}">

    <title>{{ (isset($metaTitle)) ? $metaTitle : Setting::get('site-name')}}</title>
    <style type="text/css">
    @font-face {
        font-family: LasiverBold;
        src: url('{{ asset('assets/fonts/LasiverBold.otf') }}');
    }

    @font-face {
        font-family: 'LasiverBlack';
        src: url('{{ asset('assets/fonts/LasiverBlack.otf') }}');
    }


    @font-face {
        font-family: 'LasiverMedium';
        src: url('{{ asset('assets/fonts/LasiverMedium.otf') }}');
    }

    @font-face {
        font-family: 'HandBook';
        src: url('{{ asset('assets/fonts/HandBook.ttf') }}');
    }

    </style>

    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5WWPRF2');</script>
    <!-- End Google Tag Manager -->
    
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5WWPRF2"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!--[if lt IE 9]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
