<!-- <footer class="footer" id="footer">
    <div class="container has-border">
        <div class="footer-sections">
            @foreach($footerMenus as $menu)
                @if(!$menu->parent_id)
                    <div class="footer-section">
                        <h4 class="footer-heading">{{ $menu->title }} 
                        @if($menu->icon_image)
                            <img src="{{ asset($menu->getThumbnail('icon_image', 'full_height_small')) }}" width="20">
                        @endif
                        </h4>
                        <ul class="footer-navs list-nostyle">
                            @foreach ($menu->childs as $firstchild)
                                <li>
                                    @if($firstchild->icon_image)
                                        <img src="{{ asset($firstchild->getThumbnail('icon_image', 'full_height_small')) }}" alt="{{ $firstchild->title }}">
                                    @endif
                                    <a href="{{ $firstchild->real_url }}" target="{{ str_contains($firstchild->real_url, env('APP_URL', '')) ? '' : '_blank' }}">{{ $firstchild->title }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            @endforeach
            <div class="footer-section">
                <h4 class="footer-heading">Call Center
                    @if(Setting::get('footer-call-center-image-icon'))
                        <img src="{{ asset(Setting::get('footer-call-center-image-icon')) }}" width="20">
                    @endif
                </h4>
                <a class="js-call-center" href="tel:{{ str_replace('-', '', Setting::get('footer-call-center-number')) }}">
                    <p>{{ Setting::get('footer-call-center-number') }}</p>
                </a>
            </div>
            <div class="footer-section">
                <form class="footer-subscribe" action="{{ route('frontend.subscribe') }}" method="post">
                    <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                    <h4 class="footer-heading"><label for="inputEmailSubscribe">Subscribe Newsletter</label></h4>
                    <input id="email" name="email" class="form-input block-half" id="inputEmailSubscribe" type="text" placeholder="{{ trans('footer.subscribe.email-placeholder') }}">
                    <span class="msg-error">{{ session('email') }}</span>
                    <button class="btn btn--block btn--primary">
                        <span class="fa fa-fw fa-envelope-o"></span>
                        {{ trans('footer.subscribe.btn-title') }}
                    </button>
                </form>
            </div>
            <div class="footer-section">
                <h4 class="footer-heading">Social Media</h4>

                <ul class="footer-navs list-nostyle">
                    <div class="social-medias">
                        @if($fbUsername = Setting::get('social-facebook-username'))
                            <a class="social-media --fb" href="https://www.facebook.com/{{ $fbUsername }}" target="blank">
                                <span class="fa fa-fw fa-facebook-official"></span>
                            </a>
                        @endif
                        @if($twitterUsername = Setting::get('social-twitter-username'))
                            <a class="social-media --tw" href="https://twitter.com/{{ $twitterUsername }}" target="blank">
                                <span class="fa fa-fw fa-twitter"></span>
                            </a>
                        @endif
                        @if($igUsername = Setting::get('social-instagram-username'))
                            <a class="social-media --ig" href="https://www.instagram.com/{{ $igUsername }}" target="blank">
                                <span class="fa fa-fw fa-instagram"></span>
                            </a>
                        @endif
                        @if($youtubeUsername = Setting::get('social-youtube-username'))
                            <a class="social-media --yt" href="https://www.youtube.com/channel/{{ $youtubeUsername }}" target="blank">
                                <span class="fa fa-fw fa-youtube"></span>
                            </a>
                        @endif
                    </div>

                    <a class="text-orange" href="{{ route('frontend.social-wall.index') }}">
                        {{ trans('footer.social-wall.link-title') }}
                    </a>
                </ul>
            </div>
        </div>
    </div>
    <div class="copyrights">
        <div class="container">
            {!! Setting::get('copyright-footer') !!}
        </div>
    </div>
</footer> -->


<footer class="footer footer-area" id="footer">
    <div class="mobile-hide">
        <img src="{{ asset(Setting::get('bg-footer-desktop', 'assets/img/footer.png')) }}">
    </div>
    <div class="mobile-view">
        <img src="{{ asset(Setting::get('bg-footer-mobile', 'assets/img/mobile/footer-bg.png')) }}" height="250px">
    </div>
    <div class="container has-border">
        <div class="footer-sections">
            <div class="footer-section mobile-hide">
                <div style="color:#fff;font-size:40px;font-weight: bold;margin-top: 15%;font-family: LasiverBlack">Get in touch with us</div>
                <div style="color:#fff;font-size:20px;font-weight: normal;margin-top: 0%;font-family: LasiverMedium">
                    Subscribe for updated promo, news and events</div>
            </div>

            <div class="footer-section mobile-view">
                <div style="color:#fff;font-size:20px;font-weight: bold;margin-top: -9%;text-align: center;font-family: LasiverBlack">Get in touch with us</div>
                <div style="color:#fff;font-size:12px;font-weight: normal;margin-top: 2%;text-align: center;font-family: LasiverMedium">
                Subscribe for updated promo, news and events </div>
            </div>
           
            <div class="footer-section mobile-hide">
                <form class="footer-subscribe" method="post">
                <input name="_token" type="hidden" value="{!! csrf_token() !!}" />    
                <div style="margin-top: 15%;">
                    <div style="position: absolute;top:8.3em;margin-left: 2%;">
                        <img src="{{ asset('assets/img/mail.png') }}"  alt="" style="width:29px">
                    </div>
                    <div style="    position: absolute;
                        top: 5.3em;
                        margin-left: 6%;
                        color: #5C2D93;
                        font-size: 20px;
                        border: none; ">
                        <input id="email" name="email" class="form-input block-half email_input" id="inputEmailSubscribe" type="text" value="{{ trans('footer.subscribe.email-placeholder') }}" style="border:none;padding-top: 3%;color: #bbb" onfocus="if(!this.hasText)
         {this.style.color='black'; 
         this.value='';}" 
     onblur="if(!this.value)
         {this.style.color='#bbb'; 
         this.value='{{ trans('footer.subscribe.email-placeholder') }}'; 
         this.hasText=false;}
         else{this.hasText=true};" >
                    </div>
                    <div class="">
                        <img src="{{ asset('assets/img/white_button.png') }}"  alt="" style="width:371px;height:45px">
                    </div>
                    <div style="position: absolute;
                    top: 7.5em;
                    width: 220px;margin-left: 27%;background: transparent;cursor: pointer;">
                        <img src="{{ asset('assets/img/green_button.png') }}" style="width:140px" alt="">

                        <!-- <button class="btn btn--block btn--primary">
                            <span class="fa fa-fw fa-envelope-o"></span>
                            {{ trans('footer.subscribe.btn-title') }}
                        </button> -->

                    </div>

                    <div  style="position: absolute;
                    width: 20%;
                    background: transparent;
                    top: 3.4em;
                    color: #fff;
                    font-weight: bold;
                    margin-left: 23.5%;
                    font-size: 30px;">
                        
                        <button class="btn btn--block" style="background: transparent;font-size: 20px;color: #fff;font-weight: bold">
                            {{ trans('footer.subscribe.btn-title') }}
                        </button>

                    </div>
                    <div  style="position: absolute;
                    width: 25%;
                    background: transparent;
                    top: 8em;
                    color: #fff;
                    font-weight: bold;
                    margin-left: 13%;
                    font-size: 20px;
                    color: #fff;">
                        
                        <span class="msg-error" style="color: #fff">{{ session('email') }}</span>

                    </div>

                    
                </div>
                </form>
                <!-- 
                    <input class="form-input block-half" id="inputEmailSubscribe" type="text" placeholder="Email anda">
                    <button class="btn btn--block btn--primary">
                        <span class="fa fa-fw fa-envelope-o"></span>
                        Subscribe
                    </button>
                </form> -->

                <!-- <div class="footer-section">
                <form class="footer-subscribe" action="{{ route('frontend.subscribe') }}" method="post">
                    <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                    <h4 class="footer-heading"><label for="inputEmailSubscribe">Subscribe Newsletter</label></h4>
                    <input id="email" name="email" class="form-input block-half" id="inputEmailSubscribe" type="text" placeholder="{{ trans('footer.subscribe.email-placeholder') }}">
                    <span class="msg-error">{{ session('email') }}</span>
                    <button class="btn btn--block btn--primary">
                        <span class="fa fa-fw fa-envelope-o"></span>
                        {{ trans('footer.subscribe.btn-title') }}
                    </button>
                </form>
                </div> -->

            </div>

            <div class="footer-section mobile-view">
                <form class="footer-subscribe" method="post">
                <input name="_token" type="hidden" value="{!! csrf_token() !!}" />

                <div class="footer_container_subscribe">
                    <div class="footer_mail_subscribe">
                        <img src="{{ asset('assets/img/mobile/mail.png') }}"  alt="" width="25px">
                    </div>
                    <div class="footer_textbox_subscribe">
                        <input id="email" name="email" class="form-input block-half email_input" id="inputEmailSubscribe" type="text" value="{{ trans('footer.subscribe.email-placeholder') }}" style="border:none;padding-top: 1%;color: #bbb" onfocus="if(!this.hasText)
         {this.style.color='black'; 
         this.value='';}" 
     onblur="if(!this.value)
         {this.style.color='#bbb'; 
         this.value='{{ trans('footer.subscribe.email-placeholder') }}'; 
         this.hasText=false;}
         else{this.hasText=true};">
                    </div>
                    <div style="">
                        <img src="{{ asset('assets/img/white_button.png') }}"  alt="">
                    </div>
                    <div class="footer_green_button">
                        <img src="{{ asset('assets/img/green_button.png') }}" alt="" height="33px">
                    </div>

                    <div class="footer_subscribe">
                        Subscribe
                    </div>
                </div>
                </form>
                <!-- 
                    <input class="form-input block-half" id="inputEmailSubscribe" type="text" placeholder="Email anda">
                    <button class="btn btn--block btn--primary">
                        <span class="fa fa-fw fa-envelope-o"></span>
                        Subscribe
                    </button>
                </form> -->
            </div>

             <div class="footer-section mobile-view" >
                <div style="color:#fff;font-size:14px;font-weight: bold;margin-top: -3%;text-align: center;">
                    <a  href="{{ Setting::get('page_faq', '') }}">
                        <span style="color: #fff;font-weight: bold;font-size: 15px;margin-bottom: 2%;">FAQ</span>
                    </a>
                </div>
                <div style="color:#fff;font-size:14px;font-weight: bold;margin-top: 0%;text-align: center;">
                    <a  href="{{ Setting::get('page_contact', '') }}">
                        <span style="color: #fff;font-weight: bold;font-size: 15px;margin-bottom: 2%;">Contact Us</span>
                    </a>
                </div>

                <div style="color:#fff;font-size:14px;font-weight: bold;margin-top: 0%;text-align: center;">
                    <a  href="https://www.karirkawanlama.com/" target="_blank">
                        <span style="color: #fff;font-weight: bold;font-size: 15px;margin-bottom: 2%;">Career</span>
                    </a>
                </div>

                <div class="social-medias" style="margin-top: 0%;margin-bottom: 2%;padding-bottom: 2%;">
                
                    <a class="social-media --fb" href="https://www.facebook.com/{{ $fbUsername }}" target="blank">
                        <img src="{{ asset('assets/img/mobile/facebook.png') }}" width="20px" alt="">
                    </a>
                    <a class="social-media --tw" href="https://twitter.com/{{ $twitterUsername }}" target="blank">
                        <img src="{{ asset('assets/img/mobile/twitter.png') }}" width="20px" alt="">
                    </a>
                    <a class="social-media --ig" href="https://www.instagram.com/{{ $igUsername }}" target="blank">
                        <img src="{{ asset('assets/img/mobile/instagram.png') }}" width="20px" alt="">
                </a>
                
                </div>

            </div>

            <!-- <div class="footer-section">
                <h4 class="footer-heading">Social media</h4>

                <div class="social-medias">
                    <a class="social-media --fb" href="#">
                        <span class="fa fa-fw fa-facebook-official"></span>
                    </a>
                    <a class="social-media --tw" href="#">
                        <span class="fa fa-fw fa-twitter"></span>
                    </a>
                    <a class="social-media --ig" href="#">
                        <span class="fa fa-fw fa-instagram"></span>
                    </a>
                    <a class="social-media --gp" href="#">
                        <span class="fa fa-fw fa-google-plus"></span>
                    </a>
                    <a class="social-media --yt" href="#">
                        <span class="fa fa-fw fa-youtube-play"></span>
                    </a>
                </div>

                <a class="text-orange" href="social-wall.php">[Check our story]</a>
            </div> -->
        </div>
    </div>
    <div class="container mobile-hide">
        <div class="footer-section" style="width: 50%;float: left;">
            <div class="social-medias" style="margin-top: 2%;margin-bottom: 1%">
                
                <a class="social-media --fb" id="sc_fb_desktop" href="https://www.facebook.com/{{ $fbUsername }}" target="blank">
                    <img class="fb_desktop" data-alt-src="{{ asset('assets/img/facebook_pnk.png') }}" src="{{ asset('assets/img/facebook.png') }}" width="25px" alt="">
                </a>

               
                <a class="social-media --tw" href="https://twitter.com/{{ $twitterUsername }}" id="sc_tw_desktop" target="blank">
                    <img class="tw_desktop" data-alt-src="{{ asset('assets/img/twitter_pnk.png') }}" src="{{ asset('assets/img/twitter.png') }}" width="25px" alt="">
                </a>

                

                <a class="social-media --ig" href="https://www.instagram.com/{{ $igUsername }}" id="sc_int_desktop" target="blank">
                    <img class="int_desktop" data-alt-src="{{ asset('assets/img/instagram_pnk.png') }}" src="{{ asset('assets/img/instagram.png') }}" width="25px" alt="">
                </a>

               
                
            </div>
        </div>
        <div class="footer-section" style="width: 50%;float: right;text-align: right;padding-right: 25px;">

            <div class="social-medias" style="margin-top: 2%;margin-bottom: 1%">
                <a class="social-media --fb" href="https://www.karirkawanlama.com/" target="_blank">
                    <span style="font-weight: bold;font-size: 15px"><b>Career</b></span>
                </a>
                <a class="social-media --fb" href="{{ Setting::get('page_contact', '') }}">
                    <span style="font-weight: bold;font-size: 15px"><b>Contact Us</b></span>
                </a>
                <a class="social-media --tw" href="{{ Setting::get('page_faq', '') }}">
                    <span style="font-weight: bold;font-size: 15px"><b>FAQ</b></span>
                </a>
                
            </div>

        </div>
    </div>

</footer>

