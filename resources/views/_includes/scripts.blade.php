    <?php
    $stat_modal=0;
    if(Session::get('SUBSCRIBE_SUCCESS')){
        $stat_modal=1;
        ?>
       

        <div class="modal is-active" id="modalSubscribe">
            <div class="modal-dialog">
                <button class="modal-dialog-close" data-dismiss="modal">Close &times;</button>
                <div class="text-center">
                    {!! Session::get('SUBSCRIBE_SUCCESS') !!}
                </div>
            </div>
        </div>


        <?php
    }
    ?>
    <!-- <div class="modal {{ Session::get('SUBSCRIBE_SUCCESS') ? 'is-active' : '' }}" id="modalSubscribe">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>
            <div class="text-center">
                {!! Session::get('SUBSCRIBE_SUCCESS') !!}
            </div>
        </div>
    </div> -->

    <div class="modal is-active" id="modal_loading">
        <center><img src="/assets/img/ajax-loading.gif" alt=""></center>
    </div>

    <div class="modal is-active" id="modalSubscribe2">
            <div class="modal-dialog">
                <!-- <button class="modal-dialog-close" data-dismiss="modal">Close &times;</button> -->
                <div class="text-center" id="tx_message">
                    
                </div>
            </div>
        </div>

    <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=default,promise,fetch" defer></script>
    <script src="{{ asset('assets/js/vendor/modernizr.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/vendor/jquery.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/bootstrap.js') }}" defer></script>

    <script src="{{ asset('assets/js/vendor/object-fit-images.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/vendor/slick.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/vendor/sprintf.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/vendor/baze.validate.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/vendor/jquery.fancybox.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/vendor/angular.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/vendor/awesomplete.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/vendor/pgw-browser.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/vendor/datepicker.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/vendor/jquery.datatables.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/vendor/hunt.min.js') }}" defer></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY', 'AIzaSyBYb5C-35s0hW5oh1D2qVRzMFW5nQXlyT8') }}" defer></script>
    <script>
        window.mapMarkerUrl = "{{ asset('assets/img/marker.png') }}"
    </script>
    <script src="{{ asset('assets/js/main.min.js') }}" defer></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>

       const sleep = (milliseconds) => {
          return new Promise(resolve => setTimeout(resolve, milliseconds))
        }

       function klik_category(id){
          event.preventDefault();
      
          $('html, body').animate({
            scrollTop: $("#panel_"+id).offset().top
          }, 1000);


        }

        function load_filter_news(slug){

            $('#modal_loading').modal('show');
            var url='/news/category/'+slug
            $('#slug_category').val(slug);
            $.ajax(
            {

                url: url,

                type: 'GET',

            }).done(

                function(data)

                {
                    //$('#loader_submit_merchant').hide();
                    $('.container_event2').html('');
                    $('.container_event2').html(data.html);
                    $('#modal_loading').modal('hide');
                }

            );  
        }

        function load_filter_news_sort(sort){

            $('#modal_loading').modal('show');
            var sort_val=$(sort).val();
            var slug_category=$('#slug_category').val();
            if(slug_category==''){
                slug_category='all';
            }
            var url='/news/category/'+slug_category+'/'+sort_val;

            $.ajax(
            {

                url: url,

                type: 'GET',

            }).done(

                function(data)

                {
                    //$('#loader_submit_merchant').hide();
                    $('.container_event2').html('');
                    $('.container_event2').html(data.html);
                    $('#modal_loading').modal('hide');
                }

            );  
        }

        function load_filter_product(slug){
            $('#modal_loading').modal('show');
            var url='/product/'+slug
            $.ajax(
            {

                url: url,

                type: 'GET',

            }).done(

                function(data)

                {
                    //$('#loader_submit_merchant').hide();
                    $('.list_product').html('');
                    $('.list_product').html(data.html);
                    $('#modal_loading').modal('hide');
                }

            );  
        }

        function load_filter_promotions(slug,mobile){
            var slug_val=$(slug).val();

            $('#modal_loading').modal('show');
            var url='/promotions/'+slug_val+'/'+mobile
            $.ajax(
            {

                url: url,

                type: 'GET',

            }).done(

                function(data)

                {
                    //$('#loader_submit_merchant').hide();
                    $('.list_promo').html('');
                    $('.list_promo').html(data.html);
                    $('#modal_loading').modal('hide');
                }

            );  
        }

        var sourceSwap = function () {
            var $this = $(this);
            var newSource = $this.data('alt-src');
            $this.data('alt-src', $this.attr('src'));
            $this.attr('src', newSource);
        }

        $('img.fb_desktop').hover(sourceSwap, sourceSwap);
        $('img.tw_desktop').hover(sourceSwap, sourceSwap);
        $('img.int_desktop').hover(sourceSwap, sourceSwap);
        

        
        


        $(".footer-subscribe").on('submit',(function(e) {
            e.preventDefault();

            $('#modal_loading').modal('show');
            $.ajax({
                url:'/subscribe',
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                //data: {_token: CSRF_TOKEN,sites_name: sites_name},
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                dataType: "json",
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(data)   // A function to be called if request succeeds
                {

                    $('#tx_message').html(data.message);
                    $('#modal_loading').modal('hide');
                    $('#modalSubscribe2').modal('show');
                    $('.email_input').val('');
                    sleep(2000).then(() => {
                      $('#modalSubscribe2').modal('hide')
                    });

                }
            });


        }));

        

        function close_modal(){
            console.log('test');

            $('#modalSubscribe2').modal('hide');
            
            location.reload(true);
        }
        var stat_modal='<?php echo $stat_modal;?>';
        $( document ).ready(function() {
            
            if(stat_modal==1){
                $('#modalSubscribe').modal('show');
                
                sleep(3000).then(() => {
                  console.log('akhirnya euy');
                  $('#modalSubscribe').modal('hide')
                });

            }

/*            $('#modalSubscribe2').modal('show');

            sleep(2000).then(() => {
              console.log('akhirnya euy');
              $('#modalSubscribe2').modal('hide')
            });
*/

            $("#image-holder").hide();
            $("iframe").width("100%");
            $("iframe").height("414");
        });
        $("#inputAvatar").on('change', function () {
            if (typeof (FileReader) != "undefined") {
                $('#previous-image-holder').hide();
                var image_holder = $("#image-holder");
                image_holder.show();
                image_holder.empty();

                var reader = new FileReader();
                reader.onload = function (e) {
                    $("<img />", {
                        "src": e.target.result,
                        "class": "img-rounded",
                        "width":'120px',
                        "height": "120px"
                    }).appendTo(image_holder);

                }
                image_holder.show();
                reader.readAsDataURL($(this)[0].files[0]);
            } else {
                alert("This browser does not support FileReader.");
            }
        });
        $("#autoRenewal").on('change', function () {
            if ($('#autoRenewal').is(':checked')) {
                $( "#renewalYesStatus" ).addClass("is-active");
                $( "#messageYesTxt" ).html("<a class='btn btn--primary' href='{{ route('frontend.auto-renewal.yes', 1) }}'>Yes</a>&nbsp;<a class='btn btn--outline btn--primary' href='{{ route('frontend.auto-renewal.no', $val=0) }}'>No</a>");
            } else {
                $( "#renewalNOStatus" ).addClass("is-active");
                $( "#messageNOTxt" ).html("<a class='btn btn--primary' href='{{ route('frontend.auto-renewal.no', $val=0) }}'>Yes</a>&nbsp;<a class='btn btn--outline btn--primary' href='{{ route('frontend.auto-renewal.yes', 1) }}'>No</a>");
            }
        });
        $('#renewalYes, #renewalNo').on('change', (e) => {
            if ( $('#renewalYes').is(':checked') ) {
                $('#modalConfirmRenewalYes').addClass('is-active')
            }
        })
        $('#selectCategoryMerchant, #selectLocationMerchant').on('change', (e) => {
            var baseUrl = window.location.protocol + "//" + window.location.host + window.location.pathname
            var category = $('#selectCategoryMerchant option:selected').val()
            var location = $('#selectLocationMerchant option:selected').val()
            additionalCategoryUrl = ''
            if (category != "0" && location != "0") {
                document.location.href = baseUrl + '?category=' + category + '&location=' + location
            } else if (category != "0") {
                document.location.href = baseUrl + '?category=' + category
            }  else if (location != "0") {
                document.location.href = baseUrl + '?location=' + location
            } else {
                document.location.href = baseUrl
            }
        })

        

    </script>
    @if (env('APP_ENV') == 'production')
        {!! Setting::get('footer-script') !!}
    @endif
</body>
</html>
