<header class="site-header">
    <div class="container">
        <button class="site-header-nav-trigger">
            <span class="fa fa-fw fa-bars"></span>
        </button>
        <!-- <button class="site-header-search-trigger js-search-trigger">
            <span class="fa fa-fw"></span>
        </button> -->

        <div class="site-header-tops">
            <a class="site-logo" href="/">
                <img src="/assets/img/logo-ace-hardware.png" alt="">
            </a>
            <div class="site-header-supports">
                
                <?php if($menu_selected!='about'){?>
                <a class="site-header-support" href="/aboutus" style="font-family: LasiverBold">
                    ABOUT US
                </a>
                <?php }else{?>
                <a class="site-header-support" href="/aboutus">
                    <div class="menu_selected2">ABOUT US</div>
                </a>    
                <?php }?>
                
                <?php if($menu_selected!='menu'){?>
                <a class="site-header-support" href="/product">
                    MENU
                </a>
                <?php }else{?>
                <a class="site-header-support" href="/product">
                    <div class="menu_selected2">MENU</div>
                </a>    
                <?php }?>
                
                <?php if($menu_selected!='promo'){?>
                <a class="site-header-support" href="/promotions">
                    PROMO
                </a>
                <?php }else{?>
                <a class="site-header-support" href="/promotions">
                    <div class="menu_selected2">PROMO</div>
                </a>    
                <?php }?>

                <?php if($menu_selected!='news'){?>
                <a class="site-header-support" href="/news">
                    NEWS
                </a>
                <?php }else{?>
                <a class="site-header-support" href="/news">
                    <div class="menu_selected2">NEWS</div>
                </a>    
                <?php }?>


                <!-- <a class="site-header-support" href="#" onclick="klik_category(2);" style="text-decoration: none;color: #fff">
                    SPEACIAL TEA
                </a> -->

                <?php if($menu_selected!='store'){?>
                <a class="site-header-support" href="/store-locations">
                    STORE LOCATION
                </a>
                <?php }else{?>
                <a class="site-header-support" href="store-locations">
                    <div class="menu_selected2">STORE LOCATION</div>
                </a>
                <?php }?>
    
                <!-- <a class="site-header-support js-app-download" href="#" onclick="klik_category(1);">
                    ATEALIER
                </a> -->
                
                <a class="site-header-support2" href="https://app.chatime.co.id" target="_blank" style="text-decoration: none;color: #fff">
                    <div class="menu_selected">MOBILE APP</div>
                </a>
                
                
                
                
            </div>
        </div>

        <div class="nav mobile-view">
            <button class="nav-close-btn">&times;</button>
            <a class="categories-trigger" href="#">
                <!-- <span class="fa fa-fw fa-plus-circle"></span> -->
                &nbsp;
            </a>
            <ul class="nav-list list-nostyle">
                <li class="nav-list-item">
                    <a class="nav-anchor" href="#"><span class="fa fa-fw fa-shopping-cart"></span></a>
                </li>
                <li class="nav-list-item" style="font-family: LasiverBlack">
                    <a class="nav-anchor" href="/aboutus">About Us</a>
                </li>
                <li class="nav-list-item" style="font-family: LasiverBlack">
                    <a class="nav-anchor" href="/product">Menu</a>
                </li>
                <li class="nav-list-item" style="font-family: LasiverBlack">
                    <a class="nav-anchor" href="/promotions">Promo</a>
                </li>
                <li class="nav-list-item" style="font-family: LasiverBlack">
                    <a class="nav-anchor" href="/news">News</a>
                </li>
                <!-- <li class="nav-list-item">
                    <a class="nav-anchor" href="#">Special Tea</a>
                </li> -->
                <li class="nav-list-item" style="font-family: LasiverBlack">
                    <a class="nav-anchor" href="/store-locations">Store Location</a>
                </li>
                <!-- <li class="nav-list-item">
                    <a class="nav-anchor" href="#">Atealier</a>
                </li> -->
                
                <li class="nav-list-item menu_selected" style="font-family: LasiverBlack;color:#fff;border-radius: 0px;
                    padding-top: 2%;
                    padding-bottom: 2%;
                        ">
                    <a class="nav-anchor" href="https://app.chatime.co.id" target="_blank">Mobile App</a>
                </li>
                
            </ul>
        </div>

        <?php $categories = [
            ['Home and Living', 'icon-category-home-living.png'],
            ['Furniture', 'icon-category-furniture.png'],
            ['Home Improvement', 'icon-category-home-improvement.png'],
            ['Hobbies and Lifestyle', 'icon-category-hobby-lifestyle.png'],
            ['Electronic and Gadget', 'icon-category-electronic.png'],
            ['Kitchen', 'icon-category-kitchen.png'],
            ['Bed and Bath', 'icon-category-bed-bath.png'],
            ['Automotive', 'icon-category-automotive.png'],
            ['Health and Sport', 'icon-category-health-sport.png'],
            ['Toy and Baby', 'icon-category-toy.png'],
            ['Holiday Gift', 'icon-category-gift.png'],
            ['Best Deal', 'icon-category-deal.png'],
        ] ?>

        <?php $subcategories = [
            'DEKORASI RUMAH',
            'PERLENGKAPAN MENCUCI DAN MENYETERIKA',
            'DEKORASI FESTIVAL',
            'TEMPAT PENYIMPANAN',
            'TEMPAT SAMPAH',
            'PEMBERSIH',
            'TEKSTIL'
        ] ?>

        <div class="category-dropdown">
            <ul class="category-list list-nostyle">
                <?php foreach ($categories as $key => $value) { ?>
                <li class="category-list-item">
                    <a class="category-list-anchor category-list-anchor--level-1" href="product.php">
                        <img src="assets/img/<?= $value[1] ?>" alt="">
                        <span><?= $value[0] ?></span>
                    </a>

                    <div class="category-list category-list--level-2">
                        <ul class="list-nostyle">
                            <?php foreach ($subcategories as $keySub => $valueSub) { ?>
                            <li class="category-list-item">
                                <a class="category-list-anchor category-list-anchor--level-2" href="product.php"><?= $valueSub ?></a>

                                <div>
                                    <ul class="category-level-3 list-nostyle">
                                        <li class="category-list-item">
                                            <a class="category-list-anchor" href="product.php">Wall Decoration</a>
                                        </li>
                                        <li class="category-list-item">
                                            <a class="category-list-anchor" href="product.php">Desk Decoration</a>
                                        </li>
                                        <li class="category-list-item">
                                            <a class="category-list-anchor" href="product.php">Watch</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>

                        <img class="category-list-img" src="assets/img/category-bg.jpg" alt="">
                    </div>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</header>
