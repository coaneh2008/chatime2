<div class="container" style="margin-top: 2%;margin-bottom: 2%;font-family: LasiverMedium">
    <div class="col-xs-12" style="padding-right:0px">
        <div class="row">

            @foreach ($list_category as $cat)
            <div onclick="load_filter_news('<?php echo $cat->slug;?>')" style="cursor:pointer;width: auto;border-radius: 10px;float: left;padding-top: 2%;padding-bottom: 2%;
            padding-left: 2%;padding-right: 2%;margin-right: 1%;font-size: 15px;color: #fff;
            background-image:url('{{ asset($cat->banner_image) }}');margin-top:2%;margin-bottom:2%;">
                {{ $cat->title }}
            </div>
            
             @endforeach

        </div>
    </div>
    <div class="col-xs-12" style="padding-left: 0px;">
        <div class="col-xs-4" style="padding-top: 3%;padding-bottom: 3%;padding-left: 0px">
            <span style="color: #5C2D91;font-size: 15px;"><b>Urutkan:</b></span>
        </div>
        <div class="col-xs-8 selectdiv" style="background-color: #5C2D91;color: #fff;border-radius: 20px;font-size: 15px">
            <select class="form-control category_promo" style="background-color:#5C2D91;border:none;color: #fff;-webkit-box-shadow:none" onchange="load_filter_news_sort(this)">
                
                <option value="1"><span style="font-family: LasiverMedium">Terbaru</span></option>
                <option value="2"><span style="font-family: LasiverMedium">Terpopuler</span></option>
                    
            </select>
        </div>
    </div>
</div>

<div class="container container_katering_service container_event2" style="font-family: LasiverMedium">
    
    @foreach ($list_event as $banner)

        <div class="col-xs-6 container_list_promo">
        <div class="thumbnail_promo" style="cursor: pointer;">
            <a href="{{ route('frontend.new.show', $banner->slug) }}">
                <img src="{{ asset($banner->banner_image) }}" alt="{{ $banner->title }}">
            </a>
        </div>
        <div class="col-xs-12" style="color:#30BD9C;padding-top: 5%;padding-bottom: 5%;text-align: left;font-size: 12px;;min-height: 100px;max-height: 100px;" >
            
            <a href="{{ route('frontend.new.show', $banner->slug) }}" style="text-decoration: none">
                <span style="color:#30BD9C"><b>{{ $banner->title }}</b></span>
            </a>

        </div>

        <div class="col-xs-12" style="color:#30BD9C;padding-top: 5%;padding-bottom: 5%;text-align: left;">
            
            <a href="{{ route('frontend.new.show', $banner->slug) }}" style="text-decoration: none">
                <span style="color:#30BD9C"><b>News. {{ date('d-m-Y',strtotime($banner->created_at)) }}</b></span>
            </a>

        </div>

    </div>

    @endforeach

    

</div>

 <link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick-theme.css') }}"/>

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/js/slick/slick.min.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/slack_custom.css') }}"/>


<script type="text/javascript">
    
    $('.multiple-items').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000
    });

    /*$('.multiple-items').slick();*/

    /*$('.carousel_group').flickity({
            // options
            groupCells: true,
            cellAlign: "left",
            pageDots: false,
            wrapAround: true,
            imagesLoaded: true
    });*/
</script>