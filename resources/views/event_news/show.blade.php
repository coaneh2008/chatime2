@include('_includes.head')
    <!-- @include('home.flash-message') -->
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <div class="mobile-hide">
                <main class="site-main">
                    <div class="container" style="margin-top: 2%">
                        <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}"><span style="color:#5C2D93;font-weight: bold;">{{ trans('home.home.title') }}</span></a></li>
                        <li><a href="/news"><span style="color:#5C2D93;font-weight: bold;">News</span></a></li>
                        <li>{{ $article->title }}</a></li>
                        </ul>

                        <h1 class="text-caps" style="font-family: LasiverBlack;color:#5C2D93 ">{{ $article->title }}</h1>
                        <time>
                            {{ Carbon\Carbon::parse($article->published_date ? $article->published_date : $article->published_at)->format('d F Y') }}
                        </time>

                        <figure>
                            <!-- <img class="full-width" src="{{ $article->getThumbnail('banner_image', 'huge') }}" alt="{{ $article->title }}"> -->

                             <img src="{{ asset($article->banner_image) }}" alt="">

                        </figure>

                        <span style="font-family: LasiverMedium">
                            {!! $article->content !!}
                        

                        @include('_includes.back')
                        </span>

                    </div>
                    
                </main>
            </div>

            <div class="mobile-view">
                <main class="site-main">
                    <div class="container" style="margin-top: 2%">
                        
                        <h1 class="text-caps">{{ $article->title }}</h1>
                        <time>
                            {{ Carbon\Carbon::parse($article->published_date ? $article->published_date : $article->published_at)->format('d F Y') }}
                        </time>

                        <figure>
                            <!-- <img class="full-width" src="{{ $article->getThumbnail('banner_image', 'huge') }}" alt="{{ $article->title }}"> -->

                            <img src="{{ asset($article->banner_image) }}" alt="">
                        </figure>

                        
                        {!! $article->content !!}
                        @include('_includes.back')
                    </div>
                    
                </main>
            </div>

            
            

        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
