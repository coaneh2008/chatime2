
@foreach ($list_event as $banner)

    <div class="col-lg-4 col-xs-6 container_list_promo" style="font-family: LasiverMedium">
        <div class="thumbnail_promo" style="cursor: pointer;">
            <a href="{{ route('frontend.new.show', $banner->slug) }}">
                <img src="{{ asset($banner->banner_image) }}" alt="{{ $banner->title }}">
            </a>
        </div>
        <div class="col-lg-12 col-xs-12" style="color:#30BD9C;padding-top: 5%;padding-bottom: 5%;text-align: left;">
            <a href="{{ route('frontend.new.show', $banner->slug) }}" style="text-decoration: none">
                <span style="color:#30BD9C"><b>{{ $banner->title }}</b></span>
            </a>
        </div>

        <div class="col-lg-12 col-xs-12" style="color:#30BD9C;padding-top: 5%;padding-bottom: 5%;text-align: left;">
            <a href="{{ route('frontend.new.show', $banner->slug) }}" style="text-decoration: none">
                <span style="color:#30BD9C"><b>News. {{ date('d-m-Y',strtotime($banner->created_at)) }}</b></span>
            </a>
        </div>

    </div>
@endforeach


