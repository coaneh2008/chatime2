<div class="container container_event" style="margin-top: 2%;margin-bottom: 2%;font-family: LasiverMedium">
    <div class="col-lg-8 padding_kategori_event" style="padding-right:0px">
        <div class="row">
            @foreach ($list_category as $cat)
            <div onclick="load_filter_news('<?php echo $cat->slug;?>')" style="cursor:pointer;width: auto;border-radius: 10px;float: left;padding-top: 1%;padding-bottom: 1%;
            padding-left: 2%;padding-right: 2%;margin-right: 1%;font-size: 20px;color: #fff;
            background-image:url('{{ asset($cat->banner_image) }}');">
                {{ $cat->title }}
            </div>
            @endforeach
            
        </div>
    </div>
    <div class="col-lg-4" style="padding-left: 0px;font-family: LasiverMedium">
        <div class="col-lg-4" style="padding-top: 3%;padding-bottom: 3%;padding-left: 0px">
            <span style="color: #5C2D91;font-size: 18px;"><b>Urutkan:</b></span>
        </div>
        <div class="col-lg-8 selectdiv" style="background-color: #5C2D91;color: #fff;border-radius: 20px;">
            <select class="form-control category_promo" style="background-color:#5C2D91;border:none;color: #fff;-webkit-box-shadow:none;font-size: 19px;font-family: LasiverMedium" onchange="load_filter_news_sort(this)">
                
                <option value="1"><span style="font-family: LasiverMedium">Terbaru</span></option>
                <option value="2"><span style="font-family: LasiverMedium">Terpopuler</span></option>
                    
            </select>

        </div>
    </div>
</div>
<div class="container container_event2" style="font-family: LasiverMedium">
    
    <!-- <div class="row">
        <div class="col-lg-8">
            <div class="row">
            <div style="width: auto;border-radius: 10px;float: left;padding-top: 1%;padding-bottom: 1%;
            padding-left: 2%;padding-right: 2%;margin-right: 1%;font-size: 20px;color: #fff;background-color:red">
                Gede Banget
            </div>
            </div>
        </div>
    </div> -->
    @foreach ($list_event as $banner)

        <div class="col-lg-4 container_list_promo">
            <div class="thumbnail_promo" style="cursor: pointer;">
                <a href="{{ route('frontend.new.show', $banner->slug) }}">
                    <img src="{{ asset($banner->banner_image) }}" alt="{{ $banner->title }}">
                </a>
            </div>
            <div class="col-lg-12" style="color:#30BD9C;padding-top: 5%;padding-bottom: 5%;text-align: left;
            min-height:90px;max-height: 90px;">
                <a href="{{ route('frontend.new.show', $banner->slug) }}" style="text-decoration: none">
                    <span style="color:#30BD9C"><b>{{ $banner->title }}</b></span>
                </a>
            </div>

            <div class="col-lg-12" style="color:#30BD9C;padding-top: 5%;padding-bottom: 5%;text-align: left;">
                <a href="{{ route('frontend.new.show', $banner->slug) }}" style="text-decoration: none">
                    <span style="color:#30BD9C"><b>News. {{ date('d-m-Y',strtotime($banner->created_at)) }}</b></span>
                </a>
            </div>

        </div>
    @endforeach

        
   


</div>

