@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>Promotion</li>
                    </ul>
                    <div class="promotion">
                        <img class="promotion-banner" src="{{ Setting::get('promotion-background-image') }}" alt="">

                        <div class="promotion-body">
                            <div class="container">
                                <div class="promotion-navs">
                                    @foreach($promotionCategories as $category)
                                        <a 
                                        class="promotion-nav {{ ($category->type == \App\Model\PromotionCategory::TYPE_FLASH_SALE) ? 'promotion-nav--popup' : ''}}"
                                        href="{{ ($category->type == \App\Model\PromotionCategory::TYPE_FLASH_SALE) ? '#' : '#contentPromo'. $category->type }}">
                                            {{ $category->title }}
                                        </a>
                                    @endforeach
                                </div>

                                <div class="promotion-content">
                                    <img class="promotion-banner-mobile" src="{{ Setting::get('promotion-background-image') }}" alt="">
                                    <div class="promotion-content-section" id="contentPromo{{ \App\Model\PromotionCategory::TYPE_HOLIDAY_PROMO }}">
                                        @if($holidayPromo)
                                            {!! $holidayPromo->content !!}
                                            @if($holidayPromo->file)
                                                <a class="promotion-btn block-half" href="{{ route('frontend.promotion-brochure.download', [$holidayPromo->id, \App\Model\PromotionCategory::TYPE_HOLIDAY_PROMO]) }}">
                                                    Download brochure
                                                </a>
                                            @endif
                                            @if($holidayPromo->type==0)
                                                <a class="promotion-btn block-half js-app-download" href="{{ Setting::get('download-app-default-url') }}" target="_blank" data-ios="{{ Setting::get('download-app-data-ios') }}" data-android="{{ Setting::get('download-app-data-android') }}">
                                                    Download App
                                                </a>
                                            @else
                                                <a class="promotion-btn block-half" href="{{ $holidayPromo->url }}" target="_blank">
                                                    {{ $holidayPromo->btn_name}}
                                                </a>
                                            @endif
                                        @else
                                            <p>No Promotion Available</p>
                                        @endif
                                    </div>

                                    <div class="promotion-content-section" id="contentPromo{{ \App\Model\PromotionCategory::TYPE_PROMO_INFO }}">
                                        @if($promoInfo)
                                            {!! $promoInfo->content !!}
                                            @if($promoInfo->file)
                                                <a class="promotion-btn block-half" href="{{ route('frontend.promotion-brochure.download', [$promoInfo->id, \App\Model\PromotionCategory::TYPE_PROMO_INFO]) }}">
                                                    Download brochure
                                                </a>
                                            @endif
                                            @if($promoInfo->type==0)
                                                <a class="promotion-btn block-half js-app-download" href="{{ Setting::get('download-app-default-url') }}" target="_blank" data-ios="{{ Setting::get('download-app-data-ios') }}" data-android="{{ Setting::get('download-app-data-android') }}">
                                                    Download App
                                                </a>
                                            @else
                                                <a class="promotion-btn block-half" href="{{ $promoInfo->url }}" target="_blank">
                                                    {{ $promoInfo->btn_name}}
                                                </a>
                                            @endif
                                        @else
                                             <p>No Promo Info Available </p>
                                        @endif
                                    </div>

                                    <div class="promotion-content-section" id="contentPromo{{ \App\Model\PromotionCategory::TYPE_PRODUCT_PROMO }}">
                                        <div class="promotion-product-slider slider-style">
                                            @if($productPromos)
                                                @foreach($productPromos as $productPromo)
                                                    <div>
                                                        <figure class="promotion-product">
                                                            <img src="{{ asset($productPromo->product->getThumbnail('image', 'medium')) }}" alt="">
                                                            <figcaption>
                                                                <!-- <span class="label label--red">Save {{ $productPromo->discount }}%</span> -->
                                                                <h2>{{ $productPromo->product->title }}</h2>
                                                                <div>
                                                                    Harga Setelah Diskon
                                                                    <h2>Rp {{ number_format($productPromo->product->price - (($productPromo->discount/100) * $productPromo->product->price), 0, ',', '.') }}</h2>
                                                                </div>

                                                                <a href="{{ $productPromo->product->url_to_ace_online }}" target="blank">
                                                                    <img src="{{ asset(Setting::get('product-promotion-btn-to-ruparupa-image')) }}" alt="">
                                                                </a>
                                                            </figcaption>
                                                        </figure>
                                                    </div>
                                                @endforeach
                                            @else
                                                 <p>No Member Product Promo Available </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>
    <div class="promotion-popup">
        <div class="promotion-popup-dialog">
            <div class="text-right">
                <button class="promotion-popup-btn">Close <span class="fa fa-fw fa-times-circle"></span></button>
            </div>
            <div>
                @if($flashSale)
                    @if($flashSale->url_to_ace_online)
                    <a href="{{ $flashSale->url_to_ace_online }}" target="_blank">
                        <img class="full-width" src="{{ asset($flashSale->getThumbnail('banner_image', 'huge')) }}" alt="">
                    </a>
                    @else
                        <img class="full-width" src="{{ asset($flashSale->getThumbnail('banner_image', 'huge')) }}" alt="">
                    @endif
                @else
                    <p>No Flash Sale Available </p>
                @endif
            </div>
        </div>
    </div>

@include('_includes.scripts')