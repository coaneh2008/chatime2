@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>{{ trans('home.featured-brand.title') }}</li>
                        <li>{{ $brand->title }}</li>
                    </ul>

                    <article>
                        <figure>
                            <img class="full-width" src="{{ asset($brand->getThumbnail('banner_image', 'huge')) }}" alt="{{ $brand->title }}">
                        </figure>

                        <div class="bzg">
                            <div class="bzg_c" data-col="m8" data-offset="m2">
                                {!! $brand->content !!}
                            </div>
                        </div>
                    </article>
                    <?php
                        $relatedProducts = \App\Model\FeaturedBrand::getFeaturedBrandAttachments($brand->id);
                    ?>
                    <br>
                    @if(count($relatedProducts)>0)
                        <section class="block">
                            <h2 class="home-section-heading">Related product</h2>

                            <div class="product-detail-related-products">
                                @foreach($relatedProducts as $articleAttachment)
                                    <div class="product-detail-related-products-item">
                                        <img src="{{ asset($articleAttachment->product->getThumbnail('image', 'medium-2')) }}" alt="">
                                        <p>{{ $articleAttachment->product->title }}</p>
                                        <a class="btn btn--block btn--primary" href="{{ $articleAttachment->product->url_to_ace_online }}"" target="_blank">Buy at ACE Online</a>
                                    </div>
                                @endforeach
                            </div>
                        </section>
                    @endif
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
