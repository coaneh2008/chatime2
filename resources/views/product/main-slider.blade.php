<style type="text/css">
    
</style>
<!-- <div class="main-slider slider-style slider-style--primary" data-slick='{
    "autoplay": true,
    "autoplaySpeed": 5000,
    "pauseOnHover": false
}'>
    @foreach ($banners2 as $banner)
        <div style="cursor: pointer;">
            <div style="position: absolute;top:20%;color: #fff;margin-left: 3%">
                <div class="row">
                    <div class="col-lg-7" style="font-size: 25px">
                        <h1><b>{{ $banner->judul }}</b></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6" style="font-size: 18px">
                        {{ $banner->deskripsi }}
                    </div>
                </div>
                
            </div>

            @if($banner->type != '')
                <a {{ $banner->type==1 ? 'data-fancybox' : '' }} href="{{ $banner->video_url }} {{ $banner->type==1 ? '&control=0&showinfo=0' : ''}}" target="_blank">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="">
                </a>
            @else
                <a href="{{ $banner->video_url }}">
                <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="">
                </a>
            @endif
        </div>
    @endforeach
</div> -->

<!-- <div class="multiple-items2">
    @foreach ($banners2 as $banner)
        <div style="cursor: pointer;width: 100%">
            <div style="position: absolute;top:20%;color: #fff;margin-left: 3%">
                <a href="{{ $banner->video_url }}">
                    <div class="row">
                        <div class="col-lg-10" style="font-size: 25px">
                            <h1><b>{{ $banner->judul }}</b></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6" style="font-size: 18px">
                            {{ $banner->deskripsi }}
                        </div>
                    </div>
                </a>
                
                <div class="row">
                    <div class="col-lg-4" style="margin-top: 2%">
                        <a href="{{ $banner->video_url }}">
                            <div style="color: #fff;font-size: 18px;border-radius: 20px;background-color: #5DD859;
                            padding-top: 6%;padding-bottom: 6%;font-weight: bold;text-align: center;">
                                More Info
                            </div>
                        </a>

                    </div>
                </div>

            </div>

            @if($banner->type != '')
                <a {{ $banner->type==1 ? 'data-fancybox' : '' }} href="{{ $banner->video_url }} {{ $banner->type==1 ? '&control=0&showinfo=0' : ''}}" target="_blank">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="">
                </a>
            @else
                <a href="{{ $banner->video_url }}">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="">
                </a>
            @endif
        </div>
    @endforeach
</div> -->
<div class="multiple-items2-product">
    @foreach ($banners2 as $banner)
        <img src="{{ asset($banner->image) }}" alt="">
    @endforeach
</div>


<div class="container">
    <div style="position: relative;margin-left: auto;margin-right: auto;margin-bottom: 2%;padding-left: 1%;margin-top: 2%;font-family: LasiverMedium">
        <div class="row">
        @foreach ($category as $banner)
            <div onclick="load_filter_product('<?php echo $banner->slug;?>')" style="cursor:pointer;width: auto;border-radius: 10px;float: left;padding-top: 1%;padding-bottom: 1%;
            padding-left: 2%;padding-right: 2%;margin-right: 1%;font-size: 20px;color: #fff;margin-top: 1%;
            background-image:url('{{ asset($banner->banner_image) }}');
            background-size: 100%;
            ">
                {{ $banner->title }}
            </div>
        @endforeach
        </div>
    </div>
    <div  style="position: relative;margin-left: auto;margin-right: auto;font-family: LasiverMedium">
            <div class="list_product" class="row">
                <?php if($productBig):?>
                <div class="col-lg-4 " style="margin-left: auto;margin-right: auto;position: relative;width: 40%;border-radius: 10px;;padding-left: 5px;padding-right: 5px">
                    <div class="col-lg-12" style="
                    background-image: url('{{ asset($productBig->bg) }}');
                    background-size: 120%;
                    border-radius: 20px;padding-top: 0%;"
                    >
                        <img src="{{ asset($productBig->banner_image) }}" alt="{{ $productBig->title }}">

                        <div style="color: #fff;padding-bottom: 1%;font-size: 25px">
                            <center><b><?php echo $productBig->title;?></b></center>
                        </div>
                        <div style="color: #fff;font-size: 18px;padding-bottom: 13%">
                            <center><?php echo $productBig->description;?></center>
                        </div>
                    </div>
                </div>
                <?php endif;?>
                 <?php
                  $i=0;
                  foreach ($productList as $row){
                    if($row->is_big==0){
                            if($row->color2==''){
                                $color2='ffffff';
                            }else{
                                $color2=$row->color2;
                            }
                        ?>
                        <div class="col-lg-2" style="margin-bottom: 1%;width: 20%;padding-left: 5px;padding-right: 5px">
                            <div class="col-lg-12" style="
                            background-image: url('{{ asset($row->bg) }}');
                            background-size: 120%;
                            border-radius: 20px;padding-top: 5%;padding-bottom: 2%;">
                                <img src="{{ asset($row->banner_image) }}" alt="{{ $row->title }}" height="184px">
                                <div style="color: #fff;padding-bottom: 1%;font-size: 20px;min-height: 90px">
                                    <center><b><?php echo $row->title;?></b></center>
                                </div>
                                
                            </div>
                        </div>
                    <?php }
                    $i++;
                  }
                 ?>

            </div>
            
    </div>
</div>

 <link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick-theme.css') }}"/>

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/js/slick/slick.min.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/slack_custom.css') }}"/>


<script type="text/javascript">
    
    $('.multiple-items').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000
    });

    /*$('.multiple-items2').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000
    });*/

    $('.multiple-items2-product').slick({
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      autoplay: true
    });

    /*$('.multiple-items').slick();*/

    /*$('.carousel_group').flickity({
            // options
            groupCells: true,
            cellAlign: "left",
            pageDots: false,
            wrapAround: true,
            imagesLoaded: true
    });*/
</script>