

<!-- <div class="multiple-items_mobile">
    @foreach ($banners2 as $banner)
       
        <div style="cursor: pointer;width: 100%;position: relative;">
            <div style="position: absolute;
  top: 30%;
  left: 50%;
  transform: translate(-50%, -50%);color: #fff;font-family: LasiverMedium">
                <a href="{{ $banner->video_url }}">
                <div class="row">
                    <div class="col-xs-12" style="font-size: 30px;text-align: left;">
                        <b>{{ $banner->judul }}</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="font-size: 20px;text-align: left;">
                        {{ $banner->deskripsi }}
                    </div>
                </div>
                </a>
                
            </div>
            @if($banner->type != '')
                <a {{ $banner->type==1 ? 'data-fancybox' : '' }} href="{{ $banner->video_url }} {{ $banner->type==1 ? '&control=0&showinfo=0' : ''}}" target="_blank">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="" width="100%">
                </a>
            @else
                <a href="{{ $banner->video_url }}">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="" width="100%">
                </a>
            @endif
        </div>

    @endforeach
</div> -->

<div class="multiple-items_mobile">
    @foreach ($banners2 as $banner)
        <img src="{{ asset($banner->image) }}" alt="">
    @endforeach
</div>

<div class="container">
    <div style="position: relative;margin-left: auto;margin-right: auto;margin-bottom: 5%;padding-left: 1%;margin-top: 5%">
        <div class="row">
        @foreach ($category as $banner)
        <div onclick="load_filter_product('<?php echo $banner->slug;?>')" style="margin-top:2%;cursor:pointer;width: auto;border-radius: 10px;float: left;padding-top: 2%;padding-bottom: 2%;
        padding-left: 5%;padding-right: 5%;margin-right: 1%;font-size: 15px;color: #fff;
        background-image: url('{{ asset($banner->banner_image) }}');
        background-size: 100%;font-family: LasiverMedium
        ">
            {{ $banner->title }}
        </div>
        @endforeach
        </div>
    </div>
    <div style="position: relative;margin-left: auto;margin-right: auto;font-family: LasiverMedium">
            <div class="list_product" class="row">

                <?php if($productBig):?>
                <div class="col-xs-12 " style="margin-left: auto;margin-right: auto;position: relative;width: 100%;border-radius: 10px;;padding-left: 5px;padding-right: 5px;margin-bottom: 2%;">
                            <div class="col-xs-12" style="background-image: url('{{ asset($productBig->bg) }}');
                            background-size: 120%;
                            border-radius: 10px;padding-top: 0%;">
                                <img src="{{ asset($productBig->banner_image) }}" alt="{{ $productBig->title }}">

                                <div style="color: #fff;padding-bottom: 1%;font-size: 25px">
                                    <center><b><?php echo $productBig->title;?></b></center>
                                </div>
                                <div style="color: #fff;font-size: 18px;padding-bottom: 13%">
                                    <center><?php echo $productBig->description;?></center>
                                </div>
                            </div>
                        </div>
                <?php endif;?>

                 <?php
                  $i=0;
                  foreach ($productList as $row){
                    if( $row->is_big==0){
                   
                        if($row->color2==''){
                            $color2='ffffff';
                        }else{
                            $color2=$row->color2;
                        }
                            
                        ?>
                        <div class="col-xs-4" style="margin-bottom: 1%;padding-left: 3px;padding-right: 3px">
                            <div class="col-xs-12" style="background-image: url('{{ asset($row->bg) }}');
                                background-size: 120%;
                                border-radius: 10px;padding-top: 5%;padding-bottom: 2%;">
                                <img src="{{ asset($row->banner_image) }}" alt="{{ $row->title }}">
                                <div style="color: #fff;padding-bottom: 1%;font-size: 10px;min-height: 50px">
                                    <center><b><?php echo $row->title;?></b></center>
                                </div>
                                
                            </div>
                        </div>
                    <?php }
                    $i++;
                  }
                 ?>

            </div>
            
    </div>
</div>

 <link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick-theme.css') }}"/>

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/js/slick/slick.min.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/slack_custom.css') }}"/>


<script type="text/javascript">
    
    $('.multiple-items_mobile').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000
    });

    $('.multiple-items').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000
    });

    /*$('.multiple-items').slick();*/

    /*$('.carousel_group').flickity({
            // options
            groupCells: true,
            cellAlign: "left",
            pageDots: false,
            wrapAround: true,
            imagesLoaded: true
    });*/
</script>