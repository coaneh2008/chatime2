 <div class="mobile-hide">
 <?php
  $i=0;
  foreach ($productList as $row){
    if($i==0){
        ?>
        <div class="col-lg-4  " style="margin-left: auto;margin-right: auto;position: relative;width: 40%;border-radius: 10px;;padding-left: 5px;padding-right: 5px">
            <div class="col-lg-12" style="
            background-image: url('{{ asset($row->bg) }}');
            background-size: 120%;
            border-radius: 20px;padding-top: 0%;font-family: LasiverMedium"
            >
                <img src="{{ asset($row->banner_image) }}" alt="{{ $row->title }}">

                <div style="color: #fff;padding-bottom: 1%;font-size: 25px">
                    <center><b><?php echo $row->title;?></b></center>
                </div>
                <div style="color: #fff;font-size: 18px;padding-bottom: 29%">
                    <center><?php echo $row->description;?></center>
                </div>
            </div>
        </div>
        <?php
    }else{
            if($row->color2==''){
                $color2='ffffff';
            }else{
                $color2=$row->color2;
            }
        ?>
        <div class="col-lg-2" style="margin-bottom: 1%;width: 20%;padding-left: 5px;padding-right: 5px">
            <div class="col-lg-12" style="
            background-image: url('{{ asset($row->bg) }}');
            background-size: 120%;
            border-radius: 20px;padding-top: 5%;padding-bottom: 2%;font-family: LasiverMedium">
                <img src="{{ asset($row->banner_image) }}" alt="{{ $row->title }}">
                <div style="color: #fff;padding-bottom: 1%;font-size: 20px;min-height: 90px">
                    <center><b><?php echo $row->title;?></b></center>
                </div>
                
            </div>
        </div>
    <?php }
    $i++;
  }
 ?>
</div>

 <div class="mobile-view">
  <?php
  $i=0;
  foreach ($productList as $row){
    if($i==0){
        ?>
        <div class="col-xs-12 " style="margin-left: auto;margin-right: auto;position: relative;width: 100%;border-radius: 10px;;padding-left: 5px;padding-right: 5px;margin-bottom: 2%;">
            <div class="col-xs-12" style="background-image: url('{{ asset($row->bg) }}');
            background-size: 120%;
            border-radius: 10px;padding-top: 0%;font-family: LasiverMedium">
                <img src="{{ asset($row->banner_image) }}" alt="{{ $row->title }}">

                <div style="color: #fff;padding-bottom: 1%;font-size: 25px">
                    <center><b><?php echo $row->title;?></b></center>
                </div>
                <div style="color: #fff;font-size: 18px;padding-bottom: 13%">
                    <center><?php echo $row->description;?></center>
                </div>
            </div>
        </div>
        <?php
    }else{
        if($row->color2==''){
            $color2='ffffff';
        }else{
            $color2=$row->color2;
        }
            
        ?>
        <div class="col-xs-4" style="margin-bottom: 1%;padding-left: 3px;padding-right: 3px">
            <div class="col-xs-12" style="background-image: url('{{ asset($row->bg) }}');
                background-size: 120%;
                border-radius: 10px;padding-top: 5%;padding-bottom: 2%;font-family: LasiverMedium">
                <img src="{{ asset($row->banner_image) }}" alt="{{ $row->title }}">
                <div style="color: #fff;padding-bottom: 1%;font-size: 10px;min-height: 50px">
                    <center><b><?php echo $row->title;?></b></center>
                </div>
                
            </div>
        </div>
    <?php }
    $i++;
  }
 ?>
</div>

            

