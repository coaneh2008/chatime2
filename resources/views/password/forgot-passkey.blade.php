@include('_includes.head')

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>{{ trans('membership.title') }}</li>
                    </ul>

                    <div class="bzg">
                        @include('_includes.flash')
                        <div class="bzg_c" data-col="l4" data-offset="l4">
                            <h1 class="h2 home-section-heading text-center">Recover passkey</h1>

                            <form class="js-validate" action="{{ route('frontend.membership.do-forgot-passkey') }}" method="post">
                                <p class="text-center">Enter your email to get your passkey</p>
                                <div class="block-half">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <label for="inputEmail">Email</label>
                                    <input class="form-input" id="inputEmail" name="email" type="email" required>
                                </div>
                                <button class="btn btn--block btn--primary mb-16">Forgot Passkey</button>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
