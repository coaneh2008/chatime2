@include('_includes.head')
    <!-- @include('home.flash-message') -->
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <div class="mobile-hide">
                <main class="site-main" style="padding-bottom: 0em;">
                    @include('home.aboutus-bg')
                    <div class="container">
                        
                    </div>
                    
                </main>
            </div>
            <div class="mobile-view">
                <main class="site-main" style="padding-bottom: 0em;">
                    @include('home.aboutus-bg-mobile')
                </main>
            </div>

            
            

        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
