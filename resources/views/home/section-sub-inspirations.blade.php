<div class="home-section">
    <!-- <h2 class="home-section-heading">Inspirations</h2> -->

    <div class="section-inspirations">
        <div class="section-sub-inspiration-big">
            <!-- <a href="inspiration-detail.php">
                <img src="{{ asset('assets/img/sosmed.png') }}" alt="">
            </a> -->

             @if(!empty($bigInspirationBanners[1]))
                <a href="{{ $bigInspirationBanners[1]->url }}">
                    <!-- <img src="{{ asset($bigInspirationBanners[0]->getThumbnail('banner_image', 'big_square')) }}" alt="{{ $bigInspirationBanners[0]->title }}"> -->

                    <img src="{{ asset($bigInspirationBanners[1]->banner_image) }}" alt="{{ $bigInspirationBanners[1]->title }}">
                    
                </a>
            @endif

        </div>
        
        <!-- <div class="section-sub-inspiration-big home-promotions-slider slider-style slider-style--primary">
            
            @foreach ($bigInspirationBannersSlider as $slider)
        
                <div>
                    <img src="{{ asset($slider->banner_image) }}" alt="{{ $slider->title }}">
                </div>

            @endforeach

            

        </div> -->

        <div class="section-sub-inspiration-big big_order_slider_sub">
            @foreach ($bigInspirationBannersSlider as $slider)
                <a href="{{ $slider->url }}">
                <div>
                    <div style="position: absolute;color: #fff;width: 30%;margin-left: 1%;margin-top: 1%">
                                        
                        <div class="row">
                            <div class="col-xs-12 title_gede" style="text-align: left;">
                                {{ $slider->title }}
                            </div>
                            <div class="col-xs-12 " style="font-size: 20px;text-align: left;">
                                {{ $slider->description }}
                            </div>

                 

                        </div>
                        <!-- @if(strlen($slider->title) > 5)
                        <div style="color: #fff;font-size: 15px;border-radius: 20px;background-color: #5DD859;width: 15%;
                        margin-left: 0%;padding-top: 1%;padding-bottom: 1%;font-weight: bold;margin-top: 2%">
                            <center>More Info</center>
                        </div>
                        @endif  -->
                        
                        
                    </div>
                    <img src="{{ asset($slider->banner_image) }}" alt="{{ $slider->title }}">
                </div>
                </a>

            @endforeach
        </div>
        

        
    </div>
</div>

<script type="text/javascript">
$('.big_order_slider_sub').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  dots: true,
  arrows: false
});
</script>

