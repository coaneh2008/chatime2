<!-- <div class="home-section">
    <h2 class="home-section-heading">{{ trans('home.inspiration.title') }}</h2>
    <div class="section-inspirations">
        <div class="section-inspiration-big">
            @if(!empty($bigInspirationBanners[0]))
                <a href="{{ route('frontend.inspiration.show', $bigInspirationBanners[0]->slug)}}">
                    <img src="{{ asset($bigInspirationBanners[0]->getThumbnail('banner_image', 'big_square')) }}" alt="{{ $bigInspirationBanners[0]->title }}">
                </a>
            @endif
        </div>
        <div class="section-inspiration-small">
            @foreach($smallInspirationBanners as $banner)
                <a href="{{ route('frontend.inspiration.show', $banner->slug) }}">
                    <img src="{{ asset($banner->getThumbnail('banner_image', 'small_square')) }}" alt="{{ $banner->title }}">
                </a>
            @endforeach
        </div>
        <div class="section-inspiration-big">
            @if(!empty($bigInspirationBanners[1]))
                <a href="{{ route('frontend.inspiration.show', $bigInspirationBanners[1]->slug)}}">
                    <img src="{{ $bigInspirationBanners[1]->getThumbnail('banner_image', 'big_square')}}" alt="{{ $bigInspirationBanners[1]->title }}">
                </a>
            @endif
        </div>
    </div>
</div> -->


<div class="home-section container ins">
    <!-- <h2 class="home-section-heading">Inspirations</h2> -->
    <div class="row">
        <div class="col-xs-6" style="border-radius: 20px;padding-right: 3px;">
            <div class="text_mainbanner_gedebgt">


                <div class="title_gedebgt">
                    @if(!empty($bigInspirationBannersMobile[0]))
                        {{ $bigInspirationBannersMobile[0]->title }}
                    @endif
                </div>
                <div class="desc_gedebgt">
                    @if(!empty($bigInspirationBannersMobile[0]))
                        {{ $bigInspirationBannersMobile[0]->description }}
                    @endif
                </div>

                @if(!empty($bigInspirationBannersMobile[0]))
                <a href="{{ $bigInspirationBannersMobile[0]->url }}">
                    <div class="more_button_gedebgt">
                        More Info
                    </div>
                </a>
                 @endif

            </div>

            


            @if(!empty($bigInspirationBannersMobile[0]))
                <a href="{{ $bigInspirationBannersMobile[0]->url }}">
                    <!-- <img src="{{ asset($bigInspirationBanners[0]->getThumbnail('banner_image', 'big_square')) }}" alt="{{ $bigInspirationBanners[0]->title }}"> -->

                    <img src="{{ asset($bigInspirationBannersMobile[0]->banner_image) }}" alt="{{ $bigInspirationBannersMobile[0]->title }}">
                    
                </a>
            @endif

        </div>
        <div class="col-xs-6" style="border-radius: 20px;padding-left: 4px">
            
            <div class="text_mainbanner_gedebgt">


                <div class="title_gedebgt">
                    @if(!empty($bigInspirationBannersMobile[1]))
                        {{ $bigInspirationBannersMobile[1]->title }}
                    @endif
                </div>
                <div class="desc_gedebgt">
                    @if(!empty($bigInspirationBannersMobile[1]))
                        {{ $bigInspirationBannersMobile[1]->description }}
                    @endif
                </div>

                @if(!empty($bigInspirationBannersMobile[1]))
                <a href="{{ $bigInspirationBannersMobile[1]->url }}">
                    <div class="more_button_gedebgt">
                        More Info
                    </div>
                </a>
                 @endif

            </div>

            


            @if(!empty($bigInspirationBannersMobile[1]))
                <a href="{{ $bigInspirationBannersMobile[1]->url }}">
                    <!-- <img src="{{ asset($bigInspirationBanners[0]->getThumbnail('banner_image', 'big_square')) }}" alt="{{ $bigInspirationBanners[0]->title }}"> -->

                    <img src="{{ asset($bigInspirationBannersMobile[1]->banner_image) }}" alt="{{ $bigInspirationBannersMobile[1]->title }}">
                    
                </a>
            @endif

        </div>
    </div>
    
    <!-- <div class="section-inspirations" style="display: flex;">
        
        
        <div class="section-inspiration-medium">
            

            <a href="inspiration-detail.php">
                <img src="{{ asset('assets/img/openingstore.png') }}" alt="">
            </a>
        </div>
        <div class="section-inspiration-medium">
            
            <a href="inspiration-detail.php">
                <img src="{{ asset('assets/img/event.png') }}" alt="">
            </a>
        </div>
    </div> -->

</div>
