<div class="" id="panel_2"></div>
<div class="home-section">
    <!-- <h2 class="home-section-heading">Inspirations</h2> -->

    <div class="section-inspirations">
        <div class="section-ingredients-big">
        	<div style="color: #5D2E96;font-weight: bold;font-size: 30px;font-family: LasiverBlack">
                <center><h2>Best Quality<br/>Ingredients</h2></center>
            </div>
            <img src="{{ asset(Setting::get('ingredients-desktop', 'assets/img/ingredients.png')) }}">
        </div>
       
        
    </div>
</div>
