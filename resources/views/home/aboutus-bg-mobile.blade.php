<!-- <div class="main-slider slider-style slider-style--primary" data-slick='{
    "autoplay": true,
    "autoplaySpeed": 5000,
    "pauseOnHover": false
}'>
    @foreach ($banners as $banner)
        <div>
            @if($banner->type != '')
                <a {{ $banner->type==1 ? 'data-fancybox' : '' }} href="{{ $banner->video_url }} {{ $banner->type==1 ? '&control=0&showinfo=0' : ''}}" target="_blank">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="">
                </a>
            @else
                <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="">
            @endif
        </div>
    @endforeach
</div> -->
<div class="col-xs-12 about-area" style="padding-right: 0px; padding-left: 0px;">
    <div class="our_menu_content col-xs-12 nopadding" style="margin-top:-2%;padding-right: 0px; padding-left: 0px;">
        <div style="margin-top: 0%;width: 100%" >
            @if(!empty($contentAbout[1]))
            <img src="{{ asset($contentAbout[1]->banner_image) }}" alt="{{ $contentAbout[1]->title }}">
            @endif
        </div>

        <div style="margin-top: 5%;font-size: 28px;width: 100%;color: #5C2D91;font-family: LasiverBlack" >
            <!-- <img src="{{ asset('assets/img/first_Section_aboutus/h1.png') }}" alt="" width="75%"> -->
            <center><b>@if(!empty($contentAbout[1]))
                {!! $contentAbout[1]->title !!}
            @endif</b></center>
        </div>

        <div style="margin-top: 5%;margin-left: 2%;width: 92%;font-size: 11px;text-align:center;color: #5C2D91;font-family: LasiverMedium" >
            <!-- <img src="{{ asset('assets/img/first_Section_aboutus/body_text.png') }}" alt="" width="75%"> -->
            <!-- Chatime merupakan penyedia menuman brewed tea asal Taiwan yang menghadirkan lebih dari 50 varian rasa.
            Di Indonesia, Chatime merupakan salah satu bisnis unit di bawah payung Kawan Lama Group yang telah hadir sejak tahun 2011. -->

            @if(!empty($contentAbout[1]))
                {!! $contentAbout[1]->description !!}
            @endif

        </div> 

        
        
    </div>


    <div class="our_menu_content col-xs-12 nopadding" style="margin-top:0%;padding-right: 0px; padding-left: 0px;">
        

        <div style="margin-top: 5%;font-size: 28px;width: 100%;color: #5C2D91;font-family: LasiverBlack" >
            <!-- <img src="{{ asset('assets/img/first_Section_aboutus/h1.png') }}" alt="" width="75%"> -->
            <center><b>@if(!empty($hadir[0]))
                {!! $hadir[0]->title !!}
            @endif</b></center>
        </div>

        <div style="margin-top: 5%;margin-left: 2%;width: 92%;font-size: 11px;text-align:center;color: #5C2D91;font-family: LasiverMedium" >
            <!-- <img src="{{ asset('assets/img/first_Section_aboutus/body_text.png') }}" alt="" width="75%"> -->
            <!-- Chatime merupakan penyedia menuman brewed tea asal Taiwan yang menghadirkan lebih dari 50 varian rasa.
            Di Indonesia, Chatime merupakan salah satu bisnis unit di bawah payung Kawan Lama Group yang telah hadir sejak tahun 2011. -->

            @if(!empty($hadir[0]))
                {!! $hadir[0]->content !!}
            @endif

        </div>

        <div class="col-xs-12" >

            
            @if(!empty($video[0]))
                
                <iframe height="480" width="1215"    
                    src="https://www.youtube.com/embed/{!! $video[0]->description !!}?autoplay=1">   
                </iframe>

            @else
            
                <img src="{{ asset('assets/img/video_aboutus/video_shadow.png') }}" alt="" width="90%">

            @endif

        </div>



        
        
    </div>

    <div class="our_menu_content col-xs-12 nopadding" style="position: relative;margin-left: auto;margin-right: auto;width: 100%">
        

        <div style="margin-top: 5%;font-size: 28px;width: 100%;color: #5C2D91;font-family: LasiverBlack" >
            <!-- <img src="{{ asset('assets/img/first_Section_aboutus/h1.png') }}" alt="" width="75%"> -->
            <center><b>@if(!empty($hadir[1]))
                {!! $hadir[1]->title !!}
            @endif</b></center>
        </div>

        <div style="margin-top: 5%;margin-left: 2%;width: 92%;font-size: 11px;text-align:center;color: #5C2D91;font-family: LasiverMedium" >
            <!-- <img src="{{ asset('assets/img/first_Section_aboutus/body_text.png') }}" alt="" width="75%"> -->
            <!-- Chatime merupakan penyedia menuman brewed tea asal Taiwan yang menghadirkan lebih dari 50 varian rasa.
            Di Indonesia, Chatime merupakan salah satu bisnis unit di bawah payung Kawan Lama Group yang telah hadir sejak tahun 2011. -->

            @if(!empty($hadir[1]))
                {!! $hadir[1]->content !!}
            @endif

        </div>



        

        
        
    </div>





    <div class="our_menu_content col-xs-12 nopadding" style="margin-top:0%;padding-right: 0px; padding-left: 0px;">
        <div class="container_katering_service"> 
            <div class="row">
            @foreach ($socialmedia as $row)
                <div  class="col-xs-4" style="padding-left: 0px;padding-right: 0px;" >
                    <img src="{{ asset($row->banner_image) }}" alt="{{ $row->title }}" style="width: 130px;height: 130px">
                </div>
            @endforeach
                <div  class="col-xs-4" style="padding-left: 0px;padding-right: 0px;" >
                    <div style="position: absolute;top:10%;margin-left: 5%;">
                        <img src="{{ asset('assets/img/social_media_aboutus/instagram.png') }}" alt="" >
                    </div>
                    <div style="position: absolute;
                    top: 15%;
                    font-size: 5px;
                    color: #fff;
                    font-weight: bold;
                    top: 53%;
                    margin-left: -8%;">
                        <h1><b>FOLLOW US ON INSTAGRAM & GET PROMOTIONS</b></h1>
                    </div>
                    <img src="{{ asset('assets/img/social_media_aboutus/green.png') }}" alt="" style="width: 130px;height: 130px">
                </div>    
            </div>
            

            <!-- <div class="col-xs-4" style="padding-left: 0px;padding-right: 0px;" >
                <div style="position: absolute;top:10%;margin-left: 5%;">
                    <img src="{{ asset('assets/img/social_media_aboutus/instagram.png') }}" alt="" >
                </div>
                <div style="position: absolute;top:15%;font-size: 8px;color: #fff;font-weight: bold;top:65%;margin-left: 5%;">
                    <h1><b>FOLLOW US ON INSTAGRAM & GET PROMOTIONS</b></h1>
                </div>
                <img src="{{ asset('assets/img/social_media_aboutus/green.png') }}" alt="">
            </div> -->
        </div>
    </div>


    <div class="our_menu_content col-xs-12 nopadding" style="margin-top:5%;position: relative;margin-left: auto;margin-right: auto;width: 100%">
        
        <center><h1><span style="color: #5C2D91;font-size: 20px;font-family: LasiverBlack"><b>ALASAN UTAMA UNTUK DATANG KE CHATIME INDONESIA</b></span></h1></center>
        

        <div class="container_katering_service" style="font-family: LasiverMedium">
            @foreach ($fun as $row)
            <div class="col-xs-6" style="padding-top: 5%;">
                <div class="col-xs-12" style="padding-left: 0px;padding-right: 0px">
                    <center><img src="{{ asset($row->banner_image) }}" alt="{{ $row->title }}"></center>
                </div>
                <div class="col-xs-12" style="padding-top:10%;font-size: 15px;color: #5C2D91">
                    <center><b>{{ $row->title }}</b></center>
                </div>
                <div class="col-xs-12" style="padding-top:2%;font-size: 12px;color: #5C2D91">
                    <center>{{ $row->description }}</center>
                </div>
            </div>
            @endforeach
        </div>

    </div>


    <div class="our_menu_content col-xs-12 nopadding" style="margin-top:5%;padding-right: 0px; padding-left: 0px;">    
        
        
        @if(!empty($contentAbout[0]))
        
        <div style="position: absolute;top:27%;width: 90%;margin-left: 5%;padding-right: 0px; padding-left: 0px;">
            <img src="{{ asset($contentAbout[2]->banner_image) }}" alt="{{ $contentAbout[2]->title }}">
        </div>

        @endif

    </div>

    <div class="our_menu_content col-xs-12 nopadding" style="margin-top:5%;padding-right: 0px; padding-left: 0px;"> 
        <!-- LOGO -->
        <div class="col-xs-12" style="margin-top: 40%;margin-bottom: 10%;">
            <!-- @if(!empty($contentAbout[3]))
            <center>
                <img src="{{ asset($contentAbout[3]->banner_image) }}" alt="{{ $contentAbout[3]->title }}" style="width: 70%">
            </center>
            @endif -->
        </div>
    </div>

</div>







<!-- <div class="area-menu" style="margin-top: -0.5em;">
    <img src="{{ asset('assets/img/bg-aboutus.png') }}" alt="">
</div> -->


<!-- <script  type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
 -->
<!-- <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
 -->

<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick-theme.css') }}"/>

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/js/slick/slick.min.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/slack_custom.css') }}"/>


<script type="text/javascript">
    
    $('.multiple-items').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000
    });

    /*$('.multiple-items').slick();*/

    /*$('.carousel_group').flickity({
            // options
            groupCells: true,
            cellAlign: "left",
            pageDots: false,
            wrapAround: true,
            imagesLoaded: true
    });*/
</script>