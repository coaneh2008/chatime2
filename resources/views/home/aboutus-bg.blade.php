<!-- <div class="main-slider slider-style slider-style--primary" data-slick='{
    "autoplay": true,
    "autoplaySpeed": 5000,
    "pauseOnHover": false
}'>
    @foreach ($banners as $banner)
        <div>
            @if($banner->type != '')
                <a {{ $banner->type==1 ? 'data-fancybox' : '' }} href="{{ $banner->video_url }} {{ $banner->type==1 ? '&control=0&showinfo=0' : ''}}" target="_blank">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="">
                </a>
            @else
                <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="">
            @endif
        </div>
    @endforeach
</div> -->

<?php
//var_dump($contentAbout[2]);
?>

<!-- FIRST -->
<div class="our_menu_content mobile-hide" style="position: absolute;margin-top:0%;">
    <div style="position: absolute;margin-left: 5%;margin-top: 5%;font-size: 25px;width: 50%;color: #5C2D91" >
        <!-- <img src="{{ asset('assets/img/first_Section_aboutus/h1.png') }}" alt="" width="75%"> -->
        <h1 style="font-family: LasiverBlack"><b>@if(!empty($contentAbout[1]))
            {!! $contentAbout[1]->title !!}
        @endif</b></h1>
    </div>

    <div style="position: absolute;margin-left: 5%;margin-top: 16%; width: 38%;font-size: 20px;color: #5C2D91;
    font-family: LasiverMedium" >
        <!-- <img src="{{ asset('assets/img/first_Section_aboutus/body_text.png') }}" alt="" width="75%"> -->
        <!-- Chatime merupakan penyedia menuman brewed tea asal Taiwan yang menghadirkan lebih dari 50 varian rasa.
        Di Indonesia, Chatime merupakan salah satu bisnis unit di bawah payung Kawan Lama Group yang telah hadir sejak tahun 2011. -->

        @if(!empty($contentAbout[1]))
            {!! $contentAbout[1]->description !!}
        @endif

    </div> 

    <div style="position: absolute;margin-left: 50%;margin-top: -2%" >
        @if(!empty($contentAbout[1]))
        <img src="{{ asset($contentAbout[1]->banner_image) }}" alt="{{ $contentAbout[1]->title }}">
        @endif
    </div>
    <!-- 
    <div style="position: absolute;margin-left: 60%;margin-top: 10%" >
        <img src="{{ asset('assets/img/first_Section_aboutus/circle_brush2.png') }}" alt="" width="60%">
    </div>
    <div style="position: absolute;margin-left: 75%">
        <img src="{{ asset('assets/img/first_Section_aboutus/circle_brush.png') }}" alt="">
    </div>
    <div style="position: absolute;margin-left: 60%;margin-top: -4%" >
        <img src="{{ asset('assets/img/first_Section_aboutus/product.png') }}" alt="" width="100%">
    </div> -->
</div>

<!-- SECOND -->

<div class="our_menu_content mobile-hide" style="position: absolute;margin-top:40%;">
    
    <!-- <div style="position: absolute;margin-left: 15%;margin-top: 3%" >
        <img src="{{ asset('assets/img/chatime_hadir_abouts/circle_brush.png') }}" alt="" width="80%">
    </div>
    <div style="position: absolute;margin-left: 2%;margin-top: -12%" >
        <img src="{{ asset('assets/img/chatime_hadir_abouts/PearlMilkTea_s.png') }}" alt="" width="75%">
    </div>
    <div style="position: absolute;margin-left: 30%;margin-top: 13%" >
        <img src="{{ asset('assets/img/chatime_hadir_abouts/tea_leaf.png') }}" alt="" width="80%">
    </div> -->

    <div style="position: absolute;margin-left: 57%;margin-top: 0%;font-size: 18px;width: 50%;color: #5C2D91" >
       <h1 style="font-family: LasiverBlack"><b>@if(!empty($hadir[0]))
        {!! $hadir[0]->title !!}
     @endif</b></h1>
    </div>

    <div style="position: absolute;margin-left: 57%;margin-top: 5%;width: 40%;font-size: 18px;color: #5C2D91;font-family: LasiverMedium" >
    <!-- Chatime Indonesia hadir dengan enam kategori minuman yang dapat dinikmati oleh segala usia. Terdiri dari <br/>
    <b>Signature Milk Tea</b> (Sajian minuman milk tea favorit)<br/>
    <b>Tea Presso</b> (menu spesial ice blended)<br/>
    <b>Smoothies</b> (menu spesial ice blended)<br/>
    <b>Tea RRIFIC</b> (seri minumam teh hijau dan hitam yang penuh rasa)<br/>
    <b>Coffe and Latte</b> (sajian minuman brewed coffee segar ) dan<br/>
    <b>Mood Refresh</b> (seri minuman sehat dan menyegarkan) -->
     @if(!empty($hadir[0]))
        {!! $hadir[0]->content !!}
     @endif

    </div>

    <div style="position: absolute;margin-left: 3%;margin-top: -11%;width: 40%" >
        @if(!empty($hadir[0]))
        <img src="{{ asset($hadir[0]->banner_image) }}" alt="{{ $hadir[0]->title }}">
        @endif
    </div>

</div>

<div class="our_menu_content mobile-hide" style="position: absolute;margin-top:68%;width: 100%">
        <div class="col-lg-6">
            <div class="col-lg-12" style="font-size: 18px;color: #5C2D91" >
                <h1 style="font-family: LasiverBlack"><b>@if(!empty($hadir[1]))
                    {!! $hadir[1]->title !!}
                @endif</b></h1>
            </div>
            <div class="col-lg-12" style="font-size: 18px;color: #5C2D91;font-family: LasiverMedium" >
                @if(!empty($hadir[1]))
                    {!! $hadir[1]->content !!}
                @endif  
            </div>
        </div>
        <div class="col-lg-5">
        
            <!-- <img src="{{ asset('assets/img/video_aboutus/video_shadow.png') }}" alt="" width="90%"> -->
            @if(!empty($video[0]))
                
                <iframe height="480" width="500"    
                    src="https://www.youtube.com/embed/{!! $video[0]->description !!}?autoplay=1">   
                </iframe>

            @else
            
                <img src="{{ asset('assets/img/video_aboutus/video_shadow.png') }}" alt="" width="90%">

            @endif
               

        
        </div>
    
</div>
<div class="our_menu_content mobile-hide" style="position: absolute;margin-top:100%;width: 91%;margin-left:5% ">
    
    @foreach ($socialmedia as $row)
        <div  class="col-lg-2" style="padding-left: 0px;padding-right: 0px;font-family: LasiverBlack" >
            <img src="{{ asset($row->banner_image) }}" alt="{{ $row->title }}">
        </div>
    @endforeach
    <!-- <div  class="col-lg-2" style="padding-left: 0px;padding-right: 0px;" >
        <img src="{{ asset('assets/img/social_media_aboutus/2.png') }}" alt="">
    </div>
    <div  class="col-lg-2" style="padding-left: 0px;padding-right: 0px;" >
        <img src="{{ asset('assets/img/social_media_aboutus/3.png') }}" alt="">
    </div>
    <div  class="col-lg-2" style="padding-left: 0px;padding-right: 0px;" >
        <img src="{{ asset('assets/img/social_media_aboutus/4.png') }}" alt="">
    </div>
    <div class="col-lg-2" style="padding-left: 0px;padding-right: 0px;" >
        <img src="{{ asset('assets/img/social_media_aboutus/5.png') }}">
    </div> -->

    <div class="col-lg-2" style="padding-left: 0px;padding-right: 0px;" >
        <div style="position: absolute;top:10%;margin-left: 5%;">
            <img src="{{ asset('assets/img/social_media_aboutus/instagram.png') }}" alt="">
        </div>
        <div style="position: absolute;top:15%;font-size: 8px;color: #fff;font-weight: bold;top:65%;margin-left: 5%;">
            <h1><b>FOLLOW US ON INSTAGRAM & GET PROMOTIONS</b></h1>
        </div>
        <img src="{{ asset('assets/img/social_media_aboutus/green.png') }}" alt="">
    </div>
</div>
<div class="our_menu_content mobile-hide" style="position: absolute;margin-top:120%;width: 91%;margin-left:5% ">
    
    <center><h1><span style="color: #5C2D91;font-family: LasiverBlack"><b>ALASAN UTAMA UNTUK DATANG KE CHATIME INDONESIA</b></span></h1></center>
    <div style="position: relative;margin-left: auto;margin-right: auto;width: 70%">
        @foreach ($fun as $row)
        <div class="col-lg-4" style="font-family: LasiverMedium">
            <div class="col-lg-12">
                <center><img src="{{ asset($row->banner_image) }}" alt="{{ $row->title }}"></center>
            </div>
            <div class="col-lg-12" style="padding-top:10%;font-size: 12px;color: #5C2D91">
                <center><h1><b>{{ $row->title }}</b></h1></center>
            </div>
            <div class="col-lg-12" style="padding-top:2%;font-size: 12px;color: #5C2D91">
                <center>{{ $row->description }}</center>
            </div>
        </div>
        @endforeach
    </div>
</div>

<div class="our_menu_content mobile-hide" style="position: absolute;margin-top:155%;width: 104%;margin-left: -2%">
    
    <!-- <div class="col-lg-12" style="margin-top: 5%;">
        <img src="{{ asset('assets/img/team_aboutus/wave_bg.png') }}" alt="">
    </div>
     -->
    @if(!empty($contentAbout[0]))
    
    <div style="position: absolute;top:10%;width: 90%;margin-left: 5%;">
        <img src="{{ asset($contentAbout[2]->banner_image) }}" alt="{{ $contentAbout[2]->title }}">
    </div>

    @endif

</div>

<!-- <div class="our_menu_content mobile-hide" style="position: absolute;margin-top:155%;width: 100%"> -->
<div class="our_menu_content mobile-hide" style="position: absolute;margin-top:0%;width: 100%">
    <!-- LOGO -->
    <!-- <div class="col-lg-12" style="position: relative;margin-left: auto;margin-right: auto;margin-top: 50%;">
        @if(!empty($contentAbout[3]))
        <center>
            <img src="{{ asset($contentAbout[3]->banner_image) }}" alt="{{ $contentAbout[3]->title }}">
        </center>
        @endif
    </div> -->
</div>

<div class="area-menu">
    <!-- BG -->
    @if(!empty($contentAbout[0]))
    <center>
        <img src="{{ asset($contentAbout[0]->banner_image) }}" alt="{{ $contentAbout[0]->title }}">
    </center>
    @endif

</div>


<!-- <script  type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
 -->
<!-- <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
 -->

<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick-theme.css') }}"/>

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/js/slick/slick.min.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/slack_custom.css') }}"/>


<script type="text/javascript">
    
    $('.multiple-items').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000
    });

    /*$('.multiple-items').slick();*/

    /*$('.carousel_group').flickity({
            // options
            groupCells: true,
            cellAlign: "left",
            pageDots: false,
            wrapAround: true,
            imagesLoaded: true
    });*/
</script>