<section class="home-section">
    <h2 class="home-section-heading">{{ trans('home.featured-brand.title') }}</h2>

    <div class="featured-brands-slider slider-style slider-style--primary">
        <?php $dividedBrand = ($brands->chunk(10)); ?>
        @for ($i = 0; $i < count($dividedBrand); $i++)
        <div>
            <ul class="featured-brands list-nostyle">
                @foreach($dividedBrand[$i] as $brand)
                    <li>
                    <a href="{{ route('frontend.brand.show', $brand->slug) }}">
                        <img src="{{ $brand->getThumbnail('thumb_image', 'full_height_small')}}" alt="">
                    </a>
                    </li>
                @endforeach
            </ul>
        </div>
        @endfor
    </div>
</section>
