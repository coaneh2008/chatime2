<div class="home-section container">
    <!-- <h2 class="home-section-heading">Inspirations</h2> -->

    <div class="section-inspirations">
        <div class="section-sub-inspiration-big" style="cursor: pointer;font-family: LasiverMedium">
            <div style="color: #5D2E96;font-weight: bold;font-size: 18px;font-family: LasiverBlack">
                <b>Mobile App</b>
            </div>
            
            
            @if($homeInfoMobile)

            <div class="text_mainbanner_mobileapp">
                <div class="title_openstore">
                    {{ $homeInfoMobile->title }}
                </div>
                <div class="desc_openstore">
                    {{ $homeInfoMobile->description }}
                </div>
                <a href="{{ $homeInfoDesktop->url_to_ace_online }}">
                <div class="more_button_openstore">
                    More Info
                </div>
                </a>
            </div>

            
            <a href="{{ $homeInfoDesktop->url_to_ace_online }}">
                <img src="{{ asset($homeInfoMobile->banner_image) }}" alt="{{ $homeInfoMobile->title }}">
            </a>

            @endif

        </div>
        
        <div class="section-sub-inspiration-big" style="font-family: LasiverMedium">
            <div style="color: #5D2E96;font-weight: bold;font-size: 18px;font-family: LasiverBlack">
                <b>Big Order</b>
            </div>
            <!-- <a href="#">
                <img src="{{ asset('assets/img/mobile/big-order-bg.png') }}" alt="">
            </a> -->

            <div class="big_order_slider_mobile">
               

                @foreach ($homeInfoMobileSlider as $slider)
                    <a href="{{ $slider->url_to_ace_online }}">
                    <div>
                        <div style="position: absolute;color: #fff;width: 15%;margin-left: 4%;margin-top: 1%">
                                        
                            <div class="row">
                                <div class="col-xs-12 " style="font-size: 18px;text-align: right;font-weight: bold;">
                                    {{ $slider->title }}
                                </div>
                                <div class="col-xs-12" style="font-size: 15px;text-align: right;">
                                    {{ $slider->description }}
                                </div>

                     

                            </div>
                            <div style="color: #fff;font-size: 13px;border-radius: 20px;background-color: #5DD859;width: 30%;
                            margin-left: 72%;padding-top: 3%;padding-bottom: 3%;font-weight: bold;margin-top: 2%">
                                <center>More Info</center>
                            </div>
                            
                            
                            
                        </div>
                        <img src="{{ asset($slider->banner_image) }}" alt="{{ $slider->title }}">
                    </div>
                    </a>

                @endforeach

            

            </div>
        </div>
        

        
    </div>
</div>

<script type="text/javascript">
    
    $('.big_order_slider_mobile').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      dots: true,
      arrows: false
    });

   
</script>