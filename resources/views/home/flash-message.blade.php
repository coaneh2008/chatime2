@if(count($stickers)>0)    
    <div id="flashMsgContent" style="display: none;" data-interval="5000">
        @foreach($stickers as $sticker)
            <a href="{{ $sticker->url }}" data-color="#{{ $sticker->color }}" target="{{ str_contains($sticker->url, env('APP_URL', '')) ? '' : '_blank' }}">{!! $sticker->content !!}</a>
        @endforeach
    </div>
    <div class="flash-msg">
    </div>
@endif