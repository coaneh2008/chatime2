<!-- <div class="main-slider slider-style slider-style--primary " data-slick='{
    "autoplay": true,
    "autoplaySpeed": 5000,
    "pauseOnHover": false
}'>
    @foreach ($banners_mobile as $banner)
        <div style="cursor: pointer;">
            

            <div class="text_mainbanner_gede">

                <div class="title_mainbanner">
                    {{ $banner->judul }}
                </div>
                <div class="desc_mainbanner">
                    {{ $banner->deskripsi }}
                </div>
                <a href="{{ $banner->video_url }}">
                    <div class="more_button_mainbanner">
                        More Info
                    </div>
                </a>
            </div>

            @if($banner->type != '')
                <a {{ $banner->type==1 ? 'data-fancybox' : '' }} href="{{ $banner->video_url }} {{ $banner->type==1 ? '&control=0&showinfo=0' : ''}}" target="_blank">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="">
                </a>
            @else
                <a href="{{ $banner->video_url }}">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}"  height="310px" alt="">
                </a>
            @endif
        </div>
    @endforeach
</div> -->


<div class="multiple-items_mobile">
    @foreach ($banners_mobile as $banner)
        <!-- <div style="cursor: pointer;width: 100%" class="container">
            <a href="{{ $banner->video_url }}">

                <img src="{{ asset($banner->image) }}">
            </a>

            <div class="text_mainbanner_gede">

                    {{ $banner->judul }}
                
            </div>
            


        </div> -->

        <div style="cursor: pointer;width: 100%;position: relative;text-align: center;">
            <div style="position: absolute;
  top: 30%;
  left: 50%;
  transform: translate(-50%, -50%);color: #fff">
                <a href="{{ $banner->video_url }}">
                <div class="row">
                    <div class="col-xs-12" style="font-size: 20px;text-align: center;font-family: LasiverMedium">
                        <b>{{ $banner->judul }}</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="font-size: 20px;text-align: center;font-family: LasiverMedium">
                        {{ $banner->deskripsi }}
                    </div>
                </div>
                </a>
                
            </div>
            <!-- <img src="{{ asset($banner->image) }}"> -->
<!--             @if($banner->type != '')
                <a {{ $banner->type==1 ? 'data-fancybox' : '' }} href="{{ $banner->video_url }} {{ $banner->type==1 ? '&control=0&showinfo=0' : ''}}" target="_blank">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="" width="100%">
                </a>
            @else
                <a href="{{ $banner->video_url }}">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="" width="100%">
                </a>
            @endif -->
            <a href="{{ $banner->video_url }}">
                <img src="{{ asset($banner->image) }}" alt="">
            </a>
        </div>

    @endforeach
</div>



<!-- <div class="main-slider slider-style slider-style--primary" data-slick='{
    "autoplay": true,
    "autoplaySpeed": 5000,
    "pauseOnHover": false
}'>
    <div>
        <div class="text_mainbanner_gede">

            <div class="title_mainbanner">
                Changing the way the world drinks tea
            </div>
            <div class="desc_mainbanner">
                Lorerm adadlkajsdk adajdlkajsd dadljaldkajl adjaldjald
            </div>
            <div class="more_button_mainbanner">
                More Info
            </div>
        </div>

        <a data-fancybox href="https://www.youtube.com/watch?v=wpzeD1nwB8g&control=0&showinfo=0">
            <img src="{{ asset('assets/img/caroucel-mobile.png') }}" alt="" height="310px">
        </a>
    </div>
</div> -->

<div class="container menu_our_menu">
    <div class="test">

        @foreach ($product_home as $product)
    
        <div class="col-xs-6">
            <img src="{{ asset($product->banner_image) }}" alt="{{ $product->title }}">
        </div>

        @endforeach

        <!-- <div class="col-xs-2">
            <img src="{{ asset('assets/img/group-1.png') }}" alt="">
        </div>
        <div class="col-xs-2">
            <img src="{{ asset('assets/img/group-2.png') }}" alt="">
        </div>
         <div class="col-xs-2">
            <img src="{{ asset('assets/img/group-3.png') }}" alt="">
        </div>
        <div class="col-xs-2">
            <img src="{{ asset('assets/img/group-4.png') }}" alt="">
        </div>
        <div class="col-xs-2">
            <img src="{{ asset('assets/img/group-5.png') }}" alt="">
        </div>

        <div class="col-xs-2">
            <img src="{{ asset('assets/img/group-1.png') }}" alt="">
        </div>
        <div class="col-xs-2">
            <img src="{{ asset('assets/img/group-2.png') }}" alt="">
        </div>
         <div class="col-xs-2">
            <img src="{{ asset('assets/img/group-3.png') }}" alt="">
        </div>
        <div class="col-xs-2">
            <img src="{{ asset('assets/img/group-4.png') }}" alt="">
        </div>
        <div class="col-xs-2">
            <img src="{{ asset('assets/img/group-5.png') }}" alt="">
        </div> -->


    </div>
</div>

<div class="our_menu_content mobile-view" style="position: absolute;margin-top:0%;">
    <div style="margin-bottom: 2%;">
            <div style="color: #fff;text-align: center;font-size: 20px;font-weight: bold;margin-bottom: 2%">
                Our Menu
            </div>
            <div style="color: #fff;text-align: center;font-size: 12px;width: 80%;margin-left: 10%;font-family: LasiverMedium">
                {{ Setting::get('ourmenu', 'Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor') }}
            </div>
    </div>

</div>

<div class="area-menu">
    <img src="{{ asset('assets/img/background-mobile.png') }}" alt="">
</div>

<div class="rectangle mobile-view">
    <div style="position: absolute;top:10%;width: 100%;display: flex;">
        <div style="width: 40%;font-family: LasiverMedium">
            <div class="rectable_free">Minum Chatime Tanpa Antre?</div>
            <div class="rectable_download_app">Download Chatime Indonesia App</div>
        </div>
        <div style="width: 5%;text-align: center;">
            <img src="{{ asset('assets/img/separatebar.png') }}" alt="" style="width: 4.2px;height:43px">
        </div>
        <div style="width: 35%;text-align: center;display: flex;padding-top: 3%">
            <div style="width: 50%">
                <a href="{{ Setting::get('googleplay', '') }}">
                    <img src="{{ asset('assets/img/gplay.png') }}" alt="" style="width: 95%;height: 19px">
                </a>
            </div>
            <div style="width: 50%">
                <a href="{{ Setting::get('appstore', '') }}">
                    <img src="{{ asset('assets/img/appstore.png') }}" alt="" style="width: 95%;height: 19px">
                </a>
            </div>
        </div>
        
        <div class="phone_home" >
            <img src="{{ asset('assets/img/phoneleft.png') }}" alt="" style="width: 76%">
        </div>
        <div class="phone_home_right">
            <img src="{{ asset('assets/img/phoneright.png') }}" alt="" style="width: 66%">
        </div>
    </div>
    <img src="{{ asset('assets/img/rounded_rect_mobile.png') }}" alt="" height="60px">
</div>
<!-- <script  type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
 -->
<!-- <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
 -->

 <link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick-theme.css') }}"/>

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/js/slick/slick.min.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/slack_custom.css') }}"/>


<script type="text/javascript">
    
    /*$('.test').slick({
      dots: true,
      infinite: true,
      speed: 500,
      fade: true,
      cssEase: 'linear'
    });*/

    $('.test').slick({
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000
    });

    $('.multiple-items_mobile').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000
    });

    /*$('.multiple-items').slick();*/

    /*$('.carousel_group').flickity({
            // options
            groupCells: true,
            cellAlign: "left",
            pageDots: false,
            wrapAround: true,
            imagesLoaded: true
    });*/
</script>