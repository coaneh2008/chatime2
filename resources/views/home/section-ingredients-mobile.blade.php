<div class="home-section">
    <!-- <h2 class="home-section-heading">Inspirations</h2> -->

    <div class="section-inspirations">
        

        <div class="section-ingredients-big">
        	<!-- <div class="header_ingredents">
                <h2>Best Quality<br/>Ingredients</h2>
            </div> -->
            <div class="section-sub-inspiration-big" style="padding-left: 3%;position: absolute;">
                <div style="color: #5D2E96;font-weight: bold;font-size: 18px;font-family: LasiverBlack">
                    Best Quality Ingredients
                </div>
            </div>
            
            <div style="margin-top: 5%">
            <img src="{{ asset(Setting::get('ingredients-mobile', 'assets/img/mobile/ingredients-bg.png')) }}">
            </div>
        </div>
       
        
    </div>
</div>
