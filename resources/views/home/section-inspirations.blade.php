<!-- <div class="home-section">
    <h2 class="home-section-heading">{{ trans('home.inspiration.title') }}</h2>
    <div class="section-inspirations">
        <div class="section-inspiration-big">
            @if(!empty($bigInspirationBanners[0]))
                <a href="{{ route('frontend.inspiration.show', $bigInspirationBanners[0]->slug)}}">
                    <img src="{{ asset($bigInspirationBanners[0]->getThumbnail('banner_image', 'big_square')) }}" alt="{{ $bigInspirationBanners[0]->title }}">
                </a>
            @endif
        </div>
        <div class="section-inspiration-small">
            @foreach($smallInspirationBanners as $banner)
                <a href="{{ route('frontend.inspiration.show', $banner->slug) }}">
                    <img src="{{ asset($banner->getThumbnail('banner_image', 'small_square')) }}" alt="{{ $banner->title }}">
                </a>
            @endforeach
        </div>
        <div class="section-inspiration-big">
            @if(!empty($bigInspirationBanners[1]))
                <a href="{{ route('frontend.inspiration.show', $bigInspirationBanners[1]->slug)}}">
                    <img src="{{ $bigInspirationBanners[1]->getThumbnail('banner_image', 'big_square')}}" alt="{{ $bigInspirationBanners[1]->title }}">
                </a>
            @endif
        </div>
    </div>
</div> -->

<div class="home-section" style="margin-top: 5%;">
    
    <div class="section-inspirations">
        <div class="section-inspiration-big">
            <div class="text_insp_gede">

                <div class="title_gede">
                    @if(!empty($bigInspirationBanners[0]))
                        {{ $bigInspirationBanners[0]->title }}
                    @endif
                </div>
                <div class="desc_gede">
                    @if(!empty($bigInspirationBanners[0]))
                        {{ $bigInspirationBanners[0]->description }}
                    @endif
                </div>

                @if(!empty($bigInspirationBanners[0]))
                <a href="{{ $bigInspirationBanners[0]->url }}" style="text-decoration: none">
                    <div class="more_button_gede">
                        More Info
                    </div>
                </a>
                 @endif

            </div>

            @if(!empty($bigInspirationBanners[0]))
                <a href="{{ $bigInspirationBanners[0]->url }}">
                    <!-- <img src="{{ asset($bigInspirationBanners[0]->getThumbnail('banner_image', 'big_square')) }}" alt="{{ $bigInspirationBanners[0]->title }}"> -->

                    <img src="{{ asset($bigInspirationBanners[0]->banner_image) }}" alt="{{ $bigInspirationBanners[0]->title }}">
                    
                </a>
            @endif

            <!-- <a href="inspiration-detail.php">
                <img src="{{ asset('assets/img/gedebgt.png') }}" alt="">
            </a> -->
        </div>
        
        <div class="section-inspiration-medium">
            <div class="text_insp_kecil">

                <div class="title_kecil">
                    @if(!empty($smallInspirationBanners[0]))
                        {{ $smallInspirationBanners[0]->title }}
                    @endif
                </div>
                <div class="desc_kecil">
                    @if(!empty($smallInspirationBanners[0]))
                        {{ $smallInspirationBanners[0]->description }}
                    @endif
                </div>

                @if(!empty($smallInspirationBanners[0]))
                <a href="{{ $smallInspirationBanners[0]->url }}" style="text-decoration: none">
                    <div class="more_button_kecil">
                        More Info
                    </div>
                </a>
                @endif

            </div>

              @if(!empty($smallInspirationBanners[0]))
                <a href="{{ $smallInspirationBanners[0]->url }}">
                    <!-- <img src="{{ asset($smallInspirationBanners[0]->getThumbnail('banner_image', 'small_square')) }}" alt="{{ $smallInspirationBanners[0]->title }}"> -->

                    <img src="{{ asset($smallInspirationBanners[0]->banner_image) }}" alt="{{ $smallInspirationBanners[0]->title }}">

                </a>
              @endif   
            
        </div>
        <div class="section-inspiration-medium">
            <div class="text_insp_kecil">

                <div class="title_kecil">
                    @if(!empty($smallInspirationBanners[1]))
                        {{ $smallInspirationBanners[1]->title }}
                    @endif
                </div>
                <div class="desc_kecil">
                    @if(!empty($smallInspirationBanners[1]))
                        {{ $smallInspirationBanners[1]->description }}
                    @endif
                </div>
                @if(!empty($smallInspirationBanners[1]))
                <a href="{{ $smallInspirationBanners[1]->url }}" style="text-decoration: none">
                <div class="more_button_kecil">
                    More Info
                </div>
                </a>
                @endif
            </div>
            @if(!empty($smallInspirationBanners[1]))
            <a href="{{ $smallInspirationBanners[1]->url }}">
                <!-- <img src="{{ asset($smallInspirationBanners[1]->getThumbnail('banner_image', 'small_square')) }}" alt="{{ $smallInspirationBanners[1]->title }}"> -->

                <img src="{{ asset($smallInspirationBanners[1]->banner_image) }}" alt="{{ $smallInspirationBanners[1]->title }}">
                
            </a>
          @endif 
        </div>
    </div>
</div>


