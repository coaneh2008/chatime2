<div class="home-section container" style="margin-top: 5%">
    <!-- <h2 class="home-section-heading">Inspirations</h2> -->

<!--     <div class="section-sub-inspiration-big">
        
        <div class="big_order_slider_mobile">
           

            @foreach ($bigInspirationBannersSliderMobile as $slider)
                <a href="{{ $slider->url }}">
                <div>
                    <img src="{{ asset($slider->banner_image) }}" alt="{{ $slider->title }}">
                </div>
                </a>

            @endforeach

        

        </div>
    </div> -->

    <div class="section-inspirations">
        <div class="section-sub-inspiration-big big_order_slider_mobile_sub">
            

            
            @foreach ($bigInspirationBannersSliderMobile as $slider)
                <a href="{{ $slider->url }}">
                <div>
                    <div style="position: absolute;color: #fff;width: 30%;margin-left: 1%;margin-top: 1%">
                                        
                        <div class="row">
                            <div class="col-xs-12 title_gedebgt" style="text-align: left;">
                                {{ $slider->title }}
                            </div>
                            <div class="col-xs-7 " style="font-size: 12px;text-align: left;">
                                {{ $slider->description }}
                            </div>

                 

                        </div>
                        <div  style="color: #fff;font-size: 12px;border-radius: 20px;background-color: #5DD859;width: 12%;
                        margin-left: 0%;padding-top: 1%;padding-bottom: 1%;font-weight: bold;margin-top: 2%;padding-left: 1%;padding-right: 1%;width: 20%">
                            <center>More Info</center>
                        </div>
                        
                        
                        
                    </div>

                    <img src="{{ asset($slider->banner_image) }}" alt="{{ $slider->title }}">
                </div>
                </a>

            @endforeach

        </div>
      
        
    </div>
</div>

<script type="text/javascript">
    
    $('.big_order_slider_mobile_sub').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      dots: true,
      arrows: false
    });

   
</script>
