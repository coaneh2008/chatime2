<section class="home-section">
    <h2 class="home-section-heading">Promotion</h2>

    <div class="home-promotions">
        <div>
            <div class="home-promotions-slider slider-style slider-style--primary">
                @foreach ($bigImageHomePromos as $bigImageHomePromo)
                    <div>
                        <a href="{{ $bigImageHomePromo->url ? $bigImageHomePromo->url : route('frontend.home-promotion.show', $bigImageHomePromo->slug) }}">
                            <img src="{{ asset($bigImageHomePromo->getThumbnail('thumb_image', 'huge')) }}" alt="{{ $bigImageHomePromo->title }}">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div>
            @foreach ($smallImageHomePromos as $smallImageHomePromo)
                <a href="{{ $smallImageHomePromo->url ? $smallImageHomePromo->url : route('frontend.home-promotion.show', $smallImageHomePromo->slug) }}">
                    <img class="home-promotions-small" src="{{ asset($smallImageHomePromo->getThumbnail('thumb_image', 'medium')) }}" alt="{{ $smallImageHomePromo->title }}">
                </a>
            @endforeach
        </div>
    </div>
</section>
