<div class="home-section">
    <!-- <h2 class="home-section-heading">Inspirations</h2> -->

    <div class="section-inspirations">
        <div class="section-sub-inspiration-big">
            <a href="inspiration-detail.php">
                <img src="{{ asset('assets/img/gedebgt.png') }}" alt="">
            </a>
        </div>
        
        <div class="section-sub-inspiration-big home-promotions-slider slider-style slider-style--primary" data-slick='{
    "autoplay": true,
    "autoplaySpeed": 5000,
    "pauseOnHover": false
}'>
            <!-- <a href="inspiration-detail.php">
                <img src="assets/img/signature.png" alt="">
            </a> -->

            @foreach ($bigInspirationBannersSlider as $slider)
        
            <div>
                <img src="{{ asset($slider->banner_image) }}" alt="{{ $slider->title }}">
            </div>



            @endforeach

            <!-- <div>
                <a href="#">
                    <img src="{{ asset('assets/img/signature.png') }}" alt="">
                </a>
            </div>
            <div>
                <a href="#">
                    <img src="{{ asset('assets/img/signature.png') }}" alt="">
                </a>
            </div> -->

        </div>
        

        
    </div>
</div>
