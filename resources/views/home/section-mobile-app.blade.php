<div class="" id="panel_1"></div>
<div class="home-section">
    <!-- <h2 class="home-section-heading">Inspirations</h2> -->

    <div class="section-inspirations">
        <div class="section-sub-inspiration-big" style="cursor: pointer;">
            <div style="color: #5D2E96;font-weight: bold;font-family: LasiverBlack">
                <h2><b>Mobile App</b></h2>
            </div>
            
            @if($homeInfoDesktop)
            <a href="{{ $homeInfoDesktop->url_to_ace_online }}" style="text-decoration: none">
            <div class="text_mobile_app" style="font-family: LasiverMedium">
                <div style="color: #fff;font-size: 20px;margin-bottom: 5%">
                    {{ $homeInfoDesktop->description }}
                </div>
                    <div style="color: #fff;font-size: 20px;border-radius: 20px;background-color: #5DD859;width: 30%;
                    margin-left: 33%;padding-top: 2%;padding-bottom: 2%;font-weight: bold;">
                        More Info
                    </div>
                </a>

            </div>
            
                <img src="{{ asset($homeInfoDesktop->banner_image) }}" alt="{{ $homeInfoDesktop->title }}">
            </a>

            @endif


        </div>
        
        <div class="section-sub-inspiration-big">
            <div style="color: #5D2E96;font-weight: bold;font-family: LasiverBlack">
                <h2><b>Big Order</b></h2>
            </div>
            <!-- <a href="#">
                <img src="{{ asset('assets/img/bigorder.png') }}" alt="">
            </a> -->
            <div class="big_order_slider">
                <?php
                  $count_data=count($homeInfoDesktopSlider);
                ?>
                @foreach ($homeInfoDesktopSlider as $slider)

                    

                    <a href="{{ $slider->url_to_ace_online }}">
                    <div>
                         @if($count_data > 1)
                        <div style="position: absolute;color: #fff;width: 15%;margin-left: 3%;margin-top: 10.5%;font-family: LasiverMedium">
                                        
                            <div class="row">
                                <div class="col-xs-12" style="font-size: 20px;text-align: center;">
                                    {{ $slider->description }}
                                </div>

                     

                            </div>
                            <div style="color: #fff;font-size: 20px;border-radius: 20px;background-color: #5DD859;width: 30%;
                            margin-left: 33%;padding-top: 2%;padding-bottom: 2%;font-weight: bold;margin-top: 5%">
                                <center>More Info</center>
                            </div>
                            
                            
                            
                        </div>
                        @else
                        <div style="position: absolute;
                        color: #fff;
                        width: 78%;
                        margin-left: 12%;
                        margin-top: 60.5%;font-family: LasiverMedium">
                                        
                            <div class="row">
                                <div class="col-xs-12" style="font-size: 20px;text-align: center;">
                                    {{ $slider->description }}
                                </div>

                     

                            </div>
                            <div style="color: #fff;font-size: 20px;border-radius: 20px;background-color: #5DD859;width: 30%;
                            margin-left: 33%;padding-top: 2%;padding-bottom: 2%;font-weight: bold;margin-top: 5%">
                                <center>More Info</center>
                            </div>
                            
                            
                            
                        </div>
                        @endif


                        <img src="{{ asset($slider->banner_image) }}" alt="{{ $slider->title }}">
                    </div>
                    </a>

                @endforeach

            

            </div>

        </div>
        

        
    </div>
</div>

<script type="text/javascript">
    
    $('.big_order_slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      dots: true,
      arrows: false
    });

    /*$('.multiple-items').slick();*/

    /*$('.carousel_group').flickity({
            // options
            groupCells: true,
            cellAlign: "left",
            pageDots: false,
            wrapAround: true,
            imagesLoaded: true
    });*/
</script>

