<section class="home-section">
    <h2 class="home-section-heading">Product category</h2>

    <ul class="section-category-list list-nostyle">
        @foreach ($productCategories as $category)
        <li>
            <a href="{{ route('frontend.product-category.show', $category->slug) }}">
                <img src="{{ $category->getThumbnail('thumb_image', 'image_icon') }}" alt="">
                <span>{{ $category->title }}</span>
            </a>
        </li>
        @endforeach
    </ul>
</section>