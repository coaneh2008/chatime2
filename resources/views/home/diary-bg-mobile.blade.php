
<div class="area-menu">
    <div class="diary_section_1">
        DEAR <span style="background-color: #b6f87b;padding-left: 1%;padding-right: 1%"><?php echo strtoupper($fullname);?></span>
    </div>


    <div class="diary_section_2">

        <div class="col-xs-12 diary_item_1">
            Mungkin ini agak sedikit kecepetan. Tapi aku sudah gk tahan buat
            ungkapinnya biar hatiku jadi plong dan gk deg-degan.<br>
        </div>
        
        <div  class="col-xs-12 diary_item_1">
            Gak kerasa, ya, Kalau tahun 2020 ini sebentar lagi bakal udahan. Banyak
            momen yang bikin hati kita jadi gk karuan.
        </div>

         <div  class="col-xs-12 diary_item_1">
            Tapi gk sedikit kejadian yang sukses bikin aku senyum-senyum<br>
            kesenengan. Abisnya kamu, tuh, bisa banget, deh, bikin setiap momen pertemuan kita jadi <span style="color:red">gk terlupakan</span>. Hehe... :D
        </div>

    </div>

    <div class="diary_section_3">

        <div  class="col-xs-12 diary_item_1">
            Contohnya momen saat pertama kali kita kenalan.<br>
            <span style="background-color: #b6f87b;padding-left: 2%;padding-right: 2%"><?php echo $date_joined;?></span>, tuh, jadi tanggal yang paling berkesan.
        </div>
        
    </div>


    <div class="diary_section_4">

        <div  class="col-xs-12 diary_item_1">
            Belum lagi saat kamu sering banget beli <span style="background-color: #b6f87b;padding-left: 2%;padding-right: 2%">(<?php echo $fav_menu;?>)</span> dan akhirnya minuman itu juga jadi <span style="color:red">minuman kesukaanku</span>. Suka, deh, kamu jago banget kasih pilihan!
        </div>

        <div  class="col-xs-12 diary_item_1">
            Aku jadi ingat, <span style="background-color: #b6f87b;padding-left: 2%;padding-right: 2%">(<?php echo $fav_store;?>)</span> sering banget jadi tempat buat kita ketemuan. Aku tebak, deh, itu pasti tempat favoritmu, kan ?
        </div>
        
    </div>


    <div class="diary_section_5">

        <div class="col-xs-12 diary_item_1">
            Saking sukanya kita jajan ke sana, Kalau di hitung-hitung, kamu udah pesan sekitar <span style="background-color: #b6f87b;padding-left: 2%;padding-right: 2%">(<?php echo $qty_cup;?>)</span> minuman favoritmu tiap kali kita jajan.
        </div>

        
        
    </div>


    <div class="diary_section_6">

        <div  class="col-xs-12 diary_item_2">
            Oh ya, tahun ini kamu juga sudah <span style="background-color: #b6f87b;padding-left: 2%;padding-right: 2%">(transaksi selama tahun 2020)</span> dan <br>punya <span style="color:red">banyak Pointea</span> yang bisa digunakan buat traktir teman-teman <br>tongkrongan.
        </div>

        <div  class="col-xs-12 diary_item_3">
            Dan gk tau, sih, ternyata kamu sudah <span style="background-color: #b6f87b;padding-left: 2%;padding-right: 2%">(<?php echo $qty_rdm;?>)</span> nukerin Pointea itu. Aku mau tanya, Kamu tukerin buat traktir teman atau tukar merchantdise lucu?
        </div>

        <div  class="col-xs-12 diary_item_4">
            Anyway, aku pengen banget ngucapin banyak terima kasih buat kamu<br>
            untuk setiap <span style="color: red">momen berkesan</span> yang sudah kita lewati bareng-bareng.
        </div>

        <div class="col-xs-12 diary_item_5">
            Dan karena itulah, sekarang aku punya panggilan saya buat kamu, yaitu <br>
            <span style="background-color: #b6f87b;padding-left: 2%;padding-right: 2%">(<?php echo $kategori;?>)</span>. Gimana, kamu suka, kan ? Hehe... :D
        </div>

        <div class="col-xs-7 diary_item_6">
            Karena kamu udah punya banyak waktu buat aku,nih aku kasih <span style="color: red">kado spesial</span>
            yang pasti kamu suka, voucher dengan kode <span style="background-color: #b6f87b;padding-left: 2%;padding-right: 2%"><?php echo $kode_voucher;?></span> yang bisa langsung kamu gunakan. Semoga berkenan :))
        </div>

        <div  class="col-xs-11 diary_item_7">
            Jangan lupa pamerin surat cinta dari aku ke temen-temen dan mention @chatimeindo ya!
        </div>

        
        
    </div>


    <img src="{{ asset('assets/img/bg_diary.jpg') }}" alt="">
</div>



