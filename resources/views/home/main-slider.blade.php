<!-- <div class="main-slider slider-style slider-style--primary" data-slick='{
    "autoplay": true,
    "autoplaySpeed": 5000,
    "pauseOnHover": false
}'> -->

<!-- <div class="main-slider multiple-items2">
    @foreach ($banners as $banner)
        <div style="cursor: pointer;">
            <div style="position: absolute;top:20%;color: #fff;margin-left: 3%">
                <div class="row">
                    <div class="col-lg-7" style="font-size: 25px">
                        <h1><b>{{ $banner->judul }}</b></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6" style="font-size: 18px">
                        {{ $banner->deskripsi }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2" style="margin-top: 2%">
                        <a href="{{ $banner->video_url }}">
                            <div style="color: #fff;font-size: 18px;border-radius: 20px;background-color: #5DD859;
                            padding-top: 6%;padding-bottom: 6%;font-weight: bold;text-align: center;">
                                More Info
                            </div>
                        </a>

                    </div>
                </div>
            </div>

            @if($banner->type != '')
                <a {{ $banner->type==1 ? 'data-fancybox' : '' }} href="{{ $banner->video_url }} {{ $banner->type==1 ? '&control=0&showinfo=0' : ''}}" target="_blank">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="">
                </a>
            @else
                <a href="{{ $banner->video_url }}">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="">
                </a>
            @endif
        </div>
    @endforeach
</div> -->

<div class="multiple-items2">
    @foreach ($banners as $banner)
        <div style="cursor: pointer;width: 100%">
            <div style="position: absolute;top:20%;color: #fff;margin-left: 3%">
                <a href="{{ $banner->video_url }}">
                <div class="row">
                    <div class="col-lg-10" style="font-size: 25px;font-family: LasiverMedium">
                        <h1><b>{{ $banner->judul }}</b></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6" style="font-size: 18px;font-family: LasiverMedium">
                        {{ $banner->deskripsi }}
                    </div>
                </div>
                </a>
                <!-- <div class="row">
                    <div class="col-lg-4" style="margin-top: 2%">
                        <a href="{{ $banner->video_url }}">
                            <div style="color: #fff;font-size: 18px;border-radius: 20px;background-color: #5DD859;
                            padding-top: 6%;padding-bottom: 6%;font-weight: bold;text-align: center;">
                                More Info
                            </div>
                        </a>

                    </div>
                </div> -->
            </div>

            @if($banner->type != '')
                <a {{ $banner->type==1 ? 'data-fancybox' : '' }} href="{{ $banner->video_url }} {{ $banner->type==1 ? '&control=0&showinfo=0' : ''}}" target="_blank">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="" width="100%">
                </a>
            @else
                <a href="{{ $banner->video_url }}">
                    <img src="{{ asset($banner->getThumbnail('image', 'huge')) }}" alt="" width="100%">
                </a>
            @endif
        </div>
        
    @endforeach
</div>




<!-- <div class="main-slider slider-style slider-style--primary" data-slick='{
    "autoplay": true,
    "autoplaySpeed": 5000,
    "pauseOnHover": false
}'>
    <div>
        <div style="position: absolute;top:20%;color: #fff;margin-left: 3%">
            <div class="row">
                <div class="col-lg-7" style="font-size: 25px">
                    <h1><b>Changing the way the world drinks tea</b></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6" style="font-size: 18px">
                    We have an optimistic and ambitious vision for the Chatime brand. 
                    We are always striving for excellence in every element of our business.
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2" style="margin-top: 2%">
                    <div style="color: #fff;font-size: 18px;border-radius: 20px;background-color: #5DD859;
                    padding-top: 6%;padding-bottom: 6%;font-weight: bold;text-align: center;">
                        More Info
                    </div>
                </div>
            </div>
        </div>
        <a data-fancybox href="https://www.youtube.com/watch?v=wpzeD1nwB8g&control=0&showinfo=0">
            <img src="{{ asset('assets/img/carousel.png') }}" alt="">
        </a>
    </div>
    <div>
        <a href="#">
            <img src="{{ asset('assets/img/banner-2.jpg') }}" alt="">
        </a>
    </div>
</div>
-->
<div class="our_menu_content mobile-hide" style="position: absolute;margin-top:5%;">
    <div style="margin-bottom: 2%;">
        <div style="color: #fff;text-align: center;font-size: 45px;font-weight: bold;margin-bottom: 2%;font-family: LasiverBlack">
            Our Menu
        </div>
        <div style="color: #fff;text-align: center;font-size: 20px;width: 30%;margin-left: 35%;font-family: LasiverMedium">
            {{ Setting::get('ourmenu', 'Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor') }}
        </div>
    </div>
    <a href="/product">
    <div class="multiple-items" style="cursor:pointer;width: 80%;margin-bottom: 10%;margin-left: auto;margin-right: auto;position: relative;">
        @foreach ($product_home as $product)
        
        <div>
            <img src="{{ asset($product->banner_image) }}" alt="{{ $product->title }}">
        </div>

        @endforeach
        <!-- <div>
            <img src="{{ asset('assets/img/group-2.png') }}" alt="">
        </div>
        <div>
            <img src="{{ asset('assets/img/group-3.png') }}" alt="">
        </div>
        <div>
            <img src="{{ asset('assets/img/group-4.png') }}" alt="">
        </div>
        <div>
            <img src="{{ asset('assets/img/group-5.png') }}" alt="">
        </div>

        <div>
            <img src="{{ asset('assets/img/group-1.png') }}" alt="">
        </div>
        <div>
            <img src="{{ asset('assets/img/group-2.png') }}" alt="">
        </div>
        <div>
            <img src="{{ asset('assets/img/group-3.png') }}" alt="">
        </div>
        <div>
            <img src="{{ asset('assets/img/group-4.png') }}" alt="">
        </div>
        <div>
            <img src="{{ asset('assets/img/group-5.png') }}" alt="">
        </div> -->
        
    </div>
    </a>
</div>

<div class="area-menu">
    <img src="{{ asset('assets/img/background.png') }}" alt="" width="100%">
</div>

<div class="rectangle mobile-hide">
    <div style="position: absolute;top:20%;width: 100%;display: flex;">
        <div style="width: 35%;font-family: LasiverMedium">
            <div style="width: 100%;text-align: right;color: #fff;font-size: 20px;margin-bottom: 0%">Minum Chatime Tanpa Antre?</div>
            <div style="width: 100%;text-align: right;color: #fff;font-weight: bold;font-size: 20px;">Download Chatime Indonesia App</div>
            
            
        </div>
        <div style="width: 5%;text-align: center;">
            <img src="{{ asset('assets/img/separatebar.png') }}" alt="" style="width: 8px">
        </div>
        <div style="width: 40%;text-align: center;display: flex;padding-top: 0%">
            
            <div style="width: 50%">
                <a href="{{ Setting::get('googleplay', '') }}">
                    <img src="{{ asset('assets/img/gplay.png') }}" alt="" style="width: 90%">
                </a>
            </div>
            

            <div style="width: 50%">
                <a href="{{ Setting::get('appstore', '') }}">    
                    <img src="{{ asset('assets/img/appstore.png') }}" alt="" style="width: 90%">
                </a>
            </div>
        </div>
        <div class="phone_home">
            <img src="{{ asset('assets/img/phoneleft.png') }}" alt="" style="width: 62%">
        </div>
        <div class="phone_home_right">
            <img src="{{ asset('assets/img/phoneright.png') }}" alt="" style="width: 51%">
        </div>
    </div>
    <img src="{{ asset('assets/img/rounded_rect.png') }}" alt="">
</div>
<!-- <script  type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
 -->
<!-- <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
 -->

<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/slick/slick-theme.css') }}"/>

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/js/slick/slick.min.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/slack_custom.css') }}"/>


<script type="text/javascript">
    
    $('.multiple-items').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000
    });

    $('.multiple-items2').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000
    });

    /*$('.multiple-items').slick();*/

    /*$('.carousel_group').flickity({
            // options
            groupCells: true,
            cellAlign: "left",
            pageDots: false,
            wrapAround: true,
            imagesLoaded: true
    });*/
</script>