@include('_includes.head')
    <!-- @include('home.flash-message') -->
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <div class="mobile-hide">
                <main class="site-main">
                    @include('home.main-slider')
                    <div class="container">
                        <!-- @include('home.featured-brands') -->

                        <!-- @include('home.home-promotions') -->

                        @include('home.section-inspirations')
                        @include('home.section-sub-inspirations')
                        @include('home.section-mobile-app')
                        <!-- @include('home.section-category-list') -->
                    </div>
                    @include('home.section-ingredients')
                    <div class="container">
                        @include('home.section-flipimages')
                    </div>
                </main>
            </div>

            <div class="mobile-view">
                <main class="site-main">
                    @include('home.main-slider-mobile')
                    <div class="container">
                        <!-- @include('home.featured-brands') -->

                        <!-- @include('home.home-promotions') -->

                        <!-- @include('home.section-inspirations-mobile')
                        @include('home.section-sub-inspirations-mobile')
                        @include('home.section-mobile-app-mobile') -->
                        <!-- @include('home.section-category-list') -->
                    </div>
                    <!-- @include('home.section-ingredients-mobile') -->
                    <div class="container">
                        <!-- @include('home.section-flipimages-mobile') -->
                    </div>
                </main>
            </div>

        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
