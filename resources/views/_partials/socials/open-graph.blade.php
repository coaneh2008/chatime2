<meta property="og:title" content="{{ $metaTitle or Setting::get('site-name') }}"/>
<meta property="og:type" content="{{ $metaType or 'website' }}"/>
<meta property="og:image" content="{{ $metaImage or (empty(Setting::get('site-image'))?'':asset(Setting::get('site-image'))) }}"/>
<meta property="og:url" content="{{ $metaUrl or Request::url() }}"/>
<meta property="og:description" content="{{ $metaDescription or Setting::get('site-description') }}"/>
<meta property="fb:app_id" content="{{ env('FACEBOOK_APP_ID') }}" />
<meta property="og:locale" content="{{ app()->getLocale() }}" />
