<h1>HOME FOR HOLIDAY INSPIRATIONS</h1>
<p><time>22 November &mdash; 25 Desember 2017</time></p>

<p><b>HOLIDAY IS COMING!!!</b></p>
<p>Memasuki bulan Desember artinya sebentar lagi Hari Raya Natal akan tiba. Bersiaplah untuk mendekorasi rumah dengan pernak pernik Natal. Kegiatan ini selalu menjadi aktivitas wajib di rumah ketika menyambut hari yang penuh damai dan suka cita. Tak ketinggalan, makan malam bersama keluarga dan orang yang dikasihi saat malam Natal juga melengkapi momen indah ini menjadi lebih bahagia.</p>