<section class="home-section">
    <h2 class="home-section-heading">Promotion Of The Month</h2>

    <div class="thematic-promotion">
        <div>
            <table style="table-layout:fixed;">
                <th width="33%"></th>
                <th width="33%"></th>
                <th width="33%"></th>
                @for ($i = 0 ; $i < sizeof($smallImageThematicPromotions) ; $i++)
                    @if ($i%3 === 0)
                        <tr>
                    @endif
                        <td>
                            <a href="{{ $smallImageThematicPromotions[$i]->url ? $smallImageThematicPromotions[$i]->url : route('frontend.thematic-promotion.show', $smallImageThematicPromotions[$i]->slug) }}">
                                <img src="{{ asset($smallImageThematicPromotions[$i]->getThumbnail('thumb_image', 'medium')) }}" alt="{{ $smallImageThematicPromotions[$i]->title }}">
                            </a>
                        </td>
                    @if ($i%3 === 2)
                        <tr>
                    @endif
                @endfor
            </table>
        </div>
    </div>
</section>
