<section class="home-section">
    <h2 class="home-section-heading"></h2>
    <div class="shop-now-informa">
        <div class="shop-now-column shop-now-left">
            <p>Belanja Online Produk ACE Sekarang</p>
            <div class="shop-now-button">
                <a href="{{ Setting::get('thematic-shop-now-url') }}">
                    <div>
                        <b>SHOP NOW</b>&nbsp;&nbsp;&nbsp;<div class="play-button"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="shop-now-column shop-now-right">
            <img src="{{ Setting::get('banner-shop-now-image') }}">
        </div>
    </div>
    @if (isset($isOnGoingPromo))
        @if ($isOnGoingPromo)
            <div class="thematic-products">
                <div>
                    <table style="table-layout:fixed;">
                        <th width="25%"></th>
                        <th width="25%"></th>
                        <th width="25%"></th>
                        <th width="25%"></th>
                        @for ($i = 0 ; $i < sizeof($promotionProducts) ; $i++)
                            @if ($i%4 === 0)
                                <tr>
                            @endif
                                <td>
                                    <img src="{{ Setting::get('base-img-url').$promotionProducts[$i]->variants[0]->images[0]->image_url }}">
                                    @if ($promotionProducts[$i]->variants[0]->prices[0]->special_price > 0)
                                        <div class="price-box">
                                            <span class="price-old">
                                                Rp {{ number_format($promotionProducts[$i]->variants[0]->prices[0]->price,2,',','.') }}
                                            </span>
                                            <span class="price">
                                                Rp {{ number_format($promotionProducts[$i]->variants[0]->prices[0]->special_price,2,',','.') }}
                                            </span>
                                            <div class="discount-container">
                                                {{ 100-ceil($promotionProducts[$i]->variants[0]->prices[0]->special_price/$promotionProducts[$i]->variants[0]->prices[0]->price*100) }}%
                                            </div>
                                            <h3 class="title-product">{{ $promotionProducts[$i]->name }}</h3>
                                        </div>
                                    @else
                                        <div class="price-box">
                                            <span class="price-old">
                                                &nbsp;
                                            </span>
                                            <span class="price">Rp {{ number_format($promotionProducts[$i]->variants[0]->prices[0]->price,2,',','.') }}</span>
                                            <h3 class="title-product">{{ $promotionProducts[$i]->name }}</h3>
                                        </div>
                                    @endif
                                </td>
                            @if ($i%4 === 3)
                                <tr>
                            @endif
                        @endfor
                    </table>
                </div>
                <div class="see-more-products">
                    <a href="{{ Setting::get('thematic-see-more-url') }}">
                        <div>
                            <b>SEE MORE</b>&nbsp;&nbsp;&nbsp;<div class="play-button"></div>
                        </div>
                    </a>
                </div>
            </div>
        @endif
    @endif
</section>
