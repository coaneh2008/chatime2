<div class="main-banner">
    @if(sizeof($bigImageThematicPromotions)>0)
        <img src="{{ asset($bigImageThematicPromotions[0]->getThumbnail('banner_image', 'huge')) }}" alt="{{ $bigImageThematicPromotions[0]->title }}">
    @endif
</div>