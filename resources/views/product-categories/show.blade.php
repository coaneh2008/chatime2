@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>{{ $category->title }}</li>
                    </ul>
                    <article class="product">
                        <figure>
                            <img src="{{ asset($category->getThumbnail('banner_image', 'huge')) }}" alt="">
                        </figure>
                        <div class="bzg">
                            <div class="bzg_c" data-col="l8" data-offset="l2">
                                <h2 class="text-caps">{{ $category->title }}</h2>
                                <?= $category->content ?>
                            </div>
                        </div>
                        <br>

                        <ul class="product-list list-nostyle">
                            @foreach($category->childs as $subCategory)
                                <li>
                                    <a class="product-item-anchor" href="{{ $subCategory->url_to_ace_online }}" target="_blank">
                                        <figure class="product-item">
                                            <img src="{{ $subCategory->getThumbnail('thumb_image', 'secondary_medium') }}" alt="{{ $subCategory->title }}">
                                            <figcaption>
                                                <h3>{{ $subCategory->title }}</h3>
                                                {{ $subCategory->description }}
                                            </figcaption>
                                        </figure>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </article>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>
@include('_includes.scripts')