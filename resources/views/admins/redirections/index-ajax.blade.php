{
    "draw": {{ \Input::get('draw', 1) }},
    "recordsTotal": {{ $total }},
    "recordsFiltered": {{ $models->total() }},
    "data" : [
        @foreach($models as $key => $model)
        [
            "{{ $model->getKey() }}",
            "{{ stringJson($model->old_path) }}",
            "{{ stringJson($model->new_url) }}",
            "{{ $model->created_at}}",
            "{{ $model->updated_at}}",
            "{!! sprintf(
                "<div class='text-center'><a href='%s' class='btn default btn-xs green'><i class='fa fa-edit'></i> Edit </a><a href='%s' class='btn default btn-xs red delete' data-token='%s'><i class='fa fa-trash-o'></i> Delete </a></div>",
                 suitRoute($routePrefix.'.edit', $model),
                 suitRoute($routePrefix.'.destroy', $model),
                 \Session::token()
             )!!}"
        ]{{ ($models->count()-1 === $key) ? '' : ',' }}
        @endforeach
    ]
}
