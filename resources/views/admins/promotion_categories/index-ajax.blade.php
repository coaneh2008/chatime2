{
    "draw": {{ \Input::get('draw', 1) }},
    "recordsTotal": {{ $total }},
    "recordsFiltered": {{ $models->total() }},
    "data" : [
        @foreach($models as $key => $model)
        [
            "{{ $model->getKey() }}",
            "{{ stringJson($model->title) }}",
            "{{ stringJson($model->getTypeName()) }}",
            "{{ $model->order }}",
            @if ($model->published_at)
                "<span class=\"label label-success\">Published</span>",
            @else
                "<span class=\"label label-danger\">Unpublished</span>",
            @endif
            "{{ $model->created_at}}",
            "{{ $model->updated_at}}",
            "{!! sprintf(
                "<center><a href='%s' class='btn default btn-xs green'><i class='fa fa-edit'></i> Edit </a></center>",
                 suitRoute($routePrefix.'.edit', $model)
             )!!}"
        ]{{ ($models->count()-1 === $key) ? '' : ',' }}
        @endforeach
    ]
}
