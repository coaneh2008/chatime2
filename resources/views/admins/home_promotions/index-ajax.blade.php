{
    "draw": {{ \Input::get('draw', 1) }},
    "recordsTotal": {{ $total }},
    "recordsFiltered": {{ $models->total() }},
    "data" : [
        @foreach($models as $key => $model)
        [
            "{{ $model->getKey() }}",
            "{{ stringJson($model->newProductCategory->title) }}",
            "{{ stringJson($model->title) }}",
            "{!! sprintf("<img src='%s'>", $model->getThumbnail('banner_image', 'small')) !!}",
            "{!! $model->getTypeName() !!}",
            "{{ stringJson($model->kode_promo) }}",
            "{!! sprintf("<a href='%s'>$model->url", $model->url) !!}",
            {!! json_encode(Form::suitIndexSwitch('published', $model->published, suitRoute($routePrefix.'.publish', $model))) !!},
            "{{ $model->order }}",
            "{{ stringJson(strip_tags($model->content)) }}",
            "{{ stringJson($model->slug) }}",
            "{{ $model->created_at}}",
            "{{ $model->updated_at}}",
            "{!! sprintf(
                "<a href='%s' class='btn default btn-xs green'><i class='fa fa-edit'></i> Edit </a><a href='%s' class='btn default btn-xs red delete' data-token='%s'><i class='fa fa-trash-o'></i> Delete </a>",
                 suitRoute($routePrefix.'.edit', $model),
                 suitRoute($routePrefix.'.destroy', $model),
                 \Session::token()
             )!!}"
        ]{{ ($models->count()-1 === $key) ? '' : ',' }}
        @endforeach
    ]
}
