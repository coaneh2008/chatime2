<html>
    <tr>
        <td><b>No</b></td>
        <td><b>Username</b></td>
        <td><b>Name</b></td>
        <td><b>Email</b></td>
        <td><b>Telp No.</b></td>
        <td><b>HP No.</b></td>
        <td><b>Code</b></td>
        <td><b>Created at</b></td>
    </tr>
        @foreach($data as $key => $exportData)
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $exportData->user->username }}</td>
                <td>{{ $exportData->user->name }}</td>
                <td>{{ $exportData->user->email }}</td>
                <td>{{ $exportData->user->telephone ?: '-' }}</td>
                <td>{{ $exportData->user->cellphone ?: '-' }}</td>
                <td>{{ $exportData->code }}</td>
                <td>{{ $exportData->user->created_at }}</td>
            </tr>
        @endforeach
    <tr></tr>
</html>
