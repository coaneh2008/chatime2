<html>
    <tr>
        <td><b>No</b></td>
        <td><b>Title</b></td>
        <td><b>Region</b></td>
        <td><b>Type</b></td>
        <td><b>Address</b></td>
        <td><b>Latitude</b></td>
        <td><b>Longitude</b></td>
        <td><b>Published</b></td>
        <td><b>Published at</b></td>
        <td><b>Created at</b></td>
        <td><b>Updated at</b></td>
    </tr>
        @foreach($data as $key => $exportData)
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $exportData->title }}</td>
                <td>{{ $exportData->region->title }}</td>
                <td>{{ $exportData->getTypeName() }}</td>
                <td>{{ $exportData->address }}</td>
                <td>{{ $exportData->latitude }}</td>
                <td>{{ $exportData->longitude }}</td>
                <td>{{ $exportData->published ? 'Yes' : 'No' }}</td>
                <td>{{ $exportData->published_at }}</td>
                <td>{{ $exportData->created_at }}</td>
                <td>{{ $exportData->updated_at }}</td>
            </tr>
        @endforeach
    <tr></tr>
</html>
