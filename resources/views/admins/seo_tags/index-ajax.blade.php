{
    "draw": {{ \Input::get('draw', 1) }},
    "recordsTotal": {{ $total }},
    "recordsFiltered": {{ $models->total() }},
    "data" : [
        @foreach($models as $key => $model)
        [
            "{{ $model->getKey() }}",
            "{{ stringJson($model->getPageTypeName()) }}",
            "{{ stringJson($model->getPageName($model->content_id)) }}",
            "{{ stringJson($model->seo_title) }}",
            "{{ stringJson($model->seo_description) }}",
            "{{ $model->seo_slug?stringJson($model->seo_slug):'-' }}",
            "{{ $model->opengraph_title?stringJson($model->opengraph_title):'-' }}",
            "{{ $model->opengraph_description?stringJson($model->opengraph_description):'-' }}",
            "{{ $model->twitter_title?stringJson($model->twitter_title):'-' }}",
            "{{ $model->twitter_description?stringJson($model->twitter_description):'-' }}",
            "{{ $model->meta_image }}",
            "{{ $model->created_at}}",
            "{{ $model->updated_at}}",
            "{!! sprintf(
                "<a href='%s' class='btn default btn-xs green'><i class='fa fa-edit'></i> Edit </a><a href='%s' class='btn default btn-xs red delete' data-token='%s'><i class='fa fa-trash-o'></i> Delete </a>",
                 suitRoute($routePrefix.'.edit', $model),
                 suitRoute($routePrefix.'.destroy', $model),
                 \Session::token()
             )!!}"
        ]{{ ($models->count()-1 === $key) ? '' : ',' }}
        @endforeach
    ]
}

