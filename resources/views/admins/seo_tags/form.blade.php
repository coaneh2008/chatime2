@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model,array('route'=>($model->exists?[suitRouteName("$routePrefix.update"),$model]:suitRouteName("$routePrefix.store")), 'method'=>($model->exists?'PUT':'POST'))) !!}
    <div class="form-body">
        {!! Form::suitText('page_type', 'Page Type', $model->getPageTypeName(), ['disabled']) !!}
        {!! Form::suitSelect('content_id', 'Page', $staticPages) !!}
        {!! Form::suitText('seo_slug', 'SEO Slug') !!}
        {!! Form::suitText('seo_title', "SEO Title<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitTextarea('seo_description', "SEO Description<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitText('opengraph_title','Opengraph Title') !!}
        {!! Form::suitTextarea('opengraph_description', 'Opengraph Description') !!}
        {!! Form::suitText('twitter_title', 'Twitter Title') !!}
        {!! Form::suitTextarea('twitter_description', 'Twitter Description') !!}
        {!! Form::suitText('meta_image', 'Meta Image', null, ['info' => 'Please insert image URL']) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@section('page-scripts')
    @parent
    <script type="text/javascript">
    $('#content_id').on('change', function() {
        var id = ($(this).val())
        $.get("{{ asset('/') . config('suitcms.prefix_url') }}" + "/get-slug-menu/" + id, function(data) {
          $('#seo_slug').val(data)
        });
    })
    </script>
@endsection