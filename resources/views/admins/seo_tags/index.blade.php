@extends('admins._layouts.index-base')

@section('page-title')
    {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i> {{ $pageName }}
    </li>
@endsection

@section('table-title')
    {{ $pageName }} Table
@endsection

@section('page-header-toolbar')
    <div class="btn-group pull-right">
        <a href="{{ suitRoute($routePrefix.'.create') }}" class="btn btn-sm btn-primary">
            <i class="glyphicon glyphicon-plus"></i> Add New
        </a>
    </div>
@endsection

@section('table-column-checkbox')
    <label><input type="checkbox" data-name="id">#</label>
    <label><input type="checkbox" checked data-name="page_type">Page Type</label>
    <label><input type="checkbox" checked data-name="content_id">Page</label>
    <label><input type="checkbox" checked data-name="seo_title">SEO Title</label>
    <label><input type="checkbox" checked data-name="seo_description">SEO Description</label>
    <label><input type="checkbox" checked data-name="seo_slug">SEO Slug</label>
    <label><input type="checkbox" checked data-name="opengraph_title">Opengraph Title</label>
    <label><input type="checkbox" checked data-name="opengraph_description">Opengraph Description</label>
    <label><input type="checkbox" checked data-name="twitter_title">Twitter Title</label>
    <label><input type="checkbox" checked data-name="twitter_description">Twitter Description</label>
    <label><input type="checkbox" checked data-name="meta_image">Meta Image</label>
    <label><input type="checkbox" checked data-name="created_at">Created At</label>
    <label><input type="checkbox" data-name="updated_at">Updated At</label>
@endsection
