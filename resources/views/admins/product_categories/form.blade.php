@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute('menus.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">
        {!! Form::suitSelect('parent_id', 'Parent', $parents) !!}
        {!! Form::suitSelect('type', 'Type', $types) !!}
        {!! Form::suitText('title', "Title<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitTextarea('description', 'Description') !!}
        <!-- {!! Form::suitWysiwyg('content', 'Content') !!} -->
        <!-- {!! Form::suitFileBrowser('thumb_image', "Thumb Image<span style='color:#FE2A1A;'> *</span>", null, ['info' => 'Must be an image file. Recommended size for icon image primary menu 20x20, thumb image for secondary menu 358x201']) !!} -->
        <!--
        {!! Form::suitFileBrowser('sidebar_image', 'Sidebar Image', null, ['info' => 'Must be an image file. Recommended size 270x490']) !!} -->
        {!! Form::suitFileBrowser('banner_image', 'Background Image', null, ['info' => 'Must be an image file. Recommended size 1105x487']) !!}
        <!--  {!! Form::suitText('url_to_ace_online', 'Url to ACE Online') !!} -->
        <!--
        {!! Form::suitText('color', "Color 1<span style='color:#FE2A1A;'> *</span>", $model->exists ? $model->color:null, ['class' => 'jscolor']) !!}
        {!! Form::suitText('color2', "Color 2<span style='color:#FE2A1A;'> *</span>", $model->exists ? $model->color2:null, ['class' => 'jscolor']) !!}
        -->
        {!! Form::suitText('slug', 'Slug', null, ['info' => 'Must be alphanumeric. Leave blank to automatic generated']) !!}
        {!! Form::suitNumber('order', 'Order') !!}
        {!! Form::suitSwitch('published', 'Published', ['No', 'Yes'], $model->published) !!}
        <!-- @include(suitViewName('_templates._seo-tags')) -->
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection