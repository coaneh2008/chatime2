@extends('admins._layouts.form-base')

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::model($model, ['prefix' => $routePrefix, 'class' => 'form-horizontal']) !!}
    <div class="form-body">
        {!! Form::suitView($model->title, 'Title') !!}
        {!! Form::suitView($model->description, 'Description') !!}
        {!! Form::suitView($model->content?$model->content:'-', 'Content') !!}
        {!! Form::suitView($model->start_date, 'Start Date') !!}
        {!! Form::suitView($model->end_date, 'End Date') !!}
        {!! Form::suitView($model->thumb_image, 'Thumb Image') !!}
        {!! Form::suitView($model->banner_image, 'Banner Image') !!}
        {!! Form::suitView($model->order, 'Order') !!}
        {!! Form::suitView($model->published?'Yes':'No', 'Published') !!}
        {!! Form::suitView($model->created_at, 'Created at') !!}
        {!! Form::suitView($model->updated_at, 'Updated at') !!}
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="fa fa-comments-o font-dark"></i>
                        <span class="caption-subject bold uppercase">User Testimony</span>
                    </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body table-scrollable">
                <table class="table table-striped table-bordered table-hover order-column" id="sample_1">
                    @if(count($testimonies)>0)
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Username</th>
                                <th>Name</th>
                                <th>Testimony</th>
                                <th>Email</th>
                                <th>Telp No.</th>
                                <th>HP No.</th>
                                <th>Submitted at</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($testimonies as $key => $testimony)
                                <tr>
                                    <td>{{ $key+1 }} </td>
                                    <td>{{ $testimony->user->username }}</td>
                                    <td>{{ $testimony->user->name }}</td>
                                    <td>{{ $testimony->comment }}</td>
                                    <td>{{ $testimony->user->email }}</td>
                                    <td>{{ $testimony->user->telephone ?: '-' }}</td>
                                    <td>{{ $testimony->user->cellphone ?: '-' }}</td>
                                    <td>{{ $testimony->created_at }}</td>
                                </tr>
                            @endforeach                 
                        </tbody>
                    @else
                    <div>No user testimony yet</div>
                    @endif
                </table>
            </div>
        </div>
    </div>
    
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@section('page-title')
    View {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i> View {{ $pageName }}
    </li>
@endsection
