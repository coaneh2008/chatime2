<html>
    <tr>
        <td><b>No</b></td>
        <td><b>Name</b></td>
        <td><b>Comment</b></td>
        <td><b>Email</b></td>
        <td><b>Telp No.</b></td>
        <td><b>HP No.</b></td>
        <td><b>Submitted at</b></td>
    </tr>
        @foreach($data as $key => $exportData)
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $exportData->user->name }}</td>
                <td>{{ $exportData->comment }}</td>
                <td>{{ $exportData->user->email }}</td>
                <td>{{ $exportData->user->telephone ?: '-' }}</td>
                <td>{{ $exportData->user->cellphone ?: '-' }}</td>
                <td>{{ $exportData->user->created_at }}</td>
            </tr>
        @endforeach
    <tr></tr>
</html>
