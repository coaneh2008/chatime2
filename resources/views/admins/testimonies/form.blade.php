@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model,array('route'=>($model->exists?[suitRouteName("$routePrefix.update"),$model]:suitRouteName("$routePrefix.store")), 'method'=>($model->exists?'PUT':'POST'))) !!}
    <div class="form-body">
        {!! Form::hidden('type', $type) !!}
        {!! Form::suitText('title', "Title<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitTextarea('description', "Description<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitWysiwyg('content', "Content<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitDate('start_date', "Start Date<span style='color:#FE2A1A;'> *</span>")!!}
        {!! Form::suitDate('end_date', "End Date<span style='color:#FE2A1A;'> *</span>")!!}
        {!! Form::suitFileBrowser('thumb_image', "Thumb Image<span style='color:#FE2A1A;'> *</span>", null, ['info' => 'Must be an image file. Recommended size 250x200']) !!}
        {!! Form::suitFileBrowser('banner_image', "Banner Image<span style='color:#FE2A1A;'> *</span>", null, ['info' => 'Must be an image file. Recommended size 800x275']) !!}
        {!! Form::suitNumber('order', 'Order') !!}
        {!! Form::suitText('slug', 'Slug', null, ['info' => 'Must be alphanumeric. Leave blank to automatic generated']) !!}
        {!! Form::suitSwitch('published', 'Published', ['No', 'Yes'], $model->published) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
