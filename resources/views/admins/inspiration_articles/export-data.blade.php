<html>
    <tr>
        <td><b>No</b></td>
        <td><b>Title</b></td>
        <td><b>Category</b></td>
        <td><b>Thumbnail Image</b></td>
        <td><b>Banner Image</b></td>
        <td><b>Banner Image Type</b></td>
        <td><b>Description</b></td>
        <td><b>Content</b></td>
        <td><b>Author</b></td>
        <td><b>Tags</b></td>
        <td><b>Order</b></td>
        <td><b>Published</b></td>
        <td><b>Published at</b></td>
        <td><b>Is Highlighted</b></td>
        <td><b>Created at</b></td>
        <td><b>Updated at</b></td>
        <td><b>Related Products</b></td>
    </tr>
        @foreach($data as $key => $exportData)
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $exportData->title }}</td>
                <td>{{ $exportData->inspirationArticleCategory->title }}</td>
                <td>{{ $exportData->getAttachment('thumb_image') }}</td>
                <td>{{ $exportData->getAttachment('banner _image') }}</td>
                <td>{{ $exportData->image_type }}</td>
                <td>{{ $exportData->description }}</td>
                <td>{{ strip_tags($exportData->content) }}</td>
                <td>{{ $exportData->user->username }}</td>
                <td>{{ $exportData->getStringTags() }}</td>
                <td>{{ $exportData->order }}</td>
                <td>{{ $exportData->published ? 'Yes' : 'No' }}</td>
                <td>{{ $exportData->published_at }}</td> 
                <td>{{ $exportData->is_highlighted == 1 ? 'Yes' : 'No' }}</td>
                <td>{{ $exportData->created_at }}</td>
                <td>{{ $exportData->updated_at }}</td>
                @foreach($exportData->attachments as $key2 => $attachment)
                <td>{{ "[Product " . ++$key2 . '] ' . $attachment->product->url_to_ace_online }}</td>
                @endforeach
            </tr>
        @endforeach
    <tr></tr>
</html>
