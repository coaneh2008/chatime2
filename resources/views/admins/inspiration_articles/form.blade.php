@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute('menus.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">
        {!! Form::suitSelect('category_id', 'Category', $categories) !!}
        {!! Form::suitText('title', "Title<span style='color:#FE2A1A;'> *</span>") !!}
        <!-- {!! Form::suitFileBrowser('thumb_image', "Thumb Image<span style='color:#FE2A1A;'> *</span>", null, ['info' => 'Must be an image file. Recommended size 770x770']) !!} -->
        {!! Form::suitSwitch('is_highlighted', 'Is Highlighted', ['No', 'Yes'], $model->is_highlighted) !!}
        {!! Form::suitSwitch('is_slider', 'Is Slider', ['No', 'Yes'], $model->is_slider) !!}
        {!! Form::suitSelect('image_type', 'Banner Image Type', $imageTypes) !!}
        {!! Form::suitFileBrowser('banner_image', 'Banner Image', null, ['info' => 'Must be an image file. 
        Recommende size of <br/>
        <b>Desktop</b><br/>
        big image desktop <b>767x643</b> baris ke 1 <br/> 
        small image desktop <b>463x644</b> baris ke 1 kolom 2 dan 3 <br/>
        big image desktop slider kolom 2 dan non slider kolom 1 <b>868x441</b> baris ke 2 <br>
        <b>Mobile</b><br/>
        big image mobile <b>552x701</b> baris ke 1 <br/> 
        big image mobile slider <b>1015x388</b> baris ke 2 <br/> 
         ']) !!}
        {!! Form::suitText('url', 'Url', null, ['info' => 'Url and video is allowed. Video must be a youtube video URL']) !!}
        {!! Form::suitTextarea('description', 'Description') !!}
        {{-- Form::suitWysiwyg('content', 'Content') --}}
        {!! Form::suitSelect('author_id', 'Author', $authors) !!}
        {!! Form::suitText('slug', 'Slug', null, ['info' => 'Must be alphanumeric. Leave blank to automatic generated']) !!}
        {!! Form::suitTokenField('tags', 'Tags', $model->getStringTags(), ['info' => 'Use coma `,` to seperate each word. This tags will be used for getting related articles']) !!}
        {!! Form::suitNumber('order', 'Order') !!}
        {!! Form::suitSelect('device', 'Device', $typeDevice) !!}
        {!! Form::suitDate('published_date', "Published Date <span style='color:#FE2A1A;'> *</span>", $model ? $model->published_date == '0000-00-00' ? '' : $model->published_date : null, ['info' => 'Enter the date when the article is published'])!!}
        {!! Form::suitSwitch('published', 'Published', ['No', 'Yes'], $model->published) !!}
        <!-- {!! Form::suitSection('Attachment') !!} -->
        <div id="attachment-container">
            @foreach ($model->attachments as $i => $attachment)
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box yellow">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-grid"></i>
                                Attachment # {{ ++$i }}
                            </div>
                            <div class="tools">
                                <a href="javascript:void(0)" class="remove"></a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="form-body">
                                {!! Form::hidden("attachments[{$i}][id]", $attachment->getKey())!!}
                                {!! Form::suitTextarea("attachments[{$i}][url_to_ace_online]", 'Url to ACE Online', $attachment->product->url_to_ace_online) !!}
                                {!! Form::suitNumber("attachments[{$i}][order]", 'Order', $attachment->order) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @if (is_array(Input::old('attachments')))
                @foreach(Input::old('attachments') as $i => $attachment)
                    @if (!isset($attachment['id']))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-grid"></i>
                                        Attachment # {{ $i }}
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:void(0)" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="form-body">
                                        {!! Form::suitTextarea("attachments[{$i}][url_to_ace_online]", 'Url to ACE Online') !!}
                                        {!! Form::suitNumber("attachments[{$i}][order]", 'Order') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            @endif
        </div>
        <!-- <a href="#" id="attachment-button" class="btn btn-primary">Add New Attachment</a> -->
    </div>
    <!-- @include(suitViewName('_templates._seo-tags')) -->
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit('Save') !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
@section('page-scripts')
    @parent
    <script type="text/x-template" id="attachment-template">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box yellow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-grid"></i> Attachment # {#}
                        </div>
                        <div class="tools">
                            <a href="javascript:void(0)" class="remove"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-body">
                        {!! Form::suitTextarea("attachments[{#}][url_to_ace_online]", 'Url to ACE Online') !!}
                        {!! Form::suitNumber("attachments[{#}][order]", 'Order') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script type="text/javascript">
        FormField.initTemplate('#attachment-template', '#attachment-container', '#attachment-button');
    </script>
@endsection