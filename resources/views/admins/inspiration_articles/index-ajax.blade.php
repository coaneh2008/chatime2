{
    "draw": {{ \Input::get('draw', 1) }},
    "recordsTotal": {{ $total }},
    "recordsFiltered": {{ $models->total() }},
    "data" : [
        @foreach($models as $key => $model)
        [
            "{{ $model->getKey() }}",
            "{{ stringJson($model->inspirationArticleCategory->title) }}",
            "{{ stringJson($model->title) }}",
            @if ($model->banner_image)
            "{!! sprintf("<img src='%s'>", $model->getThumbnail('banner_image', 'small')) !!}",
            @else
            "-",
            @endif
            "{!! sprintf(
                "<a href='%s' target='blank'>$model->url</a>",
                stringJson($model->url)
            ) !!}",
            "{!! $model->image_type !!}",
            "{{ stringJson(($model->device==0) ? 'Desktop' : 'Mobile') }}",
            "{{ $model->published_date ? \Carbon\Carbon::parse($model->published_date)->format('d M Y') : '-'}}",
            {!! json_encode(Form::suitIndexSwitch('published', $model->published, suitRoute($routePrefix.'.publish', $model))) !!},
            {!! json_encode(Form::suitIndexSwitch('is_highlighted', $model->is_highlighted, suitRoute($routePrefix.'.publish', ['key' => $model, 'field' => 'is_highlighted']))) !!},
            
            {!! json_encode(Form::suitIndexSwitch('is_slider', $model->is_slider, suitRoute($routePrefix.'.publish', ['key' => $model, 'field' => 'is_slider']))) !!},

            "{{ $model->viewer }}",
            "{{ $model->user->username }}",
            "{!! sprintf(
                "<a href='%s' class='btn default btn-xs blue' target='blank'><i class='fa fa-eye'></i> Preview </a><a href='%s' class='btn default btn-xs green'><i class='fa fa-edit'></i> Edit </a><a href='%s' class='btn default btn-xs red delete' data-token='%s'><i class='fa fa-trash-o'></i> Delete </a>",
                 route('frontend.inspiration.show', $model->slug),
                 suitRoute($routePrefix.'.edit', $model),
                 suitRoute($routePrefix.'.destroy', $model),
                 \Session::token()
             )!!}"
        ]{{ ($models->count()-1 === $key) ? '' : ',' }}
        @endforeach
    ]
}
