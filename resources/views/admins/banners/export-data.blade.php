<html>
    <tr>
        <td><b>No</b></td>
        <td><b>Image</b></td>
        <td><b>Type</b></td>
        <td><b>Title</b></td>
        <td><b>Description</b></td>
        <td><b>Video URL</b></td>
        <td><b>Published</b></td>
        <td><b>Published at</b></td>
        <td><b>Order</b></td>
        <td><b>Created at</b></td>
        <td><b>Updated at</b></td>
    </tr>
        @foreach($data as $key => $exportData)
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $exportData->getAttachment('image') }}</td>
                <td>{{ ($exportData->type!=null) ? $exportData->getTypeName() : '-' }}</td>
                <td>{{ $exportData->judul }}</td>
                <td>{{ $exportData->deskripsi }}</td>
                <td>{{ $exportData->video_url }}</td>
                <td>{{ $exportData->published ? 'Yes' : 'No' }}</td>
                <td>{{ $exportData->published_at }}</td>
                <td>{{ $exportData->order }}</td>
                <td>{{ $exportData->created_at }}</td>
                <td>{{ $exportData->updated_at }}</td>
            </tr>
        @endforeach
    <tr></tr>
</html>
