{
    "draw": {{ \Input::get('draw', 1) }},
    "recordsTotal": {{ $total }},
    "recordsFiltered": {{ $models->total() }},
    "data" : [
        @foreach($models as $key => $model)
        [
            "{{ $model->getKey() }}",
            "{!! sprintf("<img src='%s'>", $model->getThumbnail('image', 'small')) !!}",
            "{{ stringJson(($model->type!=null) ? $model->getTypeName() : '-') }}",
            "{{ $model->page}}",
            "{{ stringJson(($model->device==0) ? 'Desktop' : 'Mobile') }}",
            "{{ $model->judul}}",
            "{{ $model->deskripsi}}",
            "{!! sprintf(
                "<a href='%s' target='blank'>$model->video_url</a>",
                stringJson($model->video_url)
            ) !!}",
            {!! json_encode(Form::suitIndexSwitch('published', $model->published, suitRoute($routePrefix.'.publish', $model))) !!},
            "{{ stringJson($model->order) }}",
            "{{ $model->created_at}}",
            "{!! sprintf(
                "<a href='%s' class='btn default btn-xs green'><i class='fa fa-edit'></i> Edit </a><a href='%s' class='btn default btn-xs red delete' data-token='%s'><i class='fa fa-trash-o'></i> Delete </a>",
                 suitRoute($routePrefix.'.edit', $model),
                 suitRoute($routePrefix.'.destroy', $model),
                 \Session::token()
             )!!}"
        ]{{ ($models->count()-1 === $key) ? '' : ',' }}
        @endforeach
    ]
}

