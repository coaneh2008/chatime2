@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model,array('route'=>($model->exists?[suitRouteName("$routePrefix.update"),$model]:suitRouteName("$routePrefix.store")), 'method'=>($model->exists?'PUT':'POST'))) !!}
    <div class="form-body">
        {!! Form::suitFileBrowser('image', "Image <span style='color:#FE2A1A;'> *</span>", null, ['info' => 'Must be an image file. Recommended size For<br/><br/> 
        <b>Desktop</b><br/>
        <b>1100x550</b> pixel <br/><br/>
        <b>Mobile</b><br/> 
        <b>1125x970</b> pixel']) !!}
        {!! Form::suitSelect('type', 'Type', $typeLists) !!}
        {!! Form::suitSelect('page', 'Page', $typePage) !!}
        {!! Form::suitSelect('device', 'Device', $typeDevice) !!}
        {!! Form::suitText('judul', 'Title') !!}
        {!! Form::suitText('deskripsi', 'Description') !!}
        {!! Form::suitText('video_url', 'Url', null, ['info' => 'Url and video is allowed. Video must be a youtube video URL']) !!}

        {!! Form::suitNumber('order', 'Order') !!}
        {!! Form::suitSwitch('published', 'Published', ['No', 'Yes'], $model->published) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
