@extends('admins._layouts.form-base')

@section('page-title')
    Export {{ $pageName }}
@endsection

@section('form-title')
    {{ $pageName }} Export Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitOpen(['method' => 'POST', 'route' => suitRouteName($routePrefix . '.export'), 'enctype' => "multipart/form-data"]) !!}
    <div class="form-body">

        {!! Form::suitSelect('type', 'Export Type', ['xls' => 'Excel', 'csv' => 'CSV']) !!}
        {!! Form::hidden('key', request()->key) !!}

    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
