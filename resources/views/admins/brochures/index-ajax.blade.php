{
    "draw": {{ \Input::get('draw', 1) }},
    "recordsTotal": {{ $total }},
    "recordsFiltered": {{ $models->total() }},
    "data" : [
        @foreach($models as $key => $model)
        [
            "{{ $model->getKey() }}",
            "{{ stringJson($model->title) }}",
            "{{ $model->start_date ? $model->start_date : '-' }}",
            "{{ ($model->end_date && $model->end_date != '0000-00-00' ) ? $model->end_date : '-' }}",
            "{{ $model->order }}",
            "{!! $model->thumb_image?
                sprintf("<img src='%s'>", $model->getThumbnail('thumb_image', 'small'))
                :'-' 
            !!}",
            "{!! $model->file?
                sprintf("<a href='%s' target='blank'>%s</a>", asset($model->file), basename($model->file))
                :'-'
            !!}",
            {!! json_encode(Form::suitIndexSwitch('published', $model->published, suitRoute($routePrefix.'.publish', $model))) !!},
            "{{ $model->created_at}}",
            "{{ $model->updated_at}}",
            "{!! sprintf(
                "<a href='%s' class='btn default btn-xs green'><i class='fa fa-edit'></i> Edit </a><a href='%s' class='btn default btn-xs red delete' data-token='%s'><i class='fa fa-trash-o'></i> Delete </a>",
                 suitRoute($routePrefix.'.edit', $model),
                 suitRoute($routePrefix.'.destroy', $model),
                 \Session::token()
             )!!}"
        ]{{ ($models->count()-1 === $key) ? '' : ',' }}
        @endforeach
    ]
}

