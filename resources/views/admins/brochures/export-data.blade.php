<html>
    <tr>
        <td><b>No</b></td>
        <td><b>Title</b></td>
        <td><b>Thumbnail Image</b></td>
        <td><b>File</b></td>
        <td><b>Order</b></td>
        <td><b>Published</b></td>
        <td><b>Published at</b></td>
        <td><b>Created at</b></td>
        <td><b>Updated at</b></td>
        <td><b>Attachments</b></td>
    </tr>
        @foreach($data as $key => $exportData)
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $exportData->title }}</td>
                <td>{{ $exportData->getAttachment('thumb_image') }}</td>
                <td>{{ $exportData->getAttachment('file') }}</td>
                <td>{{ $exportData->order }}</td>
                <td>{{ $exportData->published ? 'Yes' : 'No' }}</td>
                <td>{{ $exportData->published_at }}</td>
                <td>{{ $exportData->created_at }}</td>
                <td>{{ $exportData->updated_at }}</td>
                @foreach ($exportData->attachments as $key2 => $attachment)
                    <td>{{ "[Image " . ++$key2 . '] ' . $attachment->getAttachment('image') }}</td>
                @endforeach
            </tr>
        @endforeach
    <tr></tr>
</html>
