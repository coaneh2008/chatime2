@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model,array('route'=>($model->exists?[suitRouteName("$routePrefix.update"),$model]:suitRouteName("$routePrefix.store")), 'method'=>($model->exists?'PUT':'POST'))) !!}
    <div class="form-body">
        {!! Form::suitText('title', "Title<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitFileBrowser('thumb_image', "Thumb Image<span style='color:#FE2A1A;'> *</span>", null, ['info' => 'Must be an image file. Recommended size 445x630']) !!}
        {!! Form::suitFileBrowser('file', "File<span style='color:#FE2A1A;'> *</span>", null, ['info' => 'Full brochure file, image and pdf format']) !!}
        {!! Form::suitDate('start_date', "Start Date<span style='color:#FE2A1A;'> *</span>")!!}
        {!! Form::suitDate('end_date', "End Date", $model->end_date == '0000-00-00' ? '' : $model->end_date)!!}
        {!! Form::suitNumber('order', 'Order') !!}
        {!! Form::suitText('slug', 'Slug', null, ['info' => 'Must be alphanumeric. Leave blank to automatic generated']) !!}
        {!! Form::suitSwitch('published', 'Published', ['No', 'Yes'], $model->published) !!}
        
        {!! Form::suitSection('Attachment') !!}
        <div id="attachment-container">
            @foreach ($model->attachments as $i => $attachment)
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box yellow">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-grid"></i>
                                Attachment # {{ ++$i }}
                            </div>
                            <div class="tools">
                                <a href="javascript:void(0)" class="remove"></a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="form-body">
                                {!! Form::hidden("attachments[{$i}][id]", $attachment->getKey())!!}
                                {!! Form::suitFileBrowser("attachments[{$i}][image]", 'Image', $attachment->image, ['info' => 'Must be an image file. Recommended size 500x708']) !!}
                                {!! Form::suitNumber("attachments[{$i}][order]", 'Order', $attachment->order) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @if (is_array(Input::old('attachments')))
                @foreach(Input::old('attachments') as $i => $attachment)
                    @if (!isset($attachment['id']))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-grid"></i>
                                        Attachment # {{ $i }}
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:void(0)" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="form-body">
                                        {!! Form::suitFileBrowser("attachments[{$i}][image]", 'Image', null, ['info' => 'Must be an image file']) !!}
                                        {!! Form::suitNumber("attachments[{$i}][order]", 'Order') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            @endif
        </div>
        <a href="#" id="attachment-button" class="btn btn-primary">Add New Attachment</a>
    </div>
    @include(suitViewName('_templates._seo-tags'))
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
@section('page-scripts')
    @parent
    <script type="text/x-template" id="attachment-template">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box yellow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-grid"></i> Attachment # {#}
                        </div>
                        <div class="tools">
                            <a href="javascript:void(0)" class="remove"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-body">
                        {!! Form::suitFileBrowser("attachments[{#}][image]", 'Image', null, ['info' => 'Must be an image file. Recommended size 500x708']) !!}
                        {!! Form::suitNumber("attachments[{#}][order]", 'Order') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script type="text/javascript">
        FormField.initTemplate('#attachment-template', '#attachment-container', '#attachment-button');
    </script>
@endsection
