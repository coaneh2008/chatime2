@extends('admins._layouts.form-base')
@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::model($model, ['prefix' => $routePrefix, 'class' => 'form-horizontal']) !!}
    <div class="form-body">
        <?php $userRepo = app(App\Repositories\UserRepository::class);?>
        {!! Form::suitSection('Member Data') !!}
        @if ($model->profile_picture)
        {!! Form::suitViewThumb($model->profile_picture, 'Profile Picture') !!}
        @else
        {!! Form::suitView('-', 'Profile Picture') !!}
        @endif
        {!! Form::suitView($model->username, 'Card No.') !!}
        {!! Form::suitView($model->member_id, 'Customer ID') !!}
        {!! Form::suitView($model->name, 'Name') !!}
        {!! Form::suitView($model->email, 'Email') !!}
        {!! Form::suitView($model->telephone?$model->telephone:'-', 'Telephone No.') !!}
        {!! Form::suitView($model->cellphone, 'HP No.') !!}
        {!! Form::suitView($model->identity_card_id, 'KTP/SIM/KITAS/KITAP No.') !!}
        {!! Form::suitView(isset($userRepo->getSexTypes()[intval($model->gender)]) ? $userRepo->getSexTypes()[intval($model->gender)] : '-', 'Gender') !!}
        {!! Form::suitView($model->place_of_birth?$model->place_of_birth:'-', 'Place of birth') !!}
        {!! Form::suitView(($model->date_of_birth != '0000-00-00 00:00:00')?\Carbon\Carbon::parse($model->date_of_birth)->format('d M Y'):'-', 'Date of birth') !!}
        {!! Form::suitView(isset($userRepo->getReligions()[intval($model->religion_id)]) ? $userRepo->getReligions()[intval($model->religion_id)] : '-', 'Religion') !!}
        {!! Form::suitView(isset($userRepo->getMaritalStatus()[intval($model->marital_status)]) ? $userRepo->getMaritalStatus()[intval($model->marital_status)] : '-', 'Marital Status') !!}
        {!! Form::suitView(isset($userRepo->getCitizenships()[intval($model->citizenship_id)]) ? $userRepo->getCitizenships()[intval($model->citizenship_id)] : '-', 'Citizenship') !!}
        {!! Form::suitView(isset($userRepo->getOccupations()[intval($model->occupation_id)]) ? $userRepo->getOccupations()[intval($model->occupation_id)] : '-', 'Occupation') !!}
        {!! Form::suitView($model->shoping_purpose?$model->shoping_purpose:'-', 'Shoping Purpose') !!}
        
        {!! Form::suitSection('Current Address') !!}
        <?php
        $currentCityId = $model->current_address_city_id;
        $currentCityId = str_pad($currentCityId, 4, '0', STR_PAD_LEFT);;
        $currentCity = App\Model\City::where('code', $currentCityId)->first();
        $currentCityName = $currentCity ? $currentCity->name : '-';
        ?>
        {!! Form::suitView($currentCityName, 'Current Address') !!}
        {!! Form::suitView($model->current_address_zip_code?$model->current_address_zip_code:'-', 'Current Address Zip code') !!}
        {!! Form::suitView($model->current_address?$model->current_address:'-', 'Current Address') !!}

        {!! Form::suitSection('KTP Address') !!}
        <?php
        $ktpCityId = $model->ktp_address_city_id;
        $ktpCityId = str_pad($ktpCityId, 4, '0', STR_PAD_LEFT);;
        $ktpCity = App\Model\City::where('code', $ktpCityId)->first();
        $ktpCityName = $ktpCity ? $ktpCity->name : '-';
        ?>
        {!! Form::suitView($ktpCityName, 'KTP Address') !!}
        {!! Form::suitView($model->ktp_address_zip_code?$model->ktp_address_zip_code:'-', 'KTP Address Zip Code') !!}
        {!! Form::suitView($model->ktp_address?$model->ktp_address:'-', 'KTP Address') !!}

        {!! Form::suitSection('Administration') !!}
        {!! Form::suitView(isset($userRepo->getAdministrationTypes()[intval($model->administration_type)]) ? $userRepo->getAdministrationTypes()[intval($model->administration_type)] : '-', 'Administration') !!}
        {!! Form::suitView($model->is_renewal?'Yes':'No', 'Renewal') !!}
        <?php
            $refName = '';
            if ($model->referal_id) {
                $refCode = App\Model\ReferalCode::where('code', $model->referal_id)->first();
                if ($refCode) {
                    $user = App\Model\User::find($refCode->user_id);
                    $refName = ' (' . $user->name . ')';
                }
            }
        ?>
        {!! Form::suitView($model->referal_id . $refName, 'Referal Code') !!}
        {!! Form::suitView($model->expiry_date?\Carbon\Carbon::parse($model->expiry_date)->format('d M Y'):'-', 'Expiry Date') !!}
        {!! Form::suitView(\Carbon\Carbon::parse($model->created_at)->format('d M Y H:m:s'), 'Register at') !!}
    </div>
    
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@section('page-title')
    View {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i> View {{ $pageName }}
    </li>
@endsection
