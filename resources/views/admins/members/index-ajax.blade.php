{
    "draw": {{ \Input::get('draw', 1) }},
    "recordsTotal": {{ $total }},
    "recordsFiltered": {{ $models->total() }},
    "data" : [
        @foreach($models as $key => $model)
        [
            "{{ $model->getKey() }}",
            "{{ ($model->member_id) }}",
            "{{ ($model->name) }}",
            "{{ stringJson($model->email) }}",
            "{{ $model->created_at}}",
            "{{ $model->updated_at}}",
            "{!! sprintf(
                "<div class='text-center'><a href='%s' class='btn default btn-xs blue'><i class='fa fa-eye'></i> Show </a>",
                 suitRoute($routePrefix.'.show', $model)
             )!!}"
        ]{{ ($models->count()-1 === $key) ? '' : ',' }}
        @endforeach
    ]
}
