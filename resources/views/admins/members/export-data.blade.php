<html>
    <tr>
        <td><b>Profile Picture</b></td>
        <td><b>Card No.</b></td>
        <td><b>Customer ID</b></td>
        <td><b>Name</b></td>
        <td><b>Email</b></td>
        <td><b>Telephone No.</b></td>
        <td><b>HP No.</b></td>
        <td><b>KTP/SIM/KITAS/KITAP No.</b></td>
        <td><b>Gender</b></td>
        <td><b>Place of birth</b></td>
        <td><b>Date of birth</b></td>
        <td><b>Religion</b></td>
        <td><b>Marital Status</b></td>
        <td><b>Citizenship</b></td>
        <td><b>Occupation</b></td>
        <td><b>Shoping Purpose</b></td>
        <td><b>Current Address City</b></td>
        <td><b>Current Address</b></td>
        <td><b>Current Address Zip code</b></td>
        <td><b>KTP Address City</b></td>
        <td><b>KTP Address</b></td>
        <td><b>KTP Address Zip Code</b></td>
        <td><b>Administration</b></td>
        <td><b>Renewal</b></td>
        <td><b>Referal Code</b></td>
        <td><b>Expiry Date</b></td>
        <td><b>Register at</b></td>
    </tr>
    <?php $userRepo = app(App\Repositories\UserRepository::class);?>
        @foreach($data as $key => $exportData)
            <tr>
                <td>{{ $exportData->profile_picture ? $exportData->getAttachment('profile_picture') : '-' }}</td>
                <td>{{ $exportData->username }}</td>
                <td>{{ $exportData->member_id }}</td>
                <td>{{ $exportData->name }}</td>
                <td>{{ $exportData->email }}</td>
                <td>{{ $exportData->telephone?$exportData->telephone:'-' }}</td>
                <td>{{ $exportData->cellphone }}</td>
                <td>{{ $exportData->identity_card_id }}</td>
                <td>{{ isset($userRepo->getSexTypes()[intval($exportData->gender)]) ? $userRepo->getSexTypes()[intval($exportData->gender)] : '-' }}</td>
                <td>{{ $exportData->place_of_birth }}</td>
                <td>{{ $exportData->date_of_birth }}</td>
                <td>{{ isset($userRepo->getReligions()[intval($exportData->religion_id)]) ? $userRepo->getReligions()[intval($exportData->religion_id)] : '-' }}</td>
                <td>{{ isset($userRepo->getMaritalStatus()[intval($exportData->marital_status)]) ? $userRepo->getMaritalStatus()[intval($exportData->marital_status)] : '-' }}</td>
                <td>{{ isset($userRepo->getCitizenships()[intval($exportData->citizenship_id)]) ? $userRepo->getCitizenships()[intval($exportData->citizenship_id)] : '-' }}</td>
                <td>{{ isset($userRepo->getOccupations()[intval($exportData->occupation_id)]) ? $userRepo->getOccupations()[intval($exportData->occupation_id)] : '-' }}</td>
                <td>{{ $exportData->shoping_purpose }}</td>
                <?php
                $currentCityId = $exportData->current_address_city_id;
                $currentCityId = str_pad($currentCityId, 4, '0', STR_PAD_LEFT);;
                $currentCity = App\Model\City::where('code', $currentCityId)->first();
                $currentCityName = $currentCity ? $currentCity->name : '-';
                ?>
                <td>{{ $currentCityName}}</td>
                <td>{{ $exportData->current_address_zip_code }}</td>
                <td>{{ $exportData->current_address }}</td>
                <?php
                $ktpCityId = $exportData->ktp_address_city_id;
                $ktpCityId = str_pad($ktpCityId, 4, '0', STR_PAD_LEFT);;
                $ktpCity = App\Model\City::where('code', $ktpCityId)->first();
                $ktpCityName = $ktpCity ? $ktpCity->name : '-';
                ?>
                <td>{{ $ktpCityName }}</td>
                <td>{{ $exportData->ktp_address_zip_code }}</td>
                <td>{{ $exportData->ktp_address }}</td>
                <td>{{ isset($userRepo->getAdministrationTypes()[intval($exportData->administration_type)]) ? $userRepo->getAdministrationTypes()[intval($exportData->administration_type)] : '-' }}</td>
                <td>{{ $exportData->is_renewal?'Yes':'No' }}</td>
                <?php
                    $refName = '';
                    if ($exportData->referal_id) {
                        $refCode = App\Model\ReferalCode::where('code', $exportData->referal_id)->first();
                        if ($refCode) {
                            $user = App\Model\User::find($refCode->user_id);
                            $refName = ' (' . $user->name . ')';
                        }
                    }
                ?>
                <td>{{ $exportData->referal_id . $refName }}</td>
                <td>{{ $exportData->expiry_date?\Carbon\Carbon::parse($exportData->expiry_date)->format('d M Y'):'-' }}</td>
                <td>{{ \Carbon\Carbon::parse($exportData->created_at)->format('d M Y H:m:s') }}</td>
            </tr>
        @endforeach
    <tr></tr>
</html>
