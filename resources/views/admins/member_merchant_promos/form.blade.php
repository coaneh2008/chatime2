@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">
        {!! Form::hidden('type', $type) !!}
        {!! Form::suitText('title', "Title<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitText('slug', 'Slug', null, ['info' => 'Must be alphanumeric. Leave blank to automatic generated']) !!}
        {!! Form::suitWysiwyg('description', "Banner Description<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitFileBrowser('banner_image', "Banner Image<span style='color:#FE2A1A;'> *</span>", null, ['info' => 'Must be an image file. Recommended size 1100x500']) !!}
        {!! Form::suitFileBrowser('thumb_image', "Thumbnail Image<span style='color:#FE2A1A;'> *</span>", null, ['info' => 'Must be an image file. Recommended size 200x200']) !!}
        {!! Form::suitNumber('order', 'Order') !!}
        {!! Form::suitSwitch('published', 'Published', ['No', 'Yes'], $model->published) !!}
        {!! Form::suitSection('Merchant Promos') !!}
        <div id="member-promo-container">
            @foreach ($model->merchantPromos as $i => $merchantPromo)
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box yellow">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-grid"></i>
                                Merchant Promo # {{ ++$i }}
                            </div>
                            <div class="tools">
                                <a href="javascript:void(0)" class="remove"></a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="form-body">
                                {!! Form::hidden("merchant_promos[{$i}][id]", $merchantPromo->getKey())!!}
                                {!! Form::suitText("merchant_promos[{$i}][title]", "Title<span style='color:#FE2A1A;'> *</span>", $merchantPromo->title) !!}
                                {!! Form::suitText("merchant_promos[{$i}][merchant_name]", "Merchant Name<span style='color:#FE2A1A;'> *</span>", $merchantPromo->merchant_name) !!}
                                {!! Form::suitSelect("merchant_promos[{$i}][merchant_category_id]", 'Category', $categories, null, $merchantPromo->merchant_category_id) !!}
                                {!! Form::suitMultiSelect2Window("merchant_promos[{$i}][locations][]", 'Locations', $locations, null, $merchantPromo->locations()->get()->pluck('id')->toArray()) !!}
                                {!! Form::suitTextarea("merchant_promos[{$i}][description]", "Description<span style='color:#FE2A1A;'> *</span>", $merchantPromo->description) !!}
                                {!! Form::suitWysiwyg("merchant_promos[{$i}][content]", "Content<span style='color:#FE2A1A;'> *</span>", $merchantPromo->content) !!}
                                {!! Form::suitFileBrowser("merchant_promos[{$i}][thumb_image]", "Thumbnail Image<span style='color:#FE2A1A;'> *</span>", $merchantPromo->thumb_image) !!}
                                {!! Form::suitFileBrowser("merchant_promos[{$i}][banner_image]", "Banner Image<span style='color:#FE2A1A;'> *</span>", $merchantPromo->banner_image) !!}
                                {!! Form::suitDate("merchant_promos[{$i}][start_date]", "Start Date<span style='color:#FE2A1A;'> *</span>", $merchantPromo->start_date) !!}
                                {!! Form::suitDate("merchant_promos[{$i}][end_date]", "End Date<span style='color:#FE2A1A;'> *</span>", $merchantPromo->end_date) !!}
                                {!! Form::suitSwitch("merchant_promos[{$i}][published]", 'Published', ['No', 'Yes'], $merchantPromo->published) !!}
                                {!! Form::suitNumber("merchant_promos[{$i}][order]", 'Order', $merchantPromo->order) !!}
                                {!! Form::suitSwitch("merchant_promos[{$i}][is_highlighted]", 'Is Highlighted', ['No', 'Yes'], $merchantPromo->is_highlighted) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @if (is_array(Input::old("merchant_promos")))
                @foreach(Input::old("merchant_promos") as $i => $merchantPromo)
                    @if (!isset($merchantPromo['id']))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-grid"></i>
                                        Merchant Promo # {{ $i }}
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:void(0)" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="form-body">
                                        {!! Form::suitText("merchant_promos[{$i}][title]", "Title<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitText("merchant_promos[{$i}][merchant_name]", "Merchant Name<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitSelect("merchant_promos[{$i}][merchant_category_id]", 'Category', $categories) !!}
                                        {!! Form::suitMultiSelect2WindowValidation("merchant_promos[{$i}][locations][]", 'Locations', $locations, null,  array_map('intval', array_get(old('merchant_promos')[$i], 'locations', [])))!!}
                                        {!! Form::suitTextarea("merchant_promos[{$i}][description]", "Description<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitWysiwyg("merchant_promos[{$i}][content]", "Content<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitFileBrowser("merchant_promos[{$i}][thumb_image]", "Thumbnail Image<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitFileBrowser("merchant_promos[{$i}][banner_image]", "Banner Image<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitDate("merchant_promos[{$i}][start_date]", "Start Date<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitDate("merchant_promos[{$i}][end_date]", "End Date<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitSwitch("merchant_promos[{$i}][published]", 'Published', ['No', 'Yes']) !!}
                                        {!! Form::suitNumber("merchant_promos[{$i}][order]", 'Order') !!}
                                        {!! Form::suitSwitch("merchant_promos[{$i}][is_highlighted]", 'Is Highlighted', ['No', 'Yes']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            @endif
        </div>
        <a href="#" id="member-promo-button" class="btn btn-primary">Add New Merchant Promo</a>
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@section('page-scripts')
    @parent
    <script type="text/x-template" id="member-promo-template">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box yellow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-grid"></i> Merchant Promo # {#}
                        </div>
                        <div class="tools">
                            <a href="javascript:void(0)" class="remove"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-body">
                        {!! Form::suitText("merchant_promos[{#}][title]", "Title<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitText("merchant_promos[{#}][merchant_name]", "Merchant Name<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitSelect("merchant_promos[{#}][merchant_category_id]", 'Category', $categories) !!}
                        {!! Form::suitMultiSelect2Window("merchant_promos[{#}][locations][]", 'Locations', $locations) !!}
                        {!! Form::suitTextarea("merchant_promos[{#}][description]", "Description<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitWysiwyg("merchant_promos[{#}][content]", "Content<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitFileBrowser("merchant_promos[{#}][thumb_image]", "Thumbnail Image<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitFileBrowser("merchant_promos[{#}][banner_image]", "Banner Image<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitDate("merchant_promos[{#}][start_date]", "Start Date<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitDate("merchant_promos[{#}][end_date]", "End Date<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitSwitch("merchant_promos[{#}][published]", 'Published', ['No', 'Yes']) !!}
                        {!! Form::suitNumber("merchant_promos[{#}][order]", 'Order') !!}
                        {!! Form::suitSwitch("merchant_promos[{#}][is_highlighted]", 'Is Highlighted', ['No', 'Yes']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script type="text/javascript">
        FormField.initTemplate('#member-promo-template', '#member-promo-container', '#member-promo-button');
    </script>
@endsection
