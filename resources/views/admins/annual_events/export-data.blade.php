<html>
    <tr>
        <td><b>No</b></td>
        <td><b>Title</b></td>
        <td><b>Description</b></td>
        <td><b>Content</b></td>
        <td><b>Start Date</b></td>
        <td><b>End Date</b></td>
        <td><b>Total Participant</b></td>
        <td><b>Thumb Image</b></td>
        <td><b>Banner Image</b></td>
        <td><b>Order</b></td>
        <td><b>Published</b></td>
        <td><b>Published at</b></td>
        <td><b>Created at</b></td>
        <td><b>Updated at</b></td>
    </tr>
        @foreach($data as $key => $exportData)
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $exportData->title }}</td>
                <td>{{ strip_tags($exportData->description) }}</td>
                <td>{{ strip_tags($exportData->content) }}</td>
                <td>{{ $exportData->start_date }}</td>
                <td>{{ $exportData->end_date }}</td>
                <td>{{ $exportData->total_participant }}</td>
                <td>{{ $exportData->getAttachment('thumb_image') }}</td>
                <td>{{ $exportData->getAttachment('banner_image') }}</td>
                <td>{{ $exportData->order }}</td>
                <td>{{ $exportData->published ? 'Yes' : 'No' }}</td>
                <td>{{ $exportData->published_at }}</td>
                <td>{{ $exportData->created_at }}</td>
                <td>{{ $exportData->updated_at }}</td>
            </tr>
        @endforeach
    <tr></tr>
</html>
