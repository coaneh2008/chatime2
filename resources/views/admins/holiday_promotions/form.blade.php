@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model,array('route'=>($model->exists?[suitRouteName("$routePrefix.update"),$model]:suitRouteName("$routePrefix.store")), 'method'=>($model->exists?'PUT':'POST'))) !!}
    <div class="form-body">
        {!! Form::hidden('category_id', $categoryId) !!}
        {!! Form::suitWysiwyg('content', "Content<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitDateTime('start_date', "Start Date<span style='color:#FE2A1A;'> *</span>")!!}
        {!! Form::suitDateTime('end_date', "End Date<span style='color:#FE2A1A;'> *</span>")!!}
        {!! Form::suitFileBrowser('file', 'File', null, ['info' => 'Promotion brochure file, image and pdf format is allowed']) !!}
        {!! Form::suitSelect('type', 'Type', $typeList) !!}
        {!! Form::suitText('btn_name', 'Button Name') !!}
        {!! Form::suitText('url', 'URL') !!}
        {!! Form::suitSwitch('published', 'Published', ['No', 'Yes'], $model->published) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

