{
    "draw": {{ \Input::get('draw', 1) }},
    "recordsTotal": {{ $total }},
    "recordsFiltered": {{ $models->total() }},
    "data" : [
        @foreach($models as $key => $model)
        [
            "{{ $model->getKey() }}",
            "{{ stringJson($model->rank) }}",
            "{{ stringJson($model->member_card_id) }}",
            "{{ stringJson($model->name) }}",
            "{!! ($model->amount) ? "<center>" . number_format($model->amount, 0, ",", ".") . "</center>" : '-' !!}",
            "{!! ($model->transaction) ? "<center>" . number_format($model->transaction, 0, ",", ".") . "</center>" : '-' !!}",
            "{!! ($model->quantity) ? "<center>" . number_format($model->quantity, 0, ",", ".") . "</center>" : '-' !!}",
            "{{ $model->created_at}}",
            "{{ $model->updated_at}}",
            "{!! sprintf(
                "<div class='text-center'><a href='%s' class='btn default btn-xs red delete' data-token='%s'><i class='fa fa-trash-o'></i> Delete </a></div>",
                 suitRoute($routePrefix.'.destroy', $model),
                 \Session::token()
             )!!}"
        ]{{ ($models->count()-1 === $key) ? '' : ',' }}
        @endforeach
    ]
}
