@extends('admins._layouts.index-base')

@section('page-title')
    {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i> {{ $pageName }}
    </li>
@endsection

@section('table-title')
    {{ $pageName }} Table
@endsection

@section('page-header-toolbar')
    <div class="btn-group">
        <a href="{{ suitRoute($routePrefix.'.export') }}" class="btn btn-sm btn-success">
            <i class="glyphicon glyphicon-download"></i> Export
        </a>
        <a href="{{ suitRoute($routePrefix.'.import') }}" class="btn btn-sm btn-primary">
            <i class="glyphicon glyphicon-upload"></i> Import
        </a>
        <a href="{{ suitRoute($routePrefix.'.template') }}" class="btn btn-sm btn-warning">
            <i class="glyphicon glyphicon-download"></i> Download Template
        </a>
        <a href="{{ suitRoute($routePrefix.'.delete-all') }}" class="btn btn-sm red" onClick='return confirmationBox("delete all data")'>
            <i class="fa fa-trash-o"></i> Delete All Data
        </a>
    </div>
@endsection

@section('table-column-checkbox')
    <label><input type="checkbox" data-name="id">#</label>
    <label><input type="checkbox" checked data-name="rank">Rank</label>
    <label><input type="checkbox" checked data-name="member_card_id">Member Card ID</label>
    <label><input type="checkbox" checked data-name="name">Name</label>
    <label><input type="checkbox" checked data-name="amount">Amount</label>
    <label><input type="checkbox" checked data-name="transaction">Transaction</label>
    <label><input type="checkbox" checked data-name="quantity">Quantity</label>
    <label><input type="checkbox" checked data-name="created_at">Created At</label>
    <label><input type="checkbox" data-name="updated_at">Updated At</label>
@endsection

@section('page-scripts')
    @parent
    <script type="text/javascript">
        function confirmationBox(message) {
            if(confirm('Are you sure you want to '+ message +'?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
@endsection

