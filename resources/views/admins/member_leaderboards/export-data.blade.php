<html>
    <tr>
        <td><b>Rank</b></td>
        <td><b>Member Card ID</b></td>
        <td><b>Name</b></td>
        <td><b>Amount (Rp)</b></td>
        <td><b>Transaction (Frequency)</b></td>
        <td><b>Quantity (Pieces)</b></td>
    </tr>
        @foreach($data as $key => $exportData)
            <tr>
                <td>{{ $exportData->rank }}</td>
                <td>{{ $exportData->member_card_id }}</td>
                <td>{{ $exportData->name }}</td>
                <td>{{ $exportData->amount }}</td>
                <td>{{ $exportData->transaction }}</td>
                <td>{{ $exportData->quantity }}</td>
            </tr>
        @endforeach
    <tr></tr>
</html>
