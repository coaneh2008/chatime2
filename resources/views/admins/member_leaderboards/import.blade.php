@extends('admins._layouts.form-base')

@section('page-title')
    Import {{ $pageName }}
@endsection

@section('form-title')
    {{ $pageName }} Import Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitOpen(['method' => 'POST', 'route' => suitRouteName($routePrefix . '.import'), 'enctype' => "multipart/form-data"]) !!}
    <div class="form-body">

        {!! Form::suitFileInput('file', 'File Import') !!}

    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
