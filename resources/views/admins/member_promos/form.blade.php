@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">
        {!! Form::hidden('type', $type) !!}
        {!! Form::suitText('title', "Title<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitText('slug', 'Slug', null, ['info' => 'Must be alphanumeric. Leave blank to automatic generated']) !!}
        {!! Form::suitWysiwyg('description', "Banner Description<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitFileBrowser('banner_image', "Banner Image<span style='color:#FE2A1A;'> *</span>", null, ['info' => 'Must be an image file. Recommended size 1100x500']) !!}
        {!! Form::suitFileBrowser('thumb_image', "Thumbnail Image<span style='color:#FE2A1A;'> *</span>", null, ['info' => 'Must be an image file. Recommended size 200x200']) !!}
        {!! Form::suitNumber('order', 'Order') !!}
        {!! Form::suitSwitch('published', 'Published', ['No', 'Yes'], $model->published) !!}
        
        {!! Form::suitSection('Member Promos') !!}
        <div id="member-promo-container">
            @foreach ($model->memberPromos as $i => $memberPromo)
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box yellow">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-grid"></i>
                                Member Promo # {{ ++$i }}
                            </div>
                            <div class="tools">
                                <a href="javascript:void(0)" class="remove"></a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="form-body">
                                {!! Form::hidden("member_promos[{$i}][id]", $memberPromo->getKey())!!}
                                {!! Form::suitText("member_promos[{$i}][title]", "Title<span style='color:#FE2A1A;'> *</span>", $memberPromo->title) !!}
                                {!! Form::suitWysiwyg("member_promos[{$i}][content]", "Content<span style='color:#FE2A1A;'> *</span>", $memberPromo->content) !!}
                                {!! Form::suitFileBrowser("member_promos[{$i}][thumb_image]", "Thumbnail Image<span style='color:#FE2A1A;'> *</span>", $memberPromo->thumb_image) !!}
                                {!! Form::suitFileBrowser("member_promos[{$i}][banner_image]", "Banner Image<span style='color:#FE2A1A;'> *</span>", $memberPromo->banner_image) !!}
                                {!! Form::suitDate("member_promos[{$i}][start_date]", "Start Date<span style='color:#FE2A1A;'> *</span>", $memberPromo->start_date) !!}
                                {!! Form::suitDate("member_promos[{$i}][end_date]", "End Date<span style='color:#FE2A1A;'> *</span>", $memberPromo->end_date) !!}
                                {!! Form::suitSwitch("member_promos[{$i}][published]", 'Published', ['No', 'Yes'], $memberPromo->published) !!}
                                {!! Form::suitNumber("member_promos[{$i}][order]", 'Order', $memberPromo->order) !!}
                                {!! Form::suitSwitch("member_promos[{$i}][is_highlighted]", 'Is Highlighted', ['No', 'Yes'], $memberPromo->is_highlighted) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @if (is_array(Input::old("member_promos")))
                @foreach(Input::old("member_promos") as $i => $memberPromo)
                    @if (!isset($memberPromo['id']))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-grid"></i>
                                        Member Promo # {{ $i }}
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:void(0)" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="form-body">
                                        {!! Form::suitText("member_promos[{$i}][title]", "Title<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitWysiwyg("member_promos[{$i}][content]", "Content<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitFileBrowser("member_promos[{$i}][thumb_image]", "Thumbnail Image<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitFileBrowser("member_promos[{$i}][banner_image]", "Banner Image<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitDate("member_promos[{$i}][start_date]", "Start Date<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitDate("member_promos[{$i}][end_date]", "End Date<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitSwitch("member_promos[{$i}][published]", 'Published', ['No', 'Yes']) !!}
                                        {!! Form::suitNumber("member_promos[{$i}][order]", 'Order') !!}
                                        {!! Form::suitSwitch("member_promos[{$i}][is_highlighted]", 'Is Highlighted', ['No', 'Yes']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            @endif
        </div>
        <a href="#" id="member-promo-button" class="btn btn-primary">Add New Member Promo</a>
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@section('page-scripts')
    @parent
    <script type="text/x-template" id="member-promo-template">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box yellow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-grid"></i> Member Promo # {#}
                        </div>
                        <div class="tools">
                            <a href="javascript:void(0)" class="remove"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-body">
                        {!! Form::suitText("member_promos[{#}][title]", "Title<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitWysiwyg("member_promos[{#}][content]", "Content<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitFileBrowser("member_promos[{#}][thumb_image]", "Thumbnail Image<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitFileBrowser("member_promos[{#}][banner_image]", "Banner Image<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitDate("member_promos[{#}][start_date]", "Start Date<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitDate("member_promos[{#}][end_date]", "End Date<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitSwitch("member_promos[{#}][published]", 'Published', ['No', 'Yes']) !!}
                        {!! Form::suitNumber("member_promos[{#}][order]", 'Order') !!}
                        {!! Form::suitSwitch("member_promos[{#}][is_highlighted]", 'Is Highlighted', ['No', 'Yes']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script type="text/javascript">
        FormField.initTemplate('#member-promo-template', '#member-promo-container', '#member-promo-button');
    </script>
@endsection
