@extends('admins._layouts.login')

@section('content')
<div id="login" class="tab-pane active">
    {!! Form::open(['action'=>'Admin\RemindersController@postReset', 'method'=>'post', 'class'=>'login-form']) !!}
        {!! Form::hidden('token', $token) !!}
        <h3 class="form-title">{{ suitTrans('messages.reset_header') }}</h3>
        @include('admins._includes.flash')
        @if(Session::has('errors'))
        <div class="alert alert-danger">
          <a class="close" data-close="alert"></a>
          <span> {{ Session::get('errors')->first() }}</span>
        </div>
        @endif
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Enter Your Email" name="email" value="{{ \Input::old('email') }}"/>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password Confirmation</label>
            <input type="text" class="form-control form-control-solid placeholder-no-fix" type="password" name="password_confirmation" placeholder="Enter Password Confirmation"value="">
        </div>
        <div class="form-actions">
            <input type="submit" class="btn btn-success uppercase" value="{{ suitTrans('views.submit') }}"/>
        </div>
    {!! Form::close() !!}
</div>
@endsection
