@extends('_layouts.admin')

@section('page-content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    @section('form-title')
                        <i class="fa fa-grid"></i> Form
                    @show
                </div> <!-- /.caption -->

                <div class="tools">
                    @yield('form-tools')
                </div> <!-- /.tools -->
            </div> <!-- /.portlet-title -->

            <div class="portlet-body form">
                @yield('form-body')
            </div> <!-- /.portlet-body -->
        </div> <!-- /.portlet -->
    </div>
</div>
@endsection

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/bootstrap-fileinput/bootstrap-fileinput.css')}}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/select2/select2.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/bootstrap-datepicker/css/datepicker.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/bootstrap-switch/css/bootstrap-switch.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/colorbox/theme/colorbox.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/jquery-multi-select/css/multi-select.css') }}"/>
@endsection

@section('page-scripts')
    <script src="{{ asset('assets/admin/vendor/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/moment.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/bootstrap-switch/js/bootstrap-switch.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/colorbox/jquery.colorbox-min.js') }}"></script>
    <script src="{{ asset('packages/barryvdh/elfinder/js/standalonepopup.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/jquery-multi-select/js/jquery.multi-select.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key={{ env('GOOGLE_MAPS_KEY', 'AIzaSyBYb5C-35s0hW5oh1D2qVRzMFW5nQXlyT8') }}"> </script>

    <script src="{{ asset('assets/admin/js/form.js') }}"></script>
    <script src="{{ asset('assets/admin/js/jscolor.js') }}"></script>
@endsection

@section('init-scripts')
    @parent
    FormField.init();
@endsection
