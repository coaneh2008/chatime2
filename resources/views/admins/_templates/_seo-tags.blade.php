{!! Form::suitSection('SEO Tags Form') !!}
{!! Form::suitText('page_type', 'Page Type', $model->getPageTypeName(), ['disabled']) !!}
{!! Form::suitText('seo_title', "SEO Title<span style='color:#FE2A1A;'> *</span>", $model->getSeoTitle()) !!}
{!! Form::suitTextarea('seo_description', "SEO Description<span style='color:#FE2A1A;'> *</span>", $model->getSeoDescription()) !!}
{!! Form::suitText('opengraph_title', 'Opengraph Title', $model->getOpengraphTitle()) !!}
{!! Form::suitTextarea('opengraph_description', 'Opengraph Description', $model->getOpengraphDescription()) !!}
{!! Form::suitText('twitter_title', 'Twitter Title', $model->getTwitterTitle()) !!}
{!! Form::suitTextarea('twitter_description', 'Twitter Description', $model->getTwitterDescription()) !!}
{!! Form::suitText('meta_image', 'Meta Image', $model->getMetaImage(), ['info' => 'Please insert image URL']) !!}