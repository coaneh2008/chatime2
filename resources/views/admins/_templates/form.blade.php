@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">

        @include(suitViewName('_templates._translate-header'))

        {!! Form::suitSection('My First Section') !!}
        {!! Form::suitText('text', 'Text') !!}
        {!! Form::suitPassword('Password', 'password') !!}
        {!! Form::suitNumber('number', 'Number') !!}
        {!! Form::suitTextarea('textarea', 'Text Area') !!}
        {!! Form::suitSelect('select', 'Select With Placeholder', ['Select 1', 'Select 2'], 'Placeholder') !!}
        {!! Form::suitSelect('select', 'Select Without Placeholder', ['Select 1', 'Select 2']) !!}
        {!! Form::suitMultiSelect('multiselect[]', 'Multiselect', [ 1=>'Select 1', 2=>'Select 2', 'Group' => [3=>'Select 3', 4=>'Select 4']])!!}
        {!! Form::suitMultiSelect2Window('multiselect[]', 'Multiselect 2', [ 1=>'Select 1', 2=>'Select 2', 3=>'Select 3', 4=>'Select 5'])!!}
        {!! Form::suitTokenField('token', 'TokenField') !!}
        {!! Form::suitTimePicker('timepicker', 'TimePicker') !!}
        {!! Form::suitDate('datepicker', 'DatePicker') !!}
        {!! Form::suitDateRange('datepicker_from', 'datepicker_to', 'DatePickerRange') !!}
        {!! Form::suitFileInput('fileinput', 'File Input') !!}
        {!! Form::suitFileBrowser('filebrowser', 'File Browser')!!}
        {!! Form::suitWysiwyg('wysiwyg', 'WYSIWYG') !!}
        {!! Form::suitSwitch('switch', 'Switch', false, [0, 1], ['data-on-text' => 'Yes', 'data-off-text' => 'No']) !!}
        {{-- Location bebas ditaruh dimana saja --}}
        <div class="tab-content" >
          @foreach (config('suitcms.lang') as $lang => $value)
              <div role="tabpanel" class="tab-pane {{ ($lang == config('app.fallback_locale'))?'fade in active':'fade' }}" id="{{ $lang }}">
                {!! Form::suitText("{$lang}[title]", 'Title') !!}
                {!! Form::suitWysiwyg('wysiwyg', 'WYSIWYG') !!}
              </div>
          @endforeach
        </div>
        {!! Form::suitMap('map', 'Map') !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
