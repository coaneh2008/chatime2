<html>
    <tr>
        <td><b>No</b></td>
        <td><b>Title</b></td>
        <td><b>Label</b></td>
        <td><b>Value</b></td>
        <td><b>Point</b></td>
        <td><b>Content</b></td>
        <td><b>Order</b></td>
        <td><b>Published</b></td>
        <td><b>Published at</b></td>
        <td><b>Created at</b></td>
        <td><b>Updated at</b></td>
    </tr>
        @foreach($data as $key => $exportData)
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $exportData->title }}</td>
                <td>{{ $exportData->label }}</td>
                <td>{{ $exportData->value }}</td>
                <td>{{ $exportData->point }}</td>
                <td>{{ strip_tags($exportData->content) }}</td>
                <td>{{ $exportData->order }}</td>
                <td>{{ $exportData->published ? 'Yes' : 'No' }}</td>
                <td>{{ $exportData->published_at }}</td>
                <td>{{ $exportData->created_at }}</td>
                <td>{{ $exportData->updated_at }}</td>
            </tr>
        @endforeach
    <tr></tr>
</html>
