<select class="multi-select" multiple="1" name="{{ $name }}">
	@foreach($lists as $key => $list)
		<option value="{{ $key }}" {{ in_array($key, $selected) ? 'selected' : '' }}>{{ $list }}</option>
	@endforeach
</select>