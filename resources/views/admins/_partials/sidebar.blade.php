@extends('admins._layouts.sidebar')

@section('sidebar-menus')
<li{!! $routePrefix == 'banners' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('banners.index') }}">
        <i class="icon-picture"></i>
        <span class="title">Banner</span>
    </a>
</li>
<li{!! 
        $routePrefix == 'inspiration-articles' || $routePrefix == 'inspiration-article-categories'  ? ' class="active"' : '' 
    !!}
>    
    <a href="javascript:;">
        <i class="icon-picture"></i>
        <span class="title">Sub Banner</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <!-- <li{!! $routePrefix == 'inspiration-article-categories' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('inspiration-article-categories.index') }}">
                <i class="icon-list"></i>
                Banner Category
            </a>
        </li> -->
        <li{!! $routePrefix == 'inspiration-articles' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('inspiration-articles.index') }}">
                <i class="icon-envelope-open"></i>
                Banner
            </a>
        </li>
    </ul>
</li>
<li{!! $routePrefix == 'home-infos' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('home-infos.index') }}">
        <i class="icon-list"></i>
        Info Home
    </a>
</li>
<li{!! $routePrefix == 'content-abouts' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('content-abouts.index') }}">
        <i class="icon-picture"></i>
        Content About
    </a>
</li>
<!-- <li{!! $routePrefix == 'new-events' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('new-events.index') }}">
        <i class="icon-calendar"></i>
        News Event
    </a>
</li> -->

<li{!! 
        $routePrefix == 'new-events' || $routePrefix == 'new-event-categories'  ? ' class="active"' : '' 
    !!}
>    
    <a href="javascript:;">
        <i class="icon-calendar"></i>
        <span class="title">News Event</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <li{!! $routePrefix == 'new-event-categories' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('new-event-categories.index') }}">
                <i class="icon-list"></i>
                News Event Category
            </a>
        </li>
        <li{!! $routePrefix == 'new-events' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('new-events.index') }}">
                <i class="icon-calendar"></i>
                List New Event
            </a>
        </li>
    </ul>
</li>


<!--
<li{!! $routePrefix == 'flash-stickers' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('flash-stickers.index') }}">
        <i class="fa fa-flash"></i>
        Flash Sticker
    </a>
</li> -->
<li{!! 
        in_array($routePrefix, ['promotion-categories', 'holiday-promotions','thematic-promotions', 'promotion-informations', 'member-promotions', 'flash-sales', 'product-promotions', 'home-promotions']) ? ' class="active"' : '' 
    !!}
>
    <a href="javascript:;">
        <i class="fa fa-bullhorn"></i>
        <span class="title">Promotion</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <li{!! $routePrefix == 'promotion-categories' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('promotion-categories.index') }}">
                <i class="icon-list"></i>
                Promotion Category
            </a>
        </li>
        <!--
         <li{!! $routePrefix == 'flash-sales' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('flash-sales.index') }}">
                <i class="fa fa-flash"></i>
                Flash Sales
            </a>
        </li>
        <li{!! $routePrefix == 'holiday-promotions' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('holiday-promotions.index') }}">
                <i class="icon-calendar"></i>
                Holiday Promotion
            </a>
        </li>
         {{-- <li{!! $routePrefix == 'member-promotions' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('member-promotions.index') }}">
                <i class="icon-users"></i>
                Member Promotion
            </a>
        </li> --}}
        <li{!! $routePrefix == 'promotion-informations' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('promotion-informations.index') }}">
                <i class="icon-info"></i>
                Promotion Information
            </a>
        </li>
        <li{!! $routePrefix == 'product-promotions' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('product-promotions.index') }}">
                <i class="fa fa-codepen"></i>
                Product Promotion
            </a>
        </li> -->
        <li{!! $routePrefix == 'home-promotions' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('home-promotions.index') }}">
                <i class="icon-picture"></i>
                Home Promotion
            </a>
        </li>
        <!-- <li{!! $routePrefix == 'thematic-promotions' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('thematic-promotions.index') }}">
                <i class="icon-picture"></i>
                Thematic Promotion
            </a>
        </li> -->
    </ul>
</li>



<li{!! 
        $routePrefix == 'product-menus' || $routePrefix == 'product-categories'  ? ' class="active"' : '' 
    !!}
>    
    <a href="javascript:;">
        <i class="icon-star"></i>
        <span class="title">Product Menu</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <li{!! $routePrefix == 'new-event-categories' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('product-categories.index') }}">
                <i class="icon-list"></i>
                Product Category
            </a>
        </li>
        <li{!! $routePrefix == 'product-menus' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('product-menus.index') }}">
                <i class="icon-star"></i>
                List Product
            </a>
        </li>
    </ul>
</li>


<!-- <li{!! $routePrefix == 'brochures' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('brochures.index') }}">
        <i class="fa fa-newspaper-o"></i>
        Brochures
    </a>
</li> -->
{{-- <li{!! $routePrefix == 'instagram-posts' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('instagram-posts.index') }}">
        <i class="fa fa-instagram"></i>
        Instagram Post
    </a>
</li> --}}
<li{!! 
        $routePrefix == 'regions' || $routePrefix == 'store-locations'  ? ' class="active"' : '' 
    !!}
>    
    <a href="javascript:;">
        <i class="fa fa-map-marker"></i>
        <span class="title">Store Location</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <li{!! $routePrefix == 'regions' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('regions.index') }}">
                <i class="fa fa-globe"></i>
                Region
            </a>
        </li>
        <li{!! $routePrefix == 'store-locations' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('store-locations.index') }}">
                <i class="fa fa-map-marker"></i>
                Store Location
            </a>
        </li>
    </ul>
</li>
<!-- <li{!! $routePrefix == 'inbox' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('inbox.index') }}">
        <i class="fa fa-comments"></i>
        <span class="title">Inbox</span>
    </a>
</li> -->
<li{!! $routePrefix == 'subscribers' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('subscribers.index') }}">
        <i class="icon-envelope"></i>
        <span class="title">Subscribers</span>
    </a>
</li>
<!-- <li class="heading">
    <h3 class="uppercase"><strong>Membership</strong></h3>
</li>
<li{!!
        in_array($routePrefix, ['annual-events', 'free-memberships', 'member-get-members', 'testimonies']) ? ' class="active"' : ''
    !!}
>
    <a href="javascript:;">
        <i class="icon-calendar"></i>
        <span class="title">Events</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <li{!! $routePrefix == 'annual-events' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('annual-events.index') }}">
                <i class="icon-calendar"></i>
                Annual Event
            </a>
        </li>
        <li{!! $routePrefix == 'free-memberships' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('free-memberships.index') }}">
                <i class="fa fa-user"></i>
                Free Membership
            </a>
        </li>
        <li{!! $routePrefix == 'member-get-members' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('member-get-members.index') }}">
                <i class="fa fa-users"></i>
                Member Get Member
            </a>
        </li>
        <li{!! $routePrefix == 'testimonies' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('testimonies.index') }}">
                <i class="fa fa-comments"></i>
                Testimony
            </a>
        </li>
    </ul>
</li> -->
<!-- <li{!!
        in_array($routePrefix, ['member-free-contents', 'member-promos', 'member-news-updates', 'member-merchant-categories', 'member-merchant-locations', 'member-merchant-promos']) ? ' class="active"' : ''
    !!}
>
    <a href="javascript:;">
        <i class="fa fa-check"></i>
        <span class="title">Benefits</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <li{!! $routePrefix == 'member-free-contents' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('member-free-contents.index') }}">
                <i class="fa fa-ellipsis-h"></i>
                Free Content
            </a>
        </li>
        <li{!! $routePrefix == 'member-promos' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('member-promos.index') }}">
                <i class="fa fa-tag"></i>
                Member Promo
            </a>
        </li>
        <li{!! $routePrefix == 'member-news-updates' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('member-news-updates.index') }}">
                <i class="fa fa-globe"></i>
                Member News
            </a>
        </li>
        <li{!! in_array($routePrefix, ['member-merchant-categories', 'member-merchant-locations', 'member-merchant-promos']) ? ' class="active"' : ''
        !!}>
            <a href="javascript:;">
                <i class="fa fa-bookmark"></i>
                <span class="title">Merchant Promo</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li{!! $routePrefix == 'member-merchant-categories' ? ' class="active"' : '' !!}>
                    <a href="{{ suitRoute('member-merchant-categories.index') }}">
                        <i class="fa fa-th-list"></i>
                        Category
                    </a>
                </li>
                <li{!! $routePrefix == 'member-merchant-locations' ? ' class="active"' : '' !!}>
                    <a href="{{ suitRoute('member-merchant-locations.index') }}">
                        <i class="fa fa-map-marker"></i>
                        Location
                    </a>
                </li>
                <li{!! $routePrefix == 'member-merchant-promos' ? ' class="active"' : '' !!}>
                    <a href="{{ suitRoute('member-merchant-promos.index') }}">
                        <i class="fa fa-tags"></i>
                        Merchant Promo
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</li>
<li{!! $routePrefix == 'vouchers' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('vouchers.index') }}">
        <i class="fa fa-tags"></i>
        Voucher
    </a>
</li>
<li{!! $routePrefix == 'merchandises' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('merchandises.index') }}">
        <i class="fa fa-bars"></i>
        Merchandise
    </a>
</li>
<li{!! $routePrefix == 'member-leaderboards' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('member-leaderboards.index') }}">
        <i class="fa fa-trophy"></i>
        Leaderboard
    </a>
</li>
<li{!! $routePrefix == 'membership-settings' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('membership-settings.index') }}">
        <i class="icon-settings"></i>
        <span class="title">Membership Setting</span>
    </a>
</li> -->
<!-- <li class="heading">
    <h3 class="uppercase"><strong>User Management</strong></h3>
</li>
<li{!! $routePrefix == 'users' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('users.index') }}">
        <i class="icon-user"></i>
        <span class="title">Admin</span>
    </a>
</li>
<li{!! $routePrefix == 'members' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('members.index') }}">
        <i class="fa fa-users"></i>
        <span class="title">Member</span>
    </a>
</li> -->
<!-- <li class="heading">
    <h3 class="uppercase"><strong>Preferences</strong></h3>
</li>
<li{!!
        in_array($routePrefix, ['products', 'seo-tags']) ? ' class="active"' : ''
    !!}
>
    <a href="javascript:;">
        <i class="fa fa-th"></i>
        <span class="title">Master Data</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <li{!! $routePrefix == 'products' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('products.index') }}">
                <i class="fa fa-life-saver"></i>
                Product
            </a>
        </li>
        <li{!! $routePrefix == 'seo-tags' ? ' class="active"' : '' !!}>
            <a href="{{ suitRoute('seo-tags.index') }}">
                <i class="fa fa-tag"></i>
                SEO Tag
            </a>
        </li>
    </ul>
</li> -->
<!-- <li{!! $routePrefix == 'menus' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('menus.index') }}">
        <i class="icon-directions"></i>
        <span class="title">Menu</span>
    </a>
</li> -->
<li{!! $routePrefix == 'pages' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('pages.index') }}">
        <i class="icon-grid"></i>
        <span class="title">Page</span>
    </a>
</li>
<!-- <li{!! $routePrefix == 'html-templates' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('html-templates.index') }}">
        <i class="fa fa-html5"></i>
        <span class="title">Html Template</span>
    </a>
</li>
<li{!! $routePrefix == 'redirections' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('redirections.index') }}">
        <i class="fa fa-angle-double-right"></i>
        <span class="title">Redirection</span>
    </a>
</li> -->
@endsection
