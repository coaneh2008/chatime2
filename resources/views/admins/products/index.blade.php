@extends('admins._layouts.index-base')

@section('page-title')
    {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i> {{ $pageName }}
    </li>
@endsection

@section('table-title')
    {{ $pageName }} Table
@endsection

@section('table-column-checkbox')
    <label><input type="checkbox" data-name="id">#</label>
    <label><input type="checkbox" checked data-name="url_to_ace_online">Url to ACE Online</label>
    <label><input type="checkbox" checked data-name="title">Title</label>
    <label><input type="checkbox" checked data-name="brand_id">Brand</label>
    <label><input type="checkbox" checked data-name="description">Description</label>
    <label><input type="checkbox" checked data-name="image">Image</label>
    <label><input type="checkbox" checked data-name="currency">Currency</label>
    <label><input type="checkbox" checked data-name="price">Price</label>
    <label><input type="checkbox" checked data-name="is_available">Availability</label>
    <label><input type="checkbox" data-name="created_at">Created At</label>
    <label><input type="checkbox" data-name="updated_at">Updated At</label>
@endsection
