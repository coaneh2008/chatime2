@extends('admins._layouts.form-base')
@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::model($model, ['prefix' => $routePrefix, 'class' => 'form-horizontal']) !!}
    <div class="form-body">
        <?php $userRepo = app(App\Repositories\UserRepository::class);?>
        {!! Form::suitView($model->url_to_ace_online, 'URL to ACE Online') !!}
        {!! Form::suitView($model->title, 'Title') !!}
        {!! Form::suitView($model->brand_id ? $model->brand->title : '-', 'Brand') !!}
        {!! Form::suitView($model->description, 'Description') !!}
        @if ($model->image)
            {!! Form::suitViewThumb($model->getThumbnail('image', 'medium-2'), 'Product Image') !!}
        @else
            {!! Form::suitView('-', 'Product Image') !!}
        @endif
        {!! Form::suitView($model->currency, 'Currency') !!}
        {!! Form::suitView($model->price ? number_format($model->price, 2, ',', '.') : '-', 'Price') !!}
        {!! Form::suitView($model->is_available ? $model->getAvailability() : '-', 'Availability') !!}
        {!! Form::suitView(\Carbon\Carbon::parse($model->created_at)->format('d M Y H:m:s'), 'Created At') !!}
        {!! Form::suitView(\Carbon\Carbon::parse($model->updated_at_at)->format('d M Y H:m:s'), 'Update At') !!}
    </div>
    
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@section('page-title')
    View {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i> View {{ $pageName }}
    </li>
@endsection
