{
    "draw": {{ \Input::get('draw', 1) }},
    "recordsTotal": {{ $total }},
    "recordsFiltered": {{ $models->total() }},
    "data" : [
        @foreach($models as $key => $model)
        [
            "{{ $model->getKey() }}",
            "{{ stringJson($model->url_to_ace_online) }}",
            "{{ $model->title ? stringJson($model->title) : '-' }}",
            "{{ $model->brand_id ? $model->brand->title : '-' }}",
            "{{ $model->description ? stringJson($model->description) : '-' }}",
            "{!! sprintf("<img src='%s'>", $model->getThumbnail('image', 'small')) !!}",
            "{{ $model->currency ? stringJson($model->currency) : '-' }}",
            "{{ $model->price ? number_format($model->price, 2, ',', '.') : '-' }}",
            "{{ $model->is_available ? $model->getAvailability() : '-' }}",
            "{{ $model->created_at}}",
            "{{ $model->updated_at}}",
            "{!! sprintf(
                "<a href='%s' class='btn default btn-xs green'><i class='fa fa-eye'></i> Show </a>",
                 suitRoute($routePrefix.'.show', $model)
             )!!}"
        ]{{ ($models->count()-1 === $key) ? '' : ',' }}
        @endforeach
    ]
}

