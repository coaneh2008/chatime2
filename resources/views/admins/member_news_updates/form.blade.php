@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">
        {!! Form::hidden('type', $type) !!}
        {!! Form::suitText('title', "Title<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitText('slug', 'Slug', null, ['info' => 'Must be alphanumeric. Leave blank to automatic generated']) !!}
        {!! Form::suitWysiwyg('description', "Banner Description<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitFileBrowser('banner_image', "Banner Image<span style='color:#FE2A1A;'> *</span>", null, ['info' => 'Must be an image file. Recommended size 1100x500']) !!}
        {!! Form::suitFileBrowser('thumb_image', "Thumbnail Image<span style='color:#FE2A1A;'> *</span>", null, ['info' => 'Must be an image file. Recommended size 200x200']) !!}
        {!! Form::suitNumber('order', 'Order') !!}
        {!! Form::suitSwitch('published', 'Published', ['No', 'Yes'], $model->published) !!}
        
        {!! Form::suitSection('Member News Update') !!}
        <div id="member-news-container">
            @foreach ($model->newsUpdates as $i => $memberNews)
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box yellow">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-grid"></i>
                                Member News Update # {{ ++$i }}
                            </div>
                            <div class="tools">
                                <a href="javascript:void(0)" class="remove"></a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="form-body">
                                {!! Form::hidden("member_news_updates[{$i}][id]", $memberNews->getKey())!!}
                                {!! Form::suitText("member_news_updates[{$i}][title]", "Title<span style='color:#FE2A1A;'> *</span>", $memberNews->title) !!}
                                {!! Form::suitWysiwyg("member_news_updates[{$i}][content]", "Content<span style='color:#FE2A1A;'> *</span>", $memberNews->content) !!}
                                {!! Form::suitFileBrowser("member_news_updates[{$i}][thumb_image]", "Thumbnail Image<span style='color:#FE2A1A;'> *</span>", $memberNews->thumb_image) !!}
                                {!! Form::suitFileBrowser("member_news_updates[{$i}][banner_image]", "Banner Image<span style='color:#FE2A1A;'> *</span>", $memberNews->banner_image) !!}
                                {!! Form::suitDate("member_news_updates[{$i}][start_date]", "Start Date<span style='color:#FE2A1A;'> *</span>", $memberNews->start_date) !!}
                                {!! Form::suitDate("member_news_updates[{$i}][end_date]", "End Date<span style='color:#FE2A1A;'> *</span>", $memberNews->end_date) !!}
                                {!! Form::suitSwitch("member_news_updates[{$i}][published]", 'Published', ['No', 'Yes'], $memberNews->published) !!}
                                {!! Form::suitNumber("member_news_updates[{$i}][order]", 'Order', $memberNews->order) !!}
                                {!! Form::suitSwitch("member_news_updates[{$i}][is_highlighted]", 'Is Highlighted', ['No', 'Yes'], $memberNews->is_highlighted) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @if (is_array(Input::old("member_news_updates")))
                @foreach(Input::old("member_news_updates") as $i => $memberPromo)
                    @if (!isset($memberPromo['id']))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-grid"></i>
                                        Member News Update # {{ $i }}
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:void(0)" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="form-body">
                                        {!! Form::suitText("member_news_updates[{$i}][title]", "Title<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitWysiwyg("member_news_updates[{$i}][content]", "Content<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitFileBrowser("member_news_updates[{$i}][thumb_image]", "Thumbnail Image<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitFileBrowser("member_news_updates[{$i}][banner_image]", "Banner Image<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitDate("member_news_updates[{$i}][start_date]", "Start Date<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitDate("member_news_updates[{$i}][end_date]", "End Date<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitSwitch("member_news_updates[{$i}][published]", 'Published', ['No', 'Yes']) !!}
                                        {!! Form::suitNumber("member_news_updates[{$i}][order]", 'Order') !!}
                                        {!! Form::suitSwitch("member_news_updates[{$i}][is_highlighted]", 'Is Highlighted', ['No', 'Yes']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            @endif
        </div>
        <a href="#" id="member-news-button" class="btn btn-primary">Add New Member News</a>
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@section('page-scripts')
    @parent
    <script type="text/x-template" id="member-news-template">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box yellow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-grid"></i> Member News Update # {#}
                        </div>
                        <div class="tools">
                            <a href="javascript:void(0)" class="remove"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-body">
                        {!! Form::suitText("member_news_updates[{#}][title]", "Title<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitWysiwyg("member_news_updates[{#}][content]", "Content<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitFileBrowser("member_news_updates[{#}][thumb_image]", "Thumbnail Image<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitFileBrowser("member_news_updates[{#}][banner_image]", "Banner Image<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitDate("member_news_updates[{#}][start_date]", "Start Date<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitDate("member_news_updates[{#}][end_date]", "End Date<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitSwitch("member_news_updates[{#}][published]", 'Published', ['No', 'Yes']) !!}
                        {!! Form::suitNumber("member_news_updates[{#}][order]", 'Order') !!}
                        {!! Form::suitSwitch("member_news_updates[{#}][is_highlighted]", 'Is Highlighted', ['No', 'Yes']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script type="text/javascript">
        FormField.initTemplate('#member-news-template', '#member-news-container', '#member-news-button');
    </script>
@endsection
