@extends('admins._layouts.index-base')

@section('page-title')
    {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i> {{ $pageName }}
    </li>
@endsection

@section('table-title')
    {{ $pageName }} Table
@endsection

@section('page-header-toolbar')
    <div class="btn-group pull-right">
        <a href="{{ suitRoute($routePrefix.'.create') }}" class="btn btn-sm btn-primary">
            <i class="glyphicon glyphicon-plus"></i> Add New
        </a>
    </div>
@endsection

@section('table-column-checkbox')
    <label><input type="checkbox" data-name="id">#</label>
    <label><input type="checkbox" checked data-name="title">Title</label>
    <label><input type="checkbox" checked data-name="icon_image">Icon Image</label>
    <label><input type="checkbox" checked data-name="type">Type</label>
    <label><input type="checkbox" checked data-name="parent.title">Parent</label>
    <label><input type="checkbox" checked data-name="url">Url</label>
    <label><input type="checkbox" checked data-name="order">Order</label>
    <label><input type="checkbox" data-name="updated_at">Updated Date</label>
    <label><input type="checkbox" data-name="created_at">Created Date</label>
@endsection
