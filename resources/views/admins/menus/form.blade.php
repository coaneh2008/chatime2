@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute('menus.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">
        @include(suitViewName('_templates._translate-header'))
        {!! Form::suitSelect('parent_id', 'Parent', $parents) !!}

        <div class="tab-content" >
          @foreach (config('suitcms.lang') as $lang => $value)
              <div role="tabpanel" class="tab-pane {{ ($lang == config('app.fallback_locale'))?'fade in active':'fade' }}" id="{{ $lang }}">
                {!! Form::suitText("{$lang}[title]", "Title") !!}
              </div>
          @endforeach
        </div>
        {!! Form::suitFileBrowser('icon_image', 'Icon Image', null, ['info' => 'Must be an image file']) !!}
        {!! Form::suitSelect('type', 'Type', $types) !!}
        {!! Form::suitSelect('is_link', 'Is Link', ['No', 'Yes'])!!}
        {!! Form::suitText('url', "Url<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitNumber('order', "Order<span style='color:#FE2A1A;'> *</span>") !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
