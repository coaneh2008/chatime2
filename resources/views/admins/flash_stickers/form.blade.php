@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model,array('route'=>($model->exists?[suitRouteName("$routePrefix.update"),$model]:suitRouteName("$routePrefix.store")), 'method'=>($model->exists?'PUT':'POST'))) !!}
    <div class="form-body">
        {!! Form::suitTextarea('content', "Content<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitTextarea('url', "Url<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitText('color', "Color<span style='color:#FE2A1A;'> *</span>", $model->exists ? $model->color:null, ['class' => 'jscolor']) !!}
        {!! Form::suitDateTime('start_date', "Start Date<span style='color:#FE2A1A;'> *</span>")!!}
        {!! Form::suitDateTime('end_date', "End Date<span style='color:#FE2A1A;'> *</span>")!!}
        {!! Form::suitNumber('order', 'Order') !!}
        {!! Form::suitSwitch('published', 'Published', ['No', 'Yes'], $model->published) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

