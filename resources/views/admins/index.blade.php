@extends('_layouts.admin')

@section('page-title')
    Dashboard
@endsection

@section('page-content')
	<div class="row">
        @foreach($dashboardData as $key => $data)
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-light {{ $data['color'] }}-soft" href="{{ suitRoute("$key.index") }}">
            <div class="visual">
                <i class="{{ $data['icon'] }}"></i>
            </div>
            <div class="details">
                <div class="number">
                     {{ $data['count'] }}
                </div>
                <div class="desc">
                     Total {{ $data['name'] }}
                </div>
            </div>
            </a>
        </div>
        @endforeach
    </div>
@endsection
