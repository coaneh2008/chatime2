@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute('menus.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">
        {!! Form::suitText('title', "Title<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitTextarea('description', 'Description') !!}
        {!! Form::suitWysiwyg('content', 'Content') !!}
        {!! Form::suitFileBrowser('banner_image', 'Banner Image', null, ['info' => 'Must be an image file. 
        Recommended size <br><br><b>Desktop</b><br/> <b>834x767</b> pixel <br/><br/> <b>Mobile</b><br/> <b>1111x479</b> Pixel']) !!}
        {!! Form::suitText('url_to_ace_online', 'Url') !!}
        {!! Form::suitText('slug', 'Slug', null, ['info' => 'Must be alphanumeric. Leave blank to automatic generated']) !!}
        {!! Form::suitNumber('order', 'Order') !!}
        {!! Form::suitSelect('device', 'Device', $typeDevice) !!}
        {!! Form::suitSwitch('published', 'Published', ['No', 'Yes'], $model->published) !!}
        {!! Form::suitSwitch('is_slider', 'Is Slider', ['No', 'Yes'], $model->is_slider) !!}
        <!-- @include(suitViewName('_templates._seo-tags')) -->
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection