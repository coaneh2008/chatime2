{
    "draw": {{ \Input::get('draw', 1) }},
    "recordsTotal": {{ $total }},
    "recordsFiltered": {{ $models->total() }},
    "data" : [
        @foreach($models as $key => $model)
        [
            "{{ $model->getKey() }}",
            "{{ stringJson($model->title) }}",
            "{{ stringJson($model->description) }}",
            "{!! sprintf("<img src='%s'>", $model->getThumbnail('banner_image', 'icon')) !!}",
            "{!! $model->url_to_ace_online?sprintf("<a href='%s'>%s</a>", $model->url_to_ace_online, $model->url_to_ace_online):'-' !!}",
            {!! json_encode(Form::suitIndexSwitch('published', $model->published, suitRoute($routePrefix.'.publish', $model))) !!},
            {!! json_encode(Form::suitIndexSwitch('is_slider', $model->is_slider, suitRoute($routePrefix.'.publish', ['key' => $model, 'field' => 'is_slider']))) !!},
            "{{ stringJson(($model->device==0) ? 'Desktop' : 'Mobile') }}",
            "{{ $model->order }}",
            "{{ stringJson(strip_tags($model->description)) }}",
            "{{ stringJson(strip_tags($model->content)) }}",
            "{{ stringJson($model->slug) }}",
            "{{ $model->created_at}}",
            "{{ $model->updated_at}}",
            "{!! sprintf(
                "<a href='%s' class='btn default btn-xs green'><i class='fa fa-edit'></i> Edit </a><a href='%s' class='btn default btn-xs red delete' data-token='%s'><i class='fa fa-trash-o'></i> Delete </a>",
                 suitRoute($routePrefix.'.edit', $model),
                 suitRoute($routePrefix.'.destroy', $model),
                 \Session::token()
             )!!}"
        ]{{ ($models->count()-1 === $key) ? '' : ',' }}
        @endforeach
    ]
}
