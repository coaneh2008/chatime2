<html>
    <tr>
        <td><b>No</b></td>
        <td><b>Parent</b></td>
        <td><b>Type</b></td>
        <td><b>Title</b></td>
        <td><b>Description</b></td>
        <td><b>Content</b></td>
        <td><b>Thumbnail Image</b></td>
        <td><b>Sidebar Image</b></td>
        <td><b>Banner Image</b></td>
        <td><b>URL to Ace Online</b></td>
        <td><b>Slug</b></td>
        <td><b>Order</b></td>
        <td><b>Published</b></td>
        <td><b>Published at</b></td>
        <td><b>Created at</b></td>
        <td><b>Updated at</b></td>
    </tr>
        @foreach($data as $key => $exportData)
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $exportData->parent ? $exportData->parent->title : '-' }}</td>
                <td>{{ $exportData->type }}</td>
                <td>{{ $exportData->title }}</td>
                <td>{{ $exportData->description }}</td>
                <td>{{ strip_tags($exportData->content) }}</td>
                <td>{{ $exportData->getAttachment('thumb_image') }}</td>
                <td>{{ $exportData->getAttachment('sidebar_image') }}</td>
                <td>{{ $exportData->getAttachment('banner_image') }}</td>
                <td>{{ $exportData->url_to_ace_online }}</td>
                <td>{{ $exportData->slug }}</td>
                <td>{{ $exportData->order }}</td>
                <td>{{ $exportData->published ? 'Yes' : 'No' }}</td>
                <td>{{ $exportData->published_at }}</td>
                <td>{{ $exportData->created_at }}</td>
                <td>{{ $exportData->updated_at }}</td>
            </tr>
        @endforeach
    <tr></tr>
</html>
