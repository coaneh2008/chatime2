@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute('menus.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">
        {!! Form::suitText('title', "Title<span style='color:#FE2A1A;'> *</span>") !!}
        {!! Form::suitTextarea('description', 'Description') !!}
        {!! Form::suitWysiwyg('content', 'Content') !!}
        {!! Form::suitFileBrowser('banner_image', 'Banner Image', null, ['info' => 'Must be an image file. 
        Recommended size <br>
        1. Tentang Chatime  : <b>1125x967</b><br/>
        2. Chatime Indo Hadir  : <b>743x654</b><br/>
        3. Social Media Square : <b>291x290</b><br/> 
        4. Main Reason : <b>1080x1080</b><br/>
        5. Our Team : <b>1730x549</b><br/>

        ']) !!}
        {!! Form::suitText('url_to_ace_online', 'Url') !!}
        {!! Form::suitText('slug', 'Slug', null, ['info' => 'Must be alphanumeric. Leave blank to automatic generated']) !!}
        {!! Form::suitNumber('order', 'Order') !!}
        {!! Form::suitSwitch('published', 'Published', ['No', 'Yes'], $model->published) !!}
        
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection