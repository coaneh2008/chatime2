@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute('pages.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">
        @include(suitViewName('_templates._translate-header'))
        {!! Form::suitSelect('parent_id', 'Parent', $parents) !!}
        {!! Form::suitSelect('layout', 'Page Layout', $layouts) !!}
        {!! Form::suitText('url_prefix', 'Link Prefix') !!}
        {!! Form::suitText('slug', 'Slug', null, ['info' => 'Must be alphanumeric. Leave blank to automatic generated']) !!}
        <div class="tab-content" >
          @foreach (config('suitcms.lang') as $lang => $value)
              <div role="tabpanel" class="tab-pane {{ ($lang == config('app.fallback_locale'))?'fade in active':'fade' }}" id="{{ $lang }}">
                {!! Form::suitText("{$lang}[title]", "Title<span style='color:#FE2A1A;'> *</span>") !!}
                {!! Form::suitTextarea("{$lang}[description]", 'Description') !!}
                {!! Form::suitWysiwyg("{$lang}[content]", "Content<span style='color:#FE2A1A;'> *</span>") !!}
              </div>
          @endforeach
        </div>
        {!! Form::suitSwitch('published', 'Published', ['No', 'Yes'], $model->published) !!}
        {!! Form::suitTokenField('tags', 'Tags', $model->getStringTags(), ['info' => 'Use coma `,` to seperate each word']) !!}
        {!! Form::suitSection('Attachment') !!}
        <div id="attachment-container">
            @foreach ($model->attachments as $i => $attachment)
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box yellow">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-grid"></i>
                                Attachment # {{ ++$i }}
                            </div>
                            <div class="tools">
                                <a href="javascript:void(0)" class="remove"></a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="form-body">
                                {!! Form::hidden("attachments[{$i}][id]", $attachment->getKey())!!}
                                {!! Form::suitText("attachments[{$i}][name]", "Name<span style='color:#FE2A1A;'> *</span>", $attachment->name) !!}
                                {!! Form::suitFileBrowser("attachments[{$i}][uri]", "Uri<span style='color:#FE2A1A;'> *</span>", $attachment->uri) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @if (is_array(Input::old('attachments')))
                @foreach(Input::old('attachments') as $i => $attachment)
                    @if (!isset($attachment['id']))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-grid"></i>
                                        Attachment # {{ $i }}
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:void(0)" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="form-body">
                                        {!! Form::suitText("attachments[{$i}][name]", "Name<span style='color:#FE2A1A;'> *</span>") !!}
                                        {!! Form::suitFileBrowser("attachments[{$i}][uri]", "Uri<span style='color:#FE2A1A;'> *</span>") !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            @endif
        </div>
        <a href="#" id="attachment-button" class="btn btn-primary">Add New Attachment</a>
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@section('page-scripts')
    @parent
    <script type="text/x-template" id="attachment-template">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box yellow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-grid"></i> Attachment # {#}
                        </div>
                        <div class="tools">
                            <a href="javascript:void(0)" class="remove"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-body">
                        {!! Form::suitText("attachments[{#}][name]", "Name<span style='color:#FE2A1A;'> *</span>") !!}
                        {!! Form::suitFileBrowser("attachments[{#}][uri]", "Uri<span style='color:#FE2A1A;'> *</span>") !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script type="text/javascript">
        FormField.initTemplate('#attachment-template', '#attachment-container', '#attachment-button');
    </script>
@endsection
