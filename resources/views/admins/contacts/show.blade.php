@extends(suitViewName($viewPrefix.'.form'))

@section('page-title')
    View {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i> View {{ $pageName }}
    </li>
@endsection
