@extends('admins._layouts.form-base')

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::model($model, ['prefix' => $routePrefix, 'class' => 'form-horizontal']) !!}
    <div class="form-body">
        {!! Form::suitView($model->name, 'Name') !!}
        {!! Form::suitView($model->address, 'Address') !!}
        {!! Form::suitView($model->email, 'Email') !!}
        {!! Form::suitView($model->phone, 'Phone') !!}
        {!! Form::suitView($model->message, 'Message') !!}
        {!! Form::suitView($model->created_at, 'Created at') !!}
        {!! Form::suitView($model->updated_at, 'Updated at') !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

