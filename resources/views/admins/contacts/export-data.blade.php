<html>
    <tr>
        <td><b>No</b></td>
        <td><b>Name</b></td>
        <td><b>Address</b></td>
        <td><b>Email</b></td>
        <td><b>Phone</b></td>
        <td><b>Message</b></td>
        <td><b>Created at</b></td>
        <td><b>Updated at</b></td>
    </tr>
        @foreach($data as $key => $exportData)
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $exportData->name }}</td>
                <td>{{ $exportData->address }}</td>
                <td>{{ $exportData->email }}</td>
                <td>{{ $exportData->phone }}</td>
                <td>{{ $exportData->message }}</td>
                <td>{{ $exportData->created_at }}</td>
                <td>{{ $exportData->updated_at }}</td>
            </tr>
        @endforeach
    <tr></tr>
</html>
