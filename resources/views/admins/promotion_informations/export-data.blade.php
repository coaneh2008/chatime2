<html>
    <tr>
        <td><b>No</b></td>
        <td><b>Content</b></td>
        <td><b>Start date</b></td>
        <td><b>End date</b></td>
        <td><b>File</b></td>
        <td><b>Published</b></td>
        <td><b>Published at</b></td>
        <td><b>Created at</b></td>
        <td><b>Updated at</b></td>
    </tr>
        @foreach($data as $key => $exportData)
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{ strip_tags($exportData->content) }}</td>
                <td>{{ $exportData->start_date }}</td>
                <td>{{ $exportData->end_date }}</td>
                <td>{{ $exportData->file ? asset($exportData->file) : '-' }}</td>
                <td>{{ $exportData->published ? 'Yes' : 'No' }}</td>
                <td>{{ $exportData->published_at }}</td>
                <td>{{ $exportData->created_at }}</td>
                <td>{{ $exportData->updated_at }}</td>
            </tr>
        @endforeach
    <tr></tr>
</html>
