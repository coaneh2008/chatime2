@extends('admins._layouts.index-base')

@section('page-title')
    {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i> {{ $pageName }}
    </li>
@endsection

@section('table-title')
    {{ $pageName }} Table
@endsection

@section('table-column-checkbox')
    <label><input type="checkbox" data-name="id">#</label>
    <label><input type="checkbox" checked data-name="image">Image</label>
    <label><input type="checkbox" data-name="video">Video</label>
    <label><input type="checkbox" checked data-name="caption">Caption</label>
    <label><input type="checkbox" data-name="location">Location</label>
    <label><input type="checkbox" checked data-name="type">Type</label>
    <label><input type="checkbox" checked data-name="url">Url</label>
    <label><input type="checkbox" checked data-name="total_like">Like</label>
    <label><input type="checkbox" checked data-name="total_comment">Comment</label>
    <label><input type="checkbox" checked data-name="created_at">Created At</label>
    <label><input type="checkbox" data-name="updated_at">Updated At</label>
@endsection
