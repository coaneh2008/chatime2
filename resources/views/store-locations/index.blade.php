

@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')

        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <!-- <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>{{ trans('store-location.title') }}</li>
                    </ul> -->

                    <h2 class="home-section-heading">{{ trans('store-location.title') }} </h2>

                    <div class="store-location" id="storeLocation" data-stores="{{ URL::to('/store-locations/map-data') }}" ng-app="StoreMap" ng-controller="StoreMapController as smc">
                        <div class="store-location-map"></div>

                        <div class="store-location-filter">
                            <div class="container">
                                <div class="store-location-regions">
                                    <span>{{ trans('store-location.by_region') }}</span>
                                    <button class="store-location-regions-toggle" ng-click="smc.toggleRegionsList()" ng-class="{
                                        'is-active': smc.regionsListIsActive
                                    }">
                                        <span class="fa fa-fw fa-chevron-down"></span>
                                    </button>

                                    <div class="store-location-regions-content" ng-class="{
                                        'is-active': smc.regionsListIsActive
                                    }">
                                        <ul class="store-location-regions-list list-nostyle">
                                            <li ng-repeat="region in smc.data">
                                                <label for="@{{ region.id }}">
                                                    <input id="@{{ region.id }}" type="radio" name="region" value="@{{ region.name }}">
                                                    <span>@{{ region.name }}</span>
                                                </label>
                                            </li>
                                        </ul>

                                        <div class="v-center v-center--spread">
                                            <a id="setAllRegion" href="#">{{ trans('store-location.show_all') }}</a>
                                            <button class="btn btn--primary" id="setRegion">{{ trans('store-location.select') }}</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="store-location-search">
                                    <input class="form-input" id="storeSearchInput" type="text" placeholder="Search city">
                                    <span class="fa fa-fw fa-search"></span>
                                </div>
                            </div>
                        </div>

                        <div class="container" ng-show="smc.results.length">
                            <h3 class="home-section-heading">@{{ smc.searchKeyword }}</h3>

                            <ul class="store-list list-nostyle">
                                <li class="store-list-item" ng-repeat="result in smc.results | orderBy:'name'">
                                    <h3 style="margin-bottom: 1%;"><span style="color: #5C2D93;font-weight: bold">@{{ result.name }}</span></h3>
                                    <p>@{{ result.address }}</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
