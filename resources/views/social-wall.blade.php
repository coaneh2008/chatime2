@include('_includes.head')
    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('frontend.home.index') }}">{{ trans('home.home.title') }}</a></li>
                        <li>{{ trans('social-wall.title') }}</li>
                    </ul>

                    <div class="text-center block">
                        <h2 class="home-section-heading">{{ Setting::get('social-wall-title') }}</h2>
                        <p>{{ Setting::get('social-wall-desc') }}</p>
                        <a class="btn btn--primary" href="https://www.instagram.com/{{ Setting::get('social-instagram-username') }}" target="blank">{{ trans('social-wall.stories') }}</a>
                    </div>

                    <div class="social-wall">
                        <div class="social-wall-item social-wall-item--photos">
                            @foreach ($instagramPosts as $key => $instagramPost)
                                @if ($key<=3)
                                    <a href="{{ $instagramPost->url }}" target="blank">
                                        <img src="{{ asset($instagramPost->getThumbnail('image', 'medium')) }}" alt="{{ $instagramPost->caption }}">
                                    </a>
                                @endif
                            @endforeach
                        </div>
                        <div class="social-wall-item social-wall-item--main">
                            <a href="https://www.instagram.com/{{ Setting::get('social-instagram-username') }}" target="blank">
                                <img src="{{ asset(Setting::get('social-wall-middle-image-url')) }}" alt="">
                                <div class="anchor-content">
                                    <span class="fa fa-fw fa-3x fa-instagram"></span>
                                    <div>{{ trans('social-wall.middle-image.title') }}</div>
                                </div>
                            </a>
                        </div>
                        <div class="social-wall-item social-wall-item--photos">
                            @foreach ($instagramPosts as $key => $instagramPost)
                                @if ($key>3)
                                    <a href="{{ $instagramPost->url }}" target="blank">
                                        <img src="{{ asset($instagramPost->getThumbnail('image', 'medium')) }}" alt="{{ $instagramPost->caption }}">
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
