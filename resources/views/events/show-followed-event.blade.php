@include('_includes.head')

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>
                            <a href="{{ route('frontend.home.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('frontend.membership.dashboard') }}">{{ trans('membership.title') }}</a>
                        </li>
                        <li>
                            <a href="{{ route('frontend.followed-events.index') }}">Followed Event</a>
                        </li>
                        <li>{{ $event->title }}</li>
                    </ul>
                    <div class="membership-layout">
                        @include('memberships._includes.membership-sidebar')

                        <div class="membership-layout-body">
                            @include('memberships._includes.membership-nav')

                            <div class="membership-layout-content">
                                <figure>
                                    <img class="full-width" src="{{ $event->banner_image ? $event->getThumbnail('banner_image', 'huge') : '//placehold.it/800x275' }}" alt="">
                                </figure>
                                <h2>{{ $event->title }}</h2>
                                <p>{{ $event->description }}</p>
                                {!! $event->content !!}
                                <form class="text-center" action="{{ route('frontend.followed-events.add-followed-event-response') }}"" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="slug" value="{{ $event->slug }}">

                                    @if($event->type == \App\Model\Event::TYPE_FREE_MEMBERSHIP || $event->type == \App\Model\Event::TYPE_MEMBER_GET_MEMBER)
                                        <button class="btn btn--primary">
                                            Detail
                                        </button>
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>
    @include('memberships._includes.renewal-pop-up-message')
@include('_includes.scripts')
