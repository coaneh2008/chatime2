@include('_includes.head')

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>
                            <a href="{{ route('frontend.home.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('frontend.membership.dashboard') }}">{{ trans('membership.title') }}</a>
                        </li>
                        <li>{{ $event->title }}</li>
                    </ul>
                    <div class="membership-layout">
                        @include('memberships._includes.membership-sidebar')

                        <div class="membership-layout-body">
                            @include('memberships._includes.membership-nav')

                            <div class="membership-layout-content">
                                <figure>
                                    <img class="full-width" src="{{ $event->banner_image ? $event->getThumbnail('banner_image', 'huge') : '//placehold.it/800x275' }}" alt="">
                                </figure>
                                <h2>{{ $event->title }}</h2>
                                <p>{{ $event->description }}</p>
                                {!! $event->content !!}
                                <form class="text-center" action="{{ route('frontend.events.add-event-response') }}"" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="slug" value="{{ $event->slug }}">

                                    <button class="btn btn--primary">{{ ($event->type == \App\Model\Event::TYPE_ANNUAL_EVENT) ? 'Join' : (($event->type == \App\Model\Event::TYPE_TESTIMONY) ? 'Add Testimony' : 'Detail') }}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>
    <div class="modal {{ (Session::get(NOTIF_SUCCESS) || Session::get(NOTIF_WARNING) || Session::get(NOTIF_DANGER)) ? 'is-active' : '' }}">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>

            <div class="text-center">
                @if($notifSuccess = Session::get(NOTIF_SUCCESS))
                    {!! $notifSuccess !!}
                @elseif($notifWarning = Session::get(NOTIF_WARNING))
                    {!! $notifWarning !!}
                @elseif($notifDanger = Session::get(NOTIF_DANGER))
                    {!! $notifDanger !!}
                @endif
            </div>
        </div>
    </div>
    @include('memberships._includes.renewal-pop-up-message')
@include('_includes.scripts')
