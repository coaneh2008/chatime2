@include('_includes.head')

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>
                            <a href="{{ route('frontend.home.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('frontend.membership.dashboard') }}">{{ trans('membership.title') }}</a>
                        </li>
                        <li>
                            <a href="{{ route('frontend.followed-events.index') }}">Followed Event</a>
                        </li>
                        <li><a href="{{ route('frontend.followed-events.show-followed-event', $event->slug) }}">{{ $event->title }}</a></li>
                        <li>Detail</li>
                    </ul>
                    <div class="membership-layout">
                        @include('memberships._includes.membership-sidebar')

                        <div class="membership-layout-body">
                            @include('memberships._includes.membership-nav')

                            <div class="membership-layout-content">
                                 <div class="text-center">
                                    <h2>Do you want to get extra point?</h2>
                                    <p>Invite your friends become ACE Hardware member using this following referal code:</p>
                                    <div class="text-red h1"><b>{{ $referal }}</b></div>
                                    <p>Bagikan kode referral di atas ke teman-teman kamu</p>
                                    <div class="social-medias">
                                        <div class="social-medias">
                                            <a class="social-media --fb" onClick="window.open(this.href,'targetWindow','toolbar=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=800,height=350'); return false;" href="https://www.facebook.com/sharer/sharer.php?u={{ route('frontend.membership.register') }}&title={{ Setting::get('site-name') }}&description={{ Setting::get('site-description') }}&quote=Gunakan kode referal {{ $referal }} saat Register di website {{ Setting::get('site-name') }}">
                                                <span class="fa fa-fw fa-facebook-official"></span>
                                            </a>
                                            <a class="social-media --tw" onClick="window.open(this.href,'targetWindow','toolbar=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=800,height=350'); return false;" href="https://twitter.com/intent/tweet?text=Gunakan kode referal {{ $referal }} saat Register di website {{ Setting::get('site-name') }} - {{ route('frontend.membership.register') }}">
                                                <span class="fa fa-fw fa-twitter"></span>
                                            </a>
                                            <a class="social-media" href="mailto:?subject=Gunakan kode referal {{ $referal }} saat Register di website {{ Setting::get('site-name') }} - {{ route('frontend.membership.register') }}">
                                                <span class="fa fa-fw fa-envelope"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>
    @include('memberships._includes.renewal-pop-up-message')
@include('_includes.scripts')
