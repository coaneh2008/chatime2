@include('_includes.head')

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>
                            <a href="{{ route('frontend.home.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('frontend.membership.dashboard') }}">{{ trans('membership.title') }}</a>
                        </li>
                        <li>
                            <a href="{{ route('frontend.followed-events.index') }}">Followed Event</a>
                        </li>
                        <li><a href="{{ route('frontend.followed-events.show-followed-event', $event->slug) }}">{{ $event->title }}</a></li>
                        <li>Detail</li>
                    </ul>
                    <div class="membership-layout">
                        @include('memberships._includes.membership-sidebar')

                        <div class="membership-layout-body">
                            @include('memberships._includes.membership-nav')

                            <div class="membership-layout-content">
                                 <div class="text-center">
                                    @if($totalTransaction<$minimumSpend)
                                        {!! Setting::get('event-free-membership-description') !!}
                                    @else
                                         {!! Setting::get('successfully-get-free-membership') !!}
                                    @endif

                                    <span>Total Purchase Anda:</span>
                                    <div class="h1"><b>Rp<?= number_format($totalTransaction, 0, ',', '.') ?></b></div>
                                    @if($totalTransaction<$minimumSpend)
                                        <div class="bzg">
                                            <div class="bzg_c" data-col="l8" data-offset="l2">
                                                <div class="progress" data-min="Rp 0" data-max="Rp {{ number_format($minimumSpend, 0, ',', '.') }}" data-percentage="{{ ($totalTransaction/$minimumSpend)*100 }}">
                                                    <div class="progress-track"></div>
                                                    <div class="progress-bar"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <p>Belanja Rp <?= number_format($spendMore, 0, ',', '.') ?> lagi untuk mendapatkan free membership</p>
                                    @else
                                        <div class="bzg">
                                            <div class="bzg_c" data-col="l8" data-offset="l2">
                                                <div class="progress" data-min="Rp 0" data-max="Rp {{ number_format($minimumSpend, 0, ',', '.') }}" data-percentage="{{ ($minimumSpend/$minimumSpend)*100 }}">
                                                    <div class="progress-track"></div>
                                                    <div class="progress-bar"></div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>
    @include('memberships._includes.renewal-pop-up-message')
@include('_includes.scripts')
