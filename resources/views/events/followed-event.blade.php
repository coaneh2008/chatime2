@include('_includes.head')

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>
                            <a href="{{ route('frontend.home.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('frontend.membership.dashboard') }}">{{ trans('membership.title') }}</a>
                        </li>
                        <li>
                            Followed Event
                        </li>
                    </ul>

                    <div class="membership-layout">
                        @include('memberships._includes.membership-sidebar')

                        <div class="membership-layout-body">
                            @include('memberships._includes.membership-nav')

                            <div class="membership-layout-content">
                                <h2 class="home-section-heading">Followed event</h2>

                                <div class="fg fg-480-2 fg-1024-3">
                                    @if($freeMembershipEvents)
                                        @foreach ($freeMembershipEvents as $freeMembershipEvent)
                                            <div>
                                                <a class="anchor" href="{{ route('frontend.followed-events.show-followed-event', $freeMembershipEvent->slug) }}">
                                                <img class="full-width mb-8" src="{{ $freeMembershipEvent->thumb_image ? $freeMembershipEvent->getThumbnail('thumb_image', 'medium') : '//placehold.it/250x200' }}" alt="">
                                                <div class="p-8">
                                                    <h3 class="mb-8">{{ $freeMembershipEvent->title }}</h3>
                                                    <p>{{ $freeMembershipEvent->description }}</p>
                                                </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    @endif
                                    @if($memberGetMemberEvents)
                                        @foreach ($memberGetMemberEvents as $memberGetMemberEvent)
                                            <div>
                                                <a class="anchor" href="{{ route('frontend.followed-events.show-followed-event', $memberGetMemberEvent->slug) }}">
                                                <img class="full-width mb-8" src="{{ $memberGetMemberEvent->thumb_image ? $memberGetMemberEvent->getThumbnail('thumb_image', 'medium') : '//placehold.it/250x200' }}" alt="">
                                                <div class="p-8">
                                                    <h3 class="mb-8">{{ $memberGetMemberEvent->title }}</h3>
                                                    <p>{{ $memberGetMemberEvent->description }}</p>
                                                </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    @endif
                                    @foreach ($testimonies as $testimony)
                                        <div>
                                            <a class="anchor" href="{{ route('frontend.followed-events.show-followed-event', $testimony->slug) }}">
                                            <img class="full-width mb-8" src="{{ $testimony->thumb_image ? $testimony->getThumbnail('thumb_image', 'medium') : '//placehold.it/250x200' }}" alt="">
                                            <div class="p-8">
                                                <h3 class="mb-8">{{ $testimony->title }}</h3>
                                                <p>{{ $testimony->description }}</p>
                                            </div>
                                            </a>
                                        </div>
                                    @endforeach
                                    @foreach ($events as $event)
                                        <div>
                                            <a class="anchor" href="{{ route('frontend.followed-events.show-followed-event', $event->slug) }}">
                                            <img class="full-width mb-8" src="{{ $event->thumb_image ? $event->getThumbnail('thumb_image', 'medium') : '//placehold.it/250x200' }}" alt="">
                                            <div class="p-8">
                                                <h3 class="mb-8">{{ $event->title }}</h3>
                                                <p>{{ $event->description }}</p>
                                            </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>
    <div class="modal {{ (Session::get(NOTIF_SUCCESS) || Session::get(NOTIF_WARNING) || Session::get(NOTIF_DANGER)) ? 'is-active' : '' }}">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>

            <div class="text-center">
                @if($notifSuccess = Session::get(NOTIF_SUCCESS))
                    {!! $notifSuccess !!}
                @elseif($notifWarning = Session::get(NOTIF_WARNING))
                    {!! $notifWarning !!}
                @elseif($notifDanger = Session::get(NOTIF_DANGER))
                    {!! $notifDanger !!}
                @endif
            </div>
        </div>
    </div>
    @include('memberships._includes.renewal-pop-up-message')
@include('_includes.scripts')
