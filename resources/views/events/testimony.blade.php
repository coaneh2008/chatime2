@include('_includes.head')

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>
                            <a href="{{ route('frontend.home.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('frontend.membership.dashboard') }}">{{ trans('membership.title') }}</a>
                        </li>
                        <li><a href="{{ route('frontend.events.show', $event->slug) }}">{{ $event->title }}</a></li>
                        <li>Add Testimony</li>
                    </ul>
                    <div class="membership-layout">
                        @include('memberships._includes.membership-sidebar')

                        <div class="membership-layout-body">
                            @include('memberships._includes.membership-nav')

                            <div class="membership-layout-content">
                                <form class="text-center bzg js-validate" action="{{ route('frontend.events.add-testimony') }}" method="post">
                                    <div class="bzg_c" data-col="l8" data-offset="l2">
                                        {!! Setting::get('event-testimony-description') !!}
                                        <div class="block">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="eventId" value="{{ $event->id }}">
                                            <textarea class="form-input" rows="4" name="comment" required></textarea>
                                        </div>
                                        <button class="btn btn--block btn--primary">Send</button>
                                        </form>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>
    <div class="modal {{ (Session::get(NOTIF_SUCCESS)) ? 'is-active' : '' }}">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>

            <div class="text-center">
                @if($notifSuccess = Session::get(NOTIF_SUCCESS))
                    {!! $notifSuccess !!}
                @endif
            </div>
        </div>
    </div>
    @include('memberships._includes.renewal-pop-up-message')
@include('_includes.scripts')
