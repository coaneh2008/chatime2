@include('_includes.head')

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            @include('_includes.header')
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <div class="bzg">
                        <div data-col="l7">
                            <form class="block" action="{{ route('search') }}" method="get">
                                <label for="inputSearch2">Search results for</label>
                                <input class="form-input" id="name" name="name" type="text" value="{{ request()->get('name') }}">
                            </form>
                            @if ($data->count() > 0)
                            <ul class="list-nostyle">
                                @foreach($data as $detailData)
                                <li class="mb-24">
                                    {{-- <span class="label label--red mb-4">{{ $detailData['type'] }}</span> --}}
                                    <div class="media">
                                        @if (isset($detailData['image']))
                                        <div class="media-figure">
                                            <img src="{{ $detailData['image'] }}" width="100" alt="">
                                        </div>
                                        @endif
                                        <div class="media-content"><a href="{{ $detailData['url'] }}" target="{{ isset($detailData['newTab']) ? '_blank' : '_self' }}"><b>{{ $detailData['title'] }}</b></a>
                                        <p>
                                        {{ substr(strip_tags($detailData['content']), 0, 300) }}
                                        @if (isset($detailData['promo']))
                                        {!! $detailData['promo'] !!}
                                        @endif
                                        @if (isset($detailData['price']))
                                        {!! $detailData['price'] !!}
                                        @endif
                                        </p>
                                        </div>
                                    </div>
                                </li>
                                @endforeach()
                            </ul>
                            @include('_includes.pagination', ['paginator' => $data])
                            @else
                            <p>Tidak ditemukan hasil untuk pencarian {{ request()->get('name') }}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            @include('_includes.footer')
        </div>
    </div>

@include('_includes.scripts')
