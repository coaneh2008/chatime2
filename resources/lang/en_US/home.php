<?php
return [
    'featured-brand' => [
        'title' => 'Featured Brands'
    ],
    'home' => [
        'title' => 'Home'
    ],
    'inspiration'=> [
        'title' => 'Inspirations'
    ]
];
