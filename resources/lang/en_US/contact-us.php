<?php
return [
    'title' => 'Contact Us',
    'inbox_form' => [
        'name' => 'Name',
        'address' => 'Address',
        'phone' => 'Phone',
        'email' => 'Email',
        'message_label' => 'Message',
        'btn_send' => 'Send',
        'message' => [
            'danger' => 'Something went wrong.',
            'success' => 'Thank you for contacting us. You will hear from us soon.',
            'warning' => ''
        ]
    ]
];
