<?php
return [
    'title_breadcrumb' => 'Brochures',
    'title' => 'e-Brochure',
    'description' => '',
    'show' => 'Show',
    'sort' => 'Sort',
    'view' => 'View',
    'download' => 'Download'
];
