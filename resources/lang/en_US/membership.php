<?php
return [
    'title' => 'ACE REWARDS',
    'login' => [
        'title_1' => 'Member Login',
        'title_2' => 'Login Member',
        'title_3' => 'Masuk',
        'forgot_passkey' => 'Forgot passkey?',
        'become_member' => 'Become a Member!',
        'card_number' => 'No Kartu Member',
        'pass_key' => 'Pass Key'
    ],
    'register' => [
        'title_1' => 'Member Registration',
        'title_2' => 'Become Member',
        'title_3' => 'register',
        'become_member' => 'Register Now'
    ],
    'step_1' => [
        'title' => 'Informasi Pribadi',
        'name' => 'Nama',
        'email' => 'Alamat Email',
        'telephone' => 'Nomor Telepon Rumah',
        'cellphone' => 'Nomor HP',
        'id_card' => 'Nomor KTP/SIM/KITAS/KITAP',
        'sex' => 'Jenis Kelamin',
        'birth_place' => 'Tempat Lahir',
        'birthday' => 'Tanggal Lahir',
        'religion' => 'Agama',
        'marital_status' => 'Status Perkawinan',
        'nationality' => 'Kebangsaan',
        'occupation' => 'Pekerjaan',
        'shopping_purpose' => 'Tujuan Berbelanja',
        'current_address' => 'Alamat Rumah Sekarang',
        'id_card_address' => 'Alamat Rumah Sesuai KTP',
        'street_name' => 'Alamat',
        'city' => 'Kota',
        'zip_code' => 'Kode Pos',
        'next' => 'Lanjut'
    ],
    'step_2' => [
        'title' => 'Administrasi'
    ]
];
