<?php
return [
    'title' => 'Store Location',
    'description' => '',
    'by_region' => 'By Region',
    'show_all' => 'Show All',
    'select' => 'Select',
    'default_title' => 'Ace Hardware'
];
