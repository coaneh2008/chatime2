<?php
return [
    'subscribe' => [
        'btn-title' => 'Subscribe',
        'email-placeholder' => 'Your Email'
    ],
    'social-wall' => [
        'link-title' => '[Check Our Story]'
    ]
];
