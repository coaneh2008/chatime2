<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <div class="membership-layout">
                        <?php include '_partials/membership-sidebar.php'; ?>

                        <div class="membership-layout-body">
                            <?php include '_partials/membership-nav.php'; ?>

                            <div class="membership-layout-content">
                                <div class="text-center">
                                    <h2>Dapatkan free membership</h2>
                                    <p>Belanja Rp20.000,00 akan mendapatkan free membership untuk 6 bulan. Syarat dan ketentuan berlaku.</p>

                                    <span>Total pembelanjaan anda</span>
                                    <div class="h1"><b>Rp550.000,00</b></div>

                                    <div class="bzg">
                                        <div class="bzg_c" data-col="l8" data-offset="l2">
                                            <div class="progress" data-min="0" data-max="2.000.000" data-percentage="35">
                                                <div class="progress-track"></div>
                                                <div class="progress-bar"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <p>Cukup berbelanja Rp1.450.000,00 dan anda akan mendapatkan free membership.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
