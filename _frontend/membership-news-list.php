<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <figure class="member-hero">
                        <img src="//placehold.it/1100x500" alt="">
                        <figcaption class="member-hero-content">
                            <h1 class="text-caps">Member news update</h1>
                            <h2>Beragam berita terbaru terkait ACE Hardware</h2>
                        </figcaption>
                    </figure>

                    <section class="home-section">
                        <h2 class="home-section-heading">Member news update</h2>

                        <div class="fg fg-400-2 fg-480-3 fg-640-3 fg-768-4 fg-1024-5">
                            <?php for ($i=0; $i < 10; $i++) { ?>
                            <div>
                                <a class="anchor" href="membership-news-detail.php">
                                    <img class="full-width" src="//placehold.it/200x200" alt="">
                                    <div class="p-8">
                                        <div class="mb-8 lh-1">
                                            <small>Periode Promo</small>
                                            <br>
                                            <small><b>19-28 Feb 2018</b></small>
                                        </div>
                                        <h4 class="mb-0">Ace decoration sale! up to 70%</h4>
                                    </div>
                                </a>
                            </div>
                            <?php } ?>
                        </div>

                        <?php include '_partials/back-button.php'; ?>
                    </section>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
