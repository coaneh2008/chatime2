<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <div class="membership-layout">
                        <?php include '_partials/membership-sidebar.php'; ?>

                        <div class="membership-layout-body">
                            <?php include '_partials/membership-nav.php'; ?>

                            <div class="membership-layout-content">
                                <figure>
                                    <img class="full-width" src="//placehold.it/800x275" alt="">
                                </figure>
                                <h2>Cozy Home Workshop</h2>

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur illum rem minima odio, ipsam voluptate nisi obcaecati cum deleniti, ut eligendi vero totam, voluptates at earum. Unde quod qui rem ab vitae iusto similique, perferendis praesentium adipisci sed eligendi atque ad, quos rerum dolores. Illum perspiciatis veritatis consequuntur, modi sapiente?</p>
                                <p>Distinctio, deleniti, nemo? Possimus fuga doloribus laborum ad tempora veritatis quaerat repudiandae doloremque, alias mollitia suscipit quod, quis rem voluptatem fugiat recusandae, nisi! Quae laudantium magni, unde tenetur itaque velit sunt eos minus ab molestias dolorem in odit nulla, commodi tempora cumque nihil, neque consectetur ratione! Enim, ipsum. Repellendus, odit.</p>
                                <p>Ipsum hic unde at voluptatum ullam vel cum, porro sed nobis quo excepturi fugiat voluptatibus pariatur aperiam officiis quos adipisci et ducimus architecto accusamus est eius voluptate. Repellendus iure recusandae tempora perferendis, neque hic consequuntur corporis, dolorum esse nemo inventore assumenda explicabo tenetur facilis sit velit ut harum sapiente at!</p>

                                <!-- membership-profile-event-detail-success.php -->
                                <!-- membership-profile-event-detail-fail.php -->
                                <form class="text-center" action="membership-profile-event-detail-success.php">
                                    <button class="btn btn--primary">Join</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
