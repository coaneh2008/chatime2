<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <div class="text-center">
                        <h1 class="h2 home-section-heading">Thank you for becoming our member</h1>

                        <p>As a member, you will get many rewards. Here is our latest promo for member.</p>

                        <div class="fg fg-400-2 fg-480-3 fg-640-3 fg-768-4 fg-1024-5">
                            <?php for ($i=0; $i < 5; $i++) { ?>
                            <div>
                                <a class="anchor" href="membership-promo-detail.php">
                                    <img class="full-width" src="//placehold.it/200x200" alt="">
                                    <div class="p-8">
                                        <div class="mb-8 lh-1">
                                            <small>Periode Promo</small>
                                            <br>
                                            <small><b>19-28 Feb 2018</b></small>
                                        </div>
                                        <h4 class="mb-12">Ace decoration sale! up to 70%</h4>
                                    </div>
                                </a>
                            </div>
                            <?php } ?>
                        </div>

                        <p>Before you can get rewarded, please complete the administration in the nearest store.</p>

                        <a class="btn btn--primary" href="store-location.php">Find store</a>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

    <div class="modal" id="modalConfirmRenewal">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>

            <div class="text-center">
                <h2 class="text-red">Apa anda yakin?</h2>
                <p>Apabila anda tidak memilih auto renewal anda harus membayar setiap tahun sebesar Rp50.000,00. Keterlambatan pembayaran menyebabkan akun dan poin tidak dapat digunakan kembali.</p>
            </div>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
