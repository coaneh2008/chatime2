<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <figure class="member-hero">
                        <img src="//placehold.it/1100x500" alt="">
                        <figcaption class="member-hero-content">
                            <img class="mb-24" src="//placehold.it/150x150" alt="">
                            <h2 class="text-caps">Discount 10% for lense and frame</h2>
                        </figcaption>
                    </figure>

                    <section class="home-section">
                        <h2 class="home-section-heading">Syarat dan ketentuan</h2>

                        <ol class="custom-ol">
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat eligendi omnis officiis culpa, alias ut et perspiciatis dolor vitae dolore nobis nihil animi natus rerum necessitatibus nulla vero nam laborum ratione, velit, assumenda inventore aut. Accusantium recusandae, vero quidem possimus nisi perspiciatis consequatur optio iste quod neque, voluptates, quos quasi.</li>
                            <li>Sunt explicabo expedita quasi iure, repellendus culpa enim, alias, illum ipsam sit dolores! Tempora dolore ipsum fugit ut obcaecati accusantium culpa praesentium illo enim ad laboriosam, repudiandae veritatis, qui aperiam dolores nihil! Numquam, recusandae rem nulla ab est corrupti architecto nisi. Illum, enim explicabo temporibus dolore neque esse similique eligendi?</li>
                            <li>Qui expedita, vel soluta velit, ratione repudiandae nihil distinctio aliquam. Iure provident, error nemo dolor nihil facere maxime quos, quis possimus magnam quod. Iusto dignissimos quia a temporibus, hic accusamus. Amet odit culpa veritatis atque consectetur sint autem voluptas recusandae exercitationem excepturi neque cum omnis, iste beatae dolores harum, ab.</li>
                            <li>Distinctio sequi tempora aliquid, beatae eos ex deleniti a minus explicabo dolorem necessitatibus voluptatibus nobis ad, rerum aspernatur ab officiis odit. Nobis saepe praesentium dolore quae culpa, iste ad doloremque reiciendis debitis rerum aut quis, vitae commodi dicta animi voluptatum consectetur nihil cumque natus tempore omnis ipsum obcaecati harum ipsam?</li>
                            <li>Expedita ea commodi illo in velit ut quia totam officiis hic excepturi ab natus doloribus, sapiente sed nesciunt minus dolores, eius, deserunt nisi distinctio reprehenderit perferendis libero ducimus nam. Cupiditate, ut, mollitia? Sapiente, et! Aut fugiat reprehenderit ducimus culpa labore doloremque optio autem molestiae quis, ut quod aperiam sit ipsum.</li>
                        </ol>

                        <?php include '_partials/back-button.php'; ?>
                    </section>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
