<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <div class="membership-layout">
                        <?php include '_partials/membership-sidebar.php'; ?>

                        <div class="membership-layout-body">
                            <?php include '_partials/membership-nav.php'; ?>

                            <div class="membership-layout-content">
                                <h2 class="home-section-heading">Monthly leaderboard</h2>

                                <div class="table-wrapper">
                                    <table class="table js-table-enhanced">
                                        <thead>
                                            <tr>
                                                <th align="center" width="15">Rank</th>
                                                <th align="right">Card ID</th>
                                                <th align="left">Name</th>
                                                <th align="right">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for ($i=1; $i <= 30; $i++) { ?>
                                            <tr>
                                                <td align="center"><?= $i ?></td>
                                                <td align="right"><?= mt_rand(10000, 99999) ?></td>
                                                <td align="left">Jane Doe</td>
                                                <td align="right">Rp<?= number_format(mt_rand(1000000, 5000000), 0, ',', '.') ?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
