<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Featured Brands</li>
                        <li>Krisbow</li>
                    </ul>

                    <article>
                        <figure>
                            <img class="full-width" src="assets/img/brand-hero.jpg" alt="">
                        </figure>

                        <div class="bzg">
                            <div class="bzg_c" data-col="m8" data-offset="m2">
                                <h1 class="h2">THE #1 COMMERCIAL &amp; INDUSTRY SUPPLY COMPANY IN INDONESIA</h1>

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem ullam vero eius similique ab pariatur officiis dolorum magnam recusandae. Omnis, dolorum voluptatum, quaerat aspernatur ipsum quisquam voluptates temporibus pariatur quasi commodi id rem ratione magnam facere modi, illum ut. Expedita voluptates commodi ipsum odit labore blanditiis temporibus dicta ab maiores deleniti cupiditate eaque nihil amet ipsam, velit suscipit eligendi facere. Officia delectus, asperiores. Aperiam voluptates, excepturi, aspernatur quis aliquid autem suscipit ut, deleniti velit provident rerum fuga eaque! At, eum.</p>

                                <div class="responsive-media block">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/WFWfYCz-scw?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </div>

                                <p>Autem dicta eos porro atque id alias, fugiat deserunt ullam repellat consequatur. Quidem dolore, repellendus illo dolores obcaecati, accusamus quia atque minus quam, tenetur quo ut placeat eius perspiciatis, cum deserunt natus doloribus quibusdam recusandae maxime? Odio minus porro, aliquam illo earum cumque modi aperiam consequuntur obcaecati ea odit quaerat. Omnis culpa, aut similique facere corporis voluptates. Ipsam minus reiciendis temporibus perferendis dignissimos nobis molestias perspiciatis doloribus vitae, placeat sit ab deserunt quam deleniti autem nemo laudantium et. Reiciendis, commodi.</p>
                                <p>Enim veniam cum tempore, fugit soluta repellat, explicabo error hic dolores, molestias dolore laudantium quae! Nobis nisi odio tempore sunt, eum accusantium esse temporibus. Culpa maiores placeat ad reprehenderit, facilis, nostrum, quis natus est, harum vero rerum nesciunt. Ad, autem deleniti voluptatum. Unde voluptatibus velit saepe nemo reiciendis odit mollitia ipsum ea aliquam cupiditate itaque veniam nihil vel porro, nisi dolorum inventore eveniet quis accusamus aliquid quam fugiat totam magnam. Enim minus ullam maxime beatae vel consequuntur. Delectus, provident omnis!</p>
                                <p>Corrupti quo provident dolore soluta non qui iure ea facere excepturi ipsa facilis nulla ipsam quod, inventore sed eum assumenda, aperiam libero ad accusantium ab. Omnis quia libero non odit laborum fugit doloribus dignissimos officia labore sed ratione, sequi reiciendis unde expedita nesciunt totam maiores quos neque similique, hic est autem itaque. Harum aspernatur esse eveniet dolores vitae. Ratione maxime id porro laborum, delectus expedita debitis nulla doloribus veritatis adipisci iusto quae qui accusamus, voluptates amet, fugit alias, accusantium cum.</p>
                                <p>Rerum tenetur, incidunt amet sequi ab voluptates sapiente voluptas ipsa quidem quis id natus voluptatem iste delectus sit autem aliquam, veniam placeat. Quisquam nulla quo blanditiis doloribus dolores earum voluptate totam. Quos numquam atque molestias, blanditiis ab expedita nesciunt! Deserunt amet repellat impedit aperiam rem, nam quam laboriosam cupiditate nisi harum eius necessitatibus labore qui commodi dignissimos incidunt inventore corporis explicabo veritatis error eligendi rerum, delectus accusantium numquam. Quibusdam quasi iste dignissimos adipisci quas ratione, reiciendis. Blanditiis, nobis molestias error.</p>
                            </div>
                        </div>
                    </article>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
