<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Social wall</li>
                    </ul>

                    <div class="text-center block">
                        <h2 class="home-section-heading">AceHardware Social Wall</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste ipsa quaerat vitae at optio, error eveniet. Assumenda, ducimus, neque? Totam soluta eaque dolores voluptatum inventore aut velit magni aspernatur, enim sequi illo quaerat tempora, voluptas amet commodi. Ad nulla autem magni soluta iure, explicabo amet neque. Architecto libero, asperiores quisquam.</p>
                        <a class="btn btn--primary" href="#">Stories</a>
                    </div>

                    <div class="social-wall">
                        <div class="social-wall-item social-wall-item--photos">
                            <?php for ($i=1; $i <= 4; $i++) { ?>
                            <a href="#">
                                <img src="assets/img/ig-thumb-<?= $i ?>.jpg" alt="">
                            </a>
                            <?php } ?>
                        </div>
                        <div class="social-wall-item social-wall-item--main">
                            <a href="#">
                                <img src="assets/img/ig-thumb-big.jpg" alt="">
                                <div class="anchor-content">
                                    <span class="fa fa-fw fa-3x fa-instagram"></span>
                                    <div>AceHardware on Instagram</div>
                                </div>
                            </a>
                        </div>
                        <div class="social-wall-item social-wall-item--photos">
                            <?php for ($i=1; $i <= 4; $i++) { ?>
                            <a href="#">
                                <img src="assets/img/ig-thumb-<?= $i ?>.jpg" alt="">
                            </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
