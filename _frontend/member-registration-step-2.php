<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <div class="bzg">
                        <div class="bzg_c" data-col="l6" data-offset="l3">
                            <h1 class="h2 home-section-heading text-center">Member registration</h1>

                            <div class="member-registration-steps">
                                <div class="member-registration-step">1</div>
                                <div class="member-registration-step is-active">2</div>
                            </div>

                            <form class="form-membership-step-2" action="membership-registration-success.php">
                                <fieldset class="block">
                                    <legend><b>Administration</b></legend>

                                    <fieldset class="block">
                                        <legend>Before you get rewarded, you need to complete administration fee. You can do it online or offline.</legend>

                                        <label for="inputOnline">
                                            <input id="inputOnline" type="radio" name="adminFee" required>
                                            <span>Online</span>
                                        </label>
                                        &nbsp;
                                        <label for="inputOffline">
                                            <input id="inputOffline" type="radio" name="adminFee" required>
                                            <span>Offline</span>
                                        </label>
                                        &nbsp;
                                        <label for="inputFreeMembership">
                                            <input id="inputFreeMembership" type="radio" name="adminFee" required>
                                            <span>Free membership</span>
                                        </label>
                                        <div>
                                            <small id="inputAdminFeeMsg" class="text-red" style="display: none;">Harap pilih salah satu.</small>
                                        </div>
                                    </fieldset>
                                    <fieldset class="block">
                                        <legend>You need to renew your membership every year. Would you like to do it automatically by deducting your points (20 points)?</legend>

                                        <label for="renewalYes">
                                            <input id="renewalYes" type="radio" name="renewal">
                                            <span>Yes</span>
                                        </label>
                                        &nbsp;
                                        <label for="renewalNo">
                                            <input id="renewalNo" type="radio" name="renewal">
                                            <span>No</span>
                                        </label>
                                        <div>
                                            <small id="inputRenewalMsg" class="text-red" style="display: none;">Harap pilih salah satu.</small>
                                        </div>
                                    </fieldset>
                                    <div>
                                        <label for="inputReferal">Please input referral code if you have one</label>
                                        <input class="form-input" id="inputReferal" type="text">
                                    </div>
                                </fieldset>
                                <div class="block">
                                    <label for="inputAgree">
                                        <input id="inputAgree" type="checkbox">
                                        <span>Saya telah membaca dan setuju dengan <a target="_blank" href="membership-terms.php">syarat dan ketentuan</a> yang berlaku</span>
                                        <small id="inputAgreeMsg" class="text-red" style="display: none;">Harap menyetujui syarat dan ketentuan yang berlaku.</small>
                                    </label>
                                </div>
                                <div class="text-center block">
                                    <button class="btn btn--primary">Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

    <div class="modal" id="modalConfirmRenewal">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>

            <div class="text-center">
                <h2 class="text-red">Apa anda yakin?</h2>
                <p>Apabila anda tidak memilih auto renewal anda harus membayar setiap tahun sebesar Rp50.000,00. Keterlambatan pembayaran menyebabkan akun dan poin tidak dapat digunakan kembali.</p>
            </div>
        </div>
    </div>

    <div class="modal" id="modalFreeMembership">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>

            <h2 class="text-red">FREE MEMBERSHIP</h2>
            <p><b>Syarat dan Ketentuan</b></p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis distinctio sunt dolor blanditiis sequi doloremque, voluptas quas excepturi pariatur. At rem quae asperiores, maiores molestias nobis quia ea? Ipsum sit, ratione tenetur rem? At adipisci cum quis sint molestias quia, alias, a veritatis modi numquam eum omnis itaque tempore natus.</p>
            <p>Atque, obcaecati, doloremque? Mollitia repudiandae voluptas hic unde rem neque beatae consequuntur inventore illo quibusdam voluptatum deleniti, cumque, odit similique et porro deserunt nulla delectus ad, natus quas quae enim! Rem quasi ab, similique iusto, et quod temporibus alias excepturi debitis itaque distinctio tempore, adipisci a odio harum perspiciatis quo!</p>
        </div>
    </div>

    <div class="modal" id="modalOnlineRegistration">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>

            <h2 class="text-red">ONLINE REGISTRATION</h2>
            <p>Kode TAM akan dikirimkan ke email anda. Silahkan lanjutkan pembayaran melalui <a href="#">http://www.ruparupa.com/</a>.</p>
        </div>
    </div>

    <div class="modal" id="modalOfflineRegistration">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>

            <h2 class="text-red">OFFLINE REGISTRATION</h2>
            <p>Kode TAM akan dikirimkan ke email anda. Silahkan lanjutkan pembayaran di toko ACE Hardware terdekat dengan menunjukan kode TAM tersebut.</p>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
