<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <div class="membership-layout">
                        <?php include '_partials/membership-sidebar.php'; ?>

                        <div class="membership-layout-body">
                            <?php include '_partials/membership-nav.php'; ?>

                            <div class="membership-layout-content">
                                <h2 class="home-section-heading">Ongoing event</h2>

                                <div class="fg fg-480-2 fg-1024-3">
                                    <?php for ($i=0; $i < 3; $i++) { ?>
                                    <div>
                                        <img class="full-width mb-8" src="//placehold.it/250x200" alt="">
                                        <div class="p-8">
                                            <h3 class="mb-8">Cozy Home</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis labore ducimus unde laboriosam est qui earum!</p>
                                        </div>
                                        <div class="text-center">
                                            <a class="btn btn--block btn--outline btn--primary" href="membership-profile-event-detail.php">Lihat detail</a>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
