<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <figure>
                        <img src="//placehold.it/1100x500" alt="">
                    </figure>

                    <section class="home-section">
                        <h2 class="home-section-heading">Syarat dan ketentuan</h2>

                        <ol class="custom-ol">
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit aliquid provident corrupti doloremque est, deserunt, quis in culpa ab, commodi voluptatibus. Error laudantium, fugit. Eaque, debitis. Ipsum perferendis corporis fugit unde aut quod, impedit id optio provident praesentium ducimus architecto!</li>
                            <li>Excepturi nihil fugit eveniet sit quis molestias cupiditate! Quasi dolores incidunt, exercitationem! Iste fugit suscipit incidunt eligendi, voluptatem veniam dicta illum nemo quibusdam sint adipisci, explicabo, dolor officiis quidem. Praesentium, corporis natus commodi nostrum dolor rerum officiis delectus minima quae!</li>
                            <li>Voluptate soluta, molestias dignissimos, deleniti ipsum similique molestiae rerum quae repellendus libero quia, officia debitis eligendi perferendis nesciunt ullam doloremque quaerat vitae voluptatum quam. Nemo quae, reiciendis exercitationem vitae inventore odit cupiditate repudiandae. Debitis non, nam quis natus nesciunt repellendus!</li>
                            <li>Atque minima cum quidem enim repellat mollitia voluptatibus deleniti aut animi sapiente quaerat ipsa fugiat explicabo rem laudantium beatae, voluptas voluptatum placeat! A totam maiores explicabo illum nam numquam sint mollitia, expedita nemo sequi dolorum nobis est, fuga ad soluta.</li>
                            <li>Libero nisi veritatis iste. Sit accusantium quaerat eum voluptates officiis vel ipsum suscipit magni soluta id tempore perspiciatis nihil, blanditiis reiciendis non facere illo nemo asperiores ullam distinctio voluptas molestiae. Et perspiciatis ad neque odit a doloribus explicabo ea quibusdam?</li>
                        </ol>

                        <?php include '_partials/back-button.php'; ?>
                    </section>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
