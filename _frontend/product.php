<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li><a href="product-selection.php">Product selection</a></li>
                        <li>Home and living</li>
                    </ul>

                    <article class="product">
                        <figure>
                            <img src="assets/img/product-hero-banner.jpg" alt="">
                        </figure>
                        <div class="bzg">
                            <div class="bzg_c" data-col="l8" data-offset="l2">
                                <h2 class="text-caps">Find your dream living room</h2>

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium iusto amet optio eum quam veniam aliquid molestiae nam fugiat quis nemo cupiditate libero, magni, enim, totam nulla ducimus quaerat assumenda sit magnam architecto quasi! Autem fugiat enim sint minima laudantium ab. Quo totam ipsa alias, perferendis nemo repudiandae fuga reprehenderit!</p>
                                <p>Officiis laudantium consequuntur et dolorem explicabo nulla, ullam nesciunt quibusdam quae? Sed veniam, culpa est aperiam molestiae itaque ratione numquam quisquam vero. Ex quisquam velit minima quas nulla eum quaerat suscipit asperiores, nemo accusantium harum veniam aspernatur sunt unde, autem voluptatem quo officiis a placeat, cumque omnis labore repudiandae modi!</p>
                            </div>
                        </div>

                        <?php $products = [
                            'Home decoration',
                            'Living room storage',
                            'Trash can',
                            'Cleaners',
                            'Textiles',
                            'Washing &amp; ironing'
                        ] ?>

                        <br>

                        <ul class="product-list list-nostyle">
                            <?php foreach ($products as $key => $value) { ?>
                            <li>
                                <a class="product-item-anchor" href="#">
                                    <figure class="product-item">
                                        <img src="assets/img/product-desc-<?= $key + 1 ?>.jpg" alt="">
                                        <figcaption>
                                            <h3><?= $value ?></h3>
                                            <p>Make a playful statement in the living room with the cool decoration.</p>
                                        </figcaption>
                                    </figure>
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                    </article>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
