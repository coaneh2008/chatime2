<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <figure class="member-hero">
                        <img src="//placehold.it/1100x500" alt="">
                        <figcaption class="member-hero-content">
                            <h1 class="text-caps">Ace Rewards</h1>
                            <h2>Get rewarded for shopping</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil voluptatibus voluptates placeat, debitis delectus perferendis cumque voluptatum facere optio ratione ab vero facilis ipsam reprehenderit illo a porro enim ut!</p>
                            <br>
                            <div class="block-half">
                                <a class="btn btn--outline btn--white" href="membership-login.php">Login Member</a>
                            </div>
                            <div><small>Not yet a member?</small></div>
                            <div>
                                <a class="btn btn--primary" href="membership-register.php">Become Member</a>
                            </div>
                        </figcaption>
                    </figure>

                    <section class="home-section">
                        <h2 class="home-section-heading">How to become a member</h2>

                        <ol class="custom-ol">
                            <li>Daftarkan diri anda secara online ataupun offline di toko kami</li>
                            <li>Lengkapi biaya administrasi</li>
                            <li>Anda telah menjadi member</li>
                        </ol>

                        <a class="btn btn--primary" href="#">Daftar sekarang</a>
                    </section>
                    <br>
                    <section class="home-section">
                        <h2 class="home-section-heading">Member benefits</h2>
                        <?php $benefits = ['Point reward', 'Member promo', 'Merchant promo', 'Member news update', 'Earn rewards', 'Free delivery', 'ACE protection'] ?>
                        <div class="member-benefits slider-style">
                            <?php foreach ($benefits as $key => $value) { ?>
                            <div>
                                <div class="member-benefit">
                                    <img class="block" src="//placehold.it/120x120" alt="">
                                    <button class="btn btn--primary" data-benefit-panel="#panel<?= $key + 1 ?>"><?= $value ?></button>
                                </div>
                            </div>
                            <?php } ?>
                        </div>

                        <div class="member-benefit-panel" id="panel1">
                            <div class="fg fg-768-2">
                                <div>
                                    <h3 class="text-caps">Use point</h3>

                                    <ol class="custom-ol">
                                        <li>Tukarkan point dengan voucher <a href="membership-voucher-list.php">(lihat detail)</a></li>
                                        <li>Gunakan untuk berbelanja <a href="membership-pay-points.php">(lihat detail)</a></li>
                                        <li>Tukarkan point dengan merchandise <a href="membership-merchandise.php">(lihat detail)</a></li>
                                    </ol>
                                </div>
                                <div>
                                    <h3 class="text-caps">How to get point</h3>

                                    <ol class="custom-ol">
                                        <li>Belanja sebanyak-banyaknya</li>
                                        <li>Anda akan mendapatkan point setiap pembelian 100k</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="member-benefit-panel" id="panel2">
                            <h3 class="text-caps">Use point</h3>

                            <div class="fg fg-480-2 fg-768-3 fg-1024-5">
                                <?php for ($i=0; $i < 5; $i++) { ?>
                                <div>
                                    <a class="anchor" href="membership-promo-detail.php">
                                        <img class="full-width" src="//placehold.it/200x200" alt="">
                                        <div class="p-8">
                                            <time><small>12 December 2017</small></time>
                                            <h4 class="mb-0">Ace decoration sale! up to 70%</h4>
                                        </div>
                                    </a>
                                </div>
                                <?php } ?>
                            </div>

                            <div class="text-center">
                                <a class="btn btn--primary" href="membership-promo-list.php">See more</a>
                            </div>
                        </div>
                        <div class="member-benefit-panel" id="panel3">
                            <h3 class="text-caps">Merchant promo</h3>

                            <div class="fg fg-480-2 fg-768-3 fg-1024-5">
                                <?php for ($i=0; $i < 5; $i++) { ?>
                                <div>
                                    <div class="text-center">
                                        <h4 class="text-caps mb-8">Optik Melawai</h4>
                                        <img class="full-width" src="//placehold.it/200x200" alt="">
                                        <div class="p-8 lh-1 mb-8">
                                            <h4 class="text-caps mb-2">Discount 50%</h4>
                                            <small>Lorem ipsum dolor sit amet, consectetur.</small>
                                        </div>
                                        <a class="btn btn--outline btn--small btn--secondary" href="membership-merchant-detail.php">Syarat &amp; ketentuan</a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>

                            <div class="text-center">
                                <a class="btn btn--primary" href="membership-merchant-list.php">See more</a>
                            </div>
                        </div>
                        <div class="member-benefit-panel" id="panel4">
                            <h3 class="text-caps">Member news update</h3>

                            <div class="fg fg-480-2 fg-768-3 fg-1024-5">
                                <?php for ($i=0; $i < 5; $i++) { ?>
                                <div>
                                    <a class="anchor" href="membership-news-detail.php">
                                        <img class="full-width" src="//placehold.it/200x200" alt="">
                                        <div class="p-8">
                                            <time><small>12 December 2017</small></time>
                                            <h4 class="mb-0">Ace decoration sale! up to 70%</h4>
                                        </div>
                                    </a>
                                </div>
                                <?php } ?>
                            </div>

                            <div class="text-center">
                                <a class="btn btn--primary" href="membership-news-list.php">See more</a>
                            </div>
                        </div>
                        <div class="member-benefit-panel" id="panel5">
                            <h3 class="text-caps">How to earh rewards</h3>

                            <ol class="custom-ol">
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos consectetur sunt autem, pariatur aut accusamus suscipit a, perferendis ipsam consequatur qui, deleniti, earum laboriosam delectus rem quaerat omnis blanditiis? Laudantium?</li>
                                <li>Dicta numquam enim deserunt veniam, rem excepturi quia, provident sed ipsa maiores voluptatem? Expedita distinctio, praesentium tempora autem repellat. Necessitatibus optio esse iure fuga pariatur minima aut obcaecati id quos!</li>
                                <li>Voluptate incidunt amet esse, dicta molestiae aspernatur error voluptatum non veniam est deleniti, impedit temporibus ullam sunt id quisquam aliquam voluptates possimus placeat vero repellendus. Quibusdam error totam ab blanditiis?</li>
                            </ol>
                        </div>
                        <div class="member-benefit-panel" id="panel6">
                            <h3 class="text-caps">Free delivery</h3>

                            <ol class="custom-ol">
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos consectetur sunt autem, pariatur aut accusamus suscipit a, perferendis ipsam consequatur qui, deleniti, earum laboriosam delectus rem quaerat omnis blanditiis? Laudantium?</li>
                                <li>Dicta numquam enim deserunt veniam, rem excepturi quia, provident sed ipsa maiores voluptatem? Expedita distinctio, praesentium tempora autem repellat. Necessitatibus optio esse iure fuga pariatur minima aut obcaecati id quos!</li>
                                <li>Voluptate incidunt amet esse, dicta molestiae aspernatur error voluptatum non veniam est deleniti, impedit temporibus ullam sunt id quisquam aliquam voluptates possimus placeat vero repellendus. Quibusdam error totam ab blanditiis?</li>
                            </ol>
                        </div>
                        <div class="member-benefit-panel" id="panel7">
                            <h3 class="text-caps">Ace protection</h3>

                            <ol class="custom-ol">
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos consectetur sunt autem, pariatur aut accusamus suscipit a, perferendis ipsam consequatur qui, deleniti, earum laboriosam delectus rem quaerat omnis blanditiis? Laudantium?</li>
                                <li>Dicta numquam enim deserunt veniam, rem excepturi quia, provident sed ipsa maiores voluptatem? Expedita distinctio, praesentium tempora autem repellat. Necessitatibus optio esse iure fuga pariatur minima aut obcaecati id quos!</li>
                                <li>Voluptate incidunt amet esse, dicta molestiae aspernatur error voluptatum non veniam est deleniti, impedit temporibus ullam sunt id quisquam aliquam voluptates possimus placeat vero repellendus. Quibusdam error totam ab blanditiis?</li>
                            </ol>
                        </div>
                    </section>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
