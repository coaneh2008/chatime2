<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <div class="bzg">
                        <div class="bzg_c" data-col="l4" data-offset="l4">
                            <h1 class="h2 home-section-heading text-center">Recover passkey</h1>

                            <form class="js-validate" action="membership-dashboard.php">
                                <p class="text-center">Masukan email anda untuk mendapatkan passkey</p>
                                <div class="block-half">
                                    <label for="inputEmail">Alamat email</label>
                                    <input class="form-input" id="inputEmail" type="email" required>
                                </div>
                                <button class="btn btn--block btn--primary mb-16">Ubah passkey</button>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
