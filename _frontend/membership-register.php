<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <div class="bzg">
                        <div class="bzg_c" data-col="l6" data-offset="l3">
                            <h1 class="h2 home-section-heading text-center">Member registration</h1>

                            <div class="member-registration-steps">
                                <div class="member-registration-step is-active">1</div>
                                <div class="member-registration-step">2</div>
                            </div>

                            <form class="js-validate" action="member-registration-step-2.php">
                                <fieldset class="block">
                                    <legend><b>Personal information</b></legend>

                                    <div class="block-half">
                                        <label for="inputName">Name</label>
                                        <input class="form-input" id="inputName" type="text" required>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputEmail">Email address</label>
                                        <input class="form-input" id="inputEmail" type="email" required>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputTelephone">Telephone</label>
                                        <input class="form-input" id="inputTelephone" type="text" required>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputCellphone">Cellphone</label>
                                        <input class="form-input" id="inputCellphone" type="text" required>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputIDNumber">SIM/KTP number</label>
                                        <input class="form-input" id="inputIDNumber" type="text" required>
                                    </div>
                                    <fieldset class="block-half">
                                        <legend>Sex</legend>

                                        <label for="sexMale">
                                            <input id="sexMale" type="radio" name="sex" checked>
                                            <span>Male</span>
                                        </label>
                                        &nbsp;
                                        <label for="sexFemale">
                                            <input id="sexFemale" type="radio" name="sex">
                                            <span>Female</span>
                                        </label>
                                    </fieldset>
                                    <div class="block-half">
                                        <label for="inputBirthplace">Birth place</label>
                                        <input class="form-input" id="inputBirthplace" type="text" required>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputBirthday">Birthday</label>
                                        <input class="form-input js-datepicker" id="inputBirthday" type="text" required>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputReligion">Religion</label>
                                        <select class="form-input" id="inputReligion">
                                            <option value="Religion 1">Religion 1</option>
                                        </select>
                                    </div>
                                    <fieldset class="block-half">
                                        <legend>Marital status</legend>

                                        <label for="statusSingle">
                                            <input id="statusSingle" type="radio" name="status" checked>
                                            <span>Single</span>
                                        </label>
                                        &nbsp;
                                        <label for="statusMarried">
                                            <input id="statusMarried" type="radio" name="status">
                                            <span>Married</span>
                                        </label>
                                        &nbsp;
                                        <label for="statusOthers">
                                            <input id="statusOthers" type="radio" name="status">
                                            <span>Others</span>
                                        </label>
                                    </fieldset>
                                    <fieldset class="block-half">
                                        <legend>Nationality</legend>

                                        <label for="WNI">
                                            <input id="WNI" type="radio" name="nationality" checked>
                                            <span>WNI</span>
                                        </label>
                                        &nbsp;
                                        <label for="WNA">
                                            <input id="WNA" type="radio" name="nationality">
                                            <span>WNA</span>
                                        </label>
                                    </fieldset>
                                    <div class="block-half">
                                        <label for="inputOccupation">Occupation</label>
                                        <select class="form-input" id="inputOccupation">
                                            <option value="Occupation 1">Occupation 1</option>
                                            <option value="Occupation 2">Occupation 2</option>
                                            <option value="Occupation 3">Occupation 3</option>
                                        </select>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputShoppingPurpose">Shopping purpose</label>
                                        <select class="form-input" id="inputShoppingPurpose">
                                            <option value="Purpose 1">Purpose 1</option>
                                            <option value="Purpose 2">Purpose 2</option>
                                            <option value="Purpose 3">Purpose 3</option>
                                        </select>
                                    </div>
                                </fieldset>
                                <fieldset class="block">
                                    <legend><b>Current Address</b></legend>

                                    <div class="block-half">
                                        <label for="inputCurrentAddressStreet">Street name</label>
                                        <textarea class="form-input" id="inputCurrentAddressStreet" rows="3" required></textarea>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputCUrrentAddressCity">City</label>
                                        <select class="form-input" id="inputCUrrentAddressCity">
                                            <option value="City 1">City 1</option>
                                            <option value="City 2">City 2</option>
                                            <option value="City 3">City 3</option>
                                        </select>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputCurrentAddressZip">Zip code</label>
                                        <input class="form-input" id="inputCurrentAddressZip" type="text" required>
                                    </div>
                                </fieldset>
                                <fieldset class="block">
                                    <legend><b>Address on ID card</b></legend>

                                    <div class="block-half">
                                        <label for="inputIDAddressStreet">Street name</label>
                                        <textarea class="form-input" id="inputIDAddressStreet" rows="3" required></textarea>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputIDAddressCity">City</label>
                                        <select class="form-input" id="inputIDAddressCity">
                                            <option value="City 1">City 1</option>
                                            <option value="City 2">City 2</option>
                                            <option value="City 3">City 3</option>
                                        </select>
                                    </div>
                                    <div class="block-half">
                                        <label for="inputIDAddressZip">Zip code</label>
                                        <input class="form-input" id="inputIDAddressZip" type="text" required>
                                    </div>
                                </fieldset>
                                <div class="text-right block">
                                    <button class="btn btn--primary">Next</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
