<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Promotion</li>
                    </ul>

                    <div class="promotion">
                        <img class="promotion-banner" src="assets/img/promotion-banner.jpg" alt="">

                        <div class="promotion-body">
                            <div class="container">
                                <div class="promotion-navs">
                                    <a class="promotion-nav" href="#contentPromo1">Holiday promotion</a>
                                    <a class="promotion-nav" href="#contentPromo2">Promo info</a>
                                    <a class="promotion-nav" href="#contentPromo3">Member promo</a>
                                    <a class="promotion-nav" href="#contentPromo4">Product</a>
                                    <a class="promotion-nav promotion-nav--popup" href="#">Flash sale</a>
                                </div>

                                <div class="promotion-content">
                                    <img class="promotion-banner-mobile" src="assets/img/promotion-banner-mobile.jpg" alt="">
                                    <div class="promotion-content-section" id="contentPromo1">
                                        <h1>HOME FOR HOLIDAY INSPIRATIONS</h1>
                                        <p><time>22 November &mdash; 25 Desember 2017</time></p>

                                        <p><b>HOLIDAY IS COMING!!!</b></p>
                                        <p>Memasuki bulan Desember artinya sebentar lagi Hari Raya Natal akan tiba. Bersiaplah untuk mendekorasi rumah dengan pernak pernik Natal. Kegiatan ini selalu menjadi aktivitas wajib di rumah ketika menyambut hari yang penuh damai dan suka cita. Tak ketinggalan, makan malam bersama keluarga dan orang yang dikasihi saat malam Natal juga melengkapi momen indah ini menjadi lebih bahagia.</p>

                                        <a class="promotion-btn block-half" href="#">Download brochure</a>
                                        <a class="promotion-btn block-half" href="#">Download app</a>
                                    </div>
                                    <div class="promotion-content-section" id="contentPromo2">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, excepturi voluptates modi distinctio, enim porro consectetur accusantium vitae asperiores vero repellendus nihil quidem alias delectus aut labore praesentium doloribus deserunt ratione! Necessitatibus voluptates, eos repudiandae fuga ex maiores praesentium, magnam sapiente tempora aspernatur eveniet quam voluptas laudantium velit, facilis delectus.</p>
                                        <p>Ab voluptatem aliquid facere error labore deserunt repudiandae eos ut neque, quos ducimus minus impedit excepturi, nesciunt perspiciatis architecto iusto dolor culpa consectetur, asperiores quisquam cumque temporibus? Est rem voluptatem ducimus adipisci officia totam aperiam deserunt, voluptates quae maxime natus dignissimos, similique, delectus? Nostrum inventore natus itaque vitae nam praesentium.</p>
                                        <p>Quibusdam, nobis, non quo magnam id, veritatis architecto soluta libero similique beatae asperiores? Officiis, libero nesciunt sint? Odit recusandae totam molestiae, veniam quaerat odio ullam, officia praesentium voluptatem pariatur, error tempore quis possimus amet deserunt vel sed. Accusantium, omnis, officia! Sint recusandae quas quia alias, maxime iste optio reprehenderit unde.</p>
                                        <p>Molestias assumenda atque rerum error fugiat mollitia sequi laborum, quasi velit minima autem numquam expedita quibusdam dignissimos aspernatur accusamus tenetur exercitationem harum, magni, commodi repellendus. Minus ex iure accusantium unde assumenda at nisi aperiam veritatis tenetur asperiores dignissimos, magni provident voluptate dolorum numquam quas labore cupiditate! In quasi libero sapiente.</p>
                                        <p>Minima ducimus cumque voluptates quos itaque officia, doloribus dolore! Nesciunt suscipit corporis dolor mollitia a vel voluptatum veniam hic nostrum temporibus quisquam aliquam provident, molestias asperiores pariatur recusandae repellat! Perspiciatis hic rerum maiores adipisci optio alias praesentium, quisquam cumque libero aperiam error soluta quam ut provident, et cum voluptate aut.</p>

                                        <a class="promotion-btn block-half" href="#">Download brochure</a>
                                        <a class="promotion-btn block-half" href="#">Download app</a>
                                    </div>
                                    <div class="promotion-content-section" id="contentPromo3">
                                        <div class="member-promos">
                                            <div class="member-promos-item member-promos-item--big">
                                                <div class="member-promo member-promo--intro">
                                                    Nikmati potongan harga hingga 50% semua produk ACE berlaku untuk umum
                                                </div>
                                            </div>
                                            <div class="member-promos-item member-promos-item--small">
                                                <div class="member-promo member-promo--circle">
                                                    <img src="assets/img/member-promo-icon-1.png" alt="">
                                                </div>
                                                <h4 class="text-center">Penawaran spesial member &amp; bank partner</h4>
                                            </div>
                                            <div class="member-promos-item member-promos-item--small">
                                                <div class="member-promo member-promo--circle">
                                                    <img src="assets/img/member-promo-icon-2.png" alt="">
                                                </div>
                                                <h4 class="text-center">Gift with purchases</h4>
                                            </div>
                                            <div class="member-promos-item member-promos-item--small">
                                                <div class="member-promo member-promo--circle">
                                                    <img src="assets/img/member-promo-icon-3.png" alt="">
                                                </div>
                                                <h4 class="text-center">Tukar poin</h4>
                                            </div>
                                        </div>

                                        <a class="promotion-btn block-half" href="#">Download brochure</a>
                                        <a class="promotion-btn block-half" href="#">Download app</a>
                                    </div>
                                    <div class="promotion-content-section" id="contentPromo4">
                                        <div class="promotion-product-slider slider-style">
                                            <?php for ($i=0; $i < 3; $i++) { ?>
                                            <div>
                                                <figure class="promotion-product">
                                                    <img src="assets/img/promotion-product.jpg" alt="">
                                                    <figcaption>
                                                        <span class="label label--red">Hemat 10%</span>
                                                        <h2>CHAR-BOIL TUNGKU PEMANGGANG ARANG BASIC</h2>
                                                        <div>
                                                            <del>Rp2.300.000,00</del>
                                                            <h2>Rp1.979.100,00</h2>
                                                        </div>

                                                        <a class="promotion-product-btn" href="#">
                                                            <span>Buy at</span>
                                                            <img src="assets/img/logo-rupa-rupa-positive.png" alt="">
                                                            <span>Ace exclusive online retailer</span>
                                                        </a>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                            <?php } ?>
                                        </div>

                                        <a class="promotion-btn block-half" href="#">Download brochure</a>
                                        <a class="promotion-btn block-half" href="#">Download app</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

    <div class="promotion-popup">
        <div class="promotion-popup-dialog">
            <div class="text-right">
                <button class="promotion-popup-btn">Close <span class="fa fa-fw fa-times-circle"></span></button>
            </div>
            <div>
                <img class="full-width" src="assets/img/promo-popup.jpg" alt="">
            </div>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
