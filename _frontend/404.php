<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <div class="text-center">
                        <h1 class="text-red mb-4">404</h1>
                        <h2>Sorry, that page doesn't exist</h2>
                        <p>Periksa kembali link yang ingin kamu tuju atau kembali ke <a href="home.php">halaman depan</a>.</p>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
