<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <div class="membership-layout">
                        <?php include '_partials/membership-sidebar.php'; ?>

                        <div class="membership-layout-body">
                            <?php include '_partials/membership-nav.php'; ?>

                            <div class="membership-layout-content">
                                <section class="mb-48">
                                    <h2 class="home-section-heading">Riwayat transaksi</h2>

                                    <div class="table-wrapper">
                                        <table class="table js-table-enhanced">
                                            <thead>
                                                <tr>
                                                    <th align="left">Receipt</th>
                                                    <th align="left">Toko</th>
                                                    <th align="right">Tanggal</th>
                                                    <th align="right">Nominal</th>
                                                    <th align="right">Point</th>
                                                    <th align="right">Kupon</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php for ($i=0; $i < 50; $i++) { ?>
                                                <tr>
                                                    <td align="left">FRS<?= mt_rand(1000, 9999) ?></td>
                                                    <td align="left">Ace Puri Indah</td>
                                                    <td align="right">12/12/2017</td>
                                                    <td align="right">Rp<?= number_format(mt_rand(1000000, 5000000), 2, ',', '.') ?></td>
                                                    <td align="right"><?= mt_rand(50, 500) ?></td>
                                                    <td align="right"><?= mt_rand(10000, 99999) ?></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>

                                <section class="mb-48">
                                    <h2 class="home-section-heading">Riwayat redeem point</h2>

                                    <div class="table-wrapper">
                                        <table class="table js-table-enhanced">
                                            <thead>
                                                <tr>
                                                    <th align="left">Receipt</th>
                                                    <th align="left">Toko</th>
                                                    <th align="right">Tanggal</th>
                                                    <th align="right">Nominal</th>
                                                    <th align="right">Point</th>
                                                    <th align="right">Kupon</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php for ($i=0; $i < 30; $i++) { ?>
                                                <tr>
                                                    <td align="left">FRS<?= mt_rand(1000, 9999) ?></td>
                                                    <td align="left">Ace Puri Indah</td>
                                                    <td align="right">12/12/2017</td>
                                                    <td align="right">Rp<?= number_format(mt_rand(1000000, 5000000), 2, ',', '.') ?></td>
                                                    <td align="right"><?= mt_rand(50, 500) ?></td>
                                                    <td align="right"><?= mt_rand(10000, 99999) ?></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
