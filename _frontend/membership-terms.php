<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <h1 class="home-section-heading">Free Membership</h1>
                    <h2>Syarat dan ketentuan</h2>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis recusandae, fuga distinctio, sed maxime, odio unde pariatur explicabo eligendi cum illum libero ullam! Ab voluptatem nesciunt itaque nobis saepe veniam? Dicta ea, accusantium ipsum pariatur, animi maiores, corporis a qui asperiores accusamus voluptatem veritatis. Hic similique ut animi harum deserunt odio commodi numquam aspernatur asperiores, dignissimos et vero id ea.</p>
                    <p>Odio voluptatem, in maiores deleniti repellat odit iusto consequatur veniam quis explicabo. Quasi, cupiditate magni doloremque! Culpa officiis nisi impedit obcaecati eveniet a cupiditate nobis, quisquam laboriosam vel, laudantium, saepe quas aliquam magni amet vero possimus provident. Officiis ab et numquam totam corporis molestiae quibusdam adipisci ipsa fuga nemo repudiandae, exercitationem est debitis accusamus omnis natus, enim similique tempore soluta.</p>
                    <p>Dolor neque commodi eos asperiores fuga distinctio sapiente, perferendis aut eaque facere explicabo necessitatibus qui saepe dignissimos vel nostrum magni suscipit a nihil itaque earum voluptatem quidem minima et! Nobis labore, ducimus adipisci iste ipsa fugiat vel atque dolores est officia, sapiente quia magni earum cumque repellendus modi impedit, totam mollitia odio esse at error aspernatur. Eius quas dolorem velit!</p>
                    <p>Nisi non ea quis quas iusto totam quibusdam, harum maxime eos fugiat! Laudantium fugiat magni corrupti rerum saepe voluptatum officiis autem eaque laborum obcaecati deserunt ex blanditiis quos omnis totam facere deleniti dicta accusantium, laboriosam nesciunt id recusandae. Ducimus a officiis voluptatum itaque, distinctio unde nam esse neque tenetur dignissimos, officia totam amet molestias mollitia quos doloremque quis provident laborum.</p>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
