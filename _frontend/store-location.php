<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Store location</li>
                    </ul>

                    <h2 class="home-section-heading">Store location</h2>

                    <div class="store-location" id="storeLocation" data-stores="dev/stores.json" ng-app="StoreMap" ng-controller="StoreMapController as smc">
                        <div class="store-location-map"></div>

                        <div class="store-location-filter">
                            <div class="container">
                                <div class="store-location-regions">
                                    <span>By Region</span>
                                    <button class="store-location-regions-toggle" ng-click="smc.toggleRegionsList()" ng-class="{
                                        'is-active': smc.regionsListIsActive
                                    }">
                                        <span class="fa fa-fw fa-chevron-down"></span>
                                    </button>

                                    <div class="store-location-regions-content" ng-class="{
                                        'is-active': smc.regionsListIsActive
                                    }">
                                        <ul class="store-location-regions-list list-nostyle">
                                            <li ng-repeat="region in smc.data">
                                                <label for="{{ region.id }}">
                                                    <input id="{{ region.id }}" type="radio" name="region" value="{{ region.name }}">
                                                    <span>{{ region.name }}</span>
                                                </label>
                                            </li>
                                        </ul>

                                        <div class="v-center v-center--spread">
                                            <a id="setAllRegion" href="#">Show all</a>
                                            <button class="btn btn--primary" id="setRegion">Select</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="store-location-search">
                                    <input class="form-input" id="storeSearchInput" type="text" placeholder="Search city">
                                    <span class="fa fa-fw fa-search"></span>
                                </div>
                            </div>
                        </div>

                        <div class="container" ng-show="smc.results.length">
                            <h3 class="home-section-heading">Stores near {{ smc.searchKeyword }}</h3>

                            <ul class="store-list list-nostyle">
                                <li class="store-list-item" ng-repeat="result in smc.results | orderBy:'name'">
                                    <span>Ace Hardware</span>
                                    <h3>{{ result.name }}</h3>
                                    <p>{{ result.address }}</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
