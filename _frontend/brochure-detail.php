<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Brochure</li>
                    </ul>

                    <h1 class="home-section-heading">ACE CAWANG HOLIDAY SEASON</h1>

                    <div class="brochure-slider slider-style">
                        <?php for ($i=0; $i < 4; $i++) { ?>
                        <div>
                            <img src="assets/img/catalogue.jpg" alt="">
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
