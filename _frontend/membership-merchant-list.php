<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <figure class="member-hero">
                        <img src="//placehold.it/1100x500" alt="">
                        <figcaption class="member-hero-content">
                            <h1 class="text-caps">Merchant Promo</h1>
                            <h2>Dapatkan penawaran menarik di berbagai merchant favorit</h2>
                        </figcaption>
                    </figure>

                    <section class="home-section">
                        <h2 class="home-section-heading">Merchant list</h2>

                        <form class="merchant-filter">
                            <div class="merchant-filter-item">
                                <label for="selectCategory">Category</label>
                                <select class="form-input" id="selectCategory">
                                    <option value="Category 1">Category 1</option>
                                    <option value="Category 2">Category 2</option>
                                </select>
                            </div>
                            <div class="merchant-filter-item">
                                <label for="selectLocation">Location</label>
                                <select class="form-input" id="selectLocation">
                                    <option value="Location 1">Location 1</option>
                                    <option value="Location 2">Location 2</option>
                                </select>
                            </div>
                        </form>

                        <div class="fg fg-400-2 fg-480-3 fg-640-3 fg-768-4 fg-1024-5">
                            <?php for ($i=0; $i < 10; $i++) { ?>
                            <div>
                                <a class="text-center mb-16 anchor" href="#">
                                    <h4 class="text-caps mb-8">Optik Melawai</h4>
                                    <img class="full-width" src="//placehold.it/200x200" alt="">
                                    <div class="p-8 lh-1 mb-8">
                                        <h4 class="text-caps mb-2">Discount 50%</h4>
                                        <small>Lorem ipsum dolor sit amet, consectetur.</small>
                                    </div>
                                </a>
                            </div>
                            <?php } ?>
                        </div>

                        <?php include '_partials/back-button.php'; ?>
                    </section>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
