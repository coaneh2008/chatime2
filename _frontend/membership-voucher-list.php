<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <figure class="member-hero">
                        <img src="//placehold.it/1100x500" alt="">
                        <figcaption class="member-hero-content">
                            <h1 class="text-caps">Ace Voucher</h1>
                            <h2>Tukarkan poin anda dengan voucher belanja</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil voluptatibus voluptates placeat, debitis delectus perferendis cumque voluptatum facere optio ratione ab vero facilis ipsam reprehenderit illo a porro enim ut!</p>
                        </figcaption>
                    </figure>

                    <section class="home-section">
                        <h2 class="home-section-heading">Voucher list</h2>

                        <ul class="m-voucher-list">
                            <li class="mb-24">
                                <div class="voucher">
                                    <div class="voucher-left">
                                        <div class="voucher-value" data-prefix="" data-label="GIFT VOUCHER">10%</div>
                                        <div class="voucher-point">20 points</div>
                                    </div>
                                    <div class="voucher-right">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, maiores!</p>
                                    </div>
                                </div>
                            </li>
                            <li class="mb-24">
                                <div class="voucher">
                                    <div class="voucher-left">
                                        <div class="voucher-value" data-prefix="Rp." data-label="GIFT VOUCHER">50.000</div>
                                        <div class="voucher-point">20 points</div>
                                    </div>
                                    <div class="voucher-right">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, maiores!</p>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <?php include '_partials/back-button.php'; ?>
                    </section>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
