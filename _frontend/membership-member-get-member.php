<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <div class="membership-layout">
                        <?php include '_partials/membership-sidebar.php'; ?>

                        <div class="membership-layout-body">
                            <?php include '_partials/membership-nav.php'; ?>

                            <div class="membership-layout-content">
                                <div class="text-center">
                                    <h2>Ingin dapat poin tambahan gratis?</h2>
                                    <p>Ajak temanmu untuk menjadi member ACE Hardware menggunakan kode referral berikut</p>
                                    <div class="text-red h1"><b>EA70SR</b></div>
                                    <p>Bagikan kode referral di atas ke teman-teman kamu</p>
                                    <div class="social-medias">
                                        <a class="social-media --fb" href="#">
                                            <span class="fa fa-fw fa-facebook-official"></span>
                                        </a>
                                        <a class="social-media --tw" href="#">
                                            <span class="fa fa-fw fa-twitter"></span>
                                        </a>
                                        <a class="social-media --ig" href="#">
                                            <span class="fa fa-fw fa-instagram"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
