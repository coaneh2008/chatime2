<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <div class="membership-layout">
                        <?php include '_partials/membership-sidebar.php'; ?>

                        <div class="membership-layout-body">
                            <?php include '_partials/membership-nav.php'; ?>

                            <div class="membership-layout-content">
                                <h2 class="home-section-heading">Leaderboard</h2>

                                <div class="fg fg-480-2 fg-1024-3">
                                    <?php for ($i=0; $i < 6; $i++) { ?>
                                    <div>
                                        <div class="text-center p-12 bordered">
                                            <h3>Monthly Leaderboard</h3>
                                            <div class="bzg">
                                                <div class="bzg_c" data-col="s4" data-offset="s4"><hr></div>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis, pariatur.</p>
                                            <a class="btn btn--outline btn--primary btn--block" href="membership-profile-leaderboard-detail.php">Lihat detail</a>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
