import * as _ from './utils'

export default function () {
    const search = _.queryDoc('.site-search')
    const toggleSearch = () => {
        _.toggleClass(_.IS_ACTIVE)(search)
    }

    _.pipe(
        _.queryAllDoc,
        _.each(_.addEvent('click', _.pipe(
            _.get('currentTarget'),
            toggleSearch
        )))
    )('.js-search-trigger')
}
