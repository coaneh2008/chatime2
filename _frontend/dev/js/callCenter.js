import * as _ from './utils'

export default function () {
    _.exist('.js-call-center')
        .then(_.toJqueryObject)
        .then(($callCenter) => {
            if ( _.not(_.isLarge()) ) return;

            const $content = $callCenter.children()
            $callCenter.parent().append($content)
            $callCenter.remove()
        })
        .catch(_.noop)
}
