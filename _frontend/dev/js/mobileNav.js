import * as _ from './utils'

export default function () {
    const nav = _.queryDoc('.nav')
    const toggleBodyClass = () => {
        _.toggleClass('nav-active')(document.body)
    }

    _.pipe(
        _.queryDoc,
        _.addEvent('click', (e) => {
            e.preventDefault()
            _.addClass(_.IS_ACTIVE)(nav)
            toggleBodyClass()
        })
    )('.site-header-nav-trigger')

    _.pipe(
        _.queryDoc,
        _.addEvent('click', (e) => {
            e.preventDefault()
            _.removeClass(_.IS_ACTIVE)(nav)
            toggleBodyClass()
        })
    )('.nav-close-btn')
}
