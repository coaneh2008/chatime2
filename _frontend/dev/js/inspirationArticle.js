import * as _ from './utils'

export default function () {
    const init = (loader) => {
        let isLoading = false

        const getURL = (elem) => {
            return _.attr('data-next-article')(elem)
        }

        const setNextUrl = (url) => {
            loader.setAttribute('data-next-article', url)
        }

        const registerArticleToObserve = (article) => {
            new Hunt(article, {
                persist: true,
                enter() {
                    const slug = article.getAttribute('data-slug')
                    window.history.replaceState({}, '', slug)
                }
            })
            article.classList.add('observable')
        }

        const observer = new Hunt(loader, {
            persist: true,
            enter: (elem) => {
                if ( isLoading ) return;

                isLoading = true
                const uri = getURL(loader)

                _.getJSON(uri)
                    .then((data) => {
                        $(elem).before(data.content)

                        if ( !data.next ) {
                            observer.disconnect()
                            loader.style.visibility = 'hidden'
                        } else {
                            setNextUrl(data.next)
                        }

                        registerArticleToObserve(document.querySelector('.js-inspiration-article:not(.observable)'))

                        isLoading = false
                    })
                    .catch(_.noop)
            }
        })

        registerArticleToObserve(document.querySelector('.js-inspiration-article'))
    }

    _.exist('#articleLoader')
        .then(_.get(0))
        .then(init)
        .catch(_.noop)
}
