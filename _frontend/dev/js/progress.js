import * as _ from './utils'

export default function () {
    const calculateProgress = (el) => {
        const bar = _.query(el)('.progress-bar')
        const percentage = _.attr('data-percentage')(el)
        bar.style.width = `${percentage}%`
    }

    _.exist('.progress')
        .then(_.each(calculateProgress))
        .catch(_.noop)
}
