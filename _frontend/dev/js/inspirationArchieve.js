import * as _ from './utils'

export default function () {
    const $archieveList = $('.inspiration-list-archieve')
    const openPanel = ($el) => {
        const $list = $el.next()

        if ( $list.hasClass(_.IS_ACTIVE) ) return;

        $archieveList.slideUp()
        $list.slideDown()
        window.setTimeout(() => {
            $archieveList.removeClass(_.IS_ACTIVE)
            $list.addClass(_.IS_ACTIVE)
        }, 500)
    }

    _.pipe(
        _.queryAllDoc,
        _.each(_.addEvent('click', _.pipe(
            _.get('currentTarget'),
            _.toJqueryObject,
            openPanel
        )))
    )('.inspiration-list-archieve-btn')
}
