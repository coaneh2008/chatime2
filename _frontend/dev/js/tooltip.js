import * as _ from './utils'

export default function () {
    const showTooltip = (e) => {
        const { currentTarget: el } = e
        const content = _.attr('data-tooltip')(el)
        window.launchModal(content)
    }

    _.exist('.tooltip')
        .then(_.each(_.addEvent('click', showTooltip)))
        .catch(_.noop)
}
