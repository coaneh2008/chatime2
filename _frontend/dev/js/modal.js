import * as _ from './utils'

export default function () {
    const openModal = (e) => {
        const { currentTarget: btn } = e
        const targetId = _.attr('data-trigger-modal')(btn)
        const target = _.queryDoc(targetId)
        _.addClass(_.IS_ACTIVE)(target)
    }

    const registerCloseEvent = () => {
        $(document.body).on('click.closeModal', '.modal-dialog-close', (e) => {
            $(e.currentTarget).parents('.modal').removeClass(_.IS_ACTIVE)
        })
    }

    _.exist('[data-trigger-modal]')
        .then(_.each(_.addEvent('click', openModal)))
        .catch(_.noop)

    registerCloseEvent()

    window.launchModal = (content) => {
        const modal = `<div class="modal">
            <div class="modal-dialog">
                <button class="modal-dialog-close">Close &times;</button>
                ${content}
            </div>
        </div>`
        const $modal = $(modal)
        $(document.body).append($modal)
        $modal.addClass(_.IS_ACTIVE)
    }
}
