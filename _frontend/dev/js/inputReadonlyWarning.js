import * as _ from './utils'

export default function () {
    _.exist('.js-readonly-trigger-warning')
        .then(_.each(_.addEvent('click', (e) => {
            const { currentTarget } = e
            const content = _.attr('data-warning')(currentTarget)
            window.launchModal(content)
        })))
        .catch(_.noop)
}
