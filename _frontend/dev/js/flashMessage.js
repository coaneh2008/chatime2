import * as _ from './utils'

export default function () {
    _.exist('#flashMsgContent a')
        .then((elems) => {
            const INTERVAL = $('#flashMsgContent').data('interval')
            const $msg = $('.flash-msg')
            const n = elems.length
            let counter = 0
            const updateContent = (c) => {
                const color = elems[c].getAttribute('data-color')
                $msg.css('background', color)
                $msg.empty().append(elems[c])
            }
            updateContent(counter)
            counter++;

            window.setInterval(() => {
                updateContent(counter)
                counter++;

                if ( counter > n - 1 ) {
                    counter = 0
                }
            }, INTERVAL)
        })
        .catch(_.noop)
}
