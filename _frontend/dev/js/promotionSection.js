import * as _ from './utils'

export default function () {
    const $promotion = $('.promotion')
    const $promotionContent = $('.promotion-content-section')
    const $promotionSlider = $('.promotion-product-slider')
    const $promotionPopup = $('.promotion-popup')
    const $bannerMobile = $('.promotion-banner-mobile')
    let hasSlickBuilt = false

    $(document).on('click.closePromotionPopup', '.promotion-popup-btn', (e) => {
        e.preventDefault()
        $promotionPopup.removeClass(_.IS_ACTIVE)
    })

    _.exist('.promotion')
        .then(_.get(0))
        .then(_.findChild('.promotion-nav'))
        .then(_.each(_.addEvent('click', (e) => {
            _.pipe(
                _.get('currentTarget'),
                _.hasClass('promotion-nav--popup'),
                _.runEither(showPopup(e), showSection(e))
            )(e)
        })))
        .catch(_.noop)

    function showPopup(event) {
        return function () {
            event.preventDefault()
            $bannerMobile.hide()
            $promotionPopup.addClass(_.IS_ACTIVE)
        }
    }

    function showSection(event) {
        return function () {
            $bannerMobile.hide()
            event.preventDefault()
            const $this = $(event.currentTarget)
            let $target = $this.attr('href')
            $target = $($target)
            $this.siblings().removeClass(_.IS_ACTIVE)
            $promotionContent.removeClass(_.IS_ACTIVE)
            $this.addClass(_.IS_ACTIVE)
            $promotion.addClass(_.IS_ACTIVE)
            $target.addClass(_.IS_ACTIVE)

            initPromotionSlider()
        }
    }

    function initPromotionSlider() {
        if ( $promotionSlider.parent().hasClass(_.IS_ACTIVE) ) {
            if ( hasSlickBuilt ) {
                destroyPromotionSlider()
            }

            _.createSlider()($promotionSlider)
            hasSlickBuilt = true
        }
    }

    function destroyPromotionSlider() {
        $promotionSlider.slick('unslick')
    }
}
