import * as _ from './utils'

export default function () {
    _.pipe(
        _.queryDoc,
        _.addEvent('click', (e) => {
            e.preventDefault()
            const { currentTarget: target } = e
            const iOSUrl = _.attr('data-ios')(target)
            const androidUrl = _.attr('data-android')(target)
            const downloadUrl = $.pgwBrowser().os.group === 'iOS' || $.pgwBrowser().os.group === 'Mac OS' ? iOSUrl : androidUrl;

            window.open(downloadUrl)
        })
    )('.js-app-download')
}
