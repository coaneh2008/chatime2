import * as _ from './utils'
import activeStateMobile from './activeStateMobile'
import WPViewportFix from './windowsPhoneViewportFix'
import objectFitPolyfill from './objectFitPolyfill'
import homePromotionsSlider from './homePromotionsSlider'
import mainSlider from './mainSlider'
import categoryDropdown from './categoryDropdown'
import mobileNav from './mobileNav'
import mobileSearch from './mobileSearch'
import inspirationArchieve from './inspirationArchieve'
import formValidation from './formValidation'
import brochureSlider from './brochureSlider'
import promotionSection from './promotionSection'
import storeLocationMap from './storeLocationMap'
import downloadApp from './downloadApp'
import featuredBrands from './featuredBrands'
import alert from './alert'
import memberBenefits from './memberBenefits'
import jsDatepicker from './jsDatepicker'
import memberRegistrationForm from './memberRegistrationForm'
import modal from './modal'
import tooltip from './tooltip'
import inputReadonlyWarning from './inputReadonlyWarning'
import progress from './progress'
import tableEnhanced from './tableEnhanced'
import flashMessage from './flashMessage'
import backButton from './backButton'
import inspirationArticle from './inspirationArticle'
import callCenter from './callCenter'

const App = {
    activeStateMobile,
    WPViewportFix,
    objectFitPolyfill,
    homePromotionsSlider,
    mainSlider,
    categoryDropdown,
    mobileNav,
    mobileSearch,
    inspirationArchieve,
    formValidation,
    brochureSlider,
    promotionSection,
    storeLocationMap,
    downloadApp,
    featuredBrands,
    alert,
    memberBenefits,
    jsDatepicker,
    memberRegistrationForm,
    modal,
    tooltip,
    inputReadonlyWarning,
    progress,
    tableEnhanced,
    flashMessage,
    backButton,
    inspirationArticle,
    callCenter,
}

for (let fn in App) {
    App[fn]()
}

export default App
