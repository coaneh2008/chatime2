import * as _ from './utils'

export default function () {
    const initDatepicker = ($elems) => {
        $elems.datepicker({
            dateFormat: `yyyy/mm/dd`,
            language: `en`
        })
    }

    _.exist('.js-datepicker')
        .then(_.toJqueryObject)
        .then(initDatepicker)
        .catch(_.noop)
}
