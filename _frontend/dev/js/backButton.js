import * as _ from './utils'

export default function () {
    _.exist('.js-back-button')
        .then(_.toJqueryObject)
        .then(($els) => {
            $els.on('click', (e) => {
                e.preventDefault()
                window.history.back()
            })
        })
        .catch(_.noop)
}
