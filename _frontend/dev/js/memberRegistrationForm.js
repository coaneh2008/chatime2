import * as _ from './utils'

export default function () {
    _.exist('.form-membership-step-2')
        .then(_.toJqueryObject)
        .then(($form) => {
            const onValidating = (e) => {
                const $inputAdminFee = $('#inputOnline, #inputOffline, #inputFreeMembership')
                const $inputRenewal = $('#renewalYes, #renewalNo')
                const $inputAgree = $('#inputAgree')
                const $msgAdminFee = $('#inputAdminFeeMsg')
                const $msgRenewal = $('#inputRenewalMsg')
                const $msgAgree = $('#inputAgreeMsg')
                const checkedInputAdminFee = $inputAdminFee.toArray().filter(input => input.checked)
                const checkedInputRenewal = $inputRenewal.toArray().filter(input => input.checked)

                $msgAdminFee.hide()
                $msgRenewal.hide()
                $msgAgree.hide()

                if ( !checkedInputAdminFee.length ) {
                    e.preventDefault()
                    $msgAdminFee.show()
                }

                if ( !checkedInputRenewal.length ) {
                    e.preventDefault()
                    $msgRenewal.show()
                }

                if ( !$inputAgree.is(':checked') ) {
                    e.preventDefault()
                    $msgAgree.show()
                }
            }

            $form.bazeValidate({ onValidating })
        })
        .then(() => {
            $('#renewalYes, #renewalNo').on('change', (e) => {
                if ( $('#renewalNo').is(':checked') ) {
                    $('#modalConfirmRenewal').addClass(_.IS_ACTIVE)
                }
            })

            $('#inputOnline, #inputOffline, #inputFreeMembership').on('change', (e) => {
                if ( $('#inputFreeMembership').is(':checked') ) {
                    $('#modalFreeMembership').addClass(_.IS_ACTIVE)
                }

                if ( $('#inputOnline').is(':checked') ) {
                    $('#modalOnlineRegistration').addClass(_.IS_ACTIVE)
                }

                if ( $('#inputOffline').is(':checked') ) {
                    $('#modalOfflineRegistration').addClass(_.IS_ACTIVE)
                }
            })
        })
        .catch(_.noop)
}
