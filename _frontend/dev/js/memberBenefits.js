import * as _ from './utils'

export default function () {
    const registerAction = (slider) => {
        const $panels = $('.member-benefit-panel')
        const $anchors = $('[data-benefit-panel]')
        const $benefits = $('.member-benefit')

        const openPanel = (el) => {
            const targetId = _.attr('data-benefit-panel')(el)
            const $target = $(targetId)
            const $this = $(el)
            const $currentParent = $this.parent()

            if ( $this.hasClass(_.IS_ACTIVE) ) {
                $target.slideUp()
                $this.removeClass(_.IS_ACTIVE)
                $currentParent.removeClass(_.IS_ACTIVE)
            } else {
                $panels.slideUp()
                $target.slideToggle()
                $anchors.removeClass(_.IS_ACTIVE)
                $benefits.removeClass(_.IS_ACTIVE)
                $this.addClass(_.IS_ACTIVE)
                $currentParent.addClass(_.IS_ACTIVE)
            }
        }

        $anchors.on('click', _.pipe(
            _.preventDefault,
            _.get('currentTarget'),
            openPanel
        ))

        slider.on('afterChange', (slick, current) => {
            $anchors.removeClass(_.IS_ACTIVE)
            $benefits.removeClass(_.IS_ACTIVE)
            $panels.slideUp()
        })
    }

    _.exist('.member-benefits')
        .then(_.toJqueryObject)
        .then(_.createSlider({
            prevArrow: _.sliderButton('prev', `<span class="fa fa-fw fa-angle-left"></span>`),
            nextArrow: _.sliderButton('next', `<span class="fa fa-fw fa-angle-right"></span>`),
            slidesToShow: 1,
            slidesToScroll: 1,
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 399,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 1023,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                }
            ]
        }))
        .then(registerAction)
        .catch(_.noop)
}
