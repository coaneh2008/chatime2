import * as _ from './utils'

export default function () {
    const init = ($table) => {
        $table.DataTable({
            lengthChange: false,
            searching: false,
            info: false,
            pagingType: 'numbers',
            responsive: true,
            ordering: false
        })
    }

    _.exist('.js-table-enhanced')
        .then(_.toJqueryObject)
        .then(init)
        .catch(_.noop)
}
