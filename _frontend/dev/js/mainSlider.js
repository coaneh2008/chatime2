import * as _ from './utils'

export default function () {
    _.exist('.main-slider')
        .then(_.toJqueryObject)
        .then(_.createSlider({
            prevArrow: `<button class="slick-prev"><span class="fa fa-fw fa-angle-left"></span></button>`,
            nextArrow: `<button class="slick-next"><span class="fa fa-fw fa-angle-right"></span></button>`
        }))
        .catch(_.noop)
}
