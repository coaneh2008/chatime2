import * as _ from './utils'

export default function () {
    const dismissAlert = ($el) => $el.fadeOut()

    _.pipe(
        _.queryAllDoc,
        _.each(_.addEvent('click', _.pipe(
            _.preventDefault,
            _.get('currentTarget'),
            _.get('parentElement'),
            _.toJqueryObject,
            dismissAlert
        )))
    )('.alert-close')
}
