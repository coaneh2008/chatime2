import * as _ from './utils'

export default function () {
    const dropdown = _.query(document)('.category-dropdown')
    const nav = _.query(document)('.nav')
    const $anchorsLevel1 = $('.category-list-anchor--level-1')
    const $anchorsLevel2 = $('.category-list-anchor--level-2')
    const $dropdownLevel2 = $('.category-list--level-2')

    _.pipe(
        _.queryDoc,
        _.addEvent('click', (e) => {
            if ( _.isLarge() ) {
                e.preventDefault()
                _.toggleClass(_.IS_ACTIVE)(dropdown)
                $('.category-list-item').eq(0).trigger('mouseenter')
                _.toggleClass('nav-active')(document.body)
            }
        })
    )('.categories-trigger')

    _.pipe(
        _.queryDoc,
        _.addEvent('click', () => {
            _.removeClass(_.IS_ACTIVE)(dropdown)
            _.removeClass(_.IS_ACTIVE)(nav)
            _.removeClass('nav-active')(document.body)
        })
    )('.site-cover')

    $anchorsLevel1.parent().on('mouseenter', _.pipe(
        _.get('currentTarget'),
        _.toJqueryObject,
        $target => {
            const $directDropdown = $target.find('> .category-list')
            const $subdropdown = $directDropdown.find('.category-list').eq(0)
            $dropdownLevel2.removeClass(_.IS_ACTIVE)
            $directDropdown.addClass(_.IS_ACTIVE)
            $subdropdown.addClass(_.IS_ACTIVE)
        }
    ))

    $anchorsLevel2.parent().on('mouseenter', _.pipe(
        _.get('currentTarget'),
        _.toJqueryObject,
        $target => {
            const $directDropdown = $target.find('> .category-list')
            $directDropdown.addClass(_.IS_ACTIVE)
        }
    ))
}
