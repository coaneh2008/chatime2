<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <figure class="member-hero">
                        <img src="//placehold.it/1100x500" alt="">
                        <figcaption class="member-hero-content">
                            <h1 class="text-caps">Member Promo</h1>
                            <h2>Beragam penawaran spesial hanya untuk member</h2>
                        </figcaption>
                    </figure>

                    <section class="home-section">
                        <h2 class="home-section-heading">Member promo</h2>

                        <div class="fg fg-400-2 fg-480-3 fg-640-3 fg-768-4 fg-1024-5">
                            <?php for ($i=0; $i < 5; $i++) { ?>
                            <div>
                                <a class="anchor" href="membership-promo-detail.php">
                                    <img class="full-width" src="//placehold.it/200x200" alt="">
                                    <div class="p-8">
                                        <div class="mb-8 lh-1">
                                            <small>Periode Promo</small>
                                            <br>
                                            <small><b>19-28 Feb 2018</b></small>
                                        </div>
                                        <h4 class="mb-12">Ace decoration sale! up to 70%</h4>
                                        <span class="btn btn--outline btn--block btn--primary">Lihat detail</span>
                                    </div>
                                </a>
                            </div>
                            <?php } ?>
                        </div>

                        <?php include '_partials/back-button.php'; ?>
                    </section>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
