<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Privacy policy</li>
                    </ul>

                    <div class="bzg">
                        <div class="bzg_c" data-col="l9">
                            <h2 class="home-section-heading">Privacy policy</h2>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui in voluptatum ratione amet quo quis dolorum explicabo distinctio commodi cum tenetur debitis quas repellendus aspernatur neque magni hic suscipit nihil, est libero enim quod nulla esse id labore. Et, excepturi velit. Deserunt libero enim quidem nobis. Reiciendis saepe minus veritatis, tenetur exercitationem laboriosam, deserunt ipsa at qui fugiat minima maiores sit hic veniam laudantium natus quidem voluptatum est! Temporibus, explicabo?</p>
                            <p>Ad quas consectetur possimus eligendi corporis, nostrum itaque quisquam debitis, inventore iusto dignissimos voluptate, sunt error consequatur quos omnis veritatis ratione cumque quod, eveniet optio voluptatum at aliquam! Sed eveniet qui, nihil libero eos est at repudiandae quaerat natus numquam rerum quis odit nam vel voluptas magni distinctio expedita veniam veritatis aspernatur, sint nesciunt quibusdam architecto deleniti! Velit quidem omnis, minima alias laboriosam quae consectetur, atque nemo, reiciendis cupiditate voluptas.</p>
                            <p>Nesciunt impedit unde non perferendis possimus magni dicta enim saepe dolores ratione sapiente, necessitatibus, deleniti eligendi quo, explicabo laudantium tempore quae neque ex quibusdam iure aut aspernatur repudiandae pariatur. Sequi officia, repudiandae vitae asperiores temporibus. Ducimus explicabo, architecto eos nam culpa quos laudantium a eveniet officiis sequi praesentium asperiores quas dolores atque error obcaecati temporibus adipisci aut dolor recusandae. Est, maiores facere, velit porro voluptatem quaerat hic veritatis eligendi asperiores.</p>
                            <p>Deleniti asperiores maiores pariatur mollitia, rem sequi quos quod, illum laboriosam et ut quas, ex impedit illo repudiandae molestias quam corrupti minus nesciunt cum a. Magnam laborum nihil labore doloremque commodi accusantium quam eos, vitae ab ducimus quas quae aperiam maxime autem repudiandae doloribus porro hic? Incidunt ad, asperiores! Quia fugit, repudiandae maiores esse eum omnis, harum ad ea ab voluptate obcaecati ratione consequuntur nostrum temporibus aut quos ut deserunt?</p>
                            <p>Veritatis sunt quidem esse, nostrum et reiciendis iste, ea aut? Eligendi, itaque quos aut laborum aperiam autem quaerat, suscipit earum mollitia. Veritatis officia quia harum earum repellendus deleniti, modi quis accusantium quasi cumque, distinctio ea quibusdam nisi, beatae adipisci odit quos, quidem. Dolor atque ea illum iure impedit deserunt quae magnam. In fugit quidem minima incidunt deserunt, tenetur aperiam asperiores ab, dolorum qui eveniet perferendis, ducimus maiores! Consectetur, dignissimos, obcaecati!</p>
                            <p>Nesciunt, vitae? Expedita cupiditate, molestiae ipsa quia hic aut magnam consequatur voluptates, nam vero numquam illum excepturi ullam inventore autem perferendis vel, eligendi, delectus pariatur obcaecati architecto qui rem. Sit facere distinctio laudantium veniam nemo laborum, quam voluptas officia quia qui magni nesciunt repellat error animi eum a obcaecati consequatur sed unde recusandae, quo placeat laboriosam corrupti consequuntur. Veniam temporibus quibusdam omnis, aperiam. Fugit expedita, alias, voluptatem odit in velit.</p>
                            <p>Nulla sint vero, deserunt expedita architecto ducimus nobis repellendus, deleniti odit voluptate, molestiae modi sunt error dolor! Delectus corrupti sunt dolorem odit reprehenderit, quas, sit quaerat magnam laboriosam qui eveniet, odio dignissimos aperiam ipsum pariatur excepturi, at rem incidunt esse mollitia omnis obcaecati minus tenetur molestias harum! Deserunt dolorum, assumenda excepturi ipsa animi ab nisi, harum dolor eos incidunt quas? Ut autem maiores, harum, laboriosam ipsum inventore beatae odio ducimus.</p>
                            <p>Atque dolorum, iure ipsum voluptatem iste ratione animi itaque accusantium quod explicabo soluta temporibus quaerat provident ex non. Labore voluptates dolore beatae ratione consequatur obcaecati odio ab, eos maxime repellendus ipsum dolorum blanditiis rerum necessitatibus nostrum ea eius velit vitae, optio totam laborum vel? Maxime alias eos ipsa, sequi voluptatibus ullam. Sequi cupiditate adipisci ducimus, molestias ea laudantium in neque similique ab pariatur, sed, quibusdam nisi, rerum excepturi magni autem.</p>
                            <p>Veniam explicabo soluta commodi fugit, necessitatibus id quibusdam, qui dignissimos dicta accusamus nostrum nesciunt quae sapiente perspiciatis natus unde architecto! Distinctio eius laboriosam, saepe quibusdam tenetur alias, modi, nesciunt iure aliquid animi vero nobis? Eveniet totam dolorum cupiditate necessitatibus unde error, quo incidunt doloremque veniam voluptate accusamus aut eligendi explicabo neque dolores molestias, doloribus harum pariatur voluptates magnam et, ullam sint fugiat? Numquam soluta eos quaerat aliquid veniam, tenetur cumque?</p>
                            <p>Saepe dolorem magni necessitatibus commodi porro reprehenderit quam laborum ab quos, quaerat nobis accusantium voluptate eos! Voluptatum libero distinctio beatae nobis minus fugit obcaecati facilis atque ut dolore asperiores aut, illum eius quis cupiditate ex iure sit vitae natus placeat aspernatur. Vel beatae sit, adipisci magnam maiores, non explicabo saepe autem repudiandae nisi id qui. Beatae ipsa, sequi reprehenderit earum ex magnam nobis. Aperiam pariatur ab impedit, tempore laudantium temporibus!</p>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
