<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Contact us</li>
                    </ul>

                    <div class="contact-layout" style="background-image: url(assets/img/contact-bg.jpg);">
                        <div class="contact-layout-content">
                            <div class="alert alert--success">
                                Thank you for contacting us. You will hear from us soon.
                                <button class="alert-close">&times;</button>
                            </div>
                            <div class="alert alert--alert">
                                Something went wrong.
                                <button class="alert-close">&times;</button>
                            </div>

                            <h2 class="home-section-heading">Contact us</h2>

                            <form class="js-validate" action="#">
                                <div class="contact-form-row">
                                    <div>
                                        <label class="sr-only" for="inputName">Name</label>
                                        <input class="form-input" id="inputName" type="text" placeholder="Name" required>
                                    </div>
                                    <div>
                                        <label class="sr-only" for="inputAddress">Address</label>
                                        <input class="form-input" id="inputAddress" type="text" placeholder="Address" required>
                                    </div>
                                </div>
                                <div class="contact-form-row">
                                    <div>
                                        <label class="sr-only" for="inputPhone">Phone</label>
                                        <input class="form-input" id="inputPhone" type="text" placeholder="Phone" required>
                                    </div>
                                    <div>
                                        <label class="sr-only" for="inputEmail">Email</label>
                                        <input class="form-input" id="inputEmail" type="email" placeholder="Email" required>
                                    </div>
                                </div>
                                <div class="contact-form-row">
                                    <div>
                                        <label class="sr-only" for="inputMessage">Message</label>
                                        <textarea class="form-input" id="inputMessage" cols="30" rows="5" placeholder="Message" required></textarea>
                                    </div>
                                </div>
                                <button class="btn btn--wide btn--primary">Send</button>
                                &nbsp;
                                &nbsp;
                                <span>info@acehardware.co.id</span>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
