<div class="membership-layout-sidebar">
    <div class="text-center">
        <figure>
            <img class="img-rounded mb-8" src="//placehold.it/120x120" alt="">
            <figcaption>
                <h4 class="mb-8">Daisy Mulia</h4>
                <div>ID: 856123</div>
                <div class="mb-8">exp date: 3/12/2019</div>
                <a class="btn btn--small btn--outline btn--secondary" href="membership-profile-edit.php">Edit</a>
            </figcaption>
        </figure>
    </div>

    <div class="text-center block">
        <label class="toggler" for="autoRenewal">
            <span>Auto renewal</span>
            <input class="toggler-input sr-only" id="autoRenewal" type="checkbox" checked>
            <span class="toggler-icon fa"></span>
        </label>
        <span class="tooltip" data-tooltip="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis deserunt dicta autem accusantium, dignissimos fuga?">
            <span class="fa fa-lg fa-fw fa-question-circle"></span>
        </span>
    </div>

    <table>
        <tr>
            <td style="vertical-align: top;">Total points asdasd asdasd</td>
            <td style="vertical-align: top;" class="text-red" align="right">0 Point</td>
            <td style="vertical-align: top;">
                <span class="tooltip" data-tooltip="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis deserunt dicta autem accusantium, dignissimos fuga?">
                    <span class="fa fa-lg fa-fw fa-question-circle"></span>
                </span>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Point expired</td>
            <td style="vertical-align: top;" class="text-red" align="right">0 Point</td>
            <td style="vertical-align: top;">
                <span class="tooltip" data-tooltip="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis deserunt dicta autem accusantium, dignissimos fuga?">
                    <span class="fa fa-lg fa-fw fa-question-circle"></span>
                </span>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Point exp. date</td>
            <td style="vertical-align: top;" class="text-red" align="right">30/Jun/2018</td>
            <td style="vertical-align: top;">
                <span class="tooltip" data-tooltip="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis deserunt dicta autem accusantium, dignissimos fuga?">
                    <span class="fa fa-lg fa-fw fa-question-circle"></span>
                </span>
            </td>
        </tr>
    </table>
</div>
