<div class="main-slider slider-style slider-style--primary" data-slick='{
    "autoplay": true,
    "autoplaySpeed": 5000,
    "pauseOnHover": false
}'>
    <div>
        <a data-fancybox href="https://www.youtube.com/watch?v=wpzeD1nwB8g&control=0&showinfo=0">
            <img src="assets/img/banner-1.jpg" alt="">
        </a>
    </div>
    <div>
        <a href="#">
            <img src="assets/img/banner-2.jpg" alt="">
        </a>
    </div>
</div>
