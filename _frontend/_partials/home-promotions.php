<section class="home-section">
    <h2 class="home-section-heading">Promotion</h2>

    <div class="home-promotions">
        <div>
            <div class="home-promotions-slider slider-style slider-style--primary">
                <div>
                    <a href="#">
                        <img src="assets/img/home-promotion-big.jpg" alt="">
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="assets/img/home-promotion-big.jpg" alt="">
                    </a>
                </div>
            </div>
        </div>
        <div>
            <a href="#">
                <img class="home-promotions-small" src="assets/img/home-promotion-small-1.jpg" alt="">
            </a>
            <a href="#">
                <img class="home-promotions-small" src="assets/img/home-promotion-small-2.jpg" alt="">
            </a>
        </div>
    </div>
</section>
