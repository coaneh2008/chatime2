<section class="home-section">
    <h2 class="home-section-heading">Product category</h2>

    <ul class="section-category-list list-nostyle">
        <?php $categoriesList = [
            ['Home and Living', 'icon-category-home-living.png'],
            ['Furniture', 'icon-category-furniture.png'],
            ['Home Improvement', 'icon-category-home-improvement.png'],
            ['Hobbies and Lifestyle', 'icon-category-hobby-lifestyle.png'],
            ['Electronic and Gadget', 'icon-category-electronic.png'],
            ['Kitchen', 'icon-category-kitchen.png'],
            ['Bed and Bath', 'icon-category-bed-bath.png'],
            ['Automotive', 'icon-category-automotive.png'],
            ['Health and Sport', 'icon-category-health-sport.png'],
            ['Toy and Baby', 'icon-category-toy.png'],
            ['Holiday Gift', 'icon-category-gift.png'],
            ['Best Deal', 'icon-category-deal.png'],
        ] ?>
        <?php foreach ($categoriesList as $key => $value) { ?>
        <li>
            <a href="product.php">
                <img src="assets/img/<?= $value[1] ?>" alt="">
                <span><?= $value[0] ?></span>
            </a>
        </li>
        <?php } ?>
    </ul>
</section>
