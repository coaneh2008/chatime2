<footer class="footer">
    <div class="container has-border">
        <div class="footer-sections">
            <div class="footer-section">
                <h4 class="footer-heading">Ace hardware <img src="assets/img/flag-indonesia.jpg" width="20"></h4>

                <ul class="footer-navs list-nostyle">
                    <li><a href="#">Company</a></li>
                    <li><a href="#">Career</a></li>
                    <li><a href="promotion.php">Promotion</a></li>
                    <li><a href="inspiration-list.php">Inspiration</a></li>
                </ul>
            </div>
            <div class="footer-section">
                <h4 class="footer-heading">Customer services</h4>

                <ul class="footer-navs list-nostyle">
                    <li><a href="contact.php">Contact us</a></li>
                    <li><a href="privacy-policy.php">Privacy policy</a></li>
                    <li><a href="terms.php">Terms &amp; conditions</a></li>
                    <li><a href="store-location.php">Store location</a></li>
                </ul>
            </div>
            <div class="footer-section">
                <h4 class="footer-heading">Call center</h4>

                <a class="js-call-center" href="tel:0215829100">
                    <p><span class="fa fa-fw fa-phone"></span> 021-5829100</p>
                </a>
            </div>
            <div class="footer-section">
                <form class="footer-subscribe">
                    <h4 class="footer-heading"><label for="inputEmailSubscribe">Subscribe newsletter</label></h4>
                    <input class="form-input block-half" id="inputEmailSubscribe" type="text" placeholder="Email anda">
                    <button class="btn btn--block btn--primary">
                        <span class="fa fa-fw fa-envelope-o"></span>
                        Subscribe
                    </button>
                </form>
            </div>
            <div class="footer-section">
                <h4 class="footer-heading">Social media</h4>

                <div class="social-medias">
                    <a class="social-media --fb" href="#">
                        <span class="fa fa-fw fa-facebook-official"></span>
                    </a>
                    <a class="social-media --tw" href="#">
                        <span class="fa fa-fw fa-twitter"></span>
                    </a>
                    <a class="social-media --ig" href="#">
                        <span class="fa fa-fw fa-instagram"></span>
                    </a>
                    <a class="social-media --gp" href="#">
                        <span class="fa fa-fw fa-google-plus"></span>
                    </a>
                    <a class="social-media --yt" href="#">
                        <span class="fa fa-fw fa-youtube-play"></span>
                    </a>
                </div>

                <a class="text-orange" href="social-wall.php">[Check our story]</a>
            </div>
        </div>
    </div>
    <div class="copyrights">
        <div class="container">
            &copy; 2018 Ace Hardware Corporation. Ace Hardware and Ace Hardware logo are registered trademarks of Ace Hardware Corporation. All rights reserved.
        </div>
    </div>
</footer>
