<div class="home-section">
    <h2 class="home-section-heading">Inspirations</h2>

    <div class="section-inspirations">
        <div class="section-inspiration-big">
            <a href="inspiration-detail.php">
                <img src="assets/img/inspiration-big-1.jpg" alt="">
            </a>
        </div>
        <div class="section-inspiration-small">
            <a href="inspiration-detail.php">
                <img src="assets/img/inspiration-small-1.jpg" alt="">
            </a>
            <a href="inspiration-detail.php">
                <img src="assets/img/inspiration-small-2.jpg" alt="">
            </a>
        </div>
        <div class="section-inspiration-big">
            <a href="inspiration-detail.php">
                <img src="assets/img/inspiration-big-2.jpg" alt="">
            </a>
        </div>
    </div>
</div>
