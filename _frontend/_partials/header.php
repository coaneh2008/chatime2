<header class="site-header">
    <div class="container">
        <button class="site-header-nav-trigger">
            <span class="fa fa-fw fa-bars"></span>
        </button>
        <button class="site-header-search-trigger js-search-trigger">
            <span class="fa fa-fw"></span>
        </button>

        <div class="site-header-tops">
            <a class="site-logo" href="home.php">
                <img src="assets/img/logo-ace-hardware.png" alt="">
            </a>
            <div class="site-header-supports">
                <form class="site-search" role="search" action="search-results.php">
                    <input class="form-input" type="search" placeholder="Hello, what are you searching for?">
                    <button class="site-search-btn">
                        <span class="fa fa-fw fa-search"></span>
                    </button>
                    <button class="site-search-close js-search-trigger" type="button">
                        <span class="fa fa-fw fa-times"></span>
                    </button>
                </form>
                <a class="site-header-support" href="store-location.php">
                    <img src="assets/img/icon-nav-store.png" alt="">
                    Store Location
                </a>
                <a class="site-header-support" href="contact.php">
                    <img src="assets/img/icon-nav-contact.png" alt="">
                    Contact Us
                </a>
                <a class="site-header-support js-app-download" href="https://itunes.apple.com/us/app/ace-hardware-indonesia/id1056027974?ls=1&mt=8" target="_blank" data-ios="https://itunes.apple.com/us/app/ace-hardware-indonesia/id1056027974?ls=1&mt=8" data-android="https://play.google.com/store/apps/details?id=id.co.acehardware.acerewards&hl=en">
                    <img src="assets/img/icon-nav-download.png" alt="">
                    Download App
                </a>
                <a class="site-header-support" href="brochures.php">
                    <img src="assets/img/icon-nav-brochure.png" alt="">
                    Brochure
                </a>
            </div>
        </div>

        <div class="nav">
            <button class="nav-close-btn">&times;</button>
            <a class="categories-trigger" href="product-selection.php">
                <span class="fa fa-fw fa-plus-circle"></span>
                Product selection
            </a>
            <ul class="nav-list list-nostyle">
                <li class="nav-list-item">
                    <a class="nav-anchor" href="#"><span class="fa fa-fw fa-shopping-cart"></span></a>
                </li>
                <li class="nav-list-item">
                    <a class="nav-anchor" href="#">Buy online</a>
                </li>
                <li class="nav-list-item">
                    <a class="nav-anchor" href="membership.php">Membership</a>
                </li>
                <li class="nav-list-item">
                    <a class="nav-anchor" href="inspiration-list.php">Inspiration</a>
                </li>
                <li class="nav-list-item">
                    <a class="nav-anchor" href="#">Company</a>
                </li>
                <li class="nav-list-item hidden-large">
                    <a class="nav-anchor" href="#">Store location</a>
                </li>
                <li class="nav-list-item hidden-large">
                    <a class="nav-anchor" href="contact.php">Contact us</a>
                </li>
                <li class="nav-list-item hidden-large">
                    <a class="nav-anchor" href="#">Download app</a>
                </li>
                <li class="nav-list-item hidden-large">
                    <a class="nav-anchor" href="#">Brochure</a>
                </li>
            </ul>
        </div>

        <?php $categories = [
            ['Home and Living', 'icon-category-home-living.png'],
            ['Furniture', 'icon-category-furniture.png'],
            ['Home Improvement', 'icon-category-home-improvement.png'],
            ['Hobbies and Lifestyle', 'icon-category-hobby-lifestyle.png'],
            ['Electronic and Gadget', 'icon-category-electronic.png'],
            ['Kitchen', 'icon-category-kitchen.png'],
            ['Bed and Bath', 'icon-category-bed-bath.png'],
            ['Automotive', 'icon-category-automotive.png'],
            ['Health and Sport', 'icon-category-health-sport.png'],
            ['Toy and Baby', 'icon-category-toy.png'],
            ['Holiday Gift', 'icon-category-gift.png'],
            ['Best Deal', 'icon-category-deal.png'],
        ] ?>

        <?php $subcategories = [
            'DEKORASI RUMAH',
            'PERLENGKAPAN MENCUCI DAN MENYETERIKA',
            'DEKORASI FESTIVAL',
            'TEMPAT PENYIMPANAN',
            'TEMPAT SAMPAH',
            'PEMBERSIH',
            'TEKSTIL'
        ] ?>

        <div class="category-dropdown">
            <ul class="category-list list-nostyle">
                <?php foreach ($categories as $key => $value) { ?>
                <li class="category-list-item">
                    <a class="category-list-anchor category-list-anchor--level-1" href="product.php">
                        <img src="assets/img/<?= $value[1] ?>" alt="">
                        <span><?= $value[0] ?></span>
                    </a>

                    <div class="category-list category-list--level-2">
                        <ul class="list-nostyle">
                            <?php foreach ($subcategories as $keySub => $valueSub) { ?>
                            <li class="category-list-item">
                                <a class="category-list-anchor category-list-anchor--level-2" href="product.php"><?= $valueSub ?></a>

                                <div>
                                    <ul class="category-level-3 list-nostyle">
                                        <li class="category-list-item">
                                            <a class="category-list-anchor" href="product.php">Wall Decoration</a>
                                        </li>
                                        <li class="category-list-item">
                                            <a class="category-list-anchor" href="product.php">Desk Decoration</a>
                                        </li>
                                        <li class="category-list-item">
                                            <a class="category-list-anchor" href="product.php">Watch</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>

                        <img class="category-list-img" src="assets/img/category-bg.jpg" alt="">
                    </div>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</header>
