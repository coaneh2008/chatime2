    <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=default,promise,fetch" defer></script>
    <script src="assets/js/vendor/modernizr.min.js" defer></script>
    <script src="assets/js/vendor/jquery.min.js" defer></script>
    <script src="assets/js/vendor/object-fit-images.min.js" defer></script>
    <script src="assets/js/vendor/slick.min.js" defer></script>
    <script src="assets/js/vendor/sprintf.min.js" defer></script>
    <script src="assets/js/vendor/baze.validate.min.js" defer></script>
    <script src="assets/js/vendor/jquery.fancybox.min.js" defer></script>
    <script src="assets/js/vendor/angular.min.js" defer></script>
    <script src="assets/js/vendor/awesomplete.min.js" defer></script>
    <script src="assets/js/vendor/pgw-browser.min.js" defer></script>
    <script src="assets/js/vendor/datepicker.min.js" defer></script>
    <script src="assets/js/vendor/jquery.datatables.min.js" defer></script>
    <script src="assets/js/vendor/dataTables.responsive.min.js" defer></script>
    <script src="assets/js/vendor/hunt.min.js" defer></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZV4UHGTtmpjbkYFcxCbt8G4iFkxhAfQU" defer></script>
    <script>
        window.mapMarkerUrl = 'dev/img/marker.png'
    </script>
    <script src="assets/js/main.min.js" defer></script>
    <script src="assets/js/storeLocationMap.js" defer></script>
</body>
</html>
