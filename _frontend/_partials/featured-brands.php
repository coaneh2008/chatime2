<section class="home-section">
    <h2 class="home-section-heading">Featured brand</h2>

    <div class="featured-brands-slider slider-style slider-style--primary">
        <?php for ($j=0; $j < 2; $j++) { ?>
        <div>
            <ul class="featured-brands list-nostyle">
                <?php for ($i=1; $i <= 10; $i++) { ?>
                <li>
                    <a href="brand-detail.php">
                        <img src="assets/img/logo-brand-<?= $i ?>.jpg" alt="">
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>
        <?php } ?>
    </div>
</section>
