<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Inspirations</li>
                    </ul>

                    <?php include '_partials/section-inspirations.php'; ?>

                    <div class="inspiration-list-layout">
                        <div class="inspiration-list-layout-nav">
                            <form class="inspiration-list-mobile-nav block">
                                <label for="selectCategory">Category</label>
                                <select class="form-input" id="selectCategory">
                                    <?php foreach ($categories as $key => $value) { ?>
                                    <option value="<?= $value[0] ?>"><?= $value[0] ?></option>
                                    <?php } ?>
                                </select>
                            </form>
                            <ul class="inspiration-list-nav list-nostyle">
                                <?php foreach ($categories as $key => $value) { ?>
                                <li>
                                    <a class="<?= $key == 0 ? 'is-active' : ''; ?>" href="#"><?= $value[0] ?></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="inspiration-list-layout-content">
                            <?php $lists = [
                                'Bring your dream bathroom to reality',
                                'Designer hacks for decorating in small spaces',
                                '20 home decor trends that will be huge in 2018'
                            ] ?>
                            <ul class="inspiration-list list-nostyle">
                                <?php foreach ($lists as $key => $value) { ?>
                                <li class="inspiration-list-item">
                                    <article class="inspiration-list-media">
                                        <figure class="inspiration-list-media-figure">
                                            <a href="inspiration-detail.php">
                                                <img src="assets/img/inspiration-thumb-<?= $key + 1 ?>.jpg" alt="">
                                            </a>
                                        </figure>
                                        <div class="inspiration-list-media-content">
                                            <time>12 December 2017</time>
                                            <h2><a href="inspiration-detail.php"><?= $value ?></a></h2>
                                            <p>This product can play radio and MP3 via USB and SD Card, with only plugging the storage media filled with your favorite songs.</p>
                                        </div>
                                    </article>
                                </li>
                                <?php } ?>
                            </ul>

                            <ul class="pagination">
                                <li><a href="#"><span class="fa fa-fw fa-angle-left"></span></a></li>
                                <li><a class="active" href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#"><span class="fa fa-fw fa-angle-right"></span></a></li>
                            </ul>
                        </div>
                        <div class="inspiration-list-layout-archieve">
                            <h3 class="h4 text-caps text-red">Archieve</h3>

                            <?php $years = ['2017', '2016', '2015'] ?>
                            <ul class="inspiration-list-archieve-list list-nostyle">
                                <?php foreach ($years as $key => $value) { ?>
                                <li>
                                    <button class="inspiration-list-archieve-btn"><?= $value ?></button>
                                    <ul class="inspiration-list-archieve <?= $key == 0 ? 'is-active' : ''; ?> list-nostyle">
                                        <li><a class="text-red" href="#">Januari</a></li>
                                        <li><a href="#">Februari</a></li>
                                    </ul>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>

                    <section class="home-section">
                        <h2 class="home-section-heading">Popular articles</h2>

                        <?php $relateds = [
                            'Decorate your dining room',
                            'Preparing garden for special event',
                            'Designer hacks for decorating in small places',
                            '20 home decor trends that will be huge in 2018',
                            '15 affordable ways to elevate your living room'
                        ] ?>
                        <div class="inspiration-list-related">
                            <?php foreach ($relateds as $key => $value) { ?>
                            <div class="inspiration-list-related-item">
                                <a class="inspiration-list-related-anchors" href="inspiration-detail.php">
                                    <img src="assets/img/inspiration-thumb-1.jpg" alt="">
                                    <span><?= $value ?></span>
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                    </section>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
