<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <div class="bzg">
                        <div class="bzg_c" data-col="l7">
                            <form class="block" action="search-results.php">
                                <label for="inputSearch2">Search results for</label>
                                <input class="form-input" id="inputSearch2" type="text" value="Krisbow">
                            </form>

                            <ul class="list-nostyle">
                                <?php for ($i=0; $i < 10; $i++) { ?>
                                <li class="mb-24">
                                    <div class="media">
                                        <div class="media-figure">
                                            <img src="//placehold.it/360x360" width="100" alt="">
                                        </div>
                                        <div class="media-content">
                                            <span class="label label--red mb-4">Product</span>
                                            <div><a href="#"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></a></div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In ipsam cumque nulla iusto atque quas corrupti quidem est quasi, repellendus deleniti, ab error officiis minima optio debitis, nobis reiciendis obcaecati.</p>
                                        </div>
                                    </div>
                                </li>
                                <?php } ?>
                            </ul>

                            <ul class="pagination">
                                <li><a class="active" href="csr.php">1</a></li>
                                <li><a href="csr.php">2</a></li>
                                <li><a href="csr.php">3</a></li>
                                <li><a href="csr.php">4</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
