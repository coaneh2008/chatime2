<?php include '_partials/head.php'; ?>

    <div id="flashMsgContent" style="display: none;" data-interval="5000">
        <a href="#" data-color="#FE2A1A"><b>FLASH SALE</b>, 30 SEPTEMBER - 25 OKTOBER 2017, <b>SALE 70%</b></a>
        <a href="#" data-color="#FFA827">2 Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a>
        <a href="#" data-color="#B44870">3 Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a>
    </div>
    <div class="flash-msg"></div>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main">
                <?php include '_partials/main-slider.php'; ?>

                <div class="container">
                    <?php include '_partials/featured-brands.php'; ?>

                    <?php include '_partials/home-promotions.php'; ?>

                    <?php include '_partials/section-inspirations.php'; ?>

                    <?php include '_partials/section-category-list.php'; ?>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
