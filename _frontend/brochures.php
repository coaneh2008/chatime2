<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Brochure</li>
                    </ul>

                    <h1 class="home-section-heading">e-Brochure</h1>

                    <form class="brochure-filter">
                        <div>
                            <label for="filterShow">Show</label>
                            <select class="form-input" id="filterShow">
                                <option value="8">8</option>
                                <option value="12">12</option>
                                <option value="20">20</option>
                            </select>
                        </div>
                        <div>
                            <label for="sort">Sort</label>
                            <select class="form-input" id="sort">
                                <option value="Ascending">Ascending</option>
                                <option value="Descending">Descending</option>
                            </select>
                        </div>
                    </form>

                    <ul class="brochure-list list-nostyle">
                        <?php for ($i=0; $i < 10; $i++) { ?>
                        <li>
                            <div class="brochure-item">
                                <img src="assets/img/catalogue.jpg" alt="">
                                <h4 class="text-caps">Ace Cawang Holiday Season</h4>
                                <time>12 December 2017</time>
                                <div class="brochure-item-btns">
                                    <a class="btn btn--small btn--primary" href="brochure-detail.php">View</a>
                                    <a class="btn btn--small btn--primary" href="#" download>Download</a>
                                </div>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>

                    <ul class="pagination">
                        <li><a href="#"><span class="fa fa-fw fa-angle-left"></span></a></li>
                        <li><a class="active" href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#"><span class="fa fa-fw fa-angle-right"></span></a></li>
                    </ul>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
