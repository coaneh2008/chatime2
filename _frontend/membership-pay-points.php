<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <figure class="member-hero">
                        <img src="//placehold.it/1100x500" alt="">
                        <figcaption class="member-hero-content">
                            <h1 class="text-caps">Pay all with points</h1>
                            <h2>Gunakan poin yang anda miliki untuk berbelanja</h2>
                        </figcaption>
                    </figure>

                    <section class="home-section">
                        <h2 class="home-section-heading">Syarat dan Ketentuan</h2>

                        <ol class="custom-ol">
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, in. Voluptas ratione laudantium repellendus distinctio hic provident rerum facere ut molestias minima eaque mollitia, consequatur temporibus, nam commodi tempore placeat.</li>
                            <li>Architecto esse natus porro voluptatem dignissimos officiis amet, distinctio soluta eligendi facilis repellat autem! Eveniet repellat ex suscipit, rerum quis, aut libero iusto deserunt omnis laudantium aperiam. Quasi, perferendis saepe.</li>
                            <li>Unde quia ad quasi, possimus quos ipsum aliquid libero exercitationem eos ducimus corporis, eveniet eligendi. Voluptate, neque, quam. Officiis rerum facilis quia nesciunt voluptatum consequuntur modi id, itaque sint ipsum?</li>
                            <li>Dolorum modi consequatur vitae nisi ipsa odit, minima hic excepturi! Officia praesentium nisi, quibusdam consequatur consequuntur quos, esse rem, perferendis id sapiente eius, maiores. Velit nesciunt quidem debitis ab suscipit.</li>
                            <li>Quae blanditiis quos veritatis nostrum ut, dolorum consectetur culpa, aperiam id. Minus, saepe. Quaerat ipsa nulla ut placeat dolores, perferendis aliquid, illo voluptate, omnis quis voluptates! Dolorem ipsam, sunt suscipit.</li>
                            <li>Quod nam quibusdam, expedita alias. Consectetur architecto distinctio doloremque sunt inventore explicabo iusto, facere voluptatibus, tempora accusamus possimus omnis illum voluptatem? Eum repellendus dolore distinctio laborum temporibus explicabo laudantium sunt?</li>
                        </ol>

                        <?php include '_partials/back-button.php'; ?>
                    </section>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
