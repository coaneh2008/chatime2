<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <div class="bzg">
                        <div class="bzg_c" data-col="l4" data-offset="l4">
                            <h1 class="h2 home-section-heading text-center">Member login</h1>

                            <form class="js-validate" action="membership-dashboard.php">
                                <div class="block-half">
                                    <label for="inputCardNumber">ID card number</label>
                                    <input class="form-input" id="inputCardNumber" type="text" required>
                                </div>
                                <div class="block-half">
                                    <label for="inputPassKey">Pass key</label>
                                    <input class="form-input" id="inputPassKey" type="password" required>
                                </div>
                                <div class="block-half">
                                    <a href="membership-recover-passkey.php"><small>Forgot passkey?</small></a>
                                </div>
                                <button class="btn btn--block btn--primary mb-16">Login</button>

                                <p><small>Become a member! <a href="membership-register.php">Register</small></a></p>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
