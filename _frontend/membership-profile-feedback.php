<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <div class="membership-layout">
                        <?php include '_partials/membership-sidebar.php'; ?>

                        <div class="membership-layout-body">
                            <?php include '_partials/membership-nav.php'; ?>

                            <div class="membership-layout-content">
                                <form class="text-center bzg js-validate">
                                    <div class="bzg_c" data-col="l8" data-offset="l2">
                                        <h2>Apa pendapat anda mengenai Krisbow?</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque mollitia ducimus laborum architecto, maiores quis recusandae molestiae ipsum, beatae, ipsa excepturi eius voluptatibus, placeat ut odit accusamus iste quae facilis.</p>
                                        <div class="block">
                                            <textarea class="form-input" rows="4" required></textarea>
                                        </div>
                                        <button class="btn btn--block btn--primary">Kirim</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
