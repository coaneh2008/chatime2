<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li><a href="product.php">Inspiration</a></li>
                        <li><a href="product.php">Hobbies and lifestyle</a></li>
                        <li>How to prepare BBQ party</li>
                    </ul>

                    <div class="product-detail">
                        <div class="product-detail-article">
                            <article class="js-inspiration-article" data-slug="/Suitmedia/ace-hardware-products-frontend/inspiration-detail.php">
                                <div class="label" style="background: #FEA827;">Hobbies and lifestyle</div>

                                <h1 class="text-caps">How to prepare BBQ party</h1>
                                <time>12 December 2017</time>

                                <figure>
                                    <img class="full-width" src="assets/img/inspiration-detail-hero.jpg" alt="">
                                </figure>

                                <p>Written by: <b>Deddy Sunarja</b></p>

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In, ut. Soluta, quod quam. Repellat illum saepe ex sed vel sequi, doloremque velit quibusdam eligendi, delectus omnis similique ratione eveniet earum magnam, soluta quod. Praesentium ut porro minima repudiandae in ipsum mollitia accusantium impedit nobis vel ab et, tempora tempore expedita aperiam quidem repellat itaque quasi earum cupiditate adipisci. Omnis, blanditiis.</p>
                                <p>Cumque dolor necessitatibus eos! Saepe ratione nobis culpa natus animi voluptates, sapiente veritatis, rerum inventore reiciendis pariatur reprehenderit. Cumque corporis facere facilis. Debitis cum alias, ut nostrum eius sunt in dolor laboriosam ullam vero quibusdam, non hic obcaecati culpa doloremque dignissimos iusto libero mollitia. Earum unde nesciunt, maiores cupiditate asperiores. Voluptatibus ab, sit repellendus quia laborum et atque beatae aliquid!</p>
                                <p>Quas labore nemo ut, alias reprehenderit sequi eligendi rem. Iste explicabo assumenda nobis impedit magni! Et aliquid cum ipsum ab modi aperiam laboriosam quam molestias libero perspiciatis? Enim velit ullam consectetur reprehenderit itaque, voluptatibus error magni libero, eligendi perspiciatis facilis saepe officiis dolores aliquam expedita praesentium animi inventore id eaque ab alias fugit vitae. Blanditiis autem dicta harum doloribus deserunt!</p>

                                <div class="product-detail-share">
                                    <h4>Share</h4>

                                    <div class="social-medias">
                                        <a class="social-media --fb" href="#">
                                            <span class="fa fa-fw fa-facebook-official"></span>
                                        </a>
                                        <a class="social-media --tw" href="#">
                                            <span class="fa fa-fw fa-twitter"></span>
                                        </a>
                                        <a class="social-media --wa" href="#">
                                            <span class="fa fa-fw fa-whatsapp"></span>
                                        </a>
                                        <a class="social-media" href="#">
                                            <span class="fa fa-fw fa-envelope"></span>
                                        </a>
                                    </div>
                                </div>

                                <div class="block">
                                    <?php include '_partials/back-button.php'; ?>
                                </div>
                            </article>
                            <section class="block">
                                <h2 class="home-section-heading">Related product</h2>

                                <div class="product-detail-related-products">
                                    <?php for ($i=1; $i <= 4; $i++) { ?>
                                    <div class="product-detail-related-products-item">
                                        <img src="assets/img/product-thumb-<?= $i ?>.jpg" alt="">
                                        <p>Weber performer charcoal</p>
                                        <a class="btn btn--block btn--primary" href="#">Buy at RupaRupa</a>
                                    </div>
                                    <?php } ?>
                                </div>
                            </section>

                            <div class="text-center" id="articleLoader" data-next-article="dev/article-2.json">
                                loading...
                            </div>
                        </div>
                        <aside class="product-detail-aside">
                            <h4 class="text-red text-caps">Related article</h4>

                            <div class="product-detail-related">
                                <a href="#">
                                    <figure>
                                        <img src="assets/img/inspiration-thumb-1.jpg" alt="">
                                        <figcaption>
                                            <time>12 December 2017</time>
                                            <b class="text-caps">Bring your dream bathroom to reality</b>
                                        </figcaption>
                                    </figure>
                                </a>
                                <a href="#">
                                    <figure>
                                        <img src="assets/img/inspiration-thumb-2.jpg" alt="">
                                        <figcaption>
                                            <time>12 December 2017</time>
                                            <b class="text-caps">Tips for setting an Instagram worthy dining room table</b>
                                        </figcaption>
                                    </figure>
                                </a>
                            </div>

                            <h4 class="text-red text-caps">Most read</h4>

                            <div class="product-detail-related">
                                <a href="#">
                                    <figure>
                                        <img src="assets/img/inspiration-thumb-1.jpg" alt="">
                                        <figcaption>
                                            <time>12 December 2017</time>
                                            <b class="text-caps">Bring your dream bathroom to reality</b>
                                        </figcaption>
                                    </figure>
                                </a>
                                <a href="#">
                                    <figure>
                                        <img src="assets/img/inspiration-thumb-2.jpg" alt="">
                                        <figcaption>
                                            <time>12 December 2017</time>
                                            <b class="text-caps">Tips for setting an Instagram worthy dining room table</b>
                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </aside>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
