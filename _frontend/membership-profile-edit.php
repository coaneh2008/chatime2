<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <div class="membership-layout">
                        <?php include '_partials/membership-sidebar.php'; ?>

                        <div class="membership-layout-body">
                            <div class="membership-layout-content">
                                <h2 class="home-section-heading">Edit Profile</h2>

                                <div class="bzg">
                                    <div class="bzg_c" data-col="l9">
                                        <form class="js-validate" action="membership-profile-edit.php">
                                            <div class="block-half">
                                                <label for="inputAvatar">Avatar</label>
                                                <figure>
                                                    <img class="img-rounded" src="//placehold.it/120x120" alt="">
                                                </figure>
                                                <input class="form-input" id="inputAvatar" type="file">
                                            </div>
                                            <div class="block-half">
                                                <label for="inputName">Name</label>
                                                <input class="form-input js-readonly-trigger-warning" data-warning="Maaf anda tidak dapat mengubah data ini. Silahkan hubungi customer service di toko terdekat untuk update data ini." id="inputName" type="text" value="Deasy Mulia" readonly>
                                            </div>
                                            <div class="block-half">
                                                <label for="inputCardID">Card ID</label>
                                                <input class="form-input js-readonly-trigger-warning" data-warning="Maaf anda tidak dapat mengubah data ini. Silahkan hubungi customer service di toko terdekat untuk update data ini." id="inputCardID" type="text" value="6325801" readonly>
                                            </div>
                                            <div class="block-half">
                                                <label for="inputExpiry">Expiry date</label>
                                                <input class="form-input js-readonly-trigger-warning" data-warning="Maaf anda tidak dapat mengubah data ini. Silahkan hubungi customer service di toko terdekat untuk update data ini." id="inputExpiry" type="text" value="3/12/2019" readonly>
                                            </div>
                                            <div class="block-half">
                                                <label for="inputEmail">Email</label>
                                                <input class="form-input" id="inputEmail" type="email" value="deasy@gmail.com" required>
                                            </div>
                                            <div class="block-half">
                                                <label for="inputTel">No. Telepon</label>
                                                <input class="form-input" id="inputTel" type="text" value="88985465" required>
                                            </div>
                                            <div class="block-half">
                                                <label for="inputStatus">Status</label>
                                                <input class="form-input js-readonly-trigger-warning" data-warning="Maaf anda tidak dapat mengubah data ini. Silahkan hubungi customer service di toko terdekat untuk update data ini." id="inputStatus" type="text" value="Menikah" readonly>
                                            </div>
                                            <div class="block-half">
                                                <label for="inputGender">Gender</label>
                                                <input class="form-input js-readonly-trigger-warning" data-warning="Maaf anda tidak dapat mengubah data ini. Silahkan hubungi customer service di toko terdekat untuk update data ini." id="inputGender" type="text" value="Wanita" readonly>
                                            </div>
                                            <div class="block-half">
                                                <label for="inputBirthPlace">Tempat lahir</label>
                                                <input class="form-input js-readonly-trigger-warning" data-warning="Maaf anda tidak dapat mengubah data ini. Silahkan hubungi customer service di toko terdekat untuk update data ini." id="inputBirthPlace" type="text" value="Jakarta" readonly>
                                            </div>
                                            <div class="block-half">
                                                <label for="inputBirthDay">Tanggal lahir</label>
                                                <input class="form-input js-readonly-trigger-warning" data-warning="Maaf anda tidak dapat mengubah data ini. Silahkan hubungi customer service di toko terdekat untuk update data ini." id="inputBirthDay" type="text" value="3/12/1992" readonly>
                                            </div>
                                            <div class="block-half">
                                                <label for="inputAddress">Alamat</label>
                                                <textarea class="form-input js-readonly-trigger-warning" data-warning="Maaf anda tidak dapat mengubah data ini. Silahkan hubungi customer service di toko terdekat untuk update data ini." id="inputAddress" rows="3" readonly>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel dolor cumque aperiam praesentium rerum, nisi sapiente magni obcaecati culpa beatae, distinctio iure ducimus provident iste, laudantium consequuntur rem. Beatae, debitis.</textarea>
                                            </div>
                                            <button class="btn btn--primary">Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

    <div class="modal is-active">
        <div class="modal-dialog">
            <button class="modal-dialog-close">Close &times;</button>

            <div class="text-center">
                <h2 class="text-red">Member account renewal</h2>
                <p>Akun member anda akan memasuki masa tenggang dalam 30 hari. Mohon melakukan renewal. Anda dapat melakukan renewal dengan membayar di toko atau menggunakan point.</p>

                <a class="btn btn--primary" href="{{ route('') }}">Bayar di toko</a>
                <a class="btn btn--outline btn--primary" href="{{ route('') }}">Gunakan point</a>
            </div>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
