<?php include '_partials/head.php'; ?>

    <div class="sticky-footer-container">
        <div class="sticky-footer-container-item">
            <?php include '_partials/header.php'; ?>
        </div>
        <div class="sticky-footer-container-item --pushed">
            <div class="site-cover"></div>
            <main class="site-main site-main--pushed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="home.php">Home</a></li>
                        <li>Membership</li>
                    </ul>

                    <figure>
                        <img src="//placehold.it/1100x500" alt="">
                    </figure>

                    <article>
                        <h1 class="mb-8">Use your points before it expires</h1>
                        <div class="mb-24">
                            <time>12 December 2017</time>
                        </div>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae eum at distinctio pariatur, dolore necessitatibus laborum quidem deserunt earum corrupti, alias ullam deleniti tempora numquam beatae molestiae mollitia architecto, dolorem amet error non eveniet laudantium neque, porro. Perferendis ea repellat veniam tempore nulla porro numquam quam, libero temporibus unde provident reprehenderit aliquam itaque maiores eaque corporis vel! Dolorum, eius, eum!</p>
                        <p>Ab facere nostrum, sed harum voluptates dolorum corporis porro. Quibusdam, repudiandae! Enim ad eum autem quaerat delectus officia, explicabo ipsum, mollitia. Labore, eligendi vel dolore earum quos libero in ipsum! Veniam tenetur quae ab quibusdam autem unde, dicta sequi deleniti accusamus nisi molestias laudantium molestiae eveniet impedit numquam, reiciendis dolore praesentium error vero corporis. Blanditiis ipsa explicabo id quos quod.</p>
                        <p>Eum consequuntur, ut optio, sunt enim voluptatibus nostrum quas eius ex deleniti expedita! Doloribus ipsam iure ea deserunt harum, mollitia sapiente laudantium, quidem saepe nam enim, modi minima adipisci molestiae in. Quam, dolorem quia nulla possimus placeat quis necessitatibus repellendus, repellat inventore nihil laborum deserunt ipsum illum eos tempore expedita, minima nemo amet. Odio repellendus, dolorum accusamus vitae at incidunt.</p>

                        <?php include '_partials/back-button.php'; ?>
                    </article>
                </div>
            </main>
        </div>
        <div class="sticky-footer-container-item">
            <?php include '_partials/footer.php'; ?>
        </div>
    </div>

<?php include '_partials/scripts.php'; ?>
