<?php

namespace App\Supports\Layout;

use App\Model\Page;
use Illuminate\Http\Request;

class FreeMembershipTermAndCondition extends Layout
{
    protected static $name = 'Free Membership Term and Condition Layout';

    public function handle(Request $request, Page $page)
    {
        return view('pages.free-membership-term-and-condition', compact('page'));
    }
}
