<?php

namespace App\Supports\Layout;

use App\Model\Page;
use Illuminate\Http\Request;

class PrivacyPolicy extends Layout
{
    protected static $name = 'Privacy Policy Layout';

    public function handle(Request $request, Page $page)
    {
        return view('pages.privacy-policy', compact('page'));
    }
}
