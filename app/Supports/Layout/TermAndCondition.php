<?php

namespace App\Supports\Layout;

use App\Model\Page;
use Illuminate\Http\Request;

class TermAndCondition extends Layout
{
    protected static $name = 'Term and Condition Layout';

    public function handle(Request $request, Page $page)
    {
        return view('pages.term-and-condition', compact('page'));
    }
}
