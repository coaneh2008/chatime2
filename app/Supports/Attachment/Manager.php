<?php

namespace App\Supports\Attachment;

use App\Supports\Attachment\Downloader\AbstractDownloader;
use Illuminate\Support\Collection;

class Manager
{
    protected $downloaders;

    public function __construct(array $downloaders)
    {
        $this->downloaders = new Collection;
        foreach ($downloaders as $downloader) {
            if ($downloader instanceof AbstractDownloader) {
                $this->downloaders->push($downloader);
                continue;
            }
            $class = get_class($downloader);
            throw new InvalidDownloaderException("Invalid downloader `$class`");
        }
    }

    public function download($file, $path)
    {
        //echo 'file = ' .$file.'<br/>';
        //echo 'path = ' .$path;
        
        $notFoundMessage = '';
        foreach ($this->downloaders as $downloader) {
            try {
                if ($downloader->validate($file)) {
                    $downloader->setRootPath($path);
                    //echo $downloader->getRootPath();
                    return $downloader->download($file);
                }
            } catch (ResourceNotFoundException $e) {
                $notFoundMessage .= $e->getMessage() . " - ";
            }
        }
        if (!empty(trim($notFoundMessage))) {
            throw new ResourceNotFoundException(trim($notFoundMessage));
        }
        throw new FormatNotSupportException("Attachment Format `$file` not supported");
    }
}
