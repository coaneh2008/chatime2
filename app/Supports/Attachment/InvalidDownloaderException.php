<?php

namespace App\Supports\Attachment;

use Exception;

class InvalidDownloaderException extends Exception
{
}
