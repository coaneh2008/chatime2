<?php

namespace App\Supports\Attachment\Downloader;

use App\Supports\Attachment\File;
use App\Supports\Attachment\ResourceNotFoundException;

class LocalDownloader extends AbstractDownloader
{
    public function download($path)
    {
        //echo $this->filesystem->getUrlPath();
        //echo $path;
        if (!$this->filesystem->exists($path)) {
            throw new ResourceNotFoundException("File not found ikh `$path`");
        }

        /*if(file_exists(public_path($path))){
            throw new ResourceNotFoundException("File not foundss `$path`");
        }*/

        $mime = $this->filesystem->mimeType($path);
        $size = $this->filesystem->size($path);

        $filename = $this->filename(basename($path), $mime);
        $urlPath = $this->savePath($filename);

        $this->filesystem->copy($path, $urlPath);

        $result = new File($urlPath, $mime, $size);

        return $result;
    }

    public function validate($path)
    {
        return is_string($path);
    }
}
