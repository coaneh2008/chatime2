<?php

namespace App\Supports\Attachment\Downloader;

use App\Supports\Attachment\File;
use App\Supports\Attachment\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadedFileDownloader extends AbstractDownloader
{
    public function download($file)
    {
        $filename = $this->filename($file->getClientOriginalName(), $file->getClientMimeType());
        $urlPath = $this->savePath($filename);

        if (!$file->getRealPath()) {
            throw new ResourceNotFoundException("File not founds `{$file->getRealPath()}`");
        }

        $content = (file_exists($file->getRealPath())) ? file_get_contents($file->getRealPath()) : null;

        $this->filesystem->put($urlPath, $content);

        $result = new File($urlPath, $file->getClientMimeType(), $file->getClientSize());

        return $result;
    }

    public function validate($file)
    {
        return $file instanceof UploadedFile;
    }
}
