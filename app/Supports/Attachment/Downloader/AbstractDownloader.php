<?php

namespace App\Supports\Attachment\Downloader;

use Illuminate\Contracts\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\ExtensionGuesserInterface;

abstract class AbstractDownloader
{
    /**
     * Filesystem driver
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    protected $filesystem;

    protected $rootPath = '';

    protected $fileSeparator = '-';

    protected $extensionGuesser;

    public function __construct(Filesystem $filesystem, ExtensionGuesserInterface $extensionGuesser)
    {
        $this->filesystem = $filesystem;
        $this->extensionGuesser = $extensionGuesser;
    }

    abstract public function download($path);

    abstract public function validate($path);

    public function filename($filename, $mime = null)
    {
        $filename = urldecode($filename);
        $fileSplit = explode('.', $filename);
        $extension = '';
        
        if (count($fileSplit) > 1) {
            $extension = '.' . $fileSplit[count($fileSplit) - 1];
            $filename = implode($this->fileSeparator, array_slice($fileSplit, 0, count($fileSplit) - 1));
        }

        if (empty($extension) && !empty($mime)) {
            $extension = $this->extensionGuesser->guess($mime);
            $extension = $extension?".$extension":'';
        }

        $filename = substr($filename, 0, 50);
        $extension = substr($extension, 0, 11);

        return str_slug($filename, $this->fileSeparator) . strtolower($extension);
    }

    public function savePath($filename)
    {
        $dir = date('Y/M/d');
        $subDir = uniqid();
        $dir .= "/$subDir";
        $rootPath = $this->getRootPath()?$this->getRootPath() . '/':'';

        return sprintf("%s%s/%s", $rootPath, $dir, $filename);
    }

    public function setRootPath($path)
    {
        $this->rootPath = trim($path, "/");
    }

    public function getRootPath()
    {
        return $this->rootPath;
    }
}
