<?php

namespace App\Repositories;

use App\Model\FeaturedBrand;
use App\Model\ProductCategory;

class ProductMenuRepository extends BaseRepository
{
    public function __construct(FeaturedBrand $model)
    {
        parent::__construct($model);
    }

    
    public function getProductHome(){
        return FeaturedBrand::where('is_highlighted', 1)
            ->asc()
            ->published()
            ->get();
    }

    

    public function getProductBig(){
        return FeaturedBrand::join('product_categories', 'featured_brands.category_id', '=', 'product_categories.id')
            ->select('featured_brands.is_big','featured_brands.title','featured_brands.content','featured_brands.banner_image','featured_brands.description','product_categories.color','product_categories.color2'
                ,'product_categories.banner_image as bg')
            ->where('featured_brands.is_highlighted', 0)
            ->where('featured_brands.is_big', 1)
            ->whereNotNull('featured_brands.published_at')
            ->orderBy('featured_brands.order','asc')
            ->first();
    }

    public function getProductList(){
        return FeaturedBrand::join('product_categories', 'featured_brands.category_id', '=', 'product_categories.id')
            ->select('featured_brands.is_big','featured_brands.title','featured_brands.content','featured_brands.banner_image','featured_brands.description','product_categories.color','product_categories.color2'
                ,'product_categories.banner_image as bg')
            ->where('featured_brands.is_highlighted', 0)
            ->whereNotNull('featured_brands.published_at')
            ->orderBy('featured_brands.order','asc')
            ->get();
    }

    public function getProductList2(){
        /*return FeaturedBrand::where('is_highlighted', 0)
            ->asc()
            ->published()
            ->offset(8)
            ->get();*/


        return FeaturedBrand::join('product_categories', 'featured_brands.category_id', '=', 'product_categories.id')
            ->select('featured_brands.is_big','featured_brands.title','featured_brands.content','featured_brands.banner_image','featured_brands.description','product_categories.color','product_categories.color2','product_categories.banner_image as bg')
            ->where('featured_brands.is_highlighted', 0)
            ->whereNotNull('featured_brands.published_at')
            ->orderBy('featured_brands.order','asc')
            ->offset(8)
            ->get();


    }

    public function getProductFilter($slug){
        $category = ProductCategory::findOrFailByUrlKey($slug);

        if($category->id!=1){
            return FeaturedBrand::join('product_categories', 'featured_brands.category_id', '=', 'product_categories.id')
                ->select('featured_brands.is_big','featured_brands.title','featured_brands.content','featured_brands.banner_image','featured_brands.description','product_categories.color','product_categories.color2','product_categories.banner_image as bg')
                ->where('featured_brands.is_highlighted', 0)
                ->where('featured_brands.category_id', $category->id)
                ->whereNotNull('featured_brands.published_at')
                ->orderBy('featured_brands.order','asc')
                ->get();

        }else{

          return FeaturedBrand::join('product_categories', 'featured_brands.category_id', '=', 'product_categories.id')
            ->select('featured_brands.is_big','featured_brands.title','featured_brands.content','featured_brands.banner_image','featured_brands.description','product_categories.color','product_categories.color2','product_categories.banner_image as bg')
            ->where('featured_brands.is_highlighted', 0)
            ->whereNotNull('featured_brands.published_at')
            ->orderBy('featured_brands.order','asc')
            ->get();

        }

    }
    
}
