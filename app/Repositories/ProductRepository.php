<?php

namespace App\Repositories;

use App\Model\Product;
use App\Repositories\Extension\CacheableTrait;

class ProductRepository
{
    use CacheableTrait;

    public function storeProduct($url, $productData)
    {
        return Product::updateOrCreate(['url_to_ace_online' => $url], $productData);
    }

    public function addNewProduct($url)
    {
        return Product::create(['url_to_ace_online' => $url]);
    }

    public function getProductByUrl($url)
    {
        return Product::where('url_to_ace_online', $url)->first();
    }
}
