<?php

namespace App\Repositories;

use App\Model\ThematicPromotion;
use Carbon\Carbon;

class ThematicPromotionRepository extends BaseRepository
{
    public function __construct(ThematicPromotion $model)
    {
        parent::__construct($model);
    }

    public function getActiveThematicPromotionSmallImageBanner()
    {
        $now = Carbon::now();

        return ThematicPromotion::where('type', ThematicPromotion::TYPE_SMALL_IMAGE)
            ->where('start_date', '<=', $now)
            ->where('end_date', '>=', $now)
            // ->take(9)
            ->asc()
            ->published()
            ->get();
    }

    public function getActiveThematicPromotionBigImageBanner()
    {
        $now = Carbon::now();

        return ThematicPromotion::where('type', ThematicPromotion::TYPE_BIG_IMAGE)
            ->where('start_date', '<=', $now)
            ->where('end_date', '>=', $now)
            ->asc()
            ->published()
            ->get();
    }

    public function getThematicPromotionBySlug($slug)
    {
        return ThematicPromotion::findOrFailByUrlKey($slug);
    }
}
