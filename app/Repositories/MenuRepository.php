<?php

namespace App\Repositories;

use App\Model\Menu;

class MenuRepository extends BaseRepository
{
    public function __construct(Menu $model)
    {
        parent::__construct($model);
    }

    public function staticPageList()
    {
        return Menu::where('is_link', 1)
            ->orderBy('translate_title', 'asc')
            ->get()
            ->unique('url')
            ->pluck('translate_title', 'id');
    }

    public static function getMainMenu()
    {
        return Menu::where('type', Menu::TYPE_MAIN)
            ->asc()
            ->get();
    }

    public static function getSecondaryMenu()
    {
        return Menu::where('type', Menu::TYPE_SECONDARY)
            ->asc()
            ->get();
    }

    public static function getFooterMenu()
    {
        return Menu::where('type', Menu::TYPE_FOOTER)
            ->asc()
            ->withChilds()
            ->get();
    }
}
