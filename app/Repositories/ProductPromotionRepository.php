<?php

namespace App\Repositories;

use App\Model\ProductPromotion;
use Carbon\Carbon;

class ProductPromotionRepository
{
    public function getActiveProductPromoByCategoryId($categoryId)
    {
        $now = Carbon::now();

        return ProductPromotion::join('products', 'product_promotions.product_id', '=', 'products.id')
            ->where('products.is_available', \App\Model\Product::IN_STOCK)
            ->where('category_id', $categoryId)
            ->where('start_date', '<=', $now)
            ->where('end_date', '>=', $now)
            ->published()
            ->get();
    }

    public function getProductPromoById($promotionId)
    {
        return ProductPromotion::findOrFailByUrlKey($promotionId);
    }
}
