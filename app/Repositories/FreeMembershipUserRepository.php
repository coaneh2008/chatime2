<?php

namespace App\Repositories;

use App\Model\FreeMembershipUser;

class FreeMembershipUserRepository
{
    public function getUserByEventId($eventId)
    {
        return FreeMembershipUser::where('event_id', $eventId)
            ->get();
    }

    public function getDataByUserId($eventId, $userId)
    {
        return FreeMembershipUser::where('event_id', $eventId)->where('user_id', $userId)->first();
    }

    public function storeUser($eventId, $userId)
    {
        return FreeMembershipUser::updateOrCreate(
            ['event_id' => $eventId, 'user_id' => $userId],
            ['event_id' => $eventId, 'user_id' => $userId]
        );
    }
}
