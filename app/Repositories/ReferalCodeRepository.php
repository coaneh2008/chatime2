<?php

namespace App\Repositories;

use App\Model\Event;
use App\Model\ReferalCode;
use App\Model\User;

class ReferalCodeRepository
{
    public function getReferalCodeByEventId($eventId)
    {
        return ReferalCode::where('event_id', $eventId)
            ->get();
    }

    public function getEventReferalCodeByUserId($eventId, $userId)
    {
        return ReferalCode::where('event_id', $eventId)
            ->where('user_id', $userId)
            ->first();
    }

    public function getReferalCode($referalCode)
    {
        return ReferalCode::where('code', $referalCode)->first();
    }

    public function isEventActive($eventId)
    {
        return Event::where('id', $eventId)
            ->where('end_date', '>=', \Carbon\Carbon::now())
            ->published()
            ->first();
    }

    public function getUpline($uplineId)
    {
        return User::where('id', $uplineId)->first();
    }

    public function isReferalCodeValid($referalCode)
    {
        $referalCode = $this->getReferalCode($referalCode);
        if ($referalCode) {
            $uplineId = $referalCode->user_id;
            $eventId = $referalCode->event_id;
            $event = $this->isEventActive($eventId);
            if ($event) {
                return $this->getUpline($uplineId);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
