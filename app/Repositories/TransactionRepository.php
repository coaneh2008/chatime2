<?php

namespace App\Repositories;

use App\Model\Transaction;

class TransactionRepository
{
    public function getTransactionByUserId($userId)
    {
        return Transaction::where('user_id', $userId)
            ->orderBy('date', 'desc')
            ->get();
    }

    public function getTotalTransactionByDate($userId, $startDate, $endDate)
    {
        return Transaction::where('user_id', $userId)
            ->where('date', '>=', $startDate)
            ->where('date', '<=', $endDate)
            ->sum('amount');
    }
}
