<?php

namespace App\Repositories;

use App\Model\EventParticipant;
use App\Model\FreeMembershipUser;

class EventParticipantRepository
{
    public function getParticipantByEventId($eventId)
    {
        return EventParticipant::where('event_id', $eventId)
            ->where('status', 1)
            ->get();
    }

    public function getFreeMembershipParticipantByEventId($eventId)
    {
        return FreeMembershipUser::where('event_id', $eventId)
            // ->where('status', 1)
            ->get();
    }

    public function getParticipantByUserId($eventId, $userId)
    {
        return EventParticipant::where('event_id', $eventId)->where('user_id', $userId)->first();
    }

    public function storeParticipant($eventId, $userId, $status)
    {
        $participantData = [
            'event_id' => $eventId,
            'user_id' => $userId,
            'status' => $status
        ];

        return EventParticipant::updateOrCreate([
            'event_id' => $eventId,
            'user_id' => $userId
            ], $participantData);
    }
}
