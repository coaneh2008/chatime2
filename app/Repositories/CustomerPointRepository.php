<?php

namespace App\Repositories;

use App\Model\CustomerPoint;

class CustomerPointRepository
{
    public function getCustomerPointByUserId($userId)
    {
        return CustomerPoint::where('user_id', $userId)->first();
    }
}
