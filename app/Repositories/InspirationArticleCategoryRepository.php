<?php

namespace App\Repositories;

use App\Model\InspirationArticleCategory;

class InspirationArticleCategoryRepository extends BaseRepository
{
    public function __construct(InspirationArticleCategory $model)
    {
        parent::__construct($model);
    }

    public function getCategoryList()
    {
        return InspirationArticleCategory::asc()->published()->get();
    }

    public function getInspirationArticleCategory($slug)
    {
        return InspirationArticleCategory::findOrFailByUrlKey($slug);
    }
}
