<?php

namespace App\Repositories;

use App\Model\ProductCategory;

class ProductCategoryRepository extends BaseRepository
{
    public function __construct(ProductCategory $model)
    {
        parent::__construct($model);
    }

    public function getMainCategoryList()
    {
        return ProductCategory::where('type', ProductCategory::TYPE_PRIMARY)->asc()->published()->get();
    }

    public function getProductCategory($slug)
    {
        return ProductCategory::findOrFailByUrlKey($slug);
    }
    
    public function getChildMenuByParentId($parentId)
    {
        return ProductCategory::where('parent_id', $parentId)->asc()->published()->get();
    }
}
