<?php

namespace App\Repositories;

use App\Model\Testimony;

class TestimonyRepository
{
    public function getTestimonyByEventId($eventId)
    {
        return Testimony::where('event_id', $eventId)
            ->get();
    }

    public function storeTestimony($eventId, $userId, $comment)
    {
        return Testimony::create([
            'event_id' => $eventId,
            'user_id' => $userId,
            'comment' => $comment
        ]);
    }

    public function getTestimonyByUserId($eventId, $userId)
    {
        return Testimony::where('event_id', $eventId)
            ->where('user_id', $userId)
            ->first();
    }
}
