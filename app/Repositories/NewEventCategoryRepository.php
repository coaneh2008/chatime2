<?php

namespace App\Repositories;

use App\Model\NewEventCategory;

class NewEventCategoryRepository extends BaseRepository
{
    public function __construct(NewEventCategory $model)
    {
        parent::__construct($model);
    }

    public function getCategoryList()
    {
        return NewEventCategory::asc()->published()->get();
    }

    public function getInspirationArticleCategory($slug)
    {
        return NewEventCategory::findOrFailByUrlKey($slug);
    }
}
