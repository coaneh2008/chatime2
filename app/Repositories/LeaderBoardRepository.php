<?php

namespace App\Repositories;

use App\Model\MemberLeaderboard;

class LeaderBoardRepository
{
    public function getLeaderBoardData()
    {
        return MemberLeaderboard::get();
    }

    public function getLeaderBoardUserData($member_id)
    {
        return MemberLeaderboard::where('member_card_id',$member_id)->first();
    }

    public function isExistLeaderBoardData()
    {
        return MemberLeaderboard::first();
    }
}
