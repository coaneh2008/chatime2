<?php

namespace App\Repositories;

use App\Model\FeaturedBrand;

class BrandRepository extends BaseRepository
{
    public function __construct(FeaturedBrand $model)
    {
        parent::__construct($model);
    }

    public function getBrands()
    {
        return $this->model()->asc()->published()->get();
    }
}
