<?php

namespace App\Repositories;

use App\Model\PromotionCategory;

class PromotionCategoryRepository
{
    public function getPromotionCategoryIdByType($type)
    {
        $promoCat = PromotionCategory::where('type', $type)->first();
        return $promoCat ? $promoCat->id : null;
    }

    public function getCategoryList()
    {
        return PromotionCategory::asc()->published()->get();
    }
}
