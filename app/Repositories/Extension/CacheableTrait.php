<?php

namespace App\Repositories\Extension;

use Cache;
use Carbon\Carbon;

/**
 * Automatically cache data in function which started with get or has
 *
 * usage:
 * use CacheableTrait;
 *
 * Function name which started with get or has will be cacheable automatically.
 * Visibility function which needed to be cacheable automatically MUST BE protected function
 * e.q. getSomething() or hasSomething($array, $data)
 *
 * Cache key will be yourClassName:yourMethodName:{{sha1(serialize(yourParameter))}}
 * e.q. PollRepository:getSomething:8739602554c7f3241958e3cc9b57fdecb474d508
 *
 * For forgetting cache in specific repository, add this function in helpers
 * if (!function_exists('forgetCache')) {
        function forgetCache($prefix)
        {
            \Cache::forever($prefix, \Carbon\Carbon::now()->timestamp);
        }
    }
 * And use directly forgetCache('PollRepository')
 */

trait CacheableTrait
{
    public function __call($method, $args)
    {
        if (!method_exists($this, $method)) {
            throw new \Exception("Method " . $method . " doesn't exist in " . $this->generateClass());
        }

        $cacheKey = $this->generateCacheKey($method, $args);
        if (starts_with($method, 'has') || starts_with($method, 'get')) {
            $now = Carbon::now();
            $default = ["timestamp" => Carbon::createFromDate(2015, 1, 1)->timestamp];
            $cacheKey = $this->generateCacheKey($method, $args);
            $invalidateKey = $this->generateInvalidateKey($args);
            $cacheResult = Cache::get($cacheKey, $default);

            if (!Cache::has($invalidateKey)) {
                Cache::forever($invalidateKey, $now->timestamp);
            }
            $lastInvalidate = Cache::get($invalidateKey);

            if (array_key_exists('value', $cacheResult) && $cacheResult['timestamp'] >= $lastInvalidate) {
                return $cacheResult['value'];
            }

            $result = call_user_func_array([$this, $method], $args);
            Cache::forever($cacheKey, ['timestamp' => $now->timestamp, 'value' => $result]);
            return $result;
        }
        return call_user_func_array([$this, $method], $args);
    }

    protected function generateCacheKey($method, $args)
    {
        if (isset($this->cacheKey)) {
            return $this->cacheKey . ':' . $method . ':' . sha1(serialize($args));
        }
        return $this->generateClass() . ':' . $method . ':' . sha1(serialize($args));
    }

    protected function generateClass()
    {
        $className = get_class($this);
        return last(explode('\\', $className));
    }

    protected function generateInvalidateKey($args)
    {
        if (isset($this->cacheKey)) {
            $invalidateKey = $this->cacheKey;
        } else {
            $invalidateKey = $this->generateClass();
        }
        foreach ($args as $param) {
            if ($param instanceof \App\Model\User) {
                $invalidateKey .= ':' . $param->getKey();
            }
        }
        return $invalidateKey;
    }
}
