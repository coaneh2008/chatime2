<?php

namespace App\Repositories;

use App\Model\PointRedeem;

class PointRedeemRepository
{
    public function getRedeemByUserId($userId)
    {
        return PointRedeem::where('user_id', $userId)
            ->orderBy('date', 'desc')
            ->get();
    }
}
