<?php

namespace App\Repositories;

use App\Model\SeoTag;

class SeoTagRepository extends BaseRepository
{
    public function __construct(SeoTag $model)
    {
        parent::__construct($model);
    }

    public function getSeoTag($slug)
    {
        return $this->model()->where('seo_slug', $slug)->first();
    }
}
