<?php

namespace App\Repositories;

use App\Model\NewEvent;
use App\Model\NewEventCategory;

class NewEventRepository extends BaseRepository
{
    public function __construct(NewEvent $model)
    {
        parent::__construct($model);
    }

    public function getBanners()
    {
        return $this->model()->asc()->published()->get();
    }


    public function getListEvent()
    {
        return NewEvent::asc()
            ->published()
            ->get();
    }

    public function getListEventFilter($slug,$sort=''){

        $category = NewEventCategory::findOrFailByUrlKey($slug);

        if($sort==''){
            if($category->id!=1){
                return NewEvent::join('new_event_categories', 'new_events.category_id', '=', 'new_event_categories.id')
                ->select('new_events.title','new_events.banner_image','new_events.description',
                    'new_events.content','new_events.created_at')
                ->where('new_events.category_id', $category->id)
                ->whereNotNull('new_events.published_at')
                ->get();
            }else{
                return NewEvent::join('new_event_categories', 'new_events.category_id', '=', 'new_event_categories.id')
                ->select('new_events.title','new_events.banner_image','new_events.description',
                    'new_events.content','new_events.created_at')
                ->whereNotNull('new_events.published_at')
                ->get();
            }
        }else{
            if($category->id!=1){
                if($sort==1){
                    return NewEvent::join('new_event_categories', 'new_events.category_id', '=', 'new_event_categories.id')
                    ->select('new_events.title','new_events.banner_image','new_events.description',
                        'new_events.content','new_events.created_at')
                    ->where('new_events.category_id', $category->id)
                    ->whereNotNull('new_events.published_at')
                    ->orderBy('new_events.published_at', 'desc')
                    ->get();
                }else{
                    return NewEvent::join('new_event_categories', 'new_events.category_id', '=', 'new_event_categories.id')
                    ->select('new_events.title','new_events.banner_image','new_events.description',
                        'new_events.content','new_events.created_at')
                    ->where('new_events.category_id', $category->id)
                    ->whereNotNull('new_events.published_at')
                    ->orderBy('viewer', 'desc')
                    ->get();
                }
            }else{
                if($sort==1){
                    return NewEvent::join('new_event_categories', 'new_events.category_id', '=', 'new_event_categories.id')
                    ->select('new_events.title','new_events.banner_image','new_events.description',
                        'new_events.content','new_events.created_at')
                    ->whereNotNull('new_events.published_at')
                    ->orderBy('new_events.published_at', 'desc')
                    ->get();
                }else{
                    return NewEvent::join('new_event_categories', 'new_events.category_id', '=', 'new_event_categories.id')
                    ->select('new_events.title','new_events.banner_image','new_events.description',
                        'new_events.content','new_events.created_at')
                    ->whereNotNull('new_events.published_at')
                    ->orderBy('viewer', 'desc')
                    ->get();
                }

            }
        }
        
    }

    public function detail_news($slug){

        $event = NewEvent::published()->findOrFailByUrlKey($slug);

        return $event;
        
    }
}