<?php

namespace App\Repositories;

use App\Model\Promotion;
use Carbon\Carbon;

class PromotionRepository
{
    public function getActivePromoByCategoryId($categoryId)
    {
        $now = Carbon::now();

        return Promotion::where('category_id', $categoryId)
            ->where('start_date', '<=', $now)
            ->where('end_date', '>=', $now)
            ->published()
            ->first();
    }

    public function getPromoById($promotionId)
    {
        return Promotion::findOrFailByUrlKey($promotionId);
    }
}
