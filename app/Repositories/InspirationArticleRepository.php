<?php

namespace App\Repositories;

use App\Model\InspirationArticle;

class InspirationArticleRepository extends BaseRepository
{
    const TOTAL_NEXT_ARTICLE = 5;

    public function __construct(InspirationArticle $model)
    {
        parent::__construct($model);
    }

    public function getNextArticles($article)
    {
        return InspirationArticle::where('id', '<>', $article->id)
            ->where('category_id', $article->category_id)
            ->where('published_date', '<=', $article->published_date)
            ->orderBy('published_date', 'desc')
            ->published()
            ->random(static::TOTAL_NEXT_ARTICLE)
            ->get();
    }

    public function setNextArticles($articles, $slug)
    {
        \Cache::put('next-articles-' . $slug, $articles, 180);
    }

    public function getNextArticle($slug)
    {
        $article = InspirationArticle::published()->findOrFailByUrlKey($slug);
        return InspirationArticle::where('id', '<>', $article->id)
            ->where('category_id', $article->category_id)
            ->where('published_date', '<=', $article->published_date)
            ->orderBy('published_date', 'desc')
            ->published()
            ->random(static::TOTAL_NEXT_ARTICLE)
            ->get();
        return \Cache::get('next-articles-' . $slug);
    }

    public function getInspirationArticleByImageType($type,$slider,$take,$device=0)
    {
        if($take > 0){
            return $this->model()->where('image_type', $type)
                ->where('is_highlighted', 1)
                ->where('is_slider', $slider)
                ->where('device', $device)
                ->take($take)

                ->asc()
                ->published()
                ->get();
        }else{
            return $this->model()->where('image_type', $type)
                ->where('is_highlighted', 1)
                ->where('is_slider', $slider)
                ->where('device', $device)
                ->asc()
                ->published()
                ->get();
        }
    }
}
