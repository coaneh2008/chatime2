<?php

namespace App\Repositories;

use App\Model\HomePromotion;
use App\Model\PromotionCategory;

use Carbon\Carbon;

class HomePromotionRepository extends BaseRepository
{
    public function __construct(HomePromotion $model)
    {
        parent::__construct($model);
    }

    public function getActivePromoSmallImageBanner()
    {
        $now = Carbon::now();

        return HomePromotion::where('type', HomePromotion::TYPE_SMALL_IMAGE)
            ->where('start_date', '<=', $now)
            ->where('end_date', '>=', $now)
            ->take(2)
            ->asc()
            ->published()
            ->get();
    }

    public function getActivePromoBigImageBanner()
    {
        $now = Carbon::now();

        return HomePromotion::where('type', HomePromotion::TYPE_BIG_IMAGE)
            ->where('start_date', '<=', $now)
            ->where('end_date', '>=', $now)
            ->asc()
            ->published()
            ->get();
    }

    public function getActivePromoAllBanner()
    {
        $now = Carbon::now();

        return HomePromotion::where('start_date', '<=', $now)
            ->where('end_date', '>=', $now)
            ->asc()
            ->published()
            ->get();
    }

    public function getHomePromoBySlug($slug)
    {
        return HomePromotion::findOrFailByUrlKey($slug);
    }


    public function getPromotionFilter($slug){
        $now = Carbon::now();
        $category = PromotionCategory::findOrFailByUrlKey($slug);

        if($category->id!=1){
            return HomePromotion::join('promotion_categories', 'home_promotions.category_id', '=', 'promotion_categories.id')
                ->select('home_promotions.title','home_promotions.content','home_promotions.banner_image','home_promotions.kode_promo','home_promotions.start_date','home_promotions.end_date')
                ->where('home_promotions.category_id', $category->id)
                ->whereNotNull('home_promotions.published_at')
                ->where('start_date', '<=', $now)
                ->where('end_date', '>=', $now)
                ->get();

        }else{

            return HomePromotion::join('promotion_categories', 'home_promotions.category_id', '=', 'promotion_categories.id')
                ->select('home_promotions.title','home_promotions.content','home_promotions.banner_image','home_promotions.kode_promo','home_promotions.start_date','home_promotions.end_date')
                ->whereNotNull('home_promotions.published_at')
                ->where('start_date', '<=', $now)
                ->where('end_date', '>=', $now)
                ->get();


        }

    }

}
