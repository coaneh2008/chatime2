<?php

namespace App\Repositories;

use App\Model\Banner;

class BannerRepository extends BaseRepository
{
    public function __construct(Banner $model)
    {
        parent::__construct($model);
    }

    public function getBanners()
    {
        return $this->model()->asc()->published()->get();
    }


    public function getBannersCustom($page='',$device=0)
    {
        return Banner::where('page', $page)
            ->where('device',$device) 
            ->asc()
            ->published()
            ->get();
    }
}
