<?php

namespace App\Repositories;

use App\Model\HomeInfo;

class HomeInfoRepository extends BaseRepository
{
    public function __construct(HomeInfo $model)
    {
        parent::__construct($model);
    }

    public function getMainCategoryList()
    {
        return HomeInfo::where('type', HomeInfo::TYPE_PRIMARY)->asc()->published()->get();
    }

    public function getProductCategory($slug)
    {
        return HomeInfo::findOrFailByUrlKey($slug);
    }
    
    public function getChildMenuByParentId($parentId)
    {
        return HomeInfo::where('parent_id', $parentId)->asc()->published()->get();
    }


    public function getMainListInfo($device=0,$slider=0)
    {   
        if($slider==1){
            return HomeInfo::where('type', HomeInfo::TYPE_PRIMARY)
            ->where('device',$device)
            ->where('is_slider',$slider)
            ->asc()->published()->get();
        }else{
            return HomeInfo::where('type', HomeInfo::TYPE_PRIMARY)
            ->where('device',$device)
            ->where('is_slider',$slider)
            ->asc()->published()->first();
        }
    }

}
