<?php

namespace App\Repositories;

use App\Model\City;

class CityRepository
{
    public function getCities()
    {
        return City::orderBy('name', 'asc')->get()->pluck('name', 'code');
    }

    public function getCityById($cityId)
    {
        if ($cityId) {
            return City::find($cityId);
        } else {
            return '';
        }
    }

    public function getCityByCode($code)
    {
        if ($code) {
            return City::where('code', $code)->first();
        } else {
            return '';
        }
    }
}
