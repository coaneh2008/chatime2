<?php

namespace App\Repositories;

use App\Model\ContentAbout;

class ContentAboutRepository extends BaseRepository
{
    public function __construct(ContentAbout $model)
    {
        parent::__construct($model);
    }

    public function getAboutList($type)
    {
        return ContentAbout::where('type', $type)->asc()->published()->get();
    }

    public function getAboutList_row($device=0)
    {
        $param=array('bg','tentang','team','logo');
        return ContentAbout::whereIn('type', $param)
        ->where('device',$device)
        ->asc()->published()->get();
    }

    public function getProductCategory($slug)
    {
        return ContentAbout::findOrFailByUrlKey($slug);
    }
    
    public function getChildMenuByParentId($parentId)
    {
        return ContentAbout::where('parent_id', $parentId)->asc()->published()->get();
    }
}
