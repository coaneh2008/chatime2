<?php

namespace App\Repositories\Api;

class CustomerApiRepository extends BaseApi
{
    private $method = "Get_Cust_Data";

    public function __construct()
    {
        parent::__construct();
    }

    public function getCustomerData($message, $auth)
    {
        return parent::postRequest($this->method, $message, $auth);
    }
}
