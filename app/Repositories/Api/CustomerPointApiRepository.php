<?php

namespace App\Repositories\Api;

class CustomerPointApiRepository extends BaseApi
{
    private $method = "Add_Cust_Point";

    public function __construct()
    {
        parent::__construct();
    }

    public function addCustomerPoint($message, $auth)
    {
        return parent::postRequest($this->method, $message, $auth);
    }
}
