<?php

namespace App\Repositories\Api;

class LeaderBoardByDateApiRepository extends BaseApi
{
    private $method = "Get_LeaderBoard_by_Date";

    public function __construct()
    {
        parent::__construct();
    }

    public function getLeaderBoardByDate($message, $auth)
    {
        return parent::postRequest($this->method, $message, $auth);
    }
}
