<?php

namespace App\Repositories\Api;

class UpdatePassKeyApiRepository extends BaseApi
{
    private $method = "Update_Passkey";

    public function __construct()
    {
        parent::__construct();
    }

    public function updatePassKey($message, $auth)
    {
        return parent::postRequest($this->method, $message, $auth);
    }
}
