<?php

namespace App\Repositories\Api;

class UpdateProfileApiRepository extends BaseApi
{
    private $method = "Update_Cust_Data";

    public function __construct()
    {
        parent::__construct();
    }

    public function updateProfile($message, $auth)
    {
        return parent::postRequest($this->method, $message, $auth);
    }
}
