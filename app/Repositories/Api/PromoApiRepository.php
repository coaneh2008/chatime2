<?php

namespace App\Repositories\Api;

class PromoApiRepository extends BaseApi
{
    private $method = "Get_Master_Promo";

    public function __construct()
    {
        parent::__construct();
    }

    public function getMasterPromo($message)
    {
        return parent::postRequest($this->method, $message, null);
    }
}
