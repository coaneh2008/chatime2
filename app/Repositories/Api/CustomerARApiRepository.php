<?php

namespace App\Repositories\Api;

class CustomerARApiRepository extends BaseApi
{
    private $method = "Get_Cust_AR";

    public function __construct()
    {
        parent::__construct();
    }

    public function getCustomerAr($message, $auth)
    {
        return parent::postRequest($this->method, $message, $auth);
    }
}
