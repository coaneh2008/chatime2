<?php

namespace App\Repositories\Api;

use App\Model\City;

class CityApiRepository extends BaseApi
{
    private $method = "Get_Master_City";

    public function __construct()
    {
        parent::__construct();
    }

    public function getMasterCity($message)
    {
        return parent::postRequest($this->method, $message, null);
    }

    public function storeCity($cities)
    {
        $updatedCityDataCode = [];
        foreach ($cities as $city) {
            $code = $city->city_code;
            $name = $city->city_name;
            $cityData = $this->transform($code, $name);
            $updatedCityDataCode[] = $code;
            $this->doSave($code, $cityData);
        }
        
        $this->syncData($updatedCityDataCode);
    }

    public function transform($code, $name)
    {
        return [
            'code' => $code,
            'name' => $name
        ];
    }

    public function doSave($code, $cityData)
    {
        City::updateOrCreate(['code' => $code], $cityData);
    }

    public function syncData($updatedCityData)
    {
        $oldCityData = City::get(['code']);
        $oldCityDataCode = [];
        foreach ($oldCityData as $data) {
            $oldCityDataCode[] = $data->code;
        }
        $diffData = array_diff($oldCityDataCode, $updatedCityData);
        foreach ($diffData as $code) {
            City::where('code', $code)->delete();
        }
    }
}
