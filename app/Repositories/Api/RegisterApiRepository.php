<?php

namespace App\Repositories\Api;

class RegisterApiRepository extends BaseApi
{
    private $method = "Add_Cust_Data";

    public function __construct()
    {
        parent::__construct();
    }

    public function register($message)
    {
        return parent::postRequest($this->method, $message, null);
    }
}
