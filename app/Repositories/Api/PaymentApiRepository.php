<?php

namespace App\Repositories\Api;

class PaymentApiRepository extends BaseApi
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getPaymentUrl($user, $token)
    {
        $url = env('ACE_ONLINE_PAYMENT_URL', '') .
            '&email=' . $user->email .
            '&name=' . $user->name .
            '&memberid=' . $user->username .
            '&tokenmember=' . $token;

        return $url;
    }
}
