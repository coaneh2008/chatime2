<?php

namespace App\Repositories\Api;

class LeaderBoardByPromoApiRepository extends BaseApi
{
    private $method = "Get_LeaderBoard_by_Promo";

    public function __construct()
    {
        parent::__construct();
    }

    public function getLeaderBoardByPromo($message, $auth)
    {
        return parent::postRequest($this->method, $message, $auth);
    }
}
