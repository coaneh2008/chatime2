<?php

namespace App\Repositories\Api;

class LoginApiRepository extends BaseApi
{
    private $method = "login";

    public function __construct()
    {
        parent::__construct();
    }

    public function login($message)
    {
        return parent::postRequest($this->method, $message, null);
    }
}
