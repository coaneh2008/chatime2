<?php

namespace App\Repositories\Api;

use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;

class BaseApi
{
    /**
     * GuzzleHttp Client
     * @var GuzzleHttp\Client
     */
    protected $client;

    protected $encMethod = "AES-256-CBC";

    protected $secretKey = "31323132323031327878787832303138";

    protected $ivKey;

    protected $aceHardwareApiUrl;

    protected function __construct()
    {
        $this->aceHardwareApiUrl = env('ACE_HARDWARE_API_URL', '');
        $this->client = new Client();
        $this->setIvKey();
    }

    protected function setIvKey()
    {
        $this->ivKey = substr($this->secretKey, 0, 16);
    }

    protected function getIvKey()
    {
        return $this->ivKey;
    }

    protected function encrypt($message)
    {
        $encryptMessage = openssl_encrypt($message, $this->encMethod, $this->secretKey, 0, $this->getIvKey());
        return bin2hex(base64_decode($encryptMessage));
    }

    protected function postRequest($method, $message, $auth)
    {
        $maxTrying = 5;
        $isSuccess = false;
        $url = $this->aceHardwareApiUrl . $method;
        while ($maxTrying-- && !$isSuccess) {
            try {
                $result = $this->client->post($url, [
                    'headers' => ['Authorization' => $auth],
                    'body' => "{'p_encrypt':'" . $this->encrypt($message) . "'}"
                ]);
                $isSuccess = true;
                \Log::info(serialize(json_decode($result->getBody())));
                return json_decode($result->getBody());
            } catch (\Exception $e) {
                dd($e);
                $latestException = $e;
            }
        }
        if (!$isSuccess) {
            \Log::warning("[$url] {$latestException->getMessage()}");
            //(connection time out 5x)
            return null;
        }
        \Log::info(serialize(json_decode($result->getBody())));
        return isset($result) ? json_decode($result->getBody()) : null;
    }
}
