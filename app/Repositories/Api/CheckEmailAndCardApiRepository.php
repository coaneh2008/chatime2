<?php

namespace App\Repositories\Api;

class CheckEmailAndCardApiRepository extends BaseApi
{
    private $method = "Check_Email_And_Card";

    public function __construct()
    {
        parent::__construct();
    }

    public function checkEmailAndCard($message)
    {
        return parent::postRequest($this->method, $message, null);
    }
}
