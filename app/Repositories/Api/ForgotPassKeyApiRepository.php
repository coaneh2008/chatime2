<?php

namespace App\Repositories\Api;

class ForgotPassKeyApiRepository extends BaseApi
{
    private $method = "Forgot_Passkey";

    public function __construct()
    {
        parent::__construct();
    }

    public function forgotPassKey($message, $auth)
    {
        return parent::postRequest($this->method, $message, $auth);
    }
}
