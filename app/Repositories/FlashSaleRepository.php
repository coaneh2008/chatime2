<?php

namespace App\Repositories;

use App\Model\FlashSale;
use Carbon\Carbon;

class FlashSaleRepository
{
    public function getActiveFlashSaleByCategoryId($categoryId)
    {
        $now = Carbon::now();

        return FlashSale::where('category_id', $categoryId)
            ->where('start_date', '<=', $now)
            ->where('end_date', '>=', $now)
            ->published()
            ->first();
    }
}
