<?php

namespace App\Repositories;

use App\Model\MemberBenefit;
use App\Model\MemberMerchandise;
use App\Model\MemberNewsUpdate;
use App\Model\MemberPromo;
use App\Model\MemberVoucher;
use App\Model\MerchantCategory;
use App\Model\MerchantLocation;
use App\Model\MerchantPromo;

class MemberBenefitRepository
{
    public function getBenefits()
    {
        return MemberBenefit::published()->asc()->get();
    }

    public function getBenefitsByType($type)
    {
        return MemberBenefit::published()
            ->where('type', $type)
            ->asc()
            ->first();
    }

    public function getVoucherLists()
    {
        return MemberVoucher::published()->asc()->get();
    }

    public function getMerchandiseLists()
    {
        return MemberMerchandise::published()->asc()->get();
    }

    public function getMemberBenefitPromoLists($slug, $categorySlug = null, $locationSlug = null)
    {
        $benefit = MemberBenefit::findOrFailByUrlKey($slug);
        if ($benefit->type == MemberBenefit::TYPE_MEMBER_PROMO) {
            return [
                'promos' => $benefit->memberPromos(),
                'benefit' => $benefit,
            ];
        } elseif ($benefit->type == MemberBenefit::TYPE_MERCHANT_PROMO) {
            $benefits = $benefit->merchantPromos()->published()->asc();
            if ($categorySlug !== null) {
                $category = MerchantCategory::where('slug', $categorySlug)->first();
                if ($category) {
                    $benefits->where('merchant_category_id', $category->id);
                }
            }
            if ($locationSlug !== null) {
                $benefits->whereHas('locations', function ($query) use ($locationSlug) {
                    $query->where('slug', $locationSlug);
                });
            }
            $locations = MerchantLocation::lists('title', 'slug')->toArray();
            $categories = MerchantCategory::lists('title', 'slug')->toArray();
            return [
                'promos' => $benefits->published()->asc()->get(),
                'benefit' => $benefit,
                'locations' => array_merge(['-1' => 'Semua'], $locations),
                'categories' => array_merge(['-1' => 'Semua'], $categories),
            ];
        } elseif ($benefit->type == MemberBenefit::TYPE_NEWS_UPDATES) {
            return [
                'promos' => $benefit->newsUpdates()->published()->asc()->get(),
                'benefit' => $benefit,
            ];
        }
        abort(404);
    }

    public function getMemberBenefitDetail($slug, $detailSlug)
    {
        $benefit = MemberBenefit::findOrFailByUrlKey($slug);
        if ($benefit->type == MemberBenefit::TYPE_MEMBER_PROMO) {
            return [
                'promo' => MemberPromo::findOrFailByUrlKey($detailSlug),
                'benefit' => $benefit,
            ];
        } elseif ($benefit->type == MemberBenefit::TYPE_MERCHANT_PROMO) {
            return [
                'promo' => MerchantPromo::findOrFailByUrlKey($detailSlug),
                'benefit' => $benefit,
            ];
        } elseif ($benefit->type == MemberBenefit::TYPE_NEWS_UPDATES) {
            return [
                'promo' => MemberNewsUpdate::findOrFailByUrlKey($detailSlug),
                'benefit' => $benefit,
            ];
        }
        abort(404);
    }
}
