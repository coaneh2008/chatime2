<?php

namespace App\Repositories;

use App\Model\FlashSticker;
use Carbon\Carbon;

class FlashStickerRepository extends BaseRepository
{
    public function __construct(FlashSticker $model)
    {
        parent::__construct($model);
    }
    public function getStickerList()
    {
        $now = Carbon::now();

        return FlashSticker::where('start_date', '<=', $now)
            ->where('end_date', '>=', $now)
            ->asc()
            ->published()
            ->get();
    }
}
