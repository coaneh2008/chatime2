<?php

namespace App\Repositories;

use App\Model\User;

class UserRepository
{
    public function getSexTypes()
    {
        return [
            0 => 'Pria',
            1 => 'Wanita'
        ];
    }

    public function getReligions()
    {
        return [
            1 => 'Islam',
            2 => 'Kristen',
            3 => 'Katolik',
            4 => 'Budha',
            5 => 'Hindu',
            7 => 'Konghucu',
            6 => 'Others'

        ];
    }

    public function getMaritalStatus()
    {
        return [
            1 => 'Menikah',
            2 => 'Belum Menikah',
            3 => 'Lainnya'
        ];
    }

    public function getCitizenships()
    {
        return [
            1 => 'WNI',
            2 => 'WNA'
        ];
    }

    public function getOccupations()
    {
        return [
            1 => 'Karyawan',
            2 => 'Pengusaha',
            3 => 'Profesional',
            4 => 'Ibu Rumah Tangga',
            5 => 'Pelajar',
            6 => 'Others'
        ];
    }

    public function getShoppingPurposeList()
    {
        return [
            'Office needs',
            'House needs'
        ];
    }

    public function getAdministrationTypes()
    {
        return [
            0 => 'Online',
            1 => 'Offline',
            2 => 'Free Membership'
        ];
    }

    public function getRenewalTypes()
    {
        return [
            1 => 'Yes',
            0 => 'No',
        ];
    }

    public function getNewMemberDataStep1()
    {
        return session('newMemberData');
    }

    public function setNewMemberDataStep1($newMemberData)
    {
        session(['newMemberData' => $newMemberData]);
        session()->save();
    }

    public function setUserToken($userId, $token)
    {
        session(['user-' . $userId => $token]);
        session()->save();
    }

    public function getUserToken($userId)
    {
        return session('user-' . $userId);
    }

    public function setAccountStatus($userId, $counter)
    {
        session(['account-status-' . $userId => $counter + 1]);
        session()->save();
    }

    public function getAccountStatus($userId)
    {
        return session('account-status-' . $userId);
    }

    public function setPointStatus($userId, $counter)
    {
        session(['point-status-' . $userId => $counter + 1]);
        session()->save();
    }

    public function getPointStatus($userId)
    {
        return session('point-status-' . $userId);
    }

    public function getUserByEmail($email)
    {
        if ($email == 'nomail@gmail.com' || $email == 'nomail@yahoo.com') {
            return null;
        } else {
            $user = User::where('email', $email)->first();
            return $user ? $user : null;
        }
    }
}
