<?php

namespace App\Repositories;

use App\Model\InstagramPost;
use App\Repositories\Extension\CacheableTrait;

class InstagramRepository
{
    use CacheableTrait;

    public function storeInstagramPost($code, $instagramPostData)
    {
        $instagramPost = InstagramPost::where('code', '=', $code)->first();
        if (empty($instagramPost)) {
            InstagramPost::firstOrCreate($instagramPostData);
        } else {
            $instagramPost->update($instagramPostData);
        }
    }

    protected function instagramPost()
    {
        return InstagramPost::orderBy('code', 'desc')
            ->take(8)
            ->get();
    }
}
