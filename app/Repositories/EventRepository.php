<?php

namespace App\Repositories;

use App\Model\Event;

class EventRepository
{
    public function getActiveEvent()
    {
        return Event::where('start_date', '<=', \Carbon\Carbon::now())
            ->where('end_date', '>=', \Carbon\Carbon::now())
            ->asc()
            ->published()
            ->get();
    }

    public function getEventBySlug($slug)
    {
        return Event::findOrFailByUrlKey($slug);
    }

    public function getEventById($eventId)
    {
        return Event::find($eventId);
    }

    public function getFollowedEventByUserId($userId)
    {
        return Event::join('event_participants', 'events.id', '=', 'event_participants.event_id')
            ->where('type', Event::TYPE_ANNUAL_EVENT)
            ->where('event_participants.user_id', $userId)
            ->where('start_date', '<=', \Carbon\Carbon::now())
            ->where('end_date', '>=', \Carbon\Carbon::now())
            ->where('status', 1)
            ->distinct()
            ->published()
            ->get();
    }

    public function getActiveFreeMembershipEvent()
    {
        return Event::where('type', Event::TYPE_FREE_MEMBERSHIP)
            ->where('start_date', '<=', \Carbon\Carbon::now())
            ->where('end_date', '>=', \Carbon\Carbon::now())
            ->asc()
            ->published()
            ->first();
    }

    public function getActiveFreeMembershipEventByUserId($userId)
    {
        return Event::join('free_membership_users', 'events.id', '=', 'free_membership_users.event_id')
            ->where('free_membership_users.user_id', $userId)
            ->where('type', Event::TYPE_FREE_MEMBERSHIP)
            ->where('start_date', '<=', \Carbon\Carbon::now())
            ->where('end_date', '>=', \Carbon\Carbon::now())
            ->orderBy('order', 'asc')
            ->published()
            ->get();
    }

    public function getActiveMemberGetMemberEvent()
    {
        return Event::where('type', Event::TYPE_MEMBER_GET_MEMBER)
            ->where('start_date', '<=', \Carbon\Carbon::now())
            ->where('end_date', '>=', \Carbon\Carbon::now())
            ->asc()
            ->published()
            ->first();
    }

    public function getActiveMemberGetMemberEventByUserId($userId)
    {
        return Event::join('user_referal_codes', 'events.id', '=', 'user_referal_codes.event_id')
            ->where('type', Event::TYPE_MEMBER_GET_MEMBER)
            ->where('user_referal_codes.user_id', $userId)
            ->where('start_date', '<=', \Carbon\Carbon::now())
            ->where('end_date', '>=', \Carbon\Carbon::now())
            ->distinct()
            ->published()
            ->get();
    }

    public function getActiveEventTestimoniesByUserId($userId)
    {
        return Event::join('user_testimonies', 'events.id', '=', 'user_testimonies.event_id')
            ->where('user_testimonies.user_id', $userId)
            ->where('start_date', '<=', \Carbon\Carbon::now())
            ->where('end_date', '>=', \Carbon\Carbon::now())
            ->distinct()
            ->published()
            ->get(['event_id', 'title', 'thumb_image', 'description', 'slug']);
    }
}
