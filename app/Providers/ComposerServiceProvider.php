<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer([
            'home.flash-message'
        ], 'App\Http\ViewComposers\FlashStickerComposer');

        view()->composer([
            'memberships._includes.membership-sidebar',
            'memberships.edit-profile'
        ], 'App\Http\ViewComposers\MembershipSidebarComposer');

        view()->composer([
            '_includes.header',
            'home.section-category-list',
            '_includes.section-category-list'
        ], 'App\Http\ViewComposers\ProductCategoryComposer');

        view()->composer([
            '_includes.header',
            '_includes.footer'
        ], 'App\Http\ViewComposers\MenuComposer');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
