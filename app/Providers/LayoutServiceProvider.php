<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class LayoutServiceProvider extends ServiceProvider
{

    protected $layouts = [
        'pages' => [
            'pages.default' => \App\Supports\Layout\DefaultLayout::class,
            'pages.privacy-policy' => \App\Supports\Layout\PrivacyPolicy::class,
            'pages.term-and-condition' => \App\Supports\Layout\TermAndCondition::class,
            'pages.free-membership-term-and-condition' => \App\Supports\Layout\FreeMembershipTermAndCondition::class
        ]
    ];

    public function boot()
    {
        // pass
    }

    public function register()
    {
        $configLayouts = config('suitcms.layouts');
        foreach ($this->layouts as $clusterName => $layouts) {
            $configLayouts[$clusterName] = [];
            foreach ($layouts as $name => $layoutClass) {
                $configLayouts[$clusterName][$name] = $layoutClass::getName();
                $this->app->singleton($name, $layoutClass);
            }
        }
        config()->set('suitcms.layouts', $configLayouts);
    }
}
