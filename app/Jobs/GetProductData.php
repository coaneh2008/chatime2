<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Model\Product;
use App\Repositories\ProductRepository;
use App\Scraper\Product\ProductScraper;
use GuzzleHttp\Client;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Symfony\Component\DomCrawler\Crawler;

class GetProductData extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue;
    use SerializesModels;

    protected $product;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->getProductData($this->product);
    }

    public function getProductData($product)
    {
        $crawler = new Crawler();
        $client = new Client();
        $productRepo = app(ProductRepository::class);
        $productScraper = new ProductScraper($crawler, $client);
        $urlToAceOnline = $product->url_to_ace_online;
        $productData = $productScraper->getProductData($urlToAceOnline);
        $productRepo->storeProduct($urlToAceOnline, $productData);
    }
}
