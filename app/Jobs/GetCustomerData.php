<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Model\CustomerPoint;
use App\Model\RedeemPoint;
use App\Model\Transaction;
use App\Model\User;
use App\Model\PointRedeem;
use App\Repositories\Api\CustomerApiRepository;
use App\Repositories\CityRepository;
use App\Repositories\CustomerPointRepository;
use App\Repositories\UserRepository;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GetCustomerData extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue;
    use SerializesModels;

    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
	//echo "<script>console.log('job: construct_');</script>";
        $this->user = $user;
	$this->getCustomerData($this->user);
	//echo "<script>console.log('job: construct_ end');</script>";
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
	//echo "<script>console.log('job: handeling_');</script>";
        $this->getCustomerData($this->user);
    }

    public function getCustomerData($user)
    {
       	//echo "<script>console.log('job: ()getdatacustomer');</script>";
        $userId = $user->id;
        $memberId = $user->member_id;
        $message = "{'P_Cust_Id':'" . $memberId . "'}";
        $userApiRepo = new CustomerApiRepository();
        $userRepo = new UserRepository();
        $auth = 'token ' . $userRepo->getUserToken($userId);
        $response = $userApiRepo->getCustomerData($message, $auth);
        if ($response) {
            if ($response->is_ok == 'true') {
                $data = $response->rows[0];
                if ($user->email) {
                    $customerData = $this->transformCustomerData($data);
                } else {

                    $customerData = $this->transformNewCustomerData($data);
                }
                User::updateOrCreate(['id' => $userId], $customerData);
                
                $customerPoint = $this->transformCustomerPoint($userId, $data);

                CustomerPoint::updateOrCreate(['user_id' => $userId], $customerPoint);

                if ($trans = $data->Trans_History) {
                    foreach ($trans as $transData) {
                        $transactionData = $this->transformTransaction($userId, $transData);
                        
                        Transaction::updateOrCreate([
                            'user_id' => $userId,
                            'receive_no' => $transData->Receive_No
                        ], $transactionData);
                    }
                }
                if ($redeems = $data->Redeem_History) {
                    foreach ($redeems as $redeemData) {
                        $redeemData = $this->transformRedeem($userId, $redeemData);
                        PointRedeem::updateOrCreate([
                            'user_id' => $userId,
                            'redeem_no' => $redeemData['redeem_no']
                        ], $redeemData);
                    }
                }
            } else {
                session()->put('NOTIF_SESSION_INVALID_TOKEN', '');
            }
        }
    }

    public function transformCustomerData($customerData)
    {
        $cityRepo = new CityRepository();
        $city = $cityRepo->getCityByCode($customerData->Cust_City);
        if ($city) {
            $cityId = $city->id;
        } else {
            $cityId = null;
        }
        $gender = ($customerData->Cust_Sex == 'False') ? 0 : 1;
        $formatedRefDate = \Carbon\Carbon::parse($customerData->Ref_Date)->format('Y-m-d H:i:s');
        $formatedRegDate = \Carbon\Carbon::parse($customerData->Reg_Date)->format('Y-m-d H:i:s');

        return [
            'member_id' => $customerData->Cust_Id,
            'name' => $customerData->Cust_Name,
            'username' => $customerData->Card_Id,
            'expiry_date' => $customerData->Exp_Date,
            'current_address' => $customerData->Cust_Add,
            'cellphone' => $customerData->Cust_Hp,
            'marital_status' => $customerData->Cust_Stat_marital,
            'email' => $customerData->Cust_Mail,
            'gender' => $gender,
            'place_of_birth' => $customerData->Cust_Birth_place,
            'date_of_birth' => $customerData->Cust_Birth_date,
            'current_address_city_id' => $cityId,
            'current_address_zip_code' => $customerData->cust_poscode,
            'is_renewal' => ($customerData->AutoRen == 'True') ? 1 : 0,
            'referal_id' => $customerData->Card_Ref,
            'reference_date' => $formatedRefDate,
            'created_at' => $formatedRegDate,
            'identity_card_id' => isset($customerData->cust_idCard) ? $customerData->cust_idCard : ''
        ];
    }

    public function transformNewCustomerData($customerData)
    {
        $cityRepo = new CityRepository();
        $city = $cityRepo->getCityByCode($customerData->Cust_City);
        $cityId = $city ? $city->id : null;
        $gender = ($customerData->Cust_Sex == 'False') ? 0 : 1;
        $formatedRegDate = \Carbon\Carbon::parse($customerData->Reg_Date)->format('Y-m-d H:i:s');

        return [
            'member_id' => $customerData->Cust_Id,
            'name' => $customerData->Cust_Name,
            'username' => $customerData->Card_Id,
            'expiry_date' => $customerData->Exp_Date,
            'current_address' => $customerData->Cust_Add,
            'cellphone' => '0000',
            'identity_card_id' => '1111',
            'marital_status' => $customerData->Cust_Stat_marital,
            'email' => 'nomail@gmail.com',
            'gender' => $gender,
            'place_of_birth' => $customerData->Cust_Birth_place,
            'date_of_birth' => '1900/01/01',
            'current_address_city_id' => $cityId,
            'current_address_zip_code' => $customerData->cust_poscode,
            'is_renewal' => ($customerData->AutoRen == 'True') ? 1 : 0,
            'referal_id' => $customerData->Card_Ref,
            'reference_date' => $customerData->Ref_Date,
            'created_at' => $formatedRegDate,
            'identity_card_id' => isset($customerData->cust_idCard) ? $customerData->cust_idCard : ''

        ];
    }

    public function transformCustomerPoint($userId, $pointData)
    {
        return [
            'user_id' => $userId,
            'total_point' => $pointData->Jml_Point,
            'expiry_point' => $pointData->Point_exp,
            'expiry_date' => $pointData->point_exp_date,
        ];
    }

    public function transformTransaction($userId, $transData)
    {
        return [
            'user_id' => $userId,
            'receive_no' => $transData->Receive_No,
            'company_name' => $transData->Company_Name,
            'date' => $transData->Trans_Date,
            'amount' => $transData->Amount,
            'point' => $transData->Jml_Point,
            'coupon_no' => $transData->coupon_no
        ];
    }

    public function transformRedeem($userId, $redeemData)
    {
        return [
            'user_id' => $userId,
            'redeem_no' => $redeemData->Redeem_No,
            'company_name' => $redeemData->Company_Name,
            'date' => $redeemData->Redeem_Date,
            'point' => $redeemData->Jml_Point,
        ];
    }
}
