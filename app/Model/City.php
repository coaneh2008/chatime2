<?php

namespace App\Model;

class City extends BaseModel
{

    protected $table = 'cities';

    protected $fillable = [
        'code',
        'name'
    ];
    
    protected $searchField = [
        'name'
    ];

    public function currentAddressCities()
    {
        return $this->hasMany('App\Model\User', 'current_address_city_id');
    }

    public function ktpAddressCities()
    {
        return $this->hasMany('App\Model\User', 'ktp_address_city_id');
    }

    public static function boot()
    {
        parent::boot();

        City::deleting(function ($city) {
            foreach ($city->currentAddressCities as $currentAddressCities) {
                $currentAddressCities->delete();
            }

            foreach ($city->ktpAddressCities as $ktpAddressCities) {
                $ktpAddressCities->delete();
            }
        });
    }
}
