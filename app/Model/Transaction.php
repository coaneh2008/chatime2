<?php

namespace App\Model;

class Transaction extends BaseModel
{
    protected $table = 'transactions';

    protected $fillable = [
        'user_id',
        'receive_no',
        'coupon_no',
        'company_name',
        'amount',
        'point',
        'date'
    ];

    protected $searchField = [
        'receive_no',
        'coupon_no',
        'company_name'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Model\User::class, 'user_id');
    }
}
