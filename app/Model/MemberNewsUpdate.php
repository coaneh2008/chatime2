<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberNewsUpdate extends BaseModel implements SluggableInterface
{
    use AttachableTrait;
    use OrderableTrait;
    use PublishableTrait;
    use SluggableTrait;
    use SoftDeletes;

    protected $table = 'member_news_updates';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $urlKey = 'slug';

    protected $fillable = [
        'member_benefit_id',
        'title',
        'content',
        'thumb_image',
        'banner_image',
        'published',
        'start_date',
        'end_date',
        'slug',
        'order',
        'is_highlighted',
    ];

    protected $dates = [
        'published_at',
        'start_date',
        'end_date',
        'deleted_at'
    ];

    protected $searchField = [
        'title',
        'description'
    ];

    protected $attachable = [
        'thumb_image' => [
            'thumb' => [
                'small' => '200x200'
            ]
        ],
        'banner_image' => [
            'thumb' => [
                'default' => '1100x500'
            ]
        ],

    ];

    public function benefit()
    {
        return $this->belongsTo(\App\Model\MemberBenefit::class, 'member_benefit_id');
    }

    public function search($keyword)
    {
        $result = [];
        $now = \Carbon\Carbon::now();
        $data = $this->where('title', 'like', '%' . $keyword . '%')
            ->orWhere('content', 'like', '%' . $keyword . '%')
            ->where('end_date', '>=', $now)
            ->where('start_date', '<=', $now)
            ->published()
            ->get();
        foreach ($data as $dataItem) {
            $param = ['detailSlug' => $dataItem->slug, 'slug' => $dataItem->benefit->slug];
            $value = [
                'url' => route('frontend.membership.benefit-detail', $param),
                'title' => $dataItem->title,
                'content' => $dataItem->content,
                'type' => 'Member Benefit News Update'
            ];
            $result[] = $value;
        }

        return collect($result)->unique('url')->toArray();
    }
}
