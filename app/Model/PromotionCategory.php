<?php

namespace App\Model;

/*use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
*/
use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\TreeTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Suitmedia\Cacheable\Contracts\CacheableModel;
use Suitmedia\Cacheable\Traits\Model\CacheableTrait;

class PromotionCategory extends BaseModel implements SluggableInterface, CacheableModel
{
    /*use OrderableTrait;
    use PublishableTrait;
    use SoftDeletes;
*/
    use CacheableTrait;
    use OrderableTrait;
    use PublishableTrait;
    use SluggableTrait;
    use SoftDeletes;
    use TreeTrait;


    const TYPE_HOLIDAY_PROMO = 0;
    const TYPE_PROMO_INFO = 1;
    const TYPE_MEMBER_PROMO = 2;
    const TYPE_PRODUCT_PROMO = 3;
    const TYPE_FLASH_SALE = 4;

    protected $table = 'promotion_categories';

    public $timestamps = true;

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $urlKey = 'slug';

    protected $fillable = [
        'title',
        //'type',
        'order',
        'slug',
        'published'
    ];

    protected $dates = [
        'published_at',
        'deleted_at'
    ];

    protected $searchField = [
        'title',
    ];

    public static function typeList()
    {
        return [
            static::TYPE_HOLIDAY_PROMO => 'Holiday Promo',
            static::TYPE_PROMO_INFO => 'Promo Info',
            static::TYPE_MEMBER_PROMO => 'Member Promo',
            static::TYPE_PRODUCT_PROMO => 'Product Promo',
            static::TYPE_FLASH_SALE => 'FLash Sale'
        ];
    }

    public function getTypeName()
    {
        if (!in_array($this->type, array_keys(static::typeList()))) {
            throw new \Exception('Type [' . $this->type . '] is not defined');
        }

        return static::typeList()[$this->type];
    }
    
    public function getPromotionCategoryIdByType($type)
    {
        return PromotionCategory::where('type', $type)->first()->id;
    }
}
