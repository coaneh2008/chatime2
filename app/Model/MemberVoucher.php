<?php

namespace App\Model;

use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberVoucher extends BaseModel
{
    use OrderableTrait;
    use PublishableTrait;
    use SoftDeletes;

    protected $table = 'member_vouchers';

    protected $fillable = [
        'title',
        'content',
        'label',
        'value',
        'point',
        'order',
        'published',
    ];

    protected $dates = [
        'published_at',
        'deleted_at'
    ];

    protected $searchField = [
        'title',
        'content',
        'label',
        'point'
    ];

    public function search($keyword)
    {
        $result = [];
        $data = $this->where('title', 'like', '%' . $keyword . '%')
            ->orWhere('content', 'like', '%' . $keyword . '%')
            ->published()
            ->get();
        foreach ($data as $dataItem) {
            $value = [
                'url' => route('frontend.membership.voucher'),
                'title' => $dataItem->title,
                'content' => $dataItem->content,
                'type' => 'Member Voucher'
            ];
            $result[] = $value;
        }

        return collect($result)->toArray();
    }
}
