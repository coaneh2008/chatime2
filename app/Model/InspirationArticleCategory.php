<?php

namespace App\Model;

use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Suitmedia\Cacheable\Contracts\CacheableModel;
use Suitmedia\Cacheable\Traits\Model\CacheableTrait;

class InspirationArticleCategory extends BaseModel implements SluggableInterface, CacheableModel
{
    use CacheableTrait;
    use OrderableTrait;
    use PublishableTrait;
    use SluggableTrait;
    use SoftDeletes;

    protected $table = 'inspiration_article_categories';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $urlKey = 'slug';

    public $timestamps = true;

    protected $fillable = [
        'title',
        'slug',
        'order',
        'published'
    ];

    protected $dates = [
        'published_at'
    ];

    protected $searchField = [
        'title'
    ];

    public static function getCategoryList()
    {
        return InspirationArticleCategory::asc()->published()->get()->pluck('title', 'id');
    }

    public function search($keyword)
    {
        $result = [];
        $data = $this->where('title', 'like', '%' . $keyword . '%')
            ->published()
            ->get();
        foreach ($data as $dataItem) {
            $param = [
                'slug' => $dataItem->slug
            ];
            $value = [
                'url' => route('frontend.inspiration.index', $param),
                'title' => $dataItem->title,
                'content' => $dataItem->title,
                'type' => 'Article Category'
            ];
            $result[] = $value;
        }

        return collect($result)->unique('url')->toArray();
    }
}
