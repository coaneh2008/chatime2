<?php

namespace App\Model;

class Contact extends BaseModel
{
    protected $table = 'contacts';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'address',
        'phone',
        'email',
        'message',
    ];

    protected $searchField = [
        'email',
    ];
}
