<?php

namespace App\Model;

class ReferalCode extends BaseModel
{

    protected $table = 'user_referal_codes';

    protected $fillable = [
        'user_id',
        'event_id',
        'code'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Model\User::class, 'user_id');
    }

    public function event()
    {
        return $this->belongsTo(\App\Model\Event::class, 'event_id');
    }
}
