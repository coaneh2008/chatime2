<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\OrderableTrait;

class BrochureAttachment extends BaseModel
{
    use AttachableTrait;
    use OrderableTrait;

    protected $table = 'brochure_attachments';

    protected $fillable = [
        'image',
        'order',
        'published'
    ];

    protected $attachable = [
        'image' => [
            'thumb' => [
                'huge' => '500x708'
            ]
        ]
    ];

    public function brochure()
    {
        return $this->belongsTo(\App\Model\Brochure::class, 'brochure_id');
    }

    public function setAttachmentOption($option)
    {
        $this->attachable['image'] = $option;
    }
}
