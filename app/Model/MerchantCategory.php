<?php

namespace App\Model;

use App\Model\Extension\OrderableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class MerchantCategory extends BaseModel implements SluggableInterface
{
    use OrderableTrait;
    use SluggableTrait;
    use SoftDeletes;

    protected $table = 'merchant_promo_categories';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $fillable = [
        'title',
        'order',
        'slug',
    ];

    protected $searchField = [
        'title',
    ];

    protected $dates = [
        'deleted_at'
    ];
}
