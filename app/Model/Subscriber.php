<?php

namespace App\Model;

class Subscriber extends BaseModel
{
    protected $table = 'subscribers';

    protected $fillable = [
        'email'
    ];
    
    protected $searchField = [
        'email'
    ];
}
