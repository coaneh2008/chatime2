<?php

namespace App\Model;

class Setting extends BaseModel
{
    const TYPE_GENERAL = 0;
    const TYPE_MEMBERSHIP = 1;

    protected $table = 'settings';

    protected $fillable = [
        'name',
        'type',
        'title',
        'description',
        'value'
    ];

    protected $searchField = [
        'name',
        'title',
        'description',
        'value'
    ];

    protected static function boot()
    {
        parent::boot();

        static::initData();
        static::saved(function ($model) {
            $cacheName = "setting.{$model->name}";
            \Cache::forget($cacheName);
        });

        static::deleted(function ($model) {
            $cacheName = "setting.{$model->name}";
            \Cache::forget($cacheName);
        });
    }

    public static function initData()
    {
        $configSettings = config('settings');
        foreach ($configSettings as $name => $configSetting) {
            $cacheName = "setting.$name";
            if (!\Cache::has($cacheName)) {
                $setting = static::findByName($name);
                if ($setting === null) {
                    $setting = new static($configSetting);
                    $setting->name = $name;
                    $setting->save();
                }
                \Cache::put($cacheName, $setting->value, 120);
            }
        }
    }

    public static function findByName($name)
    {
        return static::whereName($name)->first();
    }

    public static function get($name, $default = '')
    {
        $cacheName = "setting.$name";
        $value = \Cache::get($cacheName);
        if ($value === null || $value === "") {
            $instance = static::findByName($name);
            $value = $default;

            if ($instance) {
                $value = $instance->casted_value;
            }

            \Cache::put($cacheName, $value, 120);
        }

        return $value;
    }

    public function getTypeAttribute()
    {
        return config("settings.{$this->name}.type", 'suitText');
    }

    public function getCastedValueAttribute()
    {
        switch ($this->type) {
            case 'suitTokenField':
                return explode(',', $this->value);
        }

        return $this->value;
    }
}
