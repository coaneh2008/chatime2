<?php

namespace App\Model;

class Redirection extends BaseModel
{
    protected $table = 'redirections';

    protected $fillable = [
        'old_path',
        'new_url',
    ];

    protected $searchField = [
        'old_path',
        'new_url',
    ];
}
