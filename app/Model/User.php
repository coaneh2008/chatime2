<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;

class User extends BaseModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    SluggableInterface
{
    use AttachableTrait;
    use Authenticatable;
    use Authorizable;
    use CanResetPassword;
    use SluggableTrait;

    /**
     * User Group
     */
    const USER = 0;
    const SUPER_ADMIN = 1;

    protected $table = 'users';

    public $timestamps = true;

    protected $sluggable = [
        'build_from' => 'username'
    ];

    protected $urlKey = 'slug';

    protected $fillable = [
        'member_id',
        'group_type',
        'username',
        'name',
        'email',
        'password',
        'slug',
        'about',
        'date_of_birth',
        'active',
        'referal_id',
        'reference_date',
        'current_address_city_id',
        'ktp_address_city_id',
        'place_of_birth',
        'current_address_zip_code',
        'ktp_address_zip_code',
        'current_address',
        'ktp_address',
        'expiry_date',
        'is_renewal',
        'profile_picture',
        'identity_card_id',
        'cellphone',
        'telephone',
        'gender',
        'religion_id',
        'marital_status',
        'citizenship_id',
        'occupation_id',
        'shopping_purpose',
        'administration_type',
        'created_at'
    ];

    protected $hidden = [
        'password', 'remember_token'
    ];

    protected $searchField = [
        'username',
        'name',
        'member_id',
        'email',
    ];

    protected $attachable = [
        'profile_picture' => [
            'thumb' => [
                'small' => '100x100',
                'square' => '120x120'
            ]
        ],
    ];

    public function setPasswordAttribute($value)
    {
        if (!empty($value) || is_numeric($value)) {
            $this->attributes['password'] = \Hash::make($value);
        }
    }

    public static function groups()
    {
        return [
            static::SUPER_ADMIN => 'Super Administrator',
            // static::USER => 'User'
        ];
    }

    public function getGroupTypeAttribute()
    {
        if (!isset($this->attributes['group_type'])) {
            return null;
        }

        $group = $this->attributes['group_type'];
        if (!is_numeric($group)) {
            return -1;
        }
        return (int)$group;
    }

    public function isSuper()
    {
        return $this->group_type === static::SUPER_ADMIN;
    }

    public function isUser()
    {
        return $this->group_type === static::USER;
    }

    public function login()
    {
        if ($this->current_log_in_at !== null) {
            $this->last_log_in_at = $this->current_log_in_at;
            $this->last_log_in_ip = $this->current_log_in_ip;
        }

        $this->current_log_in_at = new \DateTime;
        $this->current_log_in_ip = \Request::getClientIp();
        $this->save();

        $this->increment('log_in_count');
    }

    public function logout()
    {
        $this->last_log_in_at = new \DateTime;
        $this->last_log_in_ip = \Request::getClientIp();
        $this->current_log_in_at = null;
        $this->current_log_in_ip = '';
        $this->save();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getGroupName()
    {
        if (!in_array($this->group_type, array_keys(static::groups()))) {
            throw new \Exception('Group type [' . $this->group_type . '] is not defined');
        }

        return static::groups()[$this->group_type];
    }

    public function scopeNotMe($query)
    {
        if (\Auth::check()) {
            return $query->where('id', '<>', \Auth::user()->id);
        }
    }

    public function isMe()
    {
        $auth = \Auth::user();
        return get_class($this) == get_class($auth) && $this->id === $auth->id;
    }

    public function listById()
    {
        $users = $this->get();
        $res = [];
        foreach ($users as $user) {
            $res[$user->getKey()] = $user->name . ' (' . $user->getGroupName() . ')';
        }
        return $res;
    }

    public static function getAuthorList()
    {
        return User::where('group_type', static::SUPER_ADMIN)->get()->pluck('username', 'id');
    }

    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            if (($model->group_type == User::USER) && !str_contains($model->username, 'TAM')) {
                $now = \Carbon\Carbon::now();
                $event = \App\Model\Event::where('type', \App\Model\Event::TYPE_MEMBER_GET_MEMBER)
                    ->where('start_date', '<=', $now)
                    ->where('end_date', '>=', $now)
                    ->first();

                if ($event) {
                    $referalCode = $event->prefix_referal_code . sha1($model->id);
                    $referalData = [
                        'event_id' => $event->id,
                        'user_id' => $model->id,
                        'code' => substr($referalCode, 0, 7)
                    ];
                    ReferalCode::updateOrCreate([
                        'event_id' => $event->id,
                        'user_id' => $model->id
                    ], $referalData);
                }
            }
        });
    }

    public function currentAddressCity()
    {
        return $this->belongsTo('App\Model\City', 'current_address_city_id');
    }

    public function ktpAddressCity()
    {
        return $this->belongsTo('App\Model\City', 'ktp_address_city_id');
    }
}
