<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\PublishableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class FlashSale extends BaseModel
{
    use AttachableTrait;
    use PublishableTrait;
    use SoftDeletes;

    protected $table = 'flash_sales';

    protected $fillable = [
        'category_id',
        'start_date',
        'end_date',
        'banner_image',
        'url_to_ace_online',
        'published'
    ];

    protected $dates = [
        'published_at'
    ];

    protected $searchField = [
        'url_to_ace_online',
        'start_date',
        'end_date'
    ];

    protected $attachable = [
        'banner_image' => [
            'thumb' => [
                'small' => '100x100',
                'huge' => '800x351'
            ]
        ]
    ];

    public function productCategory()
    {
        return $this->belongsTo(\App\Model\PromotionCategory::class, 'category_id');
    }
}
