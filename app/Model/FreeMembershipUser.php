<?php

namespace App\Model;

class FreeMembershipUser extends BaseModel
{

    protected $table = 'free_membership_users';

    protected $fillable = [
        'user_id',
        'event_id',
        'status'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Model\User::class, 'user_id');
    }

    public function event()
    {
        return $this->belongsTo(\App\Model\Event::class, 'event_id');
    }
}
