<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;

class PageAttachment extends BaseModel
{
    use AttachableTrait;

    protected $table = 'page_attachments';

    protected $fillable = [
        'name',
        'uri'
    ];

    protected $attachable = [
        'uri' => []
    ];

    public function page()
    {
        return $this->belongsTo(\App\Model\Page::class, 'page_id');
    }

    public function setAttachmentOption($option)
    {
        $this->attachable['uri'] = $option;
    }
}
