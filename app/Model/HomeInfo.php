<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\SeoTagsTrait;
use App\Model\Extension\TreeTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Suitmedia\Cacheable\Contracts\CacheableModel;
use Suitmedia\Cacheable\Traits\Model\CacheableTrait;

class HomeInfo extends BaseModel implements SluggableInterface, CacheableModel
{
    use AttachableTrait;
    use CacheableTrait;
    use OrderableTrait;
    use PublishableTrait;
    use SeoTagsTrait;
    use SluggableTrait;
    use SoftDeletes;
    use TreeTrait;

    const TYPE_PRIMARY = 'Primary';
    const TYPE_SECONDARY = 'Secondary';
    const TYPE_TERTIARY = 'Tertiary';

    const SEO_TAG_TYPE = 2;

    const TYPE_URL = 0;
    const TYPE_VIDEO = 1;

    protected $table = 'home_infos';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $urlKey = 'slug';

    public $timestamps = true;

    protected $fillable = [
        'type',
        'url_to_ace_online',
        'title',
        //'thumb_image',
        //'sidebar_image',
        'banner_image',
        'description',
        'content',
        //'slug',
        'order',
        'published',
        'page_type',
        'seo_slug',
        'seo_title',
        'seo_description',
        'opengraph_title',
        'opengraph_description',
        'twitter_title',
        'twitter_description',
        'meta_image',
        'is_slider',
        'device'
    ];

    protected $dates = [
        'published_at',
        'deleted_at'
    ];

    protected $searchField = [
        'title',
        'description',
        'content',
    ];

    protected $attachable = [
        'thumb_image' => [
            'thumb' => [
                'image_icon' => '20x20',
                'icon' => '100x100',
                'secondary_medium' => '358x201'
            ]
        ],
        'banner_image' => [
            'thumb' => [
                'icon' => '100x100',
                'medium' => '270x490',
                'huge' => '1105x487'
            ]
        ],
        'sidebar_image' => [
            'thumb' => [
                'icon' => '100x100',
                'medium' => '270x490'
            ]
        ]
    ];

    public static function typeList()
    {
        return [
            static::TYPE_PRIMARY,
            static::TYPE_SECONDARY,
            static::TYPE_TERTIARY
        ];
    }

    public static function typeDevice()
    {
        return [
            static::TYPE_URL => 'Desktop',
            static::TYPE_VIDEO => 'Mobile'
        ];
    }

    public function getPageTypeName()
    {
        return ucwords(strtolower(str_replace('_', ' ', $this->table))) . ' Page';
    }

    public function setPageType()
    {
        return static::SEO_TAG_TYPE;
    }

    public function parentListById()
    {
        $query = $this;
        if ($this->exists) {
            $query = $query->notThis(true);
        }
        return $query->select(['id', 'title'])->get()->lists('title', 'id');
    }

    public static function typeListName()
    {
        return array_combine(static::typeList(), static::typeList());
    }

    public static function getCategoryList()
    {
        return HomeInfo::where('type', static::TYPE_PRIMARY)->asc()->published()->get()->pluck('title', 'id');
    }

    public function getProductCategoryByType($type)
    {
        return HomeInfo::where('type', $type)->asc()->published()->get();
    }

    public function search($keyword)
    {
        $result = [];
        $data = $this->where('title', 'like', '%' . $keyword . '%')
            ->orWhere('content', 'like', '%' . $keyword . '%')
            ->published()
            ->get();
        foreach ($data as $dataItem) {
            $param = ['slug' => $dataItem->slug];
            $value = [
                'url' => $dataItem->url_to_ace_online,
                'title' => $dataItem->title,
                'content' => $dataItem->title,
                'type' => 'Product Category'
            ];
            if ($dataItem->type == $this::TYPE_PRIMARY) {
                $value['url'] = route('frontend.product-category.show', $param);
                $value['content'] = $dataItem->content;
            } else {
                $value['newTab'] = true;
            }
            $result[] = $value;
        }

        return collect($result)->unique('url')->toArray();
    }
}
