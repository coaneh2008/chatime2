<?php

namespace App\Model;

use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Region;
use Illuminate\Database\Eloquent\SoftDeletes;

class StoreLocation extends BaseModel
{
    use OrderableTrait;
    use PublishableTrait;
    use SoftDeletes;

    const TYPE_ACE_HARDWARE = 0;
    const TYPE_ACE_EXPRESS = 1;
    const TYPE_ACE_HOME_CENTRE = 2;

    protected $table = 'store_locations';

    protected $fillable = [
        'region_id',
        'type',
        'title',
        'latitude',
        'longitude',
        'address',
        'published',
        'order'
    ];

    protected $dates = [
        'deleted_at',
        'published_at'
    ];

    protected $searchField = [
        'title'
    ];

    public function region()
    {
        return $this->belongsTo(\App\Model\Region::class, 'region_id');
    }

    public static function typeList()
    {
        return [
            static::TYPE_ACE_HARDWARE => 'Chatime'/*,
            static::TYPE_ACE_EXPRESS => 'Chatime',
            static::TYPE_ACE_HOME_CENTRE => 'Chatime'*/
        ];
    }

    public function getTypeName()
    {
        if (!in_array($this->type, array_keys(static::typeList()))) {
            throw new \Exception('Type [' . $this->type . '] is not defined');
        }

        return static::typeList()[$this->type];
    }

    public function getAllRegion()
    {
        return Region::has('storeLocations')->asc()->published()->get();
    }

    public function getStoreLocationByRegionId($regionId)
    {
        return $this->where('region_id', $regionId)
            ->orderBy('title', 'asc')
            ->published()
            ->get();
    }

    public function mapData()
    {
        $regions = $this->getAllRegion()->toArray();
        $data = [];

        foreach ($regions as $value) {
            $stores = $this->getStoreLocationByRegionId($value['id']);
            $data[] = [
                "name" => $value['title'],
                "id" => $value['id'],
                "stores" => $this->getStoreLocationData($stores)
            ];
        }

        return json_encode($data, JSON_HEX_TAG);
    }

    private function getStoreLocationData($var)
    {
        if ($var->isEmpty()) {
            return null;
        }

        return $this->transformCollection($var);
    }

    private function transformCollection($models)
    {
        $trans = [];

        foreach ($models as $model) {
            $trans[] = $this->transform($model);
        }

        return $trans;
    }

    private function transform($model)
    {
        $array = [
            'type' => $model->getTypeName(),
            'id' => $model->id,
            'name' => $model->title,
            'address' => $model->address,
            'lat' => (float) $model->latitude,
            'lng' => (float) $model->longitude,
        ];

        return $array;
    }

    public function search($keyword)
    {
        $result = [];
        $data = $this->where('title', 'like', '%' . $keyword . '%')
            ->published()
            ->get();
        foreach ($data as $dataItem) {
            $content = $dataItem->getTypeName() . ' ' . $dataItem->title . ' (' .
                $dataItem->latitude . ', ' . $dataItem->longitude . ')';
            $value = [
                'url' => route('frontend.store-location.index'),
                'title' => $dataItem->title,
                'content' => $content,
                'type' => 'Store & Location'
            ];
            $result[] = $value;
        }

        return collect($result)->toArray();
    }
}
