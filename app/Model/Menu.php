<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\OrderableTrait;
use App\Model\Extension\TreeTrait;
use App\Model\Translation\TranslateModel;
use Suitmedia\Cacheable\Contracts\CacheableModel;
use Suitmedia\Cacheable\Traits\Model\CacheableTrait;

class Menu extends TranslateModel implements CacheableModel
{
    use AttachableTrait;
    use CacheableTrait;
    use OrderableTrait;
    use TreeTrait;

    const TYPE_MAIN = 'Main';
    const TYPE_SECONDARY = 'Secondary';
    const TYPE_FOOTER = 'Footer';
    const TYPE_HOMEPAGE = 'HomePage';

    protected $table = 'menus';

    public $timestamps = true;

    protected $fillable = [
        'type',
        'is_link',
        'url',
        'order',
        'icon_image'
    ];

    protected $translateField = [
        'title'
    ];

    protected $appends = [
        'real_url'
    ];

    protected $searchField = [
        'title',
        'type',
        'url',
        'parent.title'
    ];

    protected $attachable = [
        'icon_image' => [
            'thumb' => [
                'small_square' => '15x15',
                'square' => '100x100',
                'full_height_small' => '16x_'

            ]
        ]
    ];

    public function child()
    {
        return $this->hasMany(Menu::class, 'parent_id');
    }

    public function parentListById()
    {
        $query = $this;
        if ($this->exists) {
            $query = $query->notThis(true);
        }
        return $query->select(['id', 'title'])->get()->lists('title', 'id');
    }

    public function getRealUrlAttribute()
    {
        if (!$this->is_link) {
            return '#';
        }
        if (filter_var($this->url, FILTER_VALIDATE_URL) === true) {
            return $this->url;
        }

        return url($this->url);
    }

    public function scopeOrderedMenu($query, $depth = 5)
    {
        $callback = function ($query) {
            $query->asc();
        };
        return $query->fromRoot()
                     ->withChilds(['title', 'is_link', 'url', 'icon_image'], $callback, $depth)
                     ->select([$this->getKeyName(), 'title', 'is_link', 'url', 'icon_image']);
    }

    public function scopeType($query, $type)
    {
        return $query->whereType($type);
    }

    public function scopeMain($query)
    {
        return $query->type(static::TYPE_MAIN);
    }

    public function scopeSecondary($query)
    {
        return $query->type(static::TYPE_SECONDARY);
    }

    public function scopeFooter($query)
    {
        return $query->type(static::TYPE_FOOTER);
    }

    public static function getAllMenu()
    {
        $menus = static::orderedMenu()->get();
        return $menus;
    }

    public static function getMainMenu()
    {
        $menus = static::orderedMenu()->main()->get();
        return $menus;
    }

    public static function getSecondaryMenu()
    {
        $menus = static::orderedMenu()->secondary()->get();
        return $menus;
    }

    public static function getFooterMenu()
    {
        $menus = static::orderedMenu()->footer()->get();
        return $menus;
    }

    public static function typeList()
    {
        return [
            static::TYPE_MAIN,
            static::TYPE_SECONDARY,
            static::TYPE_FOOTER,
            static::TYPE_HOMEPAGE,
        ];
    }

    public static function typeListName()
    {
        return array_combine(static::typeList(), static::typeList());
    }
}
