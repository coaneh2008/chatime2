<?php

namespace App\Model;

use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Suitmedia\Cacheable\Contracts\CacheableModel;
use Suitmedia\Cacheable\Traits\Model\CacheableTrait;

class NewEventCategory extends BaseModel implements SluggableInterface, CacheableModel
{
    use CacheableTrait;
    use OrderableTrait;
    use PublishableTrait;
    use SluggableTrait;
    use SoftDeletes;

    protected $table = 'new_event_categories';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $urlKey = 'slug';

    public $timestamps = true;

    protected $fillable = [
        'title',
        'slug',
        'order',
        'published',
        'banner_image'
    ];

    protected $dates = [
        'published_at'
    ];

    protected $searchField = [
        'title'
    ];


    protected $attachable = [
        'thumb_image' => [
            'thumb' => [
                'image_icon' => '20x20',
                'icon' => '100x100',
                'secondary_medium' => '358x201'
            ]
        ],
        'banner_image' => [
            'thumb' => [
                'icon' => '100x100',
                'medium' => '270x490',
                'huge' => '1105x487'
            ]
        ],
        'sidebar_image' => [
            'thumb' => [
                'icon' => '100x100',
                'medium' => '270x490'
            ]
        ]
    ];


    public static function getCategoryList()
    {
        return NewEventCategory::asc()->published()->get()->pluck('title', 'id');
    }

    public function search($keyword)
    {
        $result = [];
        $data = $this->where('title', 'like', '%' . $keyword . '%')
            ->published()
            ->get();
        foreach ($data as $dataItem) {
            $param = [
                'slug' => $dataItem->slug
            ];
            $value = [
                'url' => route('frontend.inspiration.index', $param),
                'title' => $dataItem->title,
                'content' => $dataItem->title,
                'type' => 'Article Category'
            ];
            $result[] = $value;
        }

        return collect($result)->unique('url')->toArray();
    }
}
