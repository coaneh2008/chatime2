<?php

namespace App\Model;

use App\Model\Extension\OrderableTrait;

class FeaturedBrandAttachment extends BaseModel
{
    use OrderableTrait;

    protected $table = 'featured_brands_attachments';

    protected $fillable = [
        'product_id',
        'order',
        'published'
    ];

    protected $attachable = [
        'image' => [
            'thumb' => [
                'huge' => '178x179'
            ]
        ]
    ];

    public function featured_brands()
    {
        return $this->belongsTo(\App\Model\FeaturedBrand::class, 'featured_brands_id');
    }

    public function product()
    {
        return $this->belongsTo(\App\Model\Product::class, 'product_id');
    }
}
