<?php

namespace App\Model;

use App\Model\Extension\OrderableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class MerchantLocation extends BaseModel implements SluggableInterface
{
    use OrderableTrait;
    use SluggableTrait;
    use SoftDeletes;

    protected $table = 'merchant_locations';

    protected $fillable = [
        'title',
        'order',
        'slug',
    ];

    protected $searchField = [
        'title',
    ];

    protected $dates = [
        'deleted_at'
    ];
}
