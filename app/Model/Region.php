<?php

namespace App\Model;

use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Region extends BaseModel
{
    use OrderableTrait;
    use PublishableTrait;
    use SoftDeletes;

    protected $table = 'regions';

    protected $fillable = [
        'title',
        'latitude',
        'longitude',
        'published',
        'order'
    ];

    protected $dates = [
        'deleted_at',
        'published_at'
    ];

    protected $searchField = [
        'title'
    ];

    public static function getRegions()
    {
        return Region::get()->pluck('title', 'id');
    }

    public function storeLocations()
    {
        return $this->hasMany('\App\Model\StoreLocation');
    }
}
