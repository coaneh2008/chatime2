<?php

namespace App\Model;

class PointRedeem extends BaseModel
{
    protected $table = 'point_redeems';
    
    protected $fillable = [
        'user_id',
        'redeem_no',
        'company_name',
        'point',
        'date'
    ];

    protected $searchField = [
        'redeem_no',
        'company_name'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Model\User::class, 'user_id');
    }
}
