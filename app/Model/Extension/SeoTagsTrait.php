<?php
namespace App\Model\Extension;

use App\Model\SeoTag;

trait SeoTagsTrait
{
    /**
     * SeoTagsTrait boot trait.
     * @return void
     */
    public static function bootSeoTagsTrait()
    {
        static::deleting(function ($model) {
            $model->seoTag()->delete();
        });

        static::saved(function ($model) {
            // for handle empty data if set published in index (ajax)
            if (!request()->ajax()) {
                $seoTagData = $model->transform($model);
                SeoTag::updateOrCreate(['page_type' => $model->setPageType(), 'content_id' => $model->id], $seoTagData);
            }
        });
    }

    public function transform($model)
    {
        return [
            'page_type' => $model->setPageType(),
            'content_id' => $model->id,
            'seo_title' => $model->seo_title,
            'seo_description' => $model->seo_description,
            'opengraph_title' => $model->opengraph_title,
            'opengraph_description' => $model->opengraph_description,
            'twitter_title' => $model->twitter_title,
            'twitter_description' => $model->twitter_description,
            'meta_image' => $model->meta_image
        ];
    }

    public function seoTag()
    {
        return SeoTag::where('page_type', $this::SEO_TAG_TYPE)->where('content_id', $this->id)->first();
    }

    public function getPageType()
    {
        return $this->seoTag() ? $this->seoTag()->page_type() : '';
    }

    public function setPageTypeAttribute($value)
    {
        $this->page_type = $value;
    }

    public function getSeoTitle()
    {
        return $this->seoTag() ? $this->seoTag()->seo_title : '';
    }

    public function setSeoTitleAttribute($value)
    {
        $this->seo_title = $value;
    }

    public function getSeoDescription()
    {
        return $this->seoTag() ? $this->seoTag()->seo_description : '';
    }

    public function setSeoDescriptionAttribute($value)
    {
        $this->seo_description = $value;
    }

    public function getOpengraphTitle()
    {
        return $this->seoTag() ? $this->seoTag()->opengraph_title : '';
    }
    
    public function setOpengraphTitleAttribute($value)
    {
        $this->opengraph_title = $value;
    }

    public function getOpengraphDescription()
    {
        return $this->seoTag() ? $this->seoTag()->opengraph_description : '';
    }

    public function setOpengraphDescriptionAttribute($value)
    {
        $this->opengraph_description = $value;
    }

    public function getTwitterTitle()
    {
        return $this->seoTag() ? $this->seoTag()->twitter_title : '';
    }

    public function setTwitterTitleAttribute($value)
    {
        $this->twitter_title = $value;
    }

    public function getTwitterDescription()
    {
        return $this->seoTag() ? $this->seoTag()->twitter_description : '';
    }

    public function setTwitterDescriptionAttribute($value)
    {
        $this->twitter_description = $value;
    }

    public function getMetaImage()
    {
        return $this->seoTag() ? $this->seoTag()->meta_image : '';
    }

    public function setMetaImageAttribute($value)
    {
        $this->meta_image = $value;
    }
}
