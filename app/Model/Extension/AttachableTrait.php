<?php

namespace App\Model\Extension;

use App\Supports\Attachment\File;
use App\Supports\Attachment\Youtube;
use Attachment;
use Croppa;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Attachable Trait
 * Manage field width attachment and handle thumbnail
 *
 * usage:
 * Declare $attachable attribute
 *
 * e.q.
 *
 * protected $attachable = [
 *     'image' => [
 *         'thumb' => [
 *             'small' => '100x100',
 *             'relatif' => '_x100',
 *         ]
 *     ]
 * ]
 *
 * $model->getThumbnail('image', 'small');
 * $model->getThumbnail('image', 'small', $defaultUrl);
 *
 * $model->getAttachment('image');         // 'http://localhost/files/path/to/file'
 * $model->getAttachmentInfo('image', 'mime_type');
 *
 * // for youtube
 * $model->getAttachmentInfo('image', 'code');         // return youtube code
 * $model->getAttachmentInfo('image', 'embed_url');    // return youtube embed url
 * $model->getAttachmentInfo('image', 'real_url');     // return youtube real url
 *
 */
trait AttachableTrait
{
    public static function bootAttachableTrait()
    {
        static::saving(function ($model) {
            $model->processAttachment();
        });

        static::saved(function ($model) {
            foreach ($model->getAttachableFields() as $field => $dirpath) {
                $filename = public_path($model->getOriginal($field));
                $fieldUrl = $model->getOriginal($field);
                if ($model->isDirty($field) && !empty($fieldUrl) && file_exists($filename)) {
                    try {
                        Croppa::delete($model->getOriginal($field));
                    } catch (\Exception $e) {
                        \Log::warning($e);
                    }
                }
            }
        });

        static::deleted(function ($model) {
            foreach ($model->getAttachableFields() as $field => $dirpath) {
                $filename = public_path($model->getOriginal($field));
                if (file_exists($filename)) {
                    try {
                        Croppa::delete($model->getOriginal($field));
                    } catch (\Exception $e) {
                        \Log::warning($e);
                    }
                }
            }
        });
    }

    protected function filename($filename, $separator = '_')
    {
        $fileSplit = explode('.', $filename);
        if (count($fileSplit) === 1) {
            return str_slug(sprintf("%s_%s", $filename, uniqid()), $separator);
        }

        $extension = $fileSplit[count($fileSplit) - 1];
        $filename = implode($separator, array_slice($fileSplit, 0, count($fileSplit) - 1));

        return implode('.', [str_slug(sprintf("%s_%s", $filename, uniqid()), $separator), $extension]);
    }

    public function getAttachableFields()
    {
        if (isset($this->attachable)) {
            return $this->attachable;
        }
        return [];
    }

    public function processAttachment()
    {
        foreach (array_keys($this->getAttachableFields()) as $field) {
            $dirpath = $this->generateUploadRelativePath($field);
            if (is_string($this->$field)) {
                $this->$field = ltrim($this->$field, '/');
            }

            if (!$this->isDirty($field)) {
                continue;
            }

            $inputValue = $this->getAttributeFromArray($field);
            if (empty($inputValue)) {
                $this->setAttribute($field, null);
                continue;
            }

            $attachment = Attachment::download($inputValue, $dirpath);

            $this->assignAttachmentField($field, $attachment);
        }
    }

    protected function assignAttachmentField($field, File $attachment)
    {
        $this->$field = $attachment->getUrlPath();

        $this->casts[$field . '_info'] = 'array';
        $this->setAttribute($field . '_info', $attachment->toArray());

        $type = Attachment::FILE;
        if ($attachment instanceof Youtube) {
            $type = Attachment::YOUTUBE;
        }

        // $this->setAttribute($field . '_type', $type);
    }

    protected function generateUploadRelativePath($field)
    {
        $dir = strtolower(ltrim(get_class($this), 'App\\Model\\'));
        $dir = str_replace('\\', '/', $dir) . "/$field";

        return uploadRelativePath("uploads/" . $dir);
    }

    public function getAttachment($field)
    {
        if ($this->$field === null) {
            return null;
        }

        return asset($this->getAttribute($field));
    }

    public function getThumbnail($field, $name, $default = null)
    {
        if ($this->$field === null ||
            !file_exists(public_path($this->attributes[$field])) ||
            exif_imagetype(public_path($this->attributes[$field])) === false) {
            return $default;
        }

        $attachables = $this->getAttachableFields();
        if (!isset($attachables[$field]) ||
            !isset($attachables[$field]['thumb']) ||
            !isset($attachables[$field]['thumb'][$name])) {
            throw new \Exception(sprintf('thumbnail not found `%s` in `%s` field', $name, $field));
        }

        $options = $this->getThumbnailInfo($attachables[$field]['thumb'][$name]);
        return asset(Croppa::url($this->attributes[$field], $options['width'], $options['height']));
    }

    public function getAttachmentType($field)
    {
        return $this->getAttribute($field . '_type');
    }

    public function checkAttachmentType($field, $type)
    {
        return $this->getAttachmentType($field) == $type;
    }

    public function getAttachmentInfo($field, $info)
    {
        $field = $field . '_info';
        $this->casts[$field] = 'array';

        $value = $this->$field;
        if (isset($value[$info])) {
            return $value[$info];
        }
        return null;
    }

    public static function getThumbnailInfo($string)
    {
        list($width, $height) = explode('x', $string);

        $width = ($width == '_')?null:$width;
        $height = ($height == '_')?null:$height;

        return [
            'width' => $width,
            'height' => $height
        ];
    }
}
