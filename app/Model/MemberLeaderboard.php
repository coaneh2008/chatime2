<?php

namespace App\Model;

class MemberLeaderboard extends BaseModel
{
    protected $table = 'member_leaderboards';

    protected $fillable = [
        'rank',
        'member_card_id',
        'name',
        'amount',
        'transaction',
        'quantity'
    ];

    protected $searchField = [
        'rank',
        'member_card_id',
        'name',
        'amount',
        'transaction',
        'quantity'
    ];
}
