<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberMerchandise extends BaseModel
{
    use AttachableTrait;
    use OrderableTrait;
    use PublishableTrait;
    use SoftDeletes;

    protected $table = 'member_merchandises';

    protected $fillable = [
        'title',
        'thumb_image',
        'point',
        'order',
        'published',
    ];

    protected $dates = [
        'published_at',
        'deleted_at'
    ];

    protected $searchField = [
        'title',
        'point'
    ];

    protected $attachable = [
        'thumb_image' => [
            'thumb' => [
                'small' => '200x200'
            ]
        ],
    ];

    public function search($keyword)
    {
        $result = [];
        $data = $this->where('title', 'like', '%' . $keyword . '%')
            ->published()
            ->get();
        foreach ($data as $dataItem) {
            $value = [
                'url' => route('frontend.membership.merchandise'),
                'title' => $dataItem->title,
                'content' => $dataItem->title,
                'type' => 'Merchandise'
            ];
            $result[] = $value;
        }

        return collect($result)->toArray();
    }
}
