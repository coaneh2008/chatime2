<?php

namespace App\Model;

use App\Model\Extension\OrderableTrait;

class ThematicPromotionAttachment extends BaseModel
{
    use OrderableTrait;

    protected $table = 'thematic_promotion_attachments';

    protected $fillable = [
        'product_id',
        'order',
        'published'
    ];

    protected $attachable = [
        'image' => [
            'thumb' => [
                'huge' => '178x179'
            ]
        ]
    ];

    public function thematic_promotion()
    {
        return $this->belongsTo(\App\Model\ThematicPromotion::class, 'thematic_promotion_id');
    }

    public function product()
    {
        return $this->belongsTo(\App\Model\Product::class, 'product_id');
    }
}
