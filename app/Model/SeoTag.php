<?php

namespace App\Model;

use App\Model\Menu;
use Suitmedia\Cacheable\Contracts\CacheableModel;
use Suitmedia\Cacheable\Traits\Model\CacheableTrait;

class SeoTag extends BaseModel implements CacheableModel
{
    use CacheableTrait;
    
    const TYPE_STATIC_PAGE = 0;
    
    protected $table = 'seo_tags';

    public $timestamps = true;

    protected $fillable = [
        'page_type',
        'content_id',
        'seo_title',
        'seo_description',
        'seo_slug',
        'opengraph_title',
        'opengraph_description',
        'twitter_title',
        'twitter_description',
        'meta_image'
    ];

    public function getPageTypeName()
    {
        return 'STATIC PAGE';
    }

    public function setPageType()
    {
        return static::TYPE_STATIC_PAGE;
    }

    public function getPageName($pageId)
    {
        $page = Menu::where('id', $pageId)->get()->pluck('translate_title');
        return $page[0];
    }

    public function getSeoTagData()
    {
        $seoData = [
            'metaTitle' => $this->seo_title,
            'metaDescription' => $this->seo_description,
            'openGraphTitle' => $this->opengraph_title ?: $this->seo_title,
            'openGraphDescription' => $this->opengraph_description ?: $this->seo_description,
            'twitterTitle' => $this->twitter_title ?: $this->seo_title,
            'twitterDescription' => $this->twitter_description ?: $this->seo_description,
        ];

        if ($this->meta_image) {
            $seoData['metaImage'] = asset($this->meta_image);
        }

        return $seoData;
    }
}
