<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\SeoTagsTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Suitmedia\Cacheable\Contracts\CacheableModel;
use Suitmedia\Cacheable\Traits\Model\CacheableTrait;

class FeaturedBrand extends BaseModel implements SluggableInterface, CacheableModel
{
    use AttachableTrait;
    use CacheableTrait;
    use OrderableTrait;
    use PublishableTrait;
    use SeoTagsTrait;
    use SluggableTrait;
    use SoftDeletes;

    const SEO_TAG_TYPE = 3;

    protected $table = 'featured_brands';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $urlKey = 'slug';

    protected $attachmentData = [];

    protected $fillable = [
        'title',
        'content',
        'thumb_image',
        'banner_image',
        'published',
        'slug',
        'order',
        'page_type',
        'seo_slug',
        'seo_title',
        'seo_description',
        'opengraph_title',
        'opengraph_description',
        'twitter_title',
        'twitter_description',
        'meta_image',
        'is_highlighted',
        'is_big',
        'description',
        'color',
        'color2',
        'category_id'
    ];

    protected $dates = [
        'deleted_at',
        'published_at'
    ];

    protected $searchField = [
        'title'
    ];

    protected $attachable = [
        'thumb_image' => [
            'thumb' => [
                'small' => '100x100',
                'full_height_small' => '213x87',
            ]
        ],
        'banner_image' => [
            'thumb' => [
                'small' => '100x100',
                'huge' => '1105x487'
            ]
        ]
    ];

    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            $model->saveAttachmentData();
        });
    }

    public function getPageTypeName()
    {
        return ucwords(strtolower(str_replace('_', ' ', $this->table))) . ' Page';
    }

    public function setPageType()
    {
        return static::SEO_TAG_TYPE;
    }

    public function search($keyword)
    {
        $result = [];
        $data = $this->where('title', 'like', '%' . $keyword . '%')
            ->orWhere('content', 'like', '%' . $keyword . '%')
            ->published()
            ->get();
        foreach ($data as $dataItem) {
            $param = [
                'slug' => $dataItem->slug
            ];
            $value = [
                'url' => route('frontend.brand.show', $param),
                'title' => $dataItem->title,
                'content' => $dataItem->content,
                'type' => 'Brochure'
            ];
            $result[] = $value;
        }

        return collect($result)->unique('url')->toArray();
    }

    public function setAttachment($productId, $order)
    {
        if (empty($attachment)) {
            $attachment = $this->attachments()->getRelated();
        }
        $attachment->product_id = $productId;
        $attachment->order = $order;
        $this->attachmentData[$productId] = $attachment;
        return $attachment;
    }

    protected function saveAttachmentData()
    {
        foreach ($this->attachmentData as $attachment) {
            $attachment->featured_brands()->associate($this);
            $attachment->save();
        }
    }

    
    public function getProductCategory()
    {
        return ProductCategory::asc()->published()->get()->pluck('title', 'id');
    }

    public function newProductCategory()
    {
        return $this->belongsTo(\App\Model\ProductCategory::class, 'category_id');
    }


    public function attachments()
    {
        return $this->hasMany(\App\Model\FeaturedBrandAttachment::class, 'featured_brands_id');
    }

    public static function getFeaturedBrandAttachments($brandId)
    {
        return FeaturedBrandAttachment::where('featured_brands_id', $brandId)
            ->take(4)
            ->asc()
            ->get();
    }
}
