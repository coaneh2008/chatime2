<?php

namespace App\Model;

use App\Model\Extension\OrderableTrait;

class InspirationArticleAttachment extends BaseModel
{
    use OrderableTrait;

    protected $table = 'inspiration_article_attachments';

    protected $fillable = [
        'product_id',
        'order',
        'published'
    ];

    protected $attachable = [
        'image' => [
            'thumb' => [
                'huge' => '178x179'
            ]
        ]
    ];

    public function inspirationArticle()
    {
        return $this->belongsTo(\App\Model\inspirationArticle::class, 'inspiration_article_id');
    }

    public function product()
    {
        return $this->belongsTo(\App\Model\Product::class, 'product_id');
    }
}
