<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;

class InstagramPost extends BaseModel
{
    use AttachableTrait;

    const TYPE_IMAGE = 0;
    const TYPE_VIDEO = 1;

    protected $table = 'instagram_posts';

    public $timestamps = true;

    protected $fillable = [
        'code',
        'image',
        'video',
        'caption',
        'location',
        'type',
        'url',
        'total_like',
        'total_comment'
    ];

    protected $searchField = [
        'caption'
    ];

    protected $attachable = [
        'image' => [
            'thumb' => [
                'small' => '100x100',
                'medium' => '150x150'
            ]
        ],
        'video' => []
    ];

    public static function types()
    {
        return [
            static::TYPE_IMAGE => 'IMAGE',
            static::TYPE_VIDEO => 'VIDEO'
        ];
    }

    public function getTypeName()
    {
        if (!in_array($this->type, array_keys(static::types()))) {
            throw new \Exception('Type [' . $this->type . '] is not defined');
        }

        return static::types()[$this->type];
    }
}
