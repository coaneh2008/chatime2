<?php

namespace App\Model;

class CustomerPoint extends BaseModel
{
    protected $table = 'customer_points';

    protected $fillable = [
        'user_id',
        'total_point',
        'expiry_point',
        'expiry_date'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Model\User::class, 'user_id');
    }
}
