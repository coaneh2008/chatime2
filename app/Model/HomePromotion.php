<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Suitmedia\Cacheable\Contracts\CacheableModel;
use Suitmedia\Cacheable\Traits\Model\CacheableTrait;

class HomePromotion extends BaseModel implements SluggableInterface, CacheableModel
{
    use AttachableTrait;
    use CacheableTrait;
    use OrderableTrait;
    use PublishableTrait;
    use SluggableTrait;
    use SoftDeletes;

    const TYPE_BIG_IMAGE = 0;
    const TYPE_SMALL_IMAGE = 1;

    protected $table = 'home_promotions';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $urlKey = 'slug';

    public $timestamps = true;

    protected $attachmentData = [];

    protected $fillable = [
        'title',
        'thumb_image',
        'banner_image',
        'url',
        'type',
        'content',
        'slug',
        'order',
        'start_date',
        'end_date',
        'published',
        'kode_promo',
        'category_id'
    ];

    protected $dates = [
        'deleted_at',
        'published_at'
    ];

    protected $searchField = [
        'title',
        'url'
    ];

    protected $attachable = [
        'thumb_image' => [
            'thumb' => [
                'small' => '100x100',
                'medium' => '402x249',
                'huge' => '668x558'
            ]
        ],
        'banner_image' => [
            'thumb' => [
                'small' => '100x100',
                'huge' => '1105x487'
            ]
        ],
    ];

    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            $model->saveAttachmentData();
        });
    }

    public static function typeList()
    {
        return [
            static::TYPE_BIG_IMAGE => 'Big Image',
            static::TYPE_SMALL_IMAGE => 'Small Image'
        ];
    }

    public function getPromotionCategory()
    {
        return PromotionCategory::asc()->published()->get()->pluck('title', 'id');
    }

    public function getTypeName()
    {
        if (!in_array($this->type, array_keys(static::typeList()))) {
            throw new \Exception('Type [' . $this->type . '] is not defined');
        }

        return static::typeList()[$this->type];
    }

    public function newProductCategory()
    {
        return $this->belongsTo(\App\Model\PromotionCategory::class, 'category_id');
    }

    public function search($keyword)
    {
        $now = \Carbon\Carbon::now();
        $result = [];
        $data = $this->where('title', 'like', '%' . $keyword . '%')
            ->orWhere('content', 'like', '%' . $keyword . '%')
            ->where('start_date', '<=', $now)
            ->where('end_date', '>=', $now)
            ->published()
            ->get();
        foreach ($data as $dataItem) {
            $param = [
                'slug' => $dataItem->slug
            ];
            $value = [
                'url' => route('frontend.home-promotion.show', $param),
                'title' => $dataItem->title,
                'content' => $dataItem->content,
                'type' => 'Home Promotion'
            ];
            $result[] = $value;
        }

        return collect($result)->unique('url')->toArray();
    }

    public function setAttachment($productId, $order)
    {
        if (empty($attachment)) {
            $attachment = $this->attachments()->getRelated();
        }
        $attachment->product_id = $productId;
        $attachment->order = $order;
        $this->attachmentData[$productId] = $attachment;
        return $attachment;
    }

    protected function saveAttachmentData()
    {
        foreach ($this->attachmentData as $attachment) {
            $attachment->home_promotion()->associate($this);
            $attachment->save();
        }
    }

    public function attachments()
    {
        return $this->hasMany(\App\Model\HomePromotionAttachment::class, 'home_promotion_id');
    }

    public static function getHomePromotionAttachments($id)
    {
        return HomePromotionAttachment::where('home_promotion_id', $id)
            ->take(4)
            ->asc()
            ->get();
    }

    
}
