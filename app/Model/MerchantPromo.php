<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class MerchantPromo extends BaseModel implements SluggableInterface
{
    use AttachableTrait;
    use OrderableTrait;
    use PublishableTrait;
    use SluggableTrait;
    use SoftDeletes;

    protected $table = 'merchant_promos';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $urlKey = 'slug';

    protected $fillable = [
        'member_benefit_id',
        'merchant_category_id',
        'title',
        'merchant_name',
        'description',
        'content',
        'thumb_image',
        'banner_image',
        'published',
        'start_date',
        'end_date',
        'slug',
        'order',
        'is_highlighted',
    ];

    protected $dates = [
        'published_at',
        'start_date',
        'end_date',
        'deleted_at'
    ];

    protected $searchField = [
        'title',
        'description'
    ];

    protected $attachable = [
        'thumb_image' => [
            'thumb' => [
                'small' => '200x200'
            ]
        ],
        'banner_image' => [
            'thumb' => [
                'default' => '1100x500'
            ]
        ],

    ];

    public function benefit()
    {
        return $this->belongsTo(\App\Model\MemberBenefit::class, 'member_benefit_id');
    }

    public function category()
    {
        return $this->belongsTo(\App\Model\MerchantCategory::class, 'merchant_category_id');
    }

    public function locations()
    {
        return $this->belongsToMany(
            \App\Model\MerchantLocation::class,
            'merchant_promo_locations',
            'merchant_promo_id',
            'merchant_location_id'
        );
    }

    public function search($keyword)
    {
        $now = \Carbon\Carbon::now();
        $result = [];
        $data = $this->where('title', 'like', '%' . $keyword . '%')
            ->orWhere('merchant_name', 'like', '%' . $keyword . '%')
            ->orWhere('description', 'like', '%' . $keyword . '%')
            ->orWhere('content', 'like', '%' . $keyword . '%')
            ->where('start_date', '<=', $now)
            ->where('end_date', '>=', $now)
            ->published()
            ->get();
        foreach ($data as $dataItem) {
            $param = ['slug' => $dataItem->benefit->slug, 'detailSlug' => $dataItem->slug];
            $value = [
                'url' => route('frontend.membership.benefit-detail', $param),
                'title' => $dataItem->merchant_name . ' - ' . $dataItem->title,
                'content' => $dataItem->content,
                'type' => 'Member Benefit Merchant Promo'
            ];
            $result[] = $value;
        }

        return collect($result)->unique('url')->toArray();
    }
}
