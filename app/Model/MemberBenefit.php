<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberBenefit extends BaseModel implements SluggableInterface
{
    use AttachableTrait;
    use OrderableTrait;
    use PublishableTrait;
    use SluggableTrait;
    use SoftDeletes;

    const TYPE_FREE_CONTENT = 0;
    const TYPE_MEMBER_PROMO = 1;
    const TYPE_MERCHANT_PROMO = 2;
    const TYPE_NEWS_UPDATES = 3;

    protected $table = 'member_benefits';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $urlKey = 'slug';

    protected $fillable = [
        'type',
        'title',
        'description',
        'content',
        'thumb_image',
        'banner_image',
        'published',
        'slug',
        'order',
    ];

    protected $dates = [
        'deleted_at',
        'published_at'
    ];

    protected $searchField = [
        'title',
        'description'
    ];

    protected $attachable = [
        'thumb_image' => [
            'thumb' => [
                'small' => '120x120'
            ]
        ],
        'banner_image' => [
            'thumb' => [
                'default' => '1100x500'
            ]
        ],
    ];

    public static function typeList()
    {
        return [
            static::TYPE_FREE_CONTENT,
            static::TYPE_MEMBER_PROMO,
            static::TYPE_MERCHANT_PROMO,
            static::TYPE_NEWS_UPDATES
        ];
    }

    public static function typeListName()
    {
        return array_combine(static::typeList(), static::typeList());
    }

    public function memberPromos()
    {
        return $this->hasMany(\App\Model\MemberPromo::class, 'member_benefit_id');
    }

    public function merchantPromos()
    {
        return $this->hasMany(\App\Model\MerchantPromo::class, 'member_benefit_id');
    }

    public function newsUpdates()
    {
        return $this->hasMany(\App\Model\MemberNewsUpdate::class, 'member_benefit_id');
    }

    public function getMemberPromoData()
    {
        $data = "";
        foreach ($this->memberPromos as $promo) {
            $data .= '<b>' . $promo->title . "</b><br />" . $promo->start_date->format('d/m/Y') .
            " - " . $promo->end_date->format('d/m/Y') . "<br /><br />";
        }
        return $data;
    }

    public function getMerchantPromoData()
    {
        $data = "";
        foreach ($this->merchantPromos as $promo) {
            $data .= '<b>' . $promo->title . "</b><br />" . $promo->start_date->format('d/m/Y') .
            " - " . $promo->end_date->format('d/m/Y') . "<br /><br />";
        }
        return $data;
    }

    public function getNewsUpdatesData()
    {
        $data = "";
        foreach ($this->newsUpdates as $promo) {
            $data .= '<b>' . $promo->title . "</b><br />" . $promo->start_date->format('d/m/Y') .
            " - " . $promo->end_date->format('d/m/Y') . "<br /><br />";
        }
        return $data;
    }

    public function search($keyword)
    {
        $result = [];
        $data = $this->where('title', 'like', '%' . $keyword . '%')
            ->orWhere('description', 'like', '%' . $keyword . '%')
            ->orWhere('content', 'like', '%' . $keyword . '%')
            ->published()
            ->get();
        foreach ($data as $dataItem) {
            if ($dataItem->type == $this::TYPE_FREE_CONTENT) {
                $content = $dataItem->content;
                $url = route('frontend.membership.index');
            } else {
                $param = [
                    'slug' => $dataItem->slug
                ];
                $content = $dataItem->description;
                $url = route('frontend.membership.benefit-index', $param);
            }
            $value = [
                'url' => $url,
                'title' => $dataItem->title,
                'content' => $content,
                'type' => 'Member Benefit',
            ];
            $result[] = $value;
        }

        return collect($result)->unique('url')->toArray();
    }
}
