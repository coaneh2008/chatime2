<?php

namespace App\Model;

use App\Model\Extension\OrderableTrait;

class HomePromotionAttachment extends BaseModel
{
    use OrderableTrait;

    protected $table = 'home_promotion_attachments';

    protected $fillable = [
        'product_id',
        'order',
        'published'
    ];

    protected $attachable = [
        'image' => [
            'thumb' => [
                'huge' => '178x179'
            ]
        ]
    ];

    public function home_promotion()
    {
        return $this->belongsTo(\App\Model\HomePromotion::class, 'home_promotion_id');
    }

    public function product()
    {
        return $this->belongsTo(\App\Model\Product::class, 'product_id');
    }
}
