<?php

namespace App\Model;

use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductPromotion extends BaseModel
{
    use OrderableTrait;
    use PublishableTrait;
    use SoftDeletes;

    protected $table = 'product_promotions';

    protected $fillable = [
        'category_id',
        'product_id',
        'url_to_ace_online',
        'start_date',
        'end_date',
        'published',
        'order'
    ];

    protected $dates = [
        'deleted_at',
        'published_at'
    ];

    protected $searchField = [
        'product.url_to_ace_online',
        'product.title',
        'product.price',
        'productCategory.title'
    ];

    public function productCategory()
    {
        return $this->belongsTo(\App\Model\PromotionCategory::class, 'category_id');
    }

    public function product()
    {
        return $this->belongsTo(\App\Model\Product::class, 'product_id');
    }
}
