<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;

class Product extends BaseModel
{
    use AttachableTrait;

    const IN_STOCK = 1;
    const OUT_OF_STOCK = 0;
    
    protected $table = 'products';

    protected $fillable = [
        'brand_id',
        'url_to_ace_online',
        'title',
        'description',
        'currency',
        'price',
        'is_available',
        'image'
    ];

    protected $searchField = [
        'title'
    ];

    protected $attachable = [
        'image' => [
            'thumb' => [
                'small' => '100x100',
                'medium' => '403x354',
                'medium-2' => '146x162'
            ]
        ]
    ];

    public function brand()
    {
        return $this->belongsTo(\App\Model\FeaturedBrand::class, 'brand_id');
    }

    public static function availability()
    {
        return [
            static::IN_STOCK => 'INSTOCK',
            static::OUT_OF_STOCK => 'OUT OF STOCK'
        ];
    }

    public function getAvailability()
    {
        if (!in_array($this->is_available, array_keys(static::availability()))) {
            throw new \Exception('Type [' . $this->is_available . '] is not defined');
        }

        return static::availability()[$this->is_available];
    }
}
