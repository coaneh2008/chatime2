<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\SeoTagsTrait;
use App\Model\Extension\TaggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Suitmedia\Cacheable\Contracts\CacheableModel;
use Suitmedia\Cacheable\Traits\Model\CacheableTrait;

class InspirationArticle extends BaseModel implements SluggableInterface, CacheableModel
{
    use AttachableTrait;
    use CacheableTrait;
    use OrderableTrait;
    use PublishableTrait;
    use SeoTagsTrait;
    use SluggableTrait;
    use SoftDeletes;
    use TaggableTrait;

    const TYPE_BIG_IMAGE = 'Big';
    const TYPE_SMALL_IMAGE = 'Small';
    
    const TYPE_URL = 0;
    const TYPE_VIDEO = 1;

    const SEO_TAG_TYPE = 1;

    protected $table = 'inspiration_articles';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $urlKey = 'slug';

    public $timestamps = true;

    protected $attachmentData = [];

    protected $fillable = [
        'category_id',
        'author_id',
        'title',
        'thumb_image',
        'banner_image',
        'image_type',
        'description',
        'content',
        'is_highlighted',
        'slug',
        'order',
        'published',
        'viewer',
        'tags',
        'page_type',
        'seo_slug',
        'seo_title',
        'seo_description',
        'opengraph_title',
        'opengraph_description',
        'twitter_title',
        'twitter_description',
        'meta_image',
        'created_at',
        'updated_at',
        'published_date',
        'device',
        'is_slider',
        'url'
    ];

    protected $dates = [
        'deleted_at',
        'published_at'
    ];

    protected $searchField = [
        'title',
    ];

    protected $attachable = [
        'thumb_image' => [
            'thumb' => [
                'small' => '100x100',
                'medium' => '194x192',
                'huge' => '770x770'
            ]
        ],
        'banner_image' => [
            'thumb' => [
                'small' => '100x100',
                'small_square' => '214x214',
                'big_square' => '402x402'
            ]
        ],
    ];

    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            $model->saveAttachmentData();
        });
    }

    public function attachments()
    {
        return $this->hasMany(\App\Model\InspirationArticleAttachment::class, 'inspiration_article_id');
    }

    public function setAttachment($productId, $order)
    {
        if (empty($attachment)) {
            $attachment = $this->attachments()->getRelated();
        }
        $attachment->product_id = $productId;
        $attachment->order = $order;
        $this->attachmentData[$productId] = $attachment;
        return $attachment;
    }

    protected function saveAttachmentData()
    {
        foreach ($this->attachmentData as $attachment) {
            $attachment->inspirationArticle()->associate($this);
            $attachment->save();
        }
    }

    public function inspirationArticleCategory()
    {
        return $this->belongsTo(\App\Model\InspirationArticleCategory::class, 'category_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Model\User::class, 'author_id');
    }

    public static function typeList()
    {
        return [
            static::TYPE_BIG_IMAGE,
            static::TYPE_SMALL_IMAGE
        ];
    }

    public static function typeDevice()
    {
        return [
            static::TYPE_URL => 'Desktop',
            static::TYPE_VIDEO => 'Mobile'
        ];
    }

    public static function typeListName()
    {
        return array_combine(static::typeList(), static::typeList());
    }

    public function getPageTypeName()
    {
        return ucwords(strtolower(str_replace('_', ' ', $this->table))) . ' Page';
    }

    public function setPageType()
    {
        return static::SEO_TAG_TYPE;
    }

    public function getInspirationArticleByImageType($type)
    {
        return InspirationArticle::where('image_type', $type)
            ->where('is_highlighted', 1)
            ->take(2)
            ->asc()
            ->published()
            ->get();
    }

    public function getInspirationArticleByCategoryId($categoryId)
    {
        return InspirationArticle::where('category_id', $categoryId)
            ->asc()
            ->published()
            ->get();
    }

    public function getArchivedYear($categoryId = null)
    {
        if ($categoryId) {
            return InspirationArticle::select(DB::raw('YEAR(published_date) year'))
                ->where('category_id', $categoryId)
                ->published()
                ->groupBy('year')
                ->orderBy('year', 'desc')
                ->get();
        }
        return InspirationArticle::select(DB::raw('YEAR(published_date) year'))
            ->published()
            ->groupBy('year')
            ->orderBy('year', 'desc')
            ->get();
    }

    public static function getArchivedMonthByYear($year)
    {
        return InspirationArticle::select(DB::raw("DATE_FORMAT(published_date, '%M') month"))
            ->where(DB::raw('YEAR(published_date)'), $year)
            ->published()
            ->groupBy('month')
            ->orderBy('published_date', 'desc')
            ->get();
    }

    public function getByDateArchived($month, $year)
    {
        return InspirationArticle::where(DB::raw("DATE_FORMAT(published_date, '%M')"), $month)
            ->where(DB::raw('YEAR(published_date)'), $year);
    }

    public static function getInspirationArticleAttachments($articleId)
    {
        return InspirationArticleAttachment::where('inspiration_article_id', $articleId)
            ->take(4)
            ->asc()
            ->get();
    }

    public function search($keyword)
    {
        $result = [];
        $data = $this->where('title', 'like', '%' . $keyword . '%')
            ->orWhere('description', 'like', '%' . $keyword . '%')
            ->orWhere('content', 'like', '%' . $keyword . '%')
            ->get();
        foreach ($data as $dataItem) {
            $param = [
                'slug' => $dataItem->slug
            ];
            $value = [
                'url' => route('frontend.inspiration.show', $param),
                'title' => $dataItem->title,
                'content' => $dataItem->content,
                'type' => 'Article'
            ];
            $result[] = $value;
        }

        return collect($result)->unique('url')->toArray();
    }
}
