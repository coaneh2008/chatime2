<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Suitmedia\Cacheable\Contracts\CacheableModel;
use Suitmedia\Cacheable\Traits\Model\CacheableTrait;

class Banner extends BaseModel implements CacheableModel
{
    use AttachableTrait;
    use CacheableTrait;
    use OrderableTrait;
    use PublishableTrait;
    use SoftDeletes;

    const TYPE_URL = 0;
    const TYPE_VIDEO = 1;

    const TYPE_HOME = 'home';
    const TYPE_PROMO = 'promo';
    const TYPE_PRODUCT = 'product';
    
    protected $table = 'banners';

    protected $fillable = [
        'image',
        'video_url',
        'published',
        'order',
        'type',
        'judul',
        'deskripsi',
        'page',
        'device'
    ];

    protected $dates = [
        'published_at',
        'deleted_at'
    ];

    protected $searchField = [
        'video_url'
    ];

    protected $attachable = [
        'image' => [
            'thumb' => [
                'small' => '100x100',
                'huge' => '1260x630'
            ]
        ],
    ];

    public static function typeList()
    {
        return [
            static::TYPE_URL => 'URL',
            static::TYPE_VIDEO => 'Video'
        ];
    }

    public static function typeDevice()
    {
        return [
            static::TYPE_URL => 'Desktop',
            static::TYPE_VIDEO => 'Mobile'
        ];
    }

    public static function typePage()
    {
        return [
            static::TYPE_HOME => 'Home',
            static::TYPE_PROMO => 'Promo',
            static::TYPE_PRODUCT => 'Product'
        ];
    }

    public function getTypeName()
    {
        if (!in_array($this->type, array_keys(static::typeList()))) {
            throw new \Exception('Type [' . $this->type . '] is not defined');
        }

        return static::typeList()[$this->type];
    }

    public function getDeviceName()
    {
        if (!in_array($this->device, array_keys(static::typeDevice()))) {
            throw new \Exception('Device [' . $this->device . '] is not defined');
        }

        return static::typeDevice()[$this->device];
    }

}
