<?php

namespace App\Model;

class Testimony extends BaseModel
{

    protected $table = 'user_testimonies';

    protected $fillable = [
        'user_id',
        'event_id',
        'comment'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Model\User::class, 'user_id');
    }

    public function event()
    {
        return $this->belongsTo(\App\Model\Event::class, 'event_id');
    }
}
