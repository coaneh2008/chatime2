<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\SeoTagsTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brochure extends BaseModel implements SluggableInterface
{
    use AttachableTrait;
    use OrderableTrait;
    use PublishableTrait;
    use SeoTagsTrait;
    use SluggableTrait;
    use SoftDeletes;

    const SEO_TAG_TYPE = 4;

    protected $table = 'brochures';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $urlKey = 'slug';

    public $timestamps = true;

    protected $attachmentData = [];

    protected $attachmentOptions = [
        'image' => [
            'thumb' => [
                'small' => '100x100'
            ]
        ]
    ];

    protected $fillable = [
        'title',
        'start_date',
        'end_date',
        'thumb_image',
        'file',
        'published',
        'slug',
        'order',
        'page_type',
        'seo_slug',
        'seo_title',
        'seo_description',
        'opengraph_title',
        'opengraph_description',
        'twitter_title',
        'twitter_description',
        'meta_image'
    ];

    protected $dates = [
        'deleted_at',
        'published_at'
    ];

    protected $searchField = [
        'title'
    ];

    protected $attachable = [
        'thumb_image' => [
            'thumb' => [
                'small' => '100x100',
                'medium' => '445x630'
            ]
        ],
        'file' => []
    ];

    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            $model->saveAttachmentData();
        });
    }

    public function attachments()
    {
        return $this->hasMany(\App\Model\BrochureAttachment::class, 'brochure_id');
    }

    public function getPageTypeName()
    {
        return ucwords(strtolower(str_replace('_', ' ', $this->table))) . ' Page';
    }

    public function setPageType()
    {
        return static::SEO_TAG_TYPE;
    }

    public function setAttachment($image, $order)
    {
        if (empty($attachment)) {
            $attachment = $this->attachments()->getRelated();
        }
        $attachment->image = $image;
        $attachment->order = $order;
        $this->attachmentData[$image] = $attachment;
        return $attachment;
    }

    protected function saveAttachmentData()
    {
        foreach ($this->attachmentData as $attachment) {
            $attachment->brochure()->associate($this);
            $attachment->save();
        }
    }

    public static function getBrochureAttachments($brochureId)
    {
        return BrochureAttachment::where('brochure_id', $brochureId)
            ->asc()
            ->get();
    }

    public function search($keyword)
    {
        $result = [];
        $data = $this->where('title', 'like', '%' . $keyword . '%')
            ->published()
            ->get();
        foreach ($data as $dataItem) {
            $param = [
                'slug' => $dataItem->slug
            ];
            $value = [
                'url' => route('frontend.brochure.show', $param),
                'title' => $dataItem->title,
                'content' => $dataItem->title,
                'type' => 'Brochure'
            ];
            $result[] = $value;
        }

        return collect($result)->unique('url')->toArray();
    }
}
