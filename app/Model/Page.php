<?php
namespace App\Model;

use App\Model\Extension\PublishableTrait;
use App\Model\Extension\TaggableTrait;
use App\Model\Extension\TreeTrait;
use App\Model\Translation\TranslateModel;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Page extends TranslateModel implements SluggableInterface
{
    use PublishableTrait;
    use SluggableTrait;
    use TaggableTrait;
    use TreeTrait;

    protected $table = 'pages';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $urlKey = 'slug';

    protected $attachmentData = [];

    protected $attachmentOptions = [
        'banner' => [
            'thumb' => [
                'small' => '800x600',
                'huge' => '1366x768'
            ]
        ]
    ];

    protected $fillable = [
        'published',
        'tags',
        'slug',
        'url_prefix',
        'layout'
    ];

    protected $dates = [
        'published_at'
    ];

    protected $translateField = [
        'title',
        'description',
        'content'
    ];

    protected $searchField = [
        'title',
        'url_prefix',
    ];

    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            $model->saveAttachmentData();
        });
    }

    public function attachments()
    {
        return $this->hasMany(\App\Model\PageAttachment::class, 'page_id');
    }

    public function parentListById()
    {
        $query = $this->newQuery();
        if ($this->exists) {
            $query = $query->notThis(true);
        }
        return $query->lists('translate_title', 'id');
    }

    public function publishedListById()
    {
        return $this->published()->get()->lists('title', 'id');
    }

    public function getUrlAttribute()
    {
        $slash = '';
        if (strlen($this->url_prefix) !== 0 || !ends_with($this->url_prefix, '/')) {
            $slash = '/';
        }
        return url(sprintf('%s%s%s', $this->url_prefix, $slash, $this->slug));
    }

    protected function parentPrefixUrl()
    {
        $prefix = '';

        $parent = $this->parent;
        if ($parent !== null) {
            $prefix = $parent->parentPrefixUrl() . $parent->getUrlKey();
        }

        return $prefix . '/';
    }

    public function layoutList()
    {
        return config()->get('suitcms.layouts.pages');
    }

    public function setAttachment($name, $file)
    {
        if ($this->exists) {
            $attachment = $this->attachments()->where('name', $name)->first();
        }
        if (empty($attachment)) {
            $attachment = $this->attachments()->getRelated();
        }
        $attachment->name = $name;
        $attachment->uri = $file;
        $this->attachmentData[$name] = $attachment;
        return $attachment;
    }

    public function getAttachmentModel($name)
    {
        $attachments = $this->attachments->where('name', $name);
        if ($attachments->count() === 0) {
            return null;
        }
        $attachment = $attachments->values()->get(0);
        if (isset($this->attachmentOptions[$name])) {
            $attachment->setAttachmentOption($this->attachmentOptions[$name]);
        }
        return $attachment;
    }

    public function getThumbnail($name, $filter, $default = null)
    {
        $attachment = $this->getAttachmentModel($name);
        if ($attachment === null) {
            return $default;
        }

        return $attachment->getThumbnail('uri', $filter, $default);
    }

    public function getAttachment($name, $default = null)
    {
        $attachment = $this->getAttachmentModel($name);
        if ($attachment === null) {
            return $default;
        }

        return $attachment->getAttachment('uri');
    }

    public function deleteAttachment($name)
    {
        $attachment = $this->getAttachmentModel($name);
        if ($attachment === null) {
            return true;
        }
        return $attachment->delete();
    }

    public function clearAttachment()
    {
        if (!$this->exists) {
            return 0;
        }

        return $this->attachments()->delete();
    }

    protected function saveAttachmentData()
    {
        foreach ($this->attachmentData as $attachment) {
            $attachment->page()->associate($this);
            $attachment->save();
        }
    }

    public function search($keyword)
    {
        $result = [];
        $data = $this->where('title', 'like', '%' . $keyword . '%')
            ->orWhere('description', 'like', '%' . $keyword . '%')
            ->orWhere('content', 'like', '%' . $keyword . '%')
            ->published()
            ->get();
        foreach ($data as $dataItem) {
            $param = ['url' => $dataItem->slug];
            $value = [
                'url' => route('static.page', $param),
                'title' => $dataItem->translate_title,
                'content' => $dataItem->translate_content,
                'type' => 'Page'
            ];
            $result[] = $value;
        }

        return collect($result)->unique('url')->toArray();
    }
}
