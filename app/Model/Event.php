<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\ReferalCode;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends BaseModel implements SluggableInterface
{
    use AttachableTrait;
    use OrderableTrait;
    use PublishableTrait;
    use SluggableTrait;
    use SoftDeletes;

    const TYPE_ANNUAL_EVENT = 0;
    const TYPE_FREE_MEMBERSHIP = 1;
    const TYPE_MEMBER_GET_MEMBER = 2;
    const TYPE_TESTIMONY = 3;

    protected $table = 'events';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $urlKey = 'slug';

    protected $fillable = [
        'type',
        'title',
        'description',
        'content',
        'thumb_image',
        'banner_image',
        'total_participant',
        'published',
        'slug',
        'order',
        'start_date',
        'end_date',
        'prefix_referal_code'
    ];

    protected $dates = [
        'deleted_at',
        'published_at'
    ];

    protected $searchField = [
        'title'
    ];

    protected $attachable = [
        'thumb_image' => [
            'thumb' => [
                'small' => '100x100',
                'medium' => '250x200'
            ]
        ],
        'banner_image' => [
            'thumb' => [
                'small' => '100x100',
                'huge' => '800x275'
            ]
        ]
    ];

    public static function typeList()
    {
        return [
            static::TYPE_ANNUAL_EVENT,
            static::TYPE_FREE_MEMBERSHIP,
            static::TYPE_MEMBER_GET_MEMBER,
            static::TYPE_TESTIMONY
        ];
    }

    public static function typeListName()
    {
        return array_combine(static::typeList(), static::typeList());
    }

    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            if ($model->type = static::TYPE_MEMBER_GET_MEMBER) {
                $customers = \App\Model\User::where('group_type', 0)->get();
                foreach ($customers as $customer) {
                    if (!str_contains($customer->username, 'TAM')) {
                        $referalCode = $model->prefix_referal_code . sha1($customer->id);
                        $referalData = [
                            'event_id' => $model->id,
                            'user_id' => $customer->id,
                            'code' => substr($referalCode, 0, 7)
                        ];
                        ReferalCode::updateOrCreate([
                            'event_id' => $model->id,
                            'user_id' => $customer->id
                        ], $referalData);
                    }
                }
            }
        });
    }
}
