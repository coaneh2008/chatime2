<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\PublishableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promotion extends BaseModel
{
    use AttachableTrait;
    use PublishableTrait;
    use SoftDeletes;

    const TYPE_BTN_DOWNLOAD_APP = 0;
    const TYPE_BTN_CUSTOM = 1;

    protected $table = 'promotions';

    protected $fillable = [
        'category_id',
        'content',
        'start_date',
        'end_date',
        'file',
        'type',
        'btn_name',
        'url',
        'published'
    ];

    protected $dates = [
        'deleted_at',
        'published_at'
    ];

    protected $searchField = [
        'content'
    ];

    protected $attachable = [
        'file' => []
    ];

    public static function typeList()
    {
        return [
            static::TYPE_BTN_DOWNLOAD_APP => 'Btn Download App',
            static::TYPE_BTN_CUSTOM => 'Btn Custom'
        ];
    }

    public function getTypeName()
    {
        if (!in_array($this->type, array_keys(static::typeList()))) {
            throw new \Exception('Type [' . $this->type . '] is not defined');
        }

        return static::typeList()[$this->type];
    }

    public function productCategory()
    {
        return $this->belongsTo(\App\Model\PromotionCategory::class, 'category_id');
    }

    public function search($keyword)
    {
        $result = [];
        $data = $this->where('content', 'like', '%' . $keyword . '%')
            ->published()
            ->get();
        foreach ($data as $dataItem) {
            $value = [
                'url' => route('frontend.promotion.index'),
                'title' => 'Promotion',
                'content' => $dataItem->content,
                'type' => 'Promotion'
            ];
            $result[] = $value;
        }

        return collect($result)->toArray();
    }
}
