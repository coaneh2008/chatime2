<?php

namespace App\Model;

use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Suitmedia\Cacheable\Contracts\CacheableModel;
use Suitmedia\Cacheable\Traits\Model\CacheableTrait;

class FlashSticker extends BaseModel implements CacheableModel
{
    use CacheableTrait;
    use OrderableTrait;
    use PublishableTrait;
    use SoftDeletes;

    protected $table = 'flash_stickers';

    protected $fillable = [
        'color',
        'url',
        'content',
        'start_date',
        'end_date',
        'published',
        'order'
    ];

    protected $dates = [
        'published_at',
        'deleted_at'
    ];

    protected $searchField = [
        'url',
        'content',
        'start_date',
        'end_date',
        'order'
    ];
}
