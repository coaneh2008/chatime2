<?php

namespace App\Feeder\Instagram;

use App\Model\InstagramPost;
use App\Repositories\InstagramRepository;
use App\Scraper\Instagram\InstagramScraper;

class InstagramFeeder
{
    private $instagramScraper;
    private $instagramRepo;

    public function __construct(
        InstagramScraper $instagramScraper,
        InstagramRepository $instagramRepo
    ) {
        $this->instagramScraper = $instagramScraper;
        $this->instagramRepo = $instagramRepo;
    }

    public function run()
    {
        $instagramPost = $this->instagramScraper->getInstagramPost();
        if ($instagramPost) {
            $count = count($instagramPost->data);
            for ($i = 0; $i < $count; $i++) {
                $instagramPostData = [];
                $code = $instagramPost->data[$i]->id;
                $image = $instagramPost->data[$i]->images->standard_resolution->url;
                if ($instagramPost->data[$i]->caption) {
                    $caption = $instagramPost->data[$i]->caption->text;
                } else {
                    $caption = null;
                }
                if ($instagramPost->data[$i]->location) {
                    $location = (string) $instagramPost->data[$i]->location->name;
                } else {
                    $location = null;
                }
                $url = $instagramPost->data[$i]->link;
                $type = $instagramPost->data[$i]->type;
                if ($type == 'video') {
                    $type = 1;
                    $video = $instagramPost->data[$i]->videos->standard_resolution->url;
                } else {
                    $video = null;
                    $type = 0;
                }
                $totalLike = $instagramPost->data[$i]->likes->count;
                $totalComment = $instagramPost->data[$i]->comments->count;

                $instagramPostData = [
                    'code' => $code,
                    'image' => $image,
                    'caption' => $caption,
                    'location' => $location,
                    'url' => $url,
                    'type' => $type,
                    'video' => $video,
                    'total_like' => $totalLike,
                    'total_comment' => $totalComment
                ];
                
                $this->instagramRepo->storeInstagramPost($code, $instagramPostData);
            }
        }
    }
}
