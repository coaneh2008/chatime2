<?php

namespace App\Feeder\Api;

use App\Repositories\Api\CityApiRepository;

class CityFeeder
{
    public function run()
    {
        $cityApiRepo = new CityApiRepository();
        $response = $cityApiRepo->getMasterCity('');
        if ($response->is_ok) {
            $cityApiRepo->storeCity($response->rows);
        }
    }
}
