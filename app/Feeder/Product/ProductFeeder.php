<?php

namespace App\Feeder\Product;

use App\Model\Product;
use App\Repositories\ProductRepository;
use App\Scraper\Product\ProductScraper;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class ProductFeeder
{
    private $productRepo;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepo = $productRepo;
    }
    public function run()
    {
        $products = Product::get();
        foreach ($products as $product) {
            $crawler = new Crawler();
            $client = new Client();
            $url = $product->url_to_ace_online;
            $productScraper = new ProductScraper($crawler, $client);
            $productData = $productScraper->getProductData($url);
            $this->productRepo->storeProduct($url, $productData);
        }
    }
}
