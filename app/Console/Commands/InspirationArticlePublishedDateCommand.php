<?php

namespace App\Console\Commands;

use App\Model\InspirationArticle;
use Carbon\Carbon;
use Illuminate\Console\Command;

class InspirationArticlePublishedDateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inspiration-article:set-published-date';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set Publish Date of Inspiration Article';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $inspirations = $this->getInspirationArticle();
        foreach ($inspirations as $inspiration) {
            $inspiration->published_date = $inspiration->published_at
                                           ? $inspiration->published_at
                                           : $inspiration->created_at;
            $inspiration->save();
            \Log::info('publish date of article: ' . $inspiration->title . 'is set successfully');
        }
    }

    public function getInspirationArticle()
    {
        return InspirationArticle::whereNull('published_date')->get();
    }
}
