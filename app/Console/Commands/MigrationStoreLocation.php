<?php

namespace App\Console\Commands;

use App\Model\Region;
use App\Model\StoreLocation;
use Excel;
use Illuminate\Console\Command;

class MigrationStoreLocation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migration:store-location';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migration store & location';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::table('regions')->delete();
        /*
        protected $fillable = [
            'region_id',
            'type',
            'title',
            'latitude',
            'longitude',
            'address',
            'published',
            'order'
        ];
        */
        $path = storage_path('app/store-locations.xlsx');
        $result = Excel::selectSheetsByIndex(0)->load($path)->toArray();
        foreach ($result as $row) {
            $regionName = $row['city'];
            $region = Region::where('title', $regionName)->first();
            if (!$region) {
                $region = Region::create(['title' => $regionName, 'published' => 1])->first();
            }
            $store = new StoreLocation();
            $title = explode(" - ", $row['company_name']);
            $store->region_id = $region->getKey();
            $store->title = $title[1];
            $store->latitude = $row['latitude'];
            $store->longitude = $row['longitude'];
            $address = $row['address1'] . ' ' . $row['address2'] . ' ' . $row['address3'];
            $address .= ($row['phone1'] !== "" && $row['phone1'] !== null) ? (' Phone: ' . $row['phone1']) : '';
            $store->address = $address;
            $store->published = 1;
            //check type
            if ($title[0] == 'ACE HOME CENTER') {
                $type = StoreLocation::TYPE_ACE_HOME_CENTRE;
            } else {
                $type = StoreLocation::TYPE_ACE_HARDWARE;
            }
            $store->type = $type;
            $store->save();
        }
    }
}
