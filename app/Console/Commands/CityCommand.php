<?php

namespace App\Console\Commands;

use App\Feeder\Api\CityFeeder;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CityCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'city:get-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get city master data from ACE Hardware API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cityFeeder = app(CityFeeder::class);
        try {
            \Log::info('Start crawling city data from ACE Hardware API' . Carbon::now());
            $cityFeeder->run();
            $this->info('City master data has been successfully saved.');
            \Log::info('Finish getting city master data' . Carbon::now());
        } catch (RequestException $requestException) {
            \Log::warning($requestException->getMessage());
            $this->error($requestException->getMessage());
        } catch (\Exception $exception) {
            \Log::warning($exception->getMessage());
            $this->error($exception->getMessage());
        }
    }
}
