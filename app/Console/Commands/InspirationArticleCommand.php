<?php

namespace App\Console\Commands;

use App\Model\InspirationArticle;
use Carbon\Carbon;
use Illuminate\Console\Command;

class InspirationArticleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inspiration-article:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish Inspiration Article';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $inspirations = $this->getInspirationArticle();
        foreach ($inspirations as $inspiration) {
            $inspiration->published_at = Carbon::now();
            $inspiration->save();
            \Log::info('publish article: ' . $inspiration->title);
        }
    }

    public function getInspirationArticle()
    {
        $now = Carbon::now()->toDateString();

        return InspirationArticle::where('published_date', $now)
            ->whereNull('published_at')
            ->get();
    }
}
