<?php

namespace App\Console\Commands;

use App\Feeder\Product\ProductFeeder;
use Carbon\Carbon;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Console\Command;

class ProductCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:get-product-info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get product info data from Ace Online Website';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $productFeeder = app(ProductFeeder::class);
        try {
            \Log::info('Start crawling product info data from Ace Online Website ' . Carbon::now());
            $productFeeder->run();
            $this->info('Product info data has been successfully saved.');
            \Log::info('Finish crawling for get product info data' . Carbon::now());
        } catch (RequestException $requestException) {
            \Log::warning($requestException->getMessage());
            $this->error($requestException->getMessage());
        } catch (\Exception $exception) {
            \Log::warning($exception->getMessage());
            $this->error($exception->getMessage());
        }
    }
}
