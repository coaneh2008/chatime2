<?php

namespace App\Console\Commands;

use App\Model\InspirationArticle;
use App\Model\InspirationArticleCategory;
use App\Model\User;
use Carbon\Carbon;
use DB;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;

class MigrationFromWordpress extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migration:wordpress';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate from wordpress';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $contentTexts = DB::table('wp_posts')->where('post_type', 'post')
            ->where('post_status', 'publish')
            ->get();

        foreach ($contentTexts as $contentText) {
            $exist = InspirationArticle::where('title', 'like', '%' . $contentText->post_title . '%')->first();
            if (!$exist) {
                $category = InspirationArticleCategory::where('title', 'Other')->first();
                if (!$category) {
                    $category = new InspirationArticleCategory();
                    $category->title = 'Other';
                    $category->published = true;
                    $category->order = 8;
                    $category->save();
                }
                $authorId = User::where('group_type', User::SUPER_ADMIN)->first()->id;
                $createdAt = Carbon::parse($contentText->post_date)->format('Y-m-d H:i:s');
                $updatedAt = Carbon::parse($contentText->post_modified)->format('Y-m-d H:i:s');
                $article = new InspirationArticle();
                $article->category_id = $category->id;
                $article->author_id = $authorId;
                $article->title = $contentText->post_title;
                $article->description = $contentText->post_title;
                $content = str_replace(
                    'http://www.acehardware.co.id/tipsntrick/wp-content/uploads/',
                    '/files/wp-content/uploads/',
                    $contentText->post_content
                );
                $article->content = $content;
                $article->image_type = InspirationArticle::TYPE_SMALL_IMAGE;
                $image = 'http://venetafernridgechamber.com/wp-content/uploads/2017/12/1080_ace-hardware.png';
                $article->thumb_image = $image;
                $article->banner_image = $image;
                $article->seo_title = $contentText->post_title;
                $article->seo_description = $contentText->post_title;
                $article->created_at = $createdAt;
                $article->updated_at = $updatedAt;
                $article->published = true;
                $article->save();

                \App\Model\Redirection::updateOrCreate([
                    'old_path' => 'tipsntrick/' . $contentText->post_name,
                ], [
                    'old_path' => 'tipsntrick/' . $contentText->post_name,
                    'new_url' => 'inspirations/' . $article->slug
                ]);
                \Log::info('Imported -> ' . $contentText->post_title);
            } else {
                    \Log::info('Exist -> ' . $contentText->post_title);
            }
        }
    }
}
