<?php

namespace App\Console\Commands;

use App\Feeder\Instagram\InstagramFeeder;
use Carbon\Carbon;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Console\Command;

class InstagramCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instagram:get-post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get post form ace hardware instagram account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $instagramFeeder = app(InstagramFeeder::class);
        try {
            \Log::info('Start crawling from ace hardware instagram account ' . Carbon::now());
            $instagramFeeder->run();
            $this->info('instagram posting data has been successfully saved.');
            \Log::info('Finish crawling data from ace hardware instagram account ' . Carbon::now());
        } catch (RequestException $requestException) {
            \Log::warning($requestException->getMessage());
            $this->error($requestException->getMessage());
        } catch (\Exception $exception) {
            \Log::warning($exception->getMessage());
            $this->error($exception->getMessage());
        }
    }
}
