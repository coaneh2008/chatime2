<?php
namespace App\Console;

use App\Console\Commands\AdminCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        AdminCommand::class,
        'App\Console\Commands\CityCommand',
        'App\Console\Commands\InstagramCommand',
        'App\Console\Commands\ProductCommand',
        'App\Console\Commands\MigrationStoreLocation',
        'App\Console\Commands\TestEmailConfig',
        'App\Console\Commands\MigrationFromWordpress',
        'App\Console\Commands\InspirationArticleCommand',
        'App\Console\Commands\InspirationArticlePublishedDateCommand'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('instagram:get-post')
            ->withoutOverlapping()
            ->hourly();

        $schedule->command('product:get-product-info')
            ->withoutOverlapping()
            ->daily();
            
        $schedule->command('city:get-data')
            ->withoutOverlapping()
            ->daily();

        $schedule->command('inspiration-article:publish')
            ->withoutOverlapping()
            ->hourly();

        $schedule->command('backup:clean')->daily()->at('02:00')->when(function () {
            return env('APP_ENV') == 'production';
        });

        $schedule->command('backup:run --only-db')->daily()->at('03:00')->when(function () {
            return env('APP_ENV') == 'production';
        });
    }
}
