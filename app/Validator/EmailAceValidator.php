<?php

namespace App\Validator;

use App\Repositories\Api\CheckEmailAndCardApiRepository;
use App\Repositories\ReferalCodeRepository;

class EmailAceValidator
{
    /**
     * Validate that an email exist in server Ace Hardware
     *
     * @param  mixed   $value
     * @return bool
     */
    public function validateEmailAce($attribute, $value, $parameters)
    {
        $email = $value;
        $apiRepo = app(CheckEmailAndCardApiRepository::class);
        $message = '{"P_Cust_Mail":"' . $email . '", "P_Cust_Card_Ref":""}';
        $response = $apiRepo->checkEmailAndCard($message);
        if ($response) {
            if ($response->is_ok == 'true') {
                return true;
            } else {
                return false;
            }
        }
        return redirect()
                ->back()
                ->with(session()
                ->flash(NOTIF_DANGER, 'Sorry, something went wrong'));
    }

    /**
     * Validate that an ends of email not .co
     *
     * @param  mixed   $value
     * @return bool
     */
    public function validateEmailDotCo($attribute, $value, $parameters)
    {
        $email = $value;
        if (ends_with($email, '.co')) {
            return false;
        }
        return true;
    }

    /**
     * Validate that an attribute contains at least MIN_CHARACTER characters.
     *
     * @param  mixed   $value
     * @return bool
     */
    public function validateReferalAce($attribute, $value, $parameters)
    {
        $referalCode = $value;
        $referalRepo = app(ReferalCodeRepository::class);
        $upline = $referalRepo->isReferalCodeValid($referalCode);
        if ($upline) {
            $cardNo = $upline->username;
            $apiRepo = app(CheckEmailAndCardApiRepository::class);
            $message = '{"P_Cust_Mail":"", "P_Cust_Card_Ref":"' . $cardNo . '"}';
            $response = $apiRepo->checkEmailAndCard($message);
            if ($response) {
                $message = $response->message;
                if ($response->is_ok == 'true') {
                    return true;
                }
            }
        }
        return false;
    }
}
