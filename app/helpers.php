<?php
if (!function_exists('suitRouteName')) {

    /**
     * Generate suitcms route name
     *
     * @param string $name
     * @return string
     */
    function suitRouteName($name)
    {
        return \Config::get('suitcms.prefix_url') . '.' . $name;
    }
}

if (!function_exists('suitRoute')) {

    /**
     * Generate a Suitcms backend URL to a named route.
     *
     * @param  string  $route
     * @param  array   $parameters
     * @return string
     */
    function suitRoute($route, $parameters = [])
    {
        return route(suitRouteName($route), $parameters);
    }
}

if (!function_exists('suitLoginUrl')) {

    /**
     * Get login_url configuration value
     *
     * @return string
     */
    function suitLoginUrl()
    {
        return \Config::get('suitcms.login_url');
    }
}

if (!function_exists('suitLogoutUrl')) {

    /**
     * Get login_url configuration value
     *
     * @return string
     */
    function suitLogoutUrl()
    {
        return \Config::get('suitcms.logout_url');
    }
}

if (!function_exists('suitPath')) {

    /**
     * Generate Suitcms backend path (without hostname)
     *
     * @param string $path
     * @return string
     */
    function suitPath($path = null)
    {
        return \Config::get('suitcms.prefix_url') . '/' . $path;
    }
}

if (!function_exists('suitUrl')) {

    /**
     * Generate a suitcms backend url for the application.
     *
     * @param  string  $path
     * @param  mixed   $parameters
     * @param  bool    $secure
     * @return string
     */
    function suitUrl($path = null, $parameters = [], $secure = null)
    {
        return url(suitPath($path), $parameters, $secure);
    }
}

if (!function_exists('suitViewName')) {

    /**
     * Generate a suitcms view
     *
     * @param string $name
     * @return string
     */
    function suitViewName($name)
    {
        return 'admins.' . $name;
    }
}

if (!function_exists('suitTrans')) {

    /**
     * Translate the given message.
     *
     * @param  string  $transId
     * @param  array   $parameters
     * @param  string  $domain
     * @param  string  $locale
     * @return string
     */
    function suitTrans($transId, $parameters = [], $domain = 'messages', $locale = null)
    {
        return trans('suitcms.' . $transId, $parameters, $domain, $locale);
    }
}

if (!function_exists('suitLangs')) {
    function suitLangs()
    {
        return \Config::get('suitcms.lang');
    }
}

if (!function_exists('isAcceptLang')) {
    function isAcceptLang($lang)
    {
        return in_array($lang, array_keys(suitLangs()));
    }
}

if (!function_exists('addStringToArrayValue')) {
    function addStringToArray($key, $value, &$array, $check = false)
    {
        if ($value === null) {
            return null;
        }

        if ($check && isset($array[$key])) {
            return $array[$key];
        }

        $array[$key] = $value . (empty($array[$key])?'':' ' . $array[$key]);
        return $array[$key];
    }
}

if (!function_exists('uploadRelativePath')) {
    function uploadRelativePath($path = '')
    {
        $path = trim($path, DIRECTORY_SEPARATOR);
        return 'files' . ($path?DIRECTORY_SEPARATOR . $path:$path);
    }
}

if (! function_exists('stringJson')) {
    function stringJson($string, $num = 200)
    {
        return addcslashes(str_limit($string, $num), "\n\r\t\\");
    }
}

if (!function_exists('generateId')) {
    function generateId($name)
    {
        $key = preg_replace('/(\]\[|\[)/', '_', $name);
        $key = preg_replace('/\]/', '', $key);
        return $key;
    }
}
