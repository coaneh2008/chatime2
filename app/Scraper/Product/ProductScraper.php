<?php

namespace App\Scraper\Product;

use App\Model\FeaturedBrand;
use App\Model\Product;
use App\Scraper\BaseScraper;
use Carbon\Carbon;

class ProductScraper extends BaseScraper
{
    public function __construct($crawler, $client)
    {
        parent::__construct($crawler, $client);
    }
    public function getProductData($url)
    {
        $this->loadCrawlerContent($url);
        return [
            'url_to_ace_online' => $url,
            'brand_id' => $this->getbrandId(),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'currency' => $this->getCurrency(),
            'price' => $this->getPrice(),
            'is_available' => $this->getIsAvailableValue(),
            'image' => $this->getImage()
        ];
    }

    public function getTitle()
    {
        if ($title = $this->crawler
                ->filterXpath("//meta[@name='title']")
                ->extract(['content'])) {
            return str_replace(["Jual ", "| Ruparupa", "| Ace Online"], "", $title[0]);
        } else {
            return null;
        }
    }

    public function getDescription()
    {
        if ($desc = $this->crawler
                ->filterXpath("//meta[@name='description']")
                ->extract(['content'])) {
            return $desc[0];
        } else {
            return null;
        }
    }
    
    public function getBrand()
    {
        if ($brand = $this->crawler
                ->filterXpath("//meta[@property='product:brand']")
                ->extract(['content'])) {
            return $brand[0];
        } else {
            return null;
        }
    }

    public function getCurrency()
    {
        if ($currency = $this->crawler
                ->filterXpath("//meta[@property='product:price:currency']")
                ->extract(['content'])) {
            return $currency[0];
        } else {
            return null;
        }
    }

    public function getPrice()
    {
        if ($price = $this->crawler
                ->filterXpath("//meta[@property='product:price:amount']")
                ->extract(['content'])) {
            return $price[0];
        } else {
            return null;
        }
    }

    public function getAvailability()
    {
        if ($availability = $this->crawler
                ->filterXpath("//meta[@property='product:availability']")
                ->extract(['content'])) {
            return $availability[0];
        } else {
            return null;
        }
    }

    public function getImage()
    {
        if ($image = $this->crawler
                ->filterXpath("//meta[@itemprop='image']")
                ->extract(['content'])) {
            return $image[0];
        } else {
            return null;
        }
    }

    public function getIsAvailableValue()
    {
        $availability = strtoupper($this->getAvailability());

        if (array_search($availability, Product::availability())) {
            return Product::IN_STOCK;
        } else {
            return Product::OUT_OF_STOCK;
        }
    }

    public function getBrandId()
    {
        $brand = FeaturedBrand::where('title', strtolower($this->getBrand()))->first();
        if (!empty($brand)) {
            return $brand->id;
        } else {
            return null;
        }
    }
}
