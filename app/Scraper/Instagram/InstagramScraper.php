<?php

namespace App\Scraper\Instagram;

class InstagramScraper
{
    public function getInstagramPost()
    {
        if (env('INSTAGRAM_USERNAME', '') && env('INSTAGRAM_ACCESS_TOKEN', '')) {
            $client = new \GuzzleHttp\Client;
            $url = sprintf(
                'https://api.instagram.com/v1/users/self/media/recent?q=%s&access_token=%s',
                env('INSTAGRAM_USERNAME', ''),
                env('INSTAGRAM_ACCESS_TOKEN', '')
            );
            $request = $client->get($url);
            return json_decode((string) $request->getBody());
        } else {
            return 0;
        }
    }
}
