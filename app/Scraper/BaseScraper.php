<?php

namespace App\Scraper;

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class BaseScraper
{
    /**
     * GuzzleHttp Client
     * @var GuzzleHttp\Client
     */
    protected $client;

    /**
     * Crawler
     * @var \Symfony\Component\DomCrawler\Crawler
     */
    protected $crawler;

    /**
     * Construct Object
     * @param \Symfony\Component\DomCrawler\Crawler $crawler []
     * @param \GuzzleHttp\Client $client []
     */
    protected function __construct(Crawler $crawler, Client $client)
    {
        $this->crawler = $crawler;
        $this->client = $client;
    }

    /**
     * Load Crawler content
     * @param string $content []
     * @return  void
     */
    protected function loadCrawlerContent($content)
    {
        $maxTrying = 5;
        $isSuccess = false;
        $this->crawler->clear();
        while ($maxTrying-- && !$isSuccess) {
            try {
                if (substr($content, 0, 4) === "http") {
                    $response = $this->client->request("GET", $content);
                    $this->crawler->addHtmlContent($response->getBody()->getContents());
                } else {
                    $this->crawler->addHtmlContent($content);
                }
                $isSuccess = true;
            } catch (\Exception $e) {
                $latestException = $e;
                $this->crawler->clear();
            }
        }
        if (!$isSuccess) {
            \Log::warning("[$content] {$latestException->getMessage()}");
            // throw new \Exception("[$content] {$latestException->getMessage()}", 1);
            // kirim email (connection time out 5x)
            return null;
        }
    }

    /**
     * post request
     * @param string $content []
     * @param array $arrayParam []
     * @return  void
     */
    protected function postRequest($content, $arrayParam)
    {
        $maxTrying = 5;
        $isSuccess = false;
        $this->crawler->clear();
        while ($maxTrying-- && !$isSuccess) {
            try {
                if (substr($content, 0, 4) === "http") {
                    $response = $this->client->request("POST", $content, $arrayParam);
                    $this->crawler->addHtmlContent($response->getBody()->getContents());
                } else {
                    $this->crawler->addHtmlContent($content);
                }
                $isSuccess = true;
            } catch (\Exception $e) {
                $latestException = $e;
                $this->crawler->clear();
            }
        }
        if (!$isSuccess) {
            \Log::warning("[$content] {$latestException->getMessage()}");
            // kirim email (connection time out 5x)
        }
    }
}
