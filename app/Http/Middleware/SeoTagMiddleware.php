<?php

namespace App\Http\Middleware;

use App\Repositories\SeoTagRepository;
use Closure;

class SeoTagMiddleware
{
    protected $seoTagRepo;

    public function __construct()
    {
        $this->seoTagRepo = \Cacheable::build(SeoTagRepository::class);
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->slug == null) {
            $slug = $request->path();
            $seoTag = $this->seoTagRepo->getSeoTag($slug);
            if ($seoTag) {
                view()->share($seoTag->getSeoTagData());
            }
        }
        return $next($request);
    }
}
