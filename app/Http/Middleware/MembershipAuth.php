<?php

namespace App\Http\Middleware;

use App\Repositories\LeaderBoardRepository;
use Closure;

class MembershipAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($user = \Auth::user()) {
            if (session()->has('NOTIF_SESSION_INVALID_TOKEN')) {
                $message = 'Session Anda telah berakhir dikarenakan login di device lain, silahkan login kembali';
                session()->flush();
                \Auth::logout();
                return redirect()->route('frontend.membership.login')
                    ->with(session()
                    ->flash(NOTIF_DANGER, $message));
            } else {
                if ($user->isUser()) {
                    $leaderboardRepo = new LeaderBoardRepository();
                    view()->share('isExistLeaderBoard', $leaderboardRepo->isExistLeaderBoardData());
                    return $next($request);
                }
            }
        }
        if ($request->path() == "membership/pay-via-ace-online") {
            $message = "Silahkan login dengan TAM yang sudah dikirimkan ke email Anda";
        } else {
            $message = 'Silahkan login kembali';
        }
        session()->put('REDIRECT_TO', $request->url());
        return redirect()->route('frontend.membership.login')
            ->with(session()
            ->flash(NOTIF_DANGER, $message));
    }
}
