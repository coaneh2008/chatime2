<?php

namespace App\Http\ViewComposers;

use App\Repositories\ProductCategoryRepository;
use Illuminate\Contracts\View\View;

class ProductCategoryComposer
{
    protected $productCatRepo;

    public function __construct()
    {
        $this->productCatRepo = \Cacheable::build(ProductCategoryRepository::class);
    }

    public function compose(View $view)
    {
        $categoryRepo = $this->productCatRepo;
        $productCategories = $categoryRepo->getMainCategoryList();
        $view->with(compact('productCategories', $productCategories))
            ->with(compact('categoryRepo', $categoryRepo));
    }
}
