<?php

namespace App\Http\ViewComposers;

use App\Repositories\FlashStickerRepository;
use Illuminate\Contracts\View\View;

class FlashStickerComposer
{
    protected $stickerRepo;

    public function __construct()
    {
        $this->stickerRepo = \Cacheable::build(FlashStickerRepository::class);
    }

    public function compose(View $view)
    {
        $stickers = $this->stickerRepo->getStickerList();
        $view->with(compact('stickers', $stickers));
    }
}
