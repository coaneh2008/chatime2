<?php

namespace App\Http\ViewComposers;

use App\Repositories\CustomerPointRepository;
use Auth;
use Illuminate\Contracts\View\View;

class MembershipSidebarComposer
{
    protected $pointRepo;

    public function __construct(CustomerPointRepository $pointRepo)
    {
        $this->pointRepo = $pointRepo;
    }

    public function compose(View $view)
    {
        $user = Auth::user();
        $point = $this->pointRepo->getCustomerPointByUserId($user->id);
        $renewalStatus = $user->is_renewal;
        $isFreeMembership = ($user->administration_type == 2 && str_contains($user->username, 'TAM')) ? true : false;
        $isTempAccount = str_contains($user->username, 'TAM');

        if ($isFreeMembership || $isTempAccount) {
            $renewalBtnIsHide = true;
        } else {
            $renewalBtnIsHide = false;
        }

        $view->with(compact('user', $user))
            ->with(compact('point', $point))
            ->with(compact('renewalBtnIsHide', $renewalBtnIsHide))
            ->with(compact('renewalStatus', $renewalStatus));
    }
}
