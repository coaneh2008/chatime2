<?php

namespace App\Http\ViewComposers;

use App\Repositories\MenuRepository;
use Illuminate\Contracts\View\View;

class MenuComposer
{
    protected $menuRepo;

    public function __construct()
    {
        $this->menuRepo = \Cacheable::build(MenuRepository::class);
    }

    public function compose(View $view)
    {
        $mainMenus = $this->menuRepo->getMainMenu();
        $secondaryMenus = $this->menuRepo->getSecondaryMenu();
        $footerMenus = $this->menuRepo->getFooterMenu();
        $view->with(compact('mainMenus', $mainMenus))
            ->with(compact('secondaryMenus', $secondaryMenus))
            ->with(compact('footerMenus', $footerMenus));
    }
}
