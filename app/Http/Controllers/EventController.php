<?php

namespace App\Http\Controllers;

use App\Repositories\EventParticipantRepository;
use App\Repositories\EventRepository;
use App\Repositories\ReferalCodeRepository;
use App\Repositories\TestimonyRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\UserRepository;
use Auth;
use Illuminate\Http\Request;

class EventController extends Controller
{
    protected $eventRepo;
    protected $participantRepo;
    protected $referalRepo;
    protected $testimonyRepo;
    protected $transactionRepo;
    protected $userRepo;

    public function __construct(
        EventRepository $eventRepo,
        EventParticipantRepository $participantRepo,
        ReferalCodeRepository $referalRepo,
        TestimonyRepository $testimonyRepo,
        TransactionRepository $transactionRepo,
        UserRepository $userRepo
    ) {
        $this->eventRepo = $eventRepo;
        $this->participantRepo = $participantRepo;
        $this->referalRepo = $referalRepo;
        $this->testimonyRepo = $testimonyRepo;
        $this->transactionRepo = $transactionRepo;
        $this->userRepo = $userRepo;
    }

    public function show($slug)
    {
        $event = $this->eventRepo->getEventBySlug($slug);
        return view('events.show', [
            'event' => $event
        ]);
    }

    public function addEventResponse(Request $request)
    {
        $event = $this->eventRepo->getEventBySlug($request->slug);
        $type = $event->type;
        $user = Auth::user();
        if ($type == \App\Model\Event::TYPE_ANNUAL_EVENT) {
            $this->addEventParticipant($event, $user);
            return redirect()->back();
        } elseif ($type == \App\Model\Event::TYPE_FREE_MEMBERSHIP) {
            return redirect()->route('frontend.events.show-free-membership', $event->slug);
        } elseif ($type == \App\Model\Event::TYPE_MEMBER_GET_MEMBER) {
            return redirect()->route('frontend.events.show-member-get-member', $event->slug);
        } else {
            return redirect()->route('frontend.events.show-testimony', $event->slug);
        }
    }

    public function addFollowedEventResponse(Request $request)
    {
        $event = $this->eventRepo->getEventBySlug($request->slug);
        $type = $event->type;
        if ($type == \App\Model\Event::TYPE_FREE_MEMBERSHIP) {
            return redirect()->route('frontend.followed-events.show-free-membership', $event->slug);
        } elseif ($type == \App\Model\Event::TYPE_MEMBER_GET_MEMBER) {
            return redirect()->route('frontend.followed-events.show-member-get-member', $event->slug);
        }
    }

    public function addEventParticipant($event, $user)
    {
        $eventId = $event->id;
        $userId = $user->id;
        
        if ($participant = $this->participantRepo->getParticipantByUserId($eventId, $userId)) {
            if ($participant->status == 1) {
                session()->flash(
                    NOTIF_WARNING,
                    \Setting::get('join-event-already-registered')
                );
            } else {
                if (count($this->participantRepo->getParticipantByEventId($eventId)) >= $event->total_participant) {
                    session()->flash(
                        NOTIF_DANGER,
                        \Setting::get('join-event-reach-maximum-quota')
                    );
                } else {
                    $this->sendEmailEventConfirmation($event, $user);
                    session()->flash(
                        NOTIF_WARNING,
                        \Setting::get('join-event-email-sent')
                    );
                }
            }
        } else {
            if (count($this->participantRepo->getParticipantByEventId($eventId)) >= $event->total_participant) {
                session()->flash(
                    NOTIF_DANGER,
                    \Setting::get('join-event-reach-maximum-quota')
                );
            } else {
                if ($this->participantRepo->storeParticipant($eventId, $userId, 0)) {
                    session()->flash(
                        NOTIF_SUCCESS,
                        \Setting::get('join-event-email-sent')
                    );
                }
                $this->sendEmailEventConfirmation($event, $user);

                return redirect()->route('frontend.followed-events.index');
            }
        }
    }

    public function sendEmailEventConfirmation($event, $user)
    {
        \Mail::queue(
            'events.send-email-confirmation',
            [
                'event' => $event,
                'user' => $user
            ],
            function ($message) use ($event, $user) {
                $message->to($user->email);
                $message->subject('Event Confirmation ' . \Setting::get('site-name'));
            }
        );
    }

    public function confirmEventParticipation(Request $request)
    {
        $userLogin = Auth::user();
        $email = $request->email;
        $slug = $request->slug;
        $user = $this->userRepo->getUserByEmail($email);
        $event = $this->eventRepo->getEventBySlug($slug);
        $currentParticipant = count($this->participantRepo->getParticipantByEventId($event->id));
        if ($user && $event) {
            if ($email == $userLogin->email) {
                $participant = $this->participantRepo->getParticipantByUserId($event->id, $userLogin->id);
                if ($participant->status == 1) {
                    session()->flash(
                        NOTIF_WARNING,
                        \Setting::get('join-event-already-registered')
                    );
                    return redirect()->route('frontend.followed-events.index');
                } else {
                    if ($currentParticipant >= $event->total_participant) {
                        session()->flash(
                            NOTIF_DANGER,
                            \Setting::get('join-event-reach-maximum-quota')
                        );
                        return redirect()->route('frontend.events.show', $slug);
                    } else {
                        if ($this->participantRepo->storeParticipant($event->id, $user->id, 1)) {
                            session()->flash(
                                NOTIF_SUCCESS,
                                \Setting::get('join-event-succesful')
                            );
                            return redirect()->route('frontend.followed-events.index');
                        }
                    }
                }
            } else {
                session()->flash(
                    NOTIF_DANGER,
                    \Setting::get('join-event-invalid-link')
                );
                return redirect()->route('frontend.events.show', $slug);
            }
        }
    }

    public function showTestimony($slug)
    {
        $event = $this->eventRepo->getEventBySlug($slug);
        $eventId = $event->id;
        $userId = Auth::user()->id;
        
        if ($this->testimonyRepo->getTestimonyByUserId($eventId, $userId)) {
            session()->flash(
                NOTIF_WARNING,
                \Setting::get('already-submit-testimony')
            );
            return redirect()->back();
        } else {
            return view('events.testimony', [
                'event' => $this->eventRepo->getEventBySlug($slug)
            ]);
        }
    }

    public function addTestimony(Request $request)
    {
        $userId = Auth::user()->id;
        $eventId = $request->eventId;
        $comment = $request->comment;
        
        if ($this->testimonyRepo->storeTestimony($eventId, $userId, $comment)) {
            // $this->sendTestimonyReply($testimony);
            session()->flash(
                NOTIF_SUCCESS,
                \Setting::get('submit-testimony-succesful')
            );
        }
        return redirect()->route('frontend.followed-events.index');
    }

    public function sendTestimonyReply($testimony)
    {
        \Mail::queue(
            'events.send-testimony-email',
            [
                'testimony' => $testimony
            ],
            function ($message) use ($testimony) {
                $message->to($testimony->user->email);
                $message->subject('Testimony for ' . \Setting::get('site-name'));
            }
        );
    }

    public function showFreeMembership($slug)
    {
        $minimumSpend = \Setting::get('event-free-membership-minimum-spend');
        $event = $this->eventRepo->getEventBySlug($slug);
        $user = Auth::user();
        $totalTransaction = $this->transactionRepo->getTotalTransactionByDate(
            $user->id,
            $event->start_date,
            $event->end_date
        );

        return view('events.free-membership', [
            'event' => $event,
            'minimumSpend' => $minimumSpend,
            'totalTransaction' => $totalTransaction,
            'spendMore' => $minimumSpend - $totalTransaction
        ]);
    }

    public function showFollowedMemberGetMember($slug)
    {
        $event = $this->eventRepo->getEventBySlug($slug);
        $eventId = $event->id;
        $userId = Auth::user()->id;
        $referal = $this->referalRepo->getEventReferalCodeByUserId($eventId, $userId);
        return view('events.followed-member-get-member', [
            'event' => $this->eventRepo->getEventBySlug($slug),
            'referal' => substr($referal->code, 0, 7)
        ]);
    }

    public function showFollowedFreeMembership($slug)
    {
        $minimumSpend = \Setting::get('event-free-membership-minimum-spend');
        $event = $this->eventRepo->getEventBySlug($slug);
        $user = Auth::user();
        $totalTransaction = $this->transactionRepo->getTotalTransactionByDate(
            $user->id,
            $event->start_date,
            $event->end_date
        );

        if ($totalTransaction >= $minimumSpend) {
            \App\Model\FreeMembershipUser::where('user_id', $user->id)
                ->where('event_id', $event->id)
                ->update(['status' => 1]);
        }

        return view('events.followed-free-membership', [
            'event' => $event,
            'minimumSpend' => $minimumSpend,
            'totalTransaction' => $totalTransaction,
            'spendMore' => $minimumSpend - $totalTransaction
        ]);
    }

    public function showMemberGetMember($slug)
    {
        $event = $this->eventRepo->getEventBySlug($slug);
        $eventId = $event->id;
        $userId = Auth::user()->id;
        $referal = $this->referalRepo->getEventReferalCodeByUserId($eventId, $userId);
        return view('events.member-get-member', [
            'event' => $this->eventRepo->getEventBySlug($slug),
            'referal' => substr($referal->code, 0, 7)
        ]);
    }

    public function followedEvent()
    {
        $user = Auth::user();
        $userId = $user->id;
        if (str_contains($user->username, 'TAM')) {
            $freeMembershipEvents = $this->eventRepo->getActiveFreeMembershipEventByUserId($userId);
            $mGetMEvents = null;
        } else {
            $freeMembershipEvents = null;
            $mGetMEvents = $this->eventRepo->getActiveMemberGetMemberEventByUserId($userId);
        }
       
        $testimonies = $this->eventRepo->getActiveEventTestimoniesByUserId($userId);
        $events = $this->eventRepo->getFollowedEventByUserId($userId);
        return view('events.followed-event', [
            'events' => $events,
            'freeMembershipEvents' => $freeMembershipEvents,
            'memberGetMemberEvents' => $mGetMEvents,
            'testimonies' => $testimonies
        ]);
    }

    public function showFollowedEvent($slug)
    {
        $event = $this->eventRepo->getEventBySlug($slug);
        return view('events.show-followed-event', [
            'event' => $event
        ]);
    }
}
