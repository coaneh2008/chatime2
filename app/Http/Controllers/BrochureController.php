<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Extension\SeoTagDetailPageTrait;
use App\Model\Brochure;
use Illuminate\Http\Request;

class BrochureController extends Controller
{
    use SeoTagDetailPageTrait;

    const MAX_PER_PAGE = 10;
    protected $brochures;

    private $show = [
        10 => 10,
        15 => 15,
        20 => 20
    ];

    private $sort = [
        'asc' => 'Ascending',
        'desc' => 'Descending'
    ];

    public function __construct(Brochure $brochures)
    {
        $this->brochures = $brochures;
    }

    public function index(Request $request)
    {
        $maxPerPage = static::MAX_PER_PAGE;
        $sort = $request->sort;

        if ($request->show && $sort) {
            $maxPerPage = $request->show;
            $brochures = $this->brochures
                ->published()
                ->orderBy('title', $sort);
        } else {
            $brochures = $this->brochures
                ->asc()
                ->published();
        }
        $brochures = $brochures->paginate($maxPerPage)->appends([
                'show' => $maxPerPage,
                'sort' => $sort
            ]);

        return view('brochures.index', [
            'brochures' => $brochures,
            'show' => $this->show,
            'sort' => $this->sort
        ]);
    }

    public function show($slug)
    {
        $brochure = $this->brochures->findOrFailByUrlKey($slug);

        view()->share($this->getSeoTags($brochure));

        return view('brochures.show', [
            'brochure' => $brochure
        ]);
    }

    public function download($slug)
    {
        $brochure = $this->brochures->findOrFailByUrlKey($slug);
        $filePath = public_path($brochure->file);
        return response()->download($filePath);
    }
}
