<?php
namespace App\Http\Controllers;

use App\Model\User;
use App\Repositories\Api\ForgotPassKeyApiRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
    protected $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function forgotPasskey()
    {
        return view('password.forgot-passkey');
    }

    public function doForgotPasskey(Request $request)
    {
        $email = $request->email;

        $this->validate($request, [
            'email' => 'required|email',
        ]);
        
        $message = "{'P_Cust_Mail':'" . $email . "'}";
        $userApiRepo = new ForgotPassKeyApiRepository();
        $response = $userApiRepo->forgotPassKey($message, null);
        if ($response) {
            if ($response->is_ok == 'true') {
                $data = $response->rows[0];
                $newPasskey = $data->passkey;
                $user = User::where('email', $email)->first();
                if ($user) {
                    $user->password = $newPasskey;
                    $user->save();
                }

                $this->sendEmailEventConfirmation_noQueue($newPasskey, $user, $email);

                return redirect()
                    ->back()
                    ->with(session()
                    ->flash(NOTIF_SUCCESS, 'Please check your email, your passkey has been sent'));
            } else {
                return redirect()
                    ->back()
                    ->with(session()
                    ->flash(NOTIF_DANGER, "Your email doesn't exist"));
            }
        } else {
            return redirect()
                ->back()
                ->with(session()
                ->flash(NOTIF_DANGER, 'Sorry, something went wrong'));
        }
    }

    public function sendEmailEventConfirmation($newPasskey, $user, $email)
    {
        \Mail::queue(
            'password.send-email-passkey-recovery',
            [
                'newPasskey' => $newPasskey,
                'user' => $user,
                'email' => $email
            ],
            function ($message) use ($newPasskey, $user, $email) {
                $message->to($email);
                $message->subject('Recovery Passkey ' . trim(\Setting::get('site-name')));
            }
        );
    }

    public function sendEmailEventConfirmation_noQueue($newPasskey, $user, $email)
    {
        \Mail::send(
            'password.send-email-passkey-recovery',
            [
                'newPasskey' => $newPasskey,
                'user' => $user,
                'email' => $email
            ],
            function ($message) use ($newPasskey, $user, $email) {
                $message->to($email);
                $message->subject('Recovery Passkey ' . trim(\Setting::get('site-name')));
            }
        );
    }

}
