<?php

namespace App\Http\Controllers;

use App\Model\StoreLocation;

class StoreLocationController extends Controller
{
    protected $storelocations;

    public function __construct(StoreLocation $storelocations)
    {
        $this->storelocations = $storelocations;
    }

    public function index()
    {
        return view('store-locations.index',['menu_selected'=>'store']);
    }

    public function mapData()
    {
        return $this->storelocations->mapData();
    }
}
