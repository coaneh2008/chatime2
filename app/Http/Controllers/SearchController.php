<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Brochure;
use App\Model\FeaturedBrand as Brand;
use App\Model\HomePromotion;
use App\Model\InspirationArticle as Article;
use App\Model\InspirationArticleCategory as ArticleCategory;
use App\Model\MemberBenefit;
use App\Model\MemberMerchandise as Merchandise;
use App\Model\MemberNewsUpdate;
use App\Model\MemberPromo;
use App\Model\MemberVoucher;
use App\Model\MerchantPromo;
use App\Model\Page;
use App\Model\ProductCategory;
use App\Model\Promotion;
use App\Model\StoreLocation;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class SearchController extends Controller
{
    public function __construct(
        Article $article,
        Brochure $brochure,
        Brand $brand,
        HomePromotion $homePromotion,
        ArticleCategory $articleCategory,
        MemberBenefit $memberBenefit,
        Merchandise $merchandise,
        MemberNewsUpdate $newsUpdate,
        MemberPromo $memberPromo,
        MemberVoucher $memberVoucher,
        MerchantPromo $merchantPromo,
        Page $page,
        ProductCategory $productCategory,
        Promotion $promotion,
        StoreLocation $store
    ) {
        parent::__construct();
        $this->article = $article;
        $this->brochure = $brochure;
        $this->brand = $brand;
        $this->homePromotion = $homePromotion;
        $this->articleCategory = $articleCategory;
        $this->memberBenefit = $memberBenefit;
        $this->merchandise = $merchandise;
        $this->newsUpdate = $newsUpdate;
        $this->memberPromo = $memberPromo;
        $this->memberVoucher = $memberVoucher;
        $this->merchantPromo = $merchantPromo;
        $this->page = $page;
        $this->productCategory = $productCategory;
        $this->promotion = $promotion;
        $this->store = $store;
    }

    public function search(Request $request)
    {
        $search = $request->input('name');

        $aceOnlineProducts = $this->searchFromAceOnline($search);
        $article = $this->article->search($search);
        $brochure = $this->brochure->search($search);
        $brand = $this->brand->search($search);
        $homePromotion = $this->homePromotion->search($search);
        $articleCategory = $this->articleCategory->search($search);
        $memberBenefit = $this->memberBenefit->search($search);
        $merchandise = $this->merchandise->search($search);
        $newsUpdate = $this->newsUpdate->search($search);
        $memberPromo = $this->memberPromo->search($search);
        $memberVoucher = $this->memberVoucher->search($search);
        $merchantPromo = $this->merchantPromo->search($search);
        $page = $this->page->search($search);
        $productCategory = $this->productCategory->search($search);
        $promotion = $this->promotion->search($search);
        $store = $this->store->search($search);

        $result = array_merge(
            $aceOnlineProducts,
            $article,
            $brochure,
            $brand,
            $homePromotion,
            $articleCategory,
            $memberBenefit,
            $merchandise,
            $newsUpdate,
            $memberPromo,
            $memberVoucher,
            $merchantPromo,
            $page,
            $productCategory,
            $promotion,
            $store
        );
        $data = collect($result);
        $page = \Input::get('page', 1);
        $paginatedResult = new LengthAwarePaginator(
            $data->forPage($page, 10),
            $data->count(),
            10,
            $page,
            [
                'path' => request()->url(),
                'query' => request()->query(),
            ]
        );
        return view('search.result', ['data' => $paginatedResult, 'search' => $search]);
    }

    protected function searchFromAceOnline($keyword)
    {
        $urlSearch = env('URL_SEARCH_ACE_ONLINE') . $keyword;
        $prefixImageUrl = 'https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto';
        $aceOnlineProducts = [];
        try {
            $client = new Client();
            $result = $client->get($urlSearch);
            $data = json_decode($result->getBody(), 1);
            $count = 5;

            foreach ($data['data']['products'] as $key => $value) {
                if ($key < $count) {
                    $variants = $value['variants'];

                    $data = [
                        'url' => env('URL_ACE_ONLINE') . $value['url_key'],
                        'title' => $value['name'],
                        'content' => $value['name'],
                        'type' => 'Product',
                        'newTab' => true
                    ];
                    if (isset($variants[0])) {
                        $price = number_format($variants[0]['prices'][0]['price'], 0, ".", ".");
                        $specialPrice = number_format($variants[0]['prices'][0]['special_price'], 0, ".", ".");
                        if ($specialPrice == 0) {
                            $data['price'] = "<br>Harga: Rp. " . $price;
                        } else {
                            $data['price'] = "<br>Harga: <del> Rp. " . $price .
                                "</del> <b>Rp. " . $specialPrice . "</b>";
                        }

                        $promo = $variants[0]['label_spec'];
                        if ($promo != "") {
                            $data['promo'] = str_replace(["<li>", "</li>"], "", $promo);
                            $data['promo'] = "<br>" . $data['promo'];
                        }

                        if (isset($variants[0]['images'][0]['image_url'])) {
                            $img = ($prefixImageUrl . $variants[0]['images'][0]['image_url']);
                            $data['image'] = $img;
                        }
                    }
                    $aceOnlineProducts[] = $data;
                }
            }
        } catch (\Exception $exception) {
            \Log::warning($exception);
        }
        return $aceOnlineProducts;
    }
}
