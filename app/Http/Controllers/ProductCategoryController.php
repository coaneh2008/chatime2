<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Extension\SeoTagDetailPageTrait;
use App\Repositories\ProductCategoryRepository;

class ProductCategoryController extends Controller
{
    use SeoTagDetailPageTrait;

    protected $productCatRepo;

    public function __construct(ProductCategoryRepository $productCatRepo)
    {
        $this->productCatRepo = $productCatRepo;
    }

    public function show($slug)
    {
        $category = $this->productCatRepo->getProductCategory($slug);

        view()->share($this->getSeoTags($category));
        
        return view('product-categories.show', [
            'category' => $category
        ]);
    }
}
