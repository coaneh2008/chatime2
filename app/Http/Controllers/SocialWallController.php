<?php

namespace App\Http\Controllers;

use App\Repositories\InstagramRepository;

class SocialWallController extends Controller
{
    protected $instagramRepo;

    public function __construct(InstagramRepository $instagramRepo)
    {
        $this->instagramRepo = $instagramRepo;
    }

    public function index()
    {
        return view('social-wall', [
            'instagramPosts' => $this->instagramRepo->instagramPost()
        ]);
    }
}
