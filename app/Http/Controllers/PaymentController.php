<?php

namespace App\Http\Controllers;

use App\Repositories\Api\PaymentApiRepository;
use App\Repositories\UserRepository;
use Auth;

class PaymentController extends Controller
{
    protected $paymentRepo;

    public function __construct(PaymentApiRepository $paymentRepo)
    {
        $this->paymentRepo = $paymentRepo;
    }

    public function payViaAceOnline()
    {
        $user = Auth::user();
        $userRepo = new UserRepository();
        $token = $userRepo->getUserToken($user->id);
        $url = $this->paymentRepo->getPaymentUrl($user, $token);
        return redirect()->away($url);
    }
}
