<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Subscriber;
use Illuminate\Http\Request;

class SubscriberController extends Controller
{
    protected $rules = [ 'email' => 'required|email|unique:subscribers,email' ];

    protected $customMessages = [
        'required' => 'Email cannot be blank',
        'email' => 'Your Email is invalid',
        'unique' => "You already subscribe newsletter"
    ];

    /*public function store(Request $request)
    {
        $request->flash();
        $validator = \Validator::make($request->all(), $this->rules, $this->customMessages);

        if ($validator->fails()) {
            session()->flash('email', $validator->errors()->first());
            return redirect('/#footer')->withInput();
        }
        Subscriber::create([
            'email' => $request->input('email')
        ]);
        session()->flash('SUBSCRIBE_SUCCESS', \Setting::get('success-subscribe-description'));

        return redirect('/#footer');
    }*/

      



    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), $this->rules, $this->customMessages);

        if ($validator->fails()) {
            $status=0;
            $message=$validator->errors()->first();
        }else{
            
            Subscriber::create([
            'email' => $request->input('email')
            ]);

            $status=1;
            $message=\Setting::get('success-subscribe-description');
 
        }

        $data['status']=$status;
        $data['message']=$message;

        return response()->json($data);

    }

}
