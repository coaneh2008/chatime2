<?php
namespace App\Http\Controllers;

use App\Model\MemberLeaderboard;
use App\Repositories\Api\LeaderBoardByDateApiRepository;
use App\Repositories\Api\LeaderBoardByPromoApiRepository;
use App\Repositories\Api\PromoApiRepository;
use App\Repositories\LeaderBoardRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\UserRepository;
use Auth;

class LeaderboardController extends Controller
{
    protected $userRepo;
    protected $transactionRepo;

    public function __construct(
        UserRepository $userRepo,
        TransactionRepository $transactionRepo,
        LeaderBoardRepository $leaderBoardRepo
    ) {
        $this->userRepo = $userRepo;
        $this->transactionRepo = $transactionRepo;
        $this->leaderBoardRepo = $leaderBoardRepo;
    }
    public function index()
    {
        $user         	= Auth::user();
        $leaderboard 	= $this->leaderBoardRepo->getLeaderBoardUserData($user->username);
        // $is_user_leaderboard    = false;
        // $leaderboards 			= [];
        // foreach($data as $q => $l){
        //     if($user->username == $l->member_card_id){
        //     	$leaderboards[] = $l;
        //     }else{

        //     }
            // if($user->username != $l->member_card_id){
            //     $first = substr($l->member_card_id,0,6);
            //     $last  = str_split(substr($l->member_card_id, 6),1);
            //     foreach($last as $ls){
            //         $first .= 'X'; 
            //     }
            //     $l->member_card_id = $first;

            //     $first = substr($l->name,0,3);
            //     $last  = str_split(substr($l->name, 3),1);
            //     foreach($last as $ls){
            //         $first .= 'X'; 
            //     }
            //     $l->name = $first;
            // }else{
            //     $is_user_leaderboard = $l;
            // }
        // }

        return view('leaderboards.index', [
            'leaderboard'          => $leaderboard
            // 'is_user_leaderboard'   => $is_user_leaderboard
        ]);
    }

    /* not used anymore */
    public function show($code, $name)
    {
        // $code = '2017004057';
        $user = Auth::user();
        $message = "{'P_Cust_Id':'" . $user->member_id . "','P_Promo_Code':'" . $code . "'}";
        $auth = 'token ' . $this->userRepo->getUserToken($user->id);
        $apiRepo = new LeaderBoardByPromoApiRepository();
        $response = $apiRepo->getLeaderBoardByPromo($message, $auth);
        if ($response) {
            if ($response->is_ok == 'true') {
                $leaderboards = $response;
            } else {
                $leaderboards = null;
                session()->flash(NOTIF_DANGER, $response->message);
            }
        } else {
            $leaderboards = null;
            session()->flash(NOTIF_DANGER, 'Sorry, something went wrong');
        }
        return view('leaderboards.show', [
            'name' => $name,
            'leaderboards' => $leaderboards
        ]);
    }
}
