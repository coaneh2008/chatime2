<?php

namespace App\Http\Controllers;

use App\Model\Contact;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    protected $inbox;

    protected $rules = [
        'name' => 'required|string|max:255',
        'address' => 'required|string|max:255',
        'email' => [
                'required',
                'regex:/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.
                [\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.
                [a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.
                |[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.
                ){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/'
            ],
        'phone' => 'required|string|max:255',
        'message' => 'required|string'
    ];

    public function __construct(Contact $contact)
    {
        $this->inbox = $contact;
    }

    public function index()
    {
        return view('contact-us.index');
    }

    public function store(Request $request)
    {
        $request->flash();
        $this->validate($request, $this->rules);

        $contact = new Contact();
        $contact->name = $request->name;
        $contact->address = $request->address;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->message = $request->message;

        if ($contact->save()) {
            $this->sendMailNotification($contact);
            $this->sendMailReply($contact);
            $request->session()->flush();
            session()->flash(NOTIF_SUCCESS, trans('contact-us.inbox_form.message.success'));
        } else {
            session()->flash(NOTIF_DANGER, trans('contact-us.inbox_form.message.danger'));
        }
        return redirect(route('frontend.contact-us.index'));
    }

    public function sendMailNotification($inbox)
    {
        if (trim(\Setting::get('inbox-email-receiver'))) {
            $receiver = explode(',', trim(\Setting::get('inbox-email-receiver')));
        } else {
            $receiver = 'info@acehardware.co.id';
        }
        \Mail::queue(
            'contact-us.send-mail-notification',
            ['inbox' => $inbox],
            function ($message) use ($inbox, $receiver) {
                $message->to($receiver);
                $message->replyTo($inbox->email);
                $message->subject('[New Message] ' . trim(\Setting::get('site-name')));
            }
        );
    }

    public function sendMailReply($inbox)
    {
        \Mail::queue(
            'contact-us.send-mail-reply',
            ['inbox' => $inbox],
            function ($message) use ($inbox) {
                $message->to($inbox->email);
                $message->subject('Message from ' . trim(\Setting::get('site-name')));
            }
        );
    }
}
