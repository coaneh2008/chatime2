<?php

namespace App\Http\Controllers;

use App\Repositories\Api\CustomerARApiRepository;
use App\Repositories\UserRepository;
use Auth;
use Illuminate\Http\Request;

class RenewalController extends Controller
{
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function renewAccount(Request $request)
    {
        $memberId = $request->memberId;
        $receiveNo = $request->receiveNo;
        $response = $this->getCustomerAr($memberId, $receiveNo);
        if ($response) {
            if ($response->is_ok == 'true') {
                $username = $response->rows[0]->card_id;
                $user = \App\Model\User::where('member_id', $memberId)->first();
                if ($user) {
                    $user->username = $username;
                    $user->save();
                    $this->sendEmailRenewal($user);
                }
            }
        }
        return serialize($response);
    }

    public function getCustomerAr($memberId, $receiveNo)
    {
        $message = '{"P_Cust_Id":"' . $memberId . '","P_ReceiveNo":"' . $receiveNo . '"}';
        $apiRepo = new CustomerARApiRepository();
        $response = $apiRepo->getCustomerAr($message, null);
        return $response;
    }

    public function sendEmailRenewal($user)
    {
        \Mail::queue(
            'memberships.send-email-renewal-account',
            [
                'user' => $user
            ],
            function ($message) use ($user) {
                $message->to($user->email);
                $message->subject('Renewal Account Success | ' . \Setting::get('site-name'));
            }
        );
    }
}
