<?php
namespace App\Http\Controllers;

use App\Model\Page;
use App\Model\Redirection;
use Illuminate\Http\Request;

class PageController extends Controller
{
    protected $page;

    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    public function show(Request $request, $url)
    {
        $urlPrefix = $this->getPrefix($url);
        $slug = $this->slug($url);

        $page = $this->page->where('url_prefix', $urlPrefix)->findByUrlKey($slug);

        if ($page) {
            $layout = app($page->layout);

            view()->share([
                'menu_selected'=>'',
                'metaTitle' => $page->title,
                'metaDescription' => $page->description
            ]);

            return $layout->handle($request, $page);
        }

        //check redirection
        $redirectUrl = $this->checkRedirection($request->path());
        if ($redirectUrl) {
            $newUrl = $redirectUrl->new_url;
            if (filter_var($newUrl, FILTER_VALIDATE_URL) === false) {
                $redirectUrl = asset($newUrl);
            }
            return redirect()->away(asset($newUrl));
        }
        abort(404);
    }

    protected function checkRedirection($path)
    {
        $paths = $this->generateUrl($path);
        $redirectTo = Redirection::whereIn('old_path', $paths)->first();
        return $redirectTo;
    }

    /**
     * Generate urls array
     * @param  \Illuminate\Http\Request $referer []
     * @return array []
     */
    public function generateUrl($oldPath)
    {
        $urls = [];
        $url = explode("/", $oldPath);
        $explodeCount = count($url);
        foreach ($url as $key => $value) {
            if ($key < $explodeCount - 1) {
                $path = "";
                for ($i = $key - 1; $i >= 0; $i--) {
                    $path .= $url[$i] . "/";
                }
                $urls[] = $path . $value . "/*";
            }
        }
        $urls[] = $oldPath;
        $urls[] = $oldPath . '/*';
        $urls[] = "*";
        return $urls;
    }

    protected function getPrefix($url)
    {
        $prefix = dirname($url);
        return trim($prefix, '.');
    }

    protected function slug($url)
    {
        return basename($url);
    }
}
