<?php

namespace App\Http\Controllers\Extension;

use Carbon\Carbon;
use DB;
use Excel;
use Exception;
use Illuminate\Http\Request;

trait ImportTrait
{
    public function import(Request $request)
    {
        if ($request->method() == 'POST') {
            return $this->postImport($request);
        }
        return $this->getImport();
    }

    public function template()
    {
        $fileName = $this->getPageName() . '_Template';
        Excel::create($fileName, function ($excel) {
            $excel->sheet('Data', function ($sheet) {
                $sheet->loadView(suitViewName($this->getViewPrefix() . '.import-template'), []);
            });
        })->export('xls');
    }

    protected function getImport()
    {
        return view(suitViewName($this->getViewPrefix() . '.import'), ['model' => $this->model]);
    }

    protected function postImport(Request $request)
    {
        $isSuccess = true;
        $this->validate($request, [
            'file' => 'required'
        ]);
        $file = $request->file;

        $tempFile = $file->move('files/', $file->getClientOriginalName(), $file);
        $tempPath = $tempFile->getPathname();
        $result = Excel::selectSheetsByIndex(0)->load($tempPath)->toArray();
        DB::beginTransaction();
        $this->beforeProceed();
        try {
            foreach ($result as $row) {
                $this->saveImport($row);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $isSuccess = false;
            \Log::info($e);
            session()->flash(NOTIF_DANGER, $e->getMessage());
        }

        \Storage::delete($tempPath);

        if ($isSuccess) {
            session()->flash(NOTIF_SUCCESS, 'New ' . $this->getControllerName() . ' has been imported.');
        }
        return $this->redirectIndex();
    }

    protected function beforeProceed()
    {
    }

    protected function saveImport($data)
    {
        $model = $this->model->newInstance();
        $model->fill($data);
        $model->save();
    }
}
