<?php

namespace App\Http\Controllers\Extension;

use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;

trait ExportTrait
{
    public function export(Request $request)
    {
        if ($request->method() == 'POST') {
            return $this->postExport($request);
        }
        return $this->getExport();
    }

    protected function getExport()
    {
        return view(suitViewName($this->getViewPrefix() . '.export'), ['model' => $this->model]);
    }

    protected function postExport(Request $request)
    {
        // get current max_execution_time & set max_execution_time to one hour
        $currentExecutionTime = ini_get('max_execution_time');
        set_time_limit(900);
        $type = $request->type;
        if ($request->key) {
            $this->key = $request->key;
            $data = $this->exportEachQuery();
            $view = suitViewName($this->getViewPrefix() . '.export-data-each-model');
        } else {
            $data = $this->exportQuery()->get();
            $view = suitViewName($this->getViewPrefix() . '.export-data');
        }
        $fileName = \Setting::get('site-name') . ' - ' . $this->getPageName();
        Excel::create($fileName, function ($excel) use ($data, $view) {
            $excel->sheet('Data', function ($sheet) use ($data, $view) {
                $sheet->loadView($view, ['data' => $data]);
            });
        })->export($type);

        // revert to max_execution_time
        set_time_limit($currentExecutionTime);
        \Log::info("3. max_execution_time > " . ini_get('max_execution_time'));
    }

    protected function exportEachQuery()
    {
        return $this->model->find($this->key);
    }

    protected function exportQuery()
    {
        return $this->model->newQuery();
    }
}
