<?php

namespace App\Http\Controllers\Extension;

use App\Model\SeoTag;
use Illuminate\Http\Request;

trait SeoTagDetailPageTrait
{
    public function getSeoTags($model)
    {
        $seoTag = $model->seoTag();
        if ($seoTag) {
            return $seoTag->getSeoTagData();
        }
        return [];
    }
}
