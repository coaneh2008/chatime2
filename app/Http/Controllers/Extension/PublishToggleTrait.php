<?php

namespace App\Http\Controllers\Extension;

use Illuminate\Http\Request;

trait PublishToggleTrait
{
    protected $field = 'published';

    public function publish(Request $request, $key, $field = null)
    {
        $state = false;
        $model = $this->find($key);

        if (isset($request->state) && $request->state == 'true') {
            $state = true;
        }

        if (!empty($this->model)) {
            if ($field == null) {
                $model->{$this->field} = $state;
            } else {
                $model->{$field} = $state;
            }
            $model->save();
        }
        $response = [
            'message' => 'test',
            'data' => $model
        ];
        return response()->json($response);
    }
}
