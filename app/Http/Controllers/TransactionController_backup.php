<?php
namespace App\Http\Controllers;

use App\Jobs\GetCustomerData;
use App\Repositories\PointRedeemRepository;
use App\Repositories\TransactionRepository;
use Auth;

class TransactionController extends Controller
{
    protected $redeemRepo;
    protected $transactionRepo;

    public function __construct(
        PointRedeemRepository $redeemRepo,
        TransactionRepository $transactionRepo
    ) {
        $this->redeemRepo = $redeemRepo;
        $this->transactionRepo = $transactionRepo;
    }
    public function index()
    {
        $user = Auth::user();
        $userId = $user->id;
        $this->dispatch(new GetCustomerData($user));

        $redeems = $this->redeemRepo->getRedeemByUserId($userId);
        $transactions = $this->transactionRepo->getTransactionByUserId($userId);
        
        return view('transactions.index', [
            'transactions' => $transactions,
            'redeems' => $redeems
        ]);
    }
}
