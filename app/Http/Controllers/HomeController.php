<?php

namespace App\Http\Controllers;

use App\Model\InspirationArticle;
use App\Repositories\BannerRepository;
use App\Repositories\BrandRepository;
use App\Repositories\HomePromotionRepository;
use App\Repositories\InspirationArticleRepository;
use App\Repositories\ProductMenuRepository;
use App\Repositories\HomeInfoRepository;
use App\Repositories\ContentAboutRepository;

use App\Supports\Attachment\File;
use Attachment;




class HomeController extends Controller
{
    protected $bannerRepo;
    protected $brandRepo;
    protected $inspirationRepo;
    protected $homePromoRepo;
    protected $productHome;
    protected $homeInfoRepo;
    protected $contentAboutRepo;

    public function __construct()
    {
        $this->bannerRepo = \Cacheable::build(BannerRepository::class);
        $this->brandRepo = \Cacheable::build(BrandRepository::class);
        $this->inspirationRepo = \Cacheable::build(InspirationArticleRepository::class);
        $this->homePromoRepo = \Cacheable::build(HomePromotionRepository::class);
        $this->productHome = \Cacheable::build(ProductMenuRepository::class);
        $this->homeInfoRepo = \Cacheable::build(HomeInfoRepository::class);
        $this->contentAboutRepo = \Cacheable::build(ContentAboutRepository::class);
    }

    public function index()
    {
        //$value = config('app.timezone');
        //echo  base_path();
        //$attachment = Attachment::download('files/image/carousel.png', 'files/uploads/banner/image');
        /*$attachment = Attachment::download('D:\chattime-admin\public\files\image\carousel.png', 'D:\chattime-admin\public\files\uploads\banner\image');
        */
        //echo base_path();
        return view('home.index', [
            'menu_selected'=>'home',
            'banners' => $this->bannerRepo->getBannersCustom('home',0),
            'banners_mobile' => $this->bannerRepo->getBannersCustom('home',1),
            'product_home' => $this->productHome->getProductHome(),
            'brands' => $this->brandRepo->getBrands(),
            
            'bigInspirationBanners' => $this->inspirationRepo
                ->getInspirationArticleByImageType(InspirationArticle::TYPE_BIG_IMAGE,0,3),

            'bigInspirationBannersMobile' => $this->inspirationRepo
                ->getInspirationArticleByImageType(InspirationArticle::TYPE_BIG_IMAGE,0,2,1),
                
            
            'bigInspirationBannersSlider' => $this->inspirationRepo
                ->getInspirationArticleByImageType(InspirationArticle::TYPE_BIG_IMAGE,1,0),
            
            'bigInspirationBannersSliderMobile' => $this->inspirationRepo
                ->getInspirationArticleByImageType(InspirationArticle::TYPE_BIG_IMAGE,1,0,1),
            

            'smallInspirationBanners' => $this->inspirationRepo
                ->getInspirationArticleByImageType(InspirationArticle::TYPE_SMALL_IMAGE,0,2),

            'smallImageHomePromos' => $this->homePromoRepo->getActivePromoSmallImageBanner(),
            'bigImageHomePromos' => $this->homePromoRepo->getActivePromoBigImageBanner(),
            'homeInfoDesktop'=>$this->homeInfoRepo->getMainListInfo(0,0),
            'homeInfoDesktopSlider'=>$this->homeInfoRepo->getMainListInfo(0,1),
            'homeInfoMobile'=>$this->homeInfoRepo->getMainListInfo(1,0),
            'homeInfoMobileSlider'=>$this->homeInfoRepo->getMainListInfo(1,1)
        ]);
    }

    public function showProductSelection()
    {
        return view('home.product-selection');
    }


    public function aboutus(){
        return view('home.aboutus', [
            'menu_selected'=>'about',
            'socialmedia'=>$this->contentAboutRepo->getAboutList('socialmedia'),
            'fun'=>$this->contentAboutRepo->getAboutList('fun'),
            'hadir'=>$this->contentAboutRepo->getAboutList('hadir'),
            'video'=>$this->contentAboutRepo->getAboutList('video'),
            
            'contentAbout'=>$this->contentAboutRepo->getAboutList_row(),
            'banners' => $this->bannerRepo->getBanners()
        ]);
    }

    public function get_data($card_code=''){
        $url=env('APP_API').$card_code;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_POST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Post Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;

    }
    public function diary($card_code=''){

        $data_api=$this->get_data($card_code);
        $data_temp = json_decode($data_api);

        //header("Content-Type: application/json");
        //echo count($data_temp);
        //var_dump($data_temp);
        if($data_temp->meta->code==200){
            
            $total_trx=$data_temp->data->qty_trx;
            $kategori='BLM ADA';
            $kode_voucher='BLM ADA';
            if($total_trx >12){
                $kategori='Sweetie Bestea';
                $kode_voucher='TRAKTIRBESTIE';
            }else if($total_trx >=9 && $total_trx <=12 ){
                $kategori='Bestea Honey';
                $kode_voucher='TRAKTIRHONEY';
            }else if($total_trx >=5 && $total_trx <=8){
                $kategori='Bestea Buddy';
                $kode_voucher='TRAKTIRBUDDY';
            }else if($total_trx >=2 && $total_trx <=4){
                $kategori='Rookie Bestea';
                $kode_voucher='TRAKTIRROOKIE';
            }else if($total_trx ==1 ){
                $kategori='Bestea Newbie';
                $kode_voucher='TRAKTIRNEWBIE';
            }
            $fav_menu=$data_temp->data->fav_menu;
            $arr_menu=explode("-",$fav_menu);

            if(count($arr_menu) > 1){
                $menu=$arr_menu[1];
            }else{
                $menu=$fav_menu;
            }

            $fav_store=$data_temp->data->fav_store;
            $arr_store=explode("-",$fav_store);

            if(count($arr_menu) > 1){
                $store=$arr_store[1];
            }else{
                $store=$fav_store;
            }

            if($data_temp->data->date_joined!='' && $data_temp->data->date_joined!=NULL){
                $data_arr_date=explode(" ", $data_temp->data->date_joined);
                $date_joined=date('d M Y',strtotime($data_arr_date[0]));
            }else{
                $date_joined='';
            }

            return view('home.diary', [
            'status'=>$data_temp->meta->code,
            'fav_menu'=>$menu,
            'fav_store'=>$store,
            'qty_cup'=>$data_temp->data->qty_cup,
            'qty_rdm'=>$data_temp->data->qty_rdm,
            'fullname'=>$data_temp->data->fullname,
            'date_joined'=>$date_joined,
            'total_trx'=>$total_trx,
            'kategori'=>$kategori,
            'kode_voucher'=>$kode_voucher 
            ]);

        }else{
           
           return view('home.diary', [
            'status'=>$data_temp->meta->code,
            'fav_menu'=>'',
            'fav_store'=>'',
            'qty_cup'=>0,
            'qty_rdm'=>0,
            'fullname'=>'',
            'date_joined'=>'',
            'total_trx'=>0,
            'kategori'=>'',
            'kode_voucher'=>'' 
             
            ]); 
        }
        


        /*$client = new GuzzleHttp\Client();
        $response = $client->request('GET', 'http://ctmapimob-qa.chatime.co.id/member-recap/?member_card_code=CT00014379');*/

        

        //$client = new Client([(['base_uri' => 'https://reqres.in/'])]);
        //$response = $client->request('GET', '/api/users?page=1');

        //$client = new Client();
        /*$client = new Client([(['base_uri' => 'http://ctmapimob-qa.chatime.co.id/'])]);
        $response = $client->request('GET', '/member-recap/?member_card_code=CT00014379');    
        //$response = $client->get('http://ctmapimob-qa.chatime.co.id/member-recap/?member_card_code=CT00014379');
        //$response = json_decode($response);

        echo $response->getBody();*/

        //$client = new Client();
        //$api_response = $client->get('http://ctmapimob-qa.chatime.co.id/member-recap/?member_card_code=CT00014379');
        //var_dump($api_response);
        //$response = json_decode($api_response);
        //var_dump($response);
        //return view('home.diary', compact('response'));
    }
}
