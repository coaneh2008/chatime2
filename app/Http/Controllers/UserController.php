<?php
namespace App\Http\Controllers;

use App\Jobs\GetCustomerData;
use App\Model\CustomerPoint;
use App\Model\User;
use App\Repositories\Api\UpdatePassKeyApiRepository;
use App\Repositories\Api\UpdateProfileApiRepository;
use App\Repositories\Api\CustomerApiRepository;
use App\Repositories\CityRepository;
use App\Repositories\UserRepository;
use Auth;
use Crypt;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $cityRepo;
    protected $userRepo;

    public function __construct(
        CityRepository $cityRepo,
        UserRepository $userRepo
    ) {
        $this->cityRepo = $cityRepo;
        $this->userRepo = $userRepo;
    }

    public function editProfile()
    {
        $user = Auth::user();
        $city = $this->cityRepo->getCityById($user->current_address_city_id);
        return view('memberships.edit-profile', [
            'cityCode' => $city ? $city->code : '',
            'cities' => $this->cityRepo->getCities(),
            'maritalStatus' => $this->userRepo->getMaritalStatus(),
            'sexTypes' => $this->userRepo->getSexTypes()
        ]);
    }

    public function doEditProfile(Request $request)
    {
        $customMessages = [
            'required' => ':attribute tidak boleh kosong',
            'email' => ':attribute tidak valid',
            'unique' => "Email telah terdaftar",
            'max' => ":attribute tidak boleh melebihi :max karakter",
            "min" => ":attribute setidaknya :min karakter"
        ];

        $this->validate($request, [
            'profile_picture' => 'image',
            // 'email' => 'required|unique:users,email,' . Auth()->user()->id,
            'email' => 'required',
            'place_of_birth' => 'string|max:100',
            'date_of_birth' => 'required',
            'current_address_city_id' => 'string|max:4',
            'current_address_zip_code' => 'string|max:15',
            'current_address' => 'string|max:100',
            'identity_card_id' => 'required|string|max:60',
            'cellphone' => [
                'required',
                'string',
                'min:9',
                'regex:/\(?(?:\+62|62|0)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}/'
            ],
            'gender' => 'integer|in:0,1',
            'marital_status' => 'integer|in:1,2,3'
        ], $customMessages);
        $user = Auth::user();
        
        if($user->email != $request->email){
            if($request->email != $request->email_verified){
                return redirect()
                    ->back()
                    ->with(session()
                    ->flash(NOTIF_DANGER, 'Email tidak sama dengan email verifikasi'));
            }
        }

        if ($image = $request->file('profile_picture')) {
            $destinationPath = public_path('/files/uploads/userprofile');
            $fileName = $user->username . '.' . $image->getClientOriginalExtension();
            $image->move($destinationPath, $fileName);
            $newProfileImage = 'files/uploads/userprofile/' . $fileName;
        } else {
            $newProfileImage = $user->profile_picture;
        }
        
        $response = $this->doUpdateProfile($request, $user);

        if ($response) {
            if ($response->is_ok == 'true') {
                User::find($user->id)->update([
                    'profile_picture' => $newProfileImage
                ]);
                
                $this->dispatch(new GetCustomerData($user));
                
                /*Add By Vincent - 134444
                    Check email for verification
                */
                $check_email = User::where('email',$request->email)->where('id','!=',$user->id)->first();
                if(!$check_email){
                    if($user->is_verified == 0 || ($request->email != $user->email)){
                        User::where('id',$user->id)->update(['is_verified' => 0]);
                        \Mail::send(
                            'memberships.send-email-verified',
                            ['user' => $user],
                            function ($message) use ($user) {
                                $message->to($user->email);
                                $message->subject('Verifikasi email ACE Rewards anda.');
                            }
                        );
                    }
                }else{
                    if($check_email->is_verified == 1){
                        return redirect()
                            ->back()
                            ->with(session()
                            ->flash(NOTIF_DANGER, 'Email already use by other member'));
                    }else{
                        User::where('id',$user->id)->update(['is_verified' => 0]);
                        \Mail::send(
                            'memberships.send-email-verified',
                            ['user' => $user],
                            function ($message) use ($user) {
                                $message->to($user->email);
                                $message->subject('Verifikasi email ACE Rewards anda');
                            }
                        );
                    }
                }
                User::where('id',$user->id)->update(['email' => $request->email]);
                
                return redirect()
                    ->back()
                    ->with(session()
                    ->flash(NOTIF_SUCCESS, trans('Your profile is successfully updated')));
            } elseif ($response->is_ok == 'false' && $response->message == 'Access Deny. Invalid Token') {
                return redirect()
                    ->back()
                    ->with(session()
                    ->flash('NOTIF_SESSION_INVALID_TOKEN', ''));
            } else {
                return redirect()
                    ->back()
                    ->with(session()
                    ->flash(NOTIF_DANGER, $response->message));
            }
        } else {
            return redirect()
                ->back()
                ->with(session()
                ->flash(NOTIF_DANGER, 'Sorry, something went wrong'));
        }
    }

    public function doUpdateProfile($request, $user)
    {
        $formatedDate = \Carbon\Carbon::parse($request->date_of_birth)->format('m/d/Y');

        $userApiRepo = new CustomerApiRepository();
        $userRepo = new UserRepository();
        $message = "{'P_Cust_Id':'" . $user->member_id . "'}";
        $auth = 'token ' . $userRepo->getUserToken($user->id);
        
        $response = $userApiRepo->getCustomerData($message, $auth);

        $message = "{'P_Cust_Id':'" . $user->member_id . "',
            'P_Cust_Name':'" . $request->name . "',
            'P_Cust_Add':'" . $request->current_address . "',
            'P_Cust_Hp':'" . $request->cellphone . "',
            'P_Cust_Mail':'" . $response->rows[0]->Cust_Mail . "',
            'P_Cust_Birth_Date':'" . $formatedDate . "',
            'P_Cust_Birth_Place':'" . $request->place_of_birth . "',
            'P_Cust_Sex':'" . $request->gender . "',
            'P_Cust_City':'" . $request->current_address_city_id . "',
            'P_Cust_PosCode':'" . $request->current_address_zip_code . "',
            'P_Cust_Add_Ktp':'" . $user->ktp_address . "',
            'P_Cust_Phone':'" . $user->telephone . "',
            'P_Cust_IdCard':'" . $request->identity_card_id . "',
            'P_Cust_Religion':'" . $user->religion_id . "',
            'P_Cust_Stat_Marital':'" . $request->marital_status . "',
            'P_Cust_citizenship':'" . $user->citizenship_id . "',
            'P_Cust_Employ':'" . $user->occupation_id . "',
            'P_Cust_Purpose':'" . $user->shopping_purpose . "',
            'P_Cust_AutoRen':'" . $user->is_renewal . "'}";

        $apiRepo = new UpdateProfileApiRepository();
        $auth = 'token ' . $this->userRepo->getUserToken($user->id);
        return $apiRepo->updateProfile($message, $auth);
    }

    public function autoRenewal($val)
    {
        $user = Auth::user();
        $statusRenewal = $val;
        $response = $this->doAutoRenewal($user, $statusRenewal);

        if ($response) {
            if ($response->is_ok == 'true') {
                User::find($user->id)->update([
                    'is_renewal' => $statusRenewal
                ]);
                return redirect()
                    ->back()
                    ->with(session()
                    ->flash(NOTIF_SUCCESS, trans('Your profile is successfully updated')));
            } elseif ($response->is_ok == 'false' && $response->message == 'Access Deny. Invalid Token') {
                return redirect()
                    ->back()
                    ->with(session()
                    ->flash('NOTIF_SESSION_INVALID_TOKEN', ''));
            } else {
                return redirect()
                    ->back()
                    ->with(session()
                    ->flash(NOTIF_DANGER, $response->message));
            }
        } else {
            return redirect()
                ->back()
                ->with(session()
                ->flash(NOTIF_DANGER, 'Sorry, something went wrong'));
        }
    }

    public function doAutoRenewal($user, $statusRenewal)
    {
        $formatedDate = \Carbon\Carbon::parse($user->date_of_birth)->format('m/d/Y');

        $city = $this->cityRepo->getCityById($user->current_address_city_id);
        if ($city) {
            $cityCode = $city->code;
        } else {
            $cityCode = '';
        }

        $message = "{'P_Cust_Id':'" . $user->member_id . "',
            'P_Cust_Name':'" . $user->name . "',
            'P_Cust_Add':'" . $user->current_address . "',
            'P_Cust_Hp':'" . $user->cellphone . "',
            'P_Cust_Mail':'" . $user->email . "',
            'P_Cust_Birth_Date':'" . $formatedDate . "',
            'P_Cust_Birth_Place':'" . $user->place_of_birth . "',
            'P_Cust_Sex':'" . $user->gender . "',
            'P_Cust_City':'" . $cityCode . "',
            'P_Cust_PosCode':'" . $user->current_address_zip_code . "',
            'P_Cust_Add_Ktp':'" . $user->ktp_address . "',
            'P_Cust_Phone':'" . $user->cellphone . "',
            'P_Cust_IdCard':'" . $user->identity_card_id . "',
            'P_Cust_Religion':'" . $user->religion_id . "',
            'P_Cust_Stat_Marital':'" . $user->marital_status . "',
            'P_Cust_citizenship':'" . $user->citizenship_id . "',
            'P_Cust_Employ':'" . $user->occupation_id . "',
            'P_Cust_Purpose':'" . $user->shopping_purpose . "',
            'P_Cust_AutoRen':'" . $statusRenewal . "'}";

        $apiRepo = new UpdateProfileApiRepository();
        $auth = 'token ' . $this->userRepo->getUserToken($user->id);
        return $apiRepo->updateProfile($message, $auth);
    }

    public function editPasskey()
    {
        return view('memberships.edit-passkey');
    }

    public function doEditPasskey(Request $request)
    {
        $customMessages = [
            'required' => ':attribute tidak boleh kosong',
            'max' => ":attribute tidak boleh melebihi :max karakter",
            "min" => ":attribute setidaknya :min karakter",
            'same' => ":attribute dan :other harus sama",
        ];

        $this->validate($request, [
            'old_passkey' => 'required|old_passkey:Auth',
            'new_passkey' => 'required|max:10',
            'confirm_new_passkey' => 'required|same:new_passkey'
        ], $customMessages);

        $user = Auth::user();
        
        $response = $this->doUpdatePasskey($request, $user);
        
        if ($response) {
            if ($response->is_ok == 'true') {
                $user->update(['password' => $request->new_passkey]);
                
                $this->dispatch(new GetCustomerData($user));

                return redirect()
                    ->back()
                    ->with(session()
                    ->flash(NOTIF_SUCCESS, trans('Your passkey is successfully updated')));
            } elseif ($response->is_ok == 'false' && $response->message == 'Access Deny. Invalid Token') {
                return redirect()
                    ->back()
                    ->with(session()
                    ->flash('NOTIF_SESSION_INVALID_TOKEN', ''));
            } else {
                return redirect()
                    ->back()
                    ->with(session()
                    ->flash(NOTIF_DANGER, $response->message));
            }
        } else {
            return redirect()
                ->back()
                ->with(session()
                ->flash(NOTIF_DANGER, 'Sorry, something went wrong'));
        }
    }

    public function doUpdatePasskey($request, $user)
    {
        $message = "{'P_Cust_Id':'" . $user->member_id . "',
            'P_Passkey_old':'" . $request->old_passkey . "',
            'P_Passkey_new':'" . $request->new_passkey . "'}";

        $apiRepo = new UpdatePassKeyApiRepository();
        $auth = 'token ' . $this->userRepo->getUserToken($user->id);
        return $apiRepo->updatePassKey($message, $auth);
    }

    public function verified_email($id,$email){
        $id = Crypt::decrypt($id);
        $email = Crypt::decrypt($email);
        $user = User::where('id',$id)->where('email',$email)->where('is_verified',0)->first();
        if($user){
            
            $user->is_verified = 1;
        
            $formatedDate = \Carbon\Carbon::parse($user->date_of_birth)->format('m/d/Y');
            
            $userApiRepo = new CustomerApiRepository();
            $userRepo = new UserRepository();
            $message = "{'P_Cust_Id':'" . $user->member_id . "'}";
            $auth = 'token ' . $userRepo->getUserToken($user->id);
            
            $response = $userApiRepo->getCustomerData($message, $auth);
    
            $message = "{'P_Cust_Id':'" . $user->member_id . "',
                'P_Cust_Name':'" . $user->name . "',
                'P_Cust_Add':'" . $user->current_address . "',
                'P_Cust_Hp':'" . $user->cellphone . "',
                'P_Cust_Mail':'" . $user->email . "',
                'P_Cust_Birth_Date':'" . $formatedDate . "',
                'P_Cust_Birth_Place':'" . $user->place_of_birth . "',
                'P_Cust_Sex':'" . $user->gender . "',
                'P_Cust_City':'" . $user->current_address_city_id . "',
                'P_Cust_PosCode':'" . $user->current_address_zip_code . "',
                'P_Cust_Add_Ktp':'" . $user->ktp_address . "',
                'P_Cust_Phone':'" . $user->telephone . "',
                'P_Cust_IdCard':'" . $user->identity_card_id . "',
                'P_Cust_Religion':'" . $user->religion_id . "',
                'P_Cust_Stat_Marital':'" . $user->marital_status . "',
                'P_Cust_citizenship':'" . $user->citizenship_id . "',
                'P_Cust_Employ':'" . $user->occupation_id . "',
                'P_Cust_Purpose':'" . $user->shopping_purpose . "',
                'P_Cust_AutoRen':'" . $user->is_renewal . "'}";
    
            $apiRepo = new UpdateProfileApiRepository();
            $auth = 'token ' . $this->userRepo->getUserToken($user->id);
            $apiRepo->updateProfile($message, $auth);
            $user->save();
    
            User::where('email',$user->email)->where('id','!=',$user->id)->update(['email' => '']);
            return redirect()
                ->route('frontend.membership.edit-profile')
                ->with(session()
                ->flash('NOTIF_VERIFIED', 'Congratulations, your account already verified'));
        }else{
            return redirect()
                ->route('frontend.membership.edit-profile')
                ->with(session()
                ->flash(NOTIF_DANGER, 'Opps, Your account already verified'));
        }
    }
}
