<?php

namespace App\Http\Controllers\Api;

use App\Model\Setting;

class SettingController extends ResourceController
{
    public function __construct(Setting $model)
    {
        parent::__construct($model);
    }
}
