<?php

namespace App\Http\Controllers;

use App\Model\InspirationArticle;
use App\Repositories\BannerRepository;
use App\Repositories\BrandRepository;
use App\Repositories\HomePromotionRepository;
use App\Repositories\InspirationArticleRepository;
use App\Repositories\ProductMenuRepository;
use App\Repositories\ProductCategoryRepository;

use App\Supports\Attachment\File;
use Attachment;


class ProductController extends Controller
{
    protected $bannerRepo;
    protected $brandRepo;
    protected $inspirationRepo;
    protected $homePromoRepo;
    protected $productList;
    protected $categoryProductList;

    public function __construct()
    {
        $this->bannerRepo = \Cacheable::build(BannerRepository::class);
        $this->brandRepo = \Cacheable::build(BrandRepository::class);
        $this->inspirationRepo = \Cacheable::build(InspirationArticleRepository::class);
        $this->homePromoRepo = \Cacheable::build(HomePromotionRepository::class);
        $this->productList = \Cacheable::build(ProductMenuRepository::class);
        $this->categoryProductList= \Cacheable::build(ProductCategoryRepository::class);
    }

    /*public function index()
    {
        //$attachment = Attachment::download('files/image/carousel.png', 'files/uploads/banner/image');

        return view('home.index', [
            'banners' => $this->bannerRepo->getBanners(),
            'brands' => $this->brandRepo->getBrands(),
            'bigInspirationBanners' => $this->inspirationRepo
                ->getInspirationArticleByImageType(InspirationArticle::TYPE_BIG_IMAGE),
            'smallInspirationBanners' => $this->inspirationRepo
                ->getInspirationArticleByImageType(InspirationArticle::TYPE_SMALL_IMAGE),
            'smallImageHomePromos' => $this->homePromoRepo->getActivePromoSmallImageBanner(),
            'bigImageHomePromos' => $this->homePromoRepo->getActivePromoBigImageBanner()
        ]);
    }

    public function showProductSelection()
    {
        return view('home.product-selection');
    }
*/

    public function index(){
        return view('product.index', [
            'menu_selected'=>'menu',
            'banners2' => $this->bannerRepo->getBannersCustom('product',0),
            'category' => $this->categoryProductList->getMainCategoryList(),
            'productList' => $this->productList->getProductList(),
            
            'banners' => $this->bannerRepo->getBanners()
        ]);
    }
}
