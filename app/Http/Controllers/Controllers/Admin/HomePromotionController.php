<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Jobs\GetProductData;
use App\Model\HomePromotion as Model;
use App\Repositories\ProductRepository;

class HomePromotionController extends ResourceController
{
    use ExportTrait;
    use PublishToggleTrait;

    private $productRepo;

    protected $rules = [
        'title' => 'required|string|max:255',
        'content' => 'string|required_without_all:url',
        'url' => 'string',
        'kode_promo' => 'required|string',
        'published' => 'required|in:0,1',
        'thumb_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'banner_image' => [
            'required_without_all:url',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'type' => 'required',
        'order' => 'integer',
        'start_date' => 'required',
        'end_date' => 'required',
        'slug' => 'alpha_dash|unique:home_promotions,slug',
        'attachments' => 'array',
        'attachments.*.id' => 'integer',
        'attachments.*.url_to_ace_online' => 'required',
        'attachments.*.order' => 'required',
    ];

    public function __construct(
        Model $model,
        ProductRepository $productRepo
    ) {
        parent::__construct($model);
        $this->productRepo = $productRepo;
    }

    protected function formRules()
    {
        if ($this->model->exists) {
            foreach (['slug'] as $key) {
                $this->rules[$key] .= ',' . $this->model->getKey();
            }
        }
        parent::formRules();
    }

    protected function formData()
    {
        parent::formData();

        view()->share('typeList', $this->model->typeList());
    }

    protected function doSave()
    {
        $attachmentIds = [];
        $attachments = $this->model->attachments;
        if ($this->form->has('attachments')) {
            $formAttachments = $this->form->all()['attachments'];
            foreach ($formAttachments as $formAttachment) {
                $url = $formAttachment['url_to_ace_online'];
                $product = $this->productRepo->getProductByUrl($url);
                $product = $product ? $product : $this->productRepo->addNewProduct($url);
                $productId = $product->id;

                $this->dispatch(new GetProductData($product));

                $attachment = $this->model->setAttachment(
                    $productId,
                    $formAttachment['order']
                );
                if (!empty($attachment->getKey())) {
                    $attachmentIds[] = $attachment->getKey();
                }
            }
        }
        parent::doSave();

        $attachments->map(function ($attachment) use ($attachmentIds) {
            if (!in_array($attachment->getKey(), $attachmentIds)) {
                $attachment->delete();
            }
        });
    }
}
