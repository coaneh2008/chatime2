<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Jobs\GetProductData;
use App\Model\FeaturedBrand as Model;
use App\Repositories\ProductRepository;

class ProductMenuController extends ResourceController
{
    use ExportTrait;
    use PublishToggleTrait;
    private $productRepo;
    
    protected $rules = [
        'title' => 'required|string|max:255',
        'content' => 'string',
        'published' => 'required|in:0,1',
        'is_highlighted' => 'required|in:0,1',
        'description'=>'required|string',
        'category_id' => 'integer|required',
        'color'=>'string',
        'thumb_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'banner_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'slug' => 'alpha_dash|unique:product_categories,slug',
        'order' => 'integer',
        'page_type' => 'string',
        'seo_title' => 'required|string',
        'seo_description' => 'required|string',
        'opengraph_title' => 'string',
        'opengraph_description' => 'string',
        'twitter_title' => 'string',
        'twitter_description' => 'string',
        'meta_image' => [
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'attachments' => 'array',
        'attachments.*.id' => 'integer',
        'attachments.*.url_to_ace_online' => 'required',
        'attachments.*.order' => 'required',
    ];

    public function __construct(
        Model $model,
        ProductRepository $productRepo
    ) {
        parent::__construct($model);
        $this->productRepo = $productRepo;
    }
    
    protected function formData()
    {
        parent::formData();
        view()->share('categories', $this->model->getProductCategory());
    }

    protected function formRules()
    {
        if ($this->model->exists) {
            foreach (['slug'] as $key) {
                $this->rules[$key] .= ',' . $this->model->getKey();
            }
        }
        parent::formRules();
    }

    protected function doSave()
    {
        $attachmentIds = [];
        $attachments = $this->model->attachments;
        if ($this->form->has('attachments')) {
            $formAttachments = $this->form->all()['attachments'];
            foreach ($formAttachments as $formAttachment) {
                $url = $formAttachment['url_to_ace_online'];
                $product = $this->productRepo->getProductByUrl($url);
                $product = $product ? $product : $this->productRepo->addNewProduct($url);
                $productId = $product->id;

                $this->dispatch(new GetProductData($product));

                $attachment = $this->model->setAttachment(
                    $productId,
                    $formAttachment['order']
                );
                if (!empty($attachment->getKey())) {
                    $attachmentIds[] = $attachment->getKey();
                }
            }
        }
        parent::doSave();

        $attachments->map(function ($attachment) use ($attachmentIds) {
            if (!in_array($attachment->getKey(), $attachmentIds)) {
                $attachment->delete();
            }
        });
    }
}
