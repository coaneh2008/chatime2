<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Jobs\GetProductData;
use App\Model\InspirationArticle as Model;
use App\Repositories\ProductRepository;

class InspirationArticleController extends ResourceController
{
    use ExportTrait;
    use PublishToggleTrait;

    private $productRepo;

    protected $rules = [
        'title' => 'required|string|max:255',
        'description' => 'string',
        'content' => 'string',
        'published' => 'required|in:0,1',
        'is_highlighted' => 'required|in:0,1',
        'is_slider' => 'required|in:0,1',
        'device'=>'in:0,1',
        'thumb_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'banner_image' => [
            'required_if:is_highlighted,==,1',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'image_type' => [
            'required_if:is_highlighted,==,1',
            'string'
        ],
        'order' => 'integer',
        'slug' => 'alpha_dash|unique:inspiration_articles,slug',
        'category_id' => 'integer|required',
        'author_id' => 'integer|required',
        'tags' => 'string',
        'page_type' => 'string',
        'seo_title' => 'required|string',
        'seo_description' => 'required|string',
        'opengraph_title' => 'string',
        'opengraph_description' => 'string',
        'twitter_title' => 'string',
        'twitter_description' => 'string',
        'meta_image' => [
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'attachments' => 'array',
        'attachments.*.id' => 'integer',
        'attachments.*.url_to_ace_online' => 'required',
        'attachments.*.order' => 'required',
        'published_date' => 'required|date'
    ];

    public function __construct(
        Model $model,
        ProductRepository $productRepo
    ) {
        parent::__construct($model);
        $this->productRepo = $productRepo;
    }

    protected function formRules()
    {
        if ($this->model->exists) {
            foreach (['slug'] as $key) {
                $this->rules[$key] .= ',' . $this->model->getKey();
            }
        }
        parent::formRules();
    }

    protected function formData()
    {
        parent::formData();
        view()->share('categories', $this->model->inspirationArticleCategory()->getRelated()->getCategoryList());
        view()->share('authors', $this->model->user()->getRelated()->getAuthorList());
        view()->share('imageTypes', ['' => 'None'] + $this->model->typeListName());
        view()->share(
            'typeDevice', $this->model->typeDevice()
        );
    }

    protected function paginateQuery()
    {
        return $this->model->newQuery()->orderBy('published_date', 'desc');
    }

    protected function doSave()
    {
        $attachmentIds = [];
        $attachments = $this->model->attachments;
        if ($this->form->has('attachments')) {
            $formAttachments = $this->form->all()['attachments'];
            foreach ($formAttachments as $formAttachment) {
                $url = $formAttachment['url_to_ace_online'];
                $product = $this->productRepo->getProductByUrl($url);
                $product = $product ? $product : $this->productRepo->addNewProduct($url);
                $productId = $product->id;

                $this->dispatch(new GetProductData($product));

                $attachment = $this->model->setAttachment(
                    $productId,
                    $formAttachment['order']
                );
                if (!empty($attachment->getKey())) {
                    $attachmentIds[] = $attachment->getKey();
                }
            }
        }
        parent::doSave();

        $attachments->map(function ($attachment) use ($attachmentIds) {
            if (!in_array($attachment->getKey(), $attachmentIds)) {
                $attachment->delete();
            }
        });
    }
}
