<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\ContentAbout as Model;

class ContentAboutController extends ResourceController
{
    use ExportTrait;
    use PublishToggleTrait;

    protected $rules = [
        'title' => 'required|string|max:255',
        'description' => [
            'required_if:type,==,Secondary',
            'string'
        ],
        'content' => [
            'required_if:type,==,Primary',
            'string'
        ],
        'published' => 'required|in:0,1',
        'device'=>'in:0,1',
        /*'thumb_image' => [
            'required_if:type,==,Primary,Secondary',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'sidebar_image' => [
            'required_if:type,==,Primary',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],*/
        'banner_image' => [
            'required_if:type,==,Primary',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'order' => 'integer',
        //'type' => 'required',
        'url_to_ace_online' => [
            'required_if:type,==,Secondary,Tertiary'
        ],
        'slug' => 'alpha_dash|unique:product_categories,slug',
        'parent_id' => 'string',
        'page_type' => 'string',
        //'seo_title' => 'required|string',
        //'seo_description' => 'required|string',
        'opengraph_title' => 'string',
        'opengraph_description' => 'string',
        'twitter_title' => 'string',
        'twitter_description' => 'string',
        'meta_image' => [
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ]
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function formData()
    {
        parent::formData();
        view()->share('parents', ['' => 'No Parent'] + $this->model->parentListById()->all());
        view()->share('types', $this->model->typeListName());
        view()->share(
            'typeDevice', $this->model->typeDevice()
        );
    }

    protected function formRules()
    {
        if ($this->model->exists) {
            foreach (['slug'] as $key) {
                $this->rules[$key] .= ',' . $this->model->getKey();
            }
        }
        parent::formRules();
    }
    
    protected function doSave()
    {
        if (!empty($this->form->input('parent_id'))) {
            $this->model->parent()->associate($this->model->find($this->form->input('parent_id')));
        }
        return parent::doSave();
    }
}
