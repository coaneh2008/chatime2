<?php

namespace App\Http\Controllers;

use App\Model\InspirationArticle;
use App\Repositories\BannerRepository;
use App\Repositories\BrandRepository;
use App\Repositories\HomePromotionRepository;
use App\Repositories\InspirationArticleRepository;
use App\Repositories\ProductMenuRepository;
use App\Repositories\HomeInfoRepository;
use App\Repositories\ContentAboutRepository;

use App\Supports\Attachment\File;
use Attachment;


class HomeController extends Controller
{
    protected $bannerRepo;
    protected $brandRepo;
    protected $inspirationRepo;
    protected $homePromoRepo;
    protected $productHome;
    protected $homeInfoRepo;
    protected $contentAboutRepo;

    public function __construct()
    {
        $this->bannerRepo = \Cacheable::build(BannerRepository::class);
        $this->brandRepo = \Cacheable::build(BrandRepository::class);
        $this->inspirationRepo = \Cacheable::build(InspirationArticleRepository::class);
        $this->homePromoRepo = \Cacheable::build(HomePromotionRepository::class);
        $this->productHome = \Cacheable::build(ProductMenuRepository::class);
        $this->homeInfoRepo = \Cacheable::build(HomeInfoRepository::class);
        $this->contentAboutRepo = \Cacheable::build(ContentAboutRepository::class);
    }

    public function index()
    {
        //$value = config('app.timezone');
        //echo  base_path();
        //$attachment = Attachment::download('files/image/carousel.png', 'files/uploads/banner/image');
        /*$attachment = Attachment::download('D:\chattime-admin\public\files\image\carousel.png', 'D:\chattime-admin\public\files\uploads\banner\image');
        */
        //echo base_path();
        return view('home.index', [
            'menu_selected'=>'home',
            'banners' => $this->bannerRepo->getBannersCustom('home',0),
            'banners_mobile' => $this->bannerRepo->getBannersCustom('home',1),
            'product_home' => $this->productHome->getProductHome(),
            'brands' => $this->brandRepo->getBrands(),
            'bigInspirationBanners' => $this->inspirationRepo
                ->getInspirationArticleByImageType(InspirationArticle::TYPE_BIG_IMAGE,0,3),
            'bigInspirationBannersSlider' => $this->inspirationRepo
                ->getInspirationArticleByImageType(InspirationArticle::TYPE_BIG_IMAGE,1,0),
                
            'smallInspirationBanners' => $this->inspirationRepo
                ->getInspirationArticleByImageType(InspirationArticle::TYPE_SMALL_IMAGE,0,2),
            'smallImageHomePromos' => $this->homePromoRepo->getActivePromoSmallImageBanner(),
            'bigImageHomePromos' => $this->homePromoRepo->getActivePromoBigImageBanner(),
            'homeInfoDesktop'=>$this->homeInfoRepo->getMainListInfo(0,0),
            'homeInfoDesktopSlider'=>$this->homeInfoRepo->getMainListInfo(0,1),
            'homeInfoMobile'=>$this->homeInfoRepo->getMainListInfo(1,0),
            'homeInfoMobileSlider'=>$this->homeInfoRepo->getMainListInfo(1,1)
        ]);
    }

    public function showProductSelection()
    {
        return view('home.product-selection');
    }


    public function aboutus(){
        return view('home.aboutus', [
            'menu_selected'=>'about',
            'socialmedia'=>$this->contentAboutRepo->getAboutList('socialmedia'),
            'fun'=>$this->contentAboutRepo->getAboutList('fun'),
            'hadir'=>$this->contentAboutRepo->getAboutList('hadir'),
            'contentAbout'=>$this->contentAboutRepo->getAboutList_row(),
            'banners' => $this->bannerRepo->getBanners()
        ]);
    }
}
