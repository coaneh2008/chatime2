<?php

namespace App\Http\Controllers;

use App\Model\PromotionCategory;
use App\Repositories\FlashSaleRepository;
use App\Repositories\ProductPromotionRepository;
use App\Repositories\PromotionCategoryRepository;
use App\Repositories\PromotionRepository;
use App\Repositories\BannerRepository;

class PromotionController extends Controller
{
    protected $categories;
    protected $flashSaleRepo;
    protected $productPromoRepo;
    protected $promotioRepo;
    protected $promotionCatRepo;
    protected $holidayPromoType;
    protected $memberPromoType;
    protected $promoInfoTye;
    protected $productPromoType;
    protected $flashSaleType;
    protected $bannerRepo;

    public function __construct(
        PromotionCategory $category,
        ProductPromotionRepository $productPromoRepo,
        FlashSaleRepository $flashSaleRepo,
        PromotionRepository $promotionRepo,
        PromotionCategoryRepository $promotionCatRepo
    ) {
        $this->categories = $category;
        $this->flashSaleRepo = $flashSaleRepo;
        $this->productPromoRepo = $productPromoRepo;
        $this->promotionRepo = $promotionRepo;
        $this->promotionCatRepo = $promotionCatRepo;
        $this->holidayPromoType = $this->categories::TYPE_HOLIDAY_PROMO;
        $this->memberPromoType = $this->categories::TYPE_MEMBER_PROMO;
        $this->promoInfoTye = $this->categories::TYPE_PROMO_INFO;
        $this->productPromoType = $this->categories::TYPE_PRODUCT_PROMO;
        $this->flashSaleType = $this->categories::TYPE_FLASH_SALE;
        $this->bannerRepo = \Cacheable::build(BannerRepository::class);
    }

    public function index()
    {
        $holidayPromoCatId = $this->promotionCatRepo
            ->getPromotionCategoryIdByType($this->holidayPromoType);
        $memberPromoCatId = $this->promotionCatRepo
            ->getPromotionCategoryIdByType($this->memberPromoType);
        $promoInfoCatId = $this->promotionCatRepo
            ->getPromotionCategoryIdByType($this->promoInfoTye);
        $productPromoCatId = $this->promotionCatRepo
            ->getPromotionCategoryIdByType($this->productPromoType);
        $flashSaleCatId = $this->promotionCatRepo
            ->getPromotionCategoryIdByType($this->flashSaleType);

        return view('promotions.index', [
            'menu_selected'=>'promo',
             'banners2' => $this->bannerRepo->getBannersCustom('promo',0),
            'promotionCategories' => $this->promotionCatRepo->getCategoryList(),
            'holidayPromo' => $this->promotionRepo->getActivePromoByCategoryId($holidayPromoCatId),
            'memberPromo' => $this->promotionRepo->getActivePromoByCategoryId($memberPromoCatId),
            'promoInfo' => $this->promotionRepo->getActivePromoByCategoryId($promoInfoCatId),
            'productPromos' => $this->productPromoRepo->getActiveProductPromoByCategoryId($productPromoCatId),
            'flashSale' => $this->flashSaleRepo->getActiveFlashSaleByCategoryId($flashSaleCatId)
        ]);
    }

    public function downloadBrochure($promotionId, $type)
    {
        if (in_array($type, [$this->holidayPromoType, $this->memberPromoType, $this->promoInfoTye])) {
            $brochure = $this->promotionRepo->getPromoById($promotionId);
        } elseif ($type = $this->productPromoType) {
            $brochure = $this->productPromoRepo->getProductPromoById($promotionId);
        }

        $filePath = public_path($brochure->file);
        return response()->download($filePath);
    }

    public function list_promo(){
        return view('product.index', [
            'banners' => $this->bannerRepo->getBanners()]);
    }
}
