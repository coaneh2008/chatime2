<?php

namespace App\Http\Controllers;

use App\Repositories\HomePromotionRepository;

class HomePromotionController extends Controller
{
    protected $homePromoRepo;

    public function __construct(HomePromotionRepository $homePromoRepo)
    {
        $this->homePromoRepo = $homePromoRepo;
    }

    public function show($slug)
    {
        $homePromo = $this->homePromoRepo->getHomePromoBySlug($slug);
        
        return view('home-promotions.show', [
            'homePromo' => $homePromo
        ]);
    }
}
