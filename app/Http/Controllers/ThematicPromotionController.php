<?php

namespace App\Http\Controllers;

use App\Repositories\ThematicPromotionRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;
use App\Model\Setting;

class ThematicPromotionController extends Controller
{
    protected $thematicPromotionRepo;
    protected $apiClient;
    protected $promotionProducts;
    protected $isOnGoingPromo;

    public function __construct(ThematicPromotionRepository $thematicPromotionRepo)
    {
        $this->thematicPromotionRepo = $thematicPromotionRepo;
        $this->apiClient = new Client([
            'timeout'  => (int)Setting::get("api-timeout"),
        ]);
        $this->isOnGoingPromo = false;
        $this->promotionProducts = array();
        if(Setting::get("thematic-promotion-api-url")!=""){
            $this->callThematicPromotionAPI(Setting::get("thematic-promotion-api-url"));
        }
    }

    public function index()
    {
        return view('thematic-promotion.index',[
            'smallImageThematicPromotions' => $this->thematicPromotionRepo->getActiveThematicPromotionSmallImageBanner(),
            'bigImageThematicPromotions' => $this->thematicPromotionRepo->getActiveThematicPromotionBigImageBanner(),
            'isOnGoingPromo' => $this->isOnGoingPromo,
            'promotionProducts' => $this->promotionProducts
        ]);
    }

    public function callThematicPromotionAPI($url) {
        try {
            $response = $this->apiClient->request('GET',$url);
        } catch (ServerException $e) {
            return;
        } catch (ConnectException $e) {
            return;
        }

        $contents = json_decode($response->getBody()->getContents());
        $this->isOnGoingPromo = $contents->data->total > 0;

        if($this->isOnGoingPromo){
            for ($i=0; $i < sizeof($contents->data->products) ; $i++) { 
                if($contents->data->products[$i]->is_in_stock){
                    array_push($this->promotionProducts,$contents->data->products[$i]);
                }
            }
        }
    }

    public function show($slug)
    {
        $thematicPromotion = $this->thematicPromotionRepo->getThematicPromotionBySlug($slug);
        return view('thematic-promotion.show', [
            'thematicPromotion' => $thematicPromotion
        ]);
    }
}
