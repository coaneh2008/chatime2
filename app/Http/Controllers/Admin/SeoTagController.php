<?php

namespace App\Http\Controllers\Admin;

use App\Model\Menu;
use App\Model\SeoTag as Model;
use App\Repositories\MenuRepository;

class SeoTagController extends ResourceController
{
    protected $menuRepo;

    protected $rules = [
        'seo_slug' => 'string|unique:seo_tags,seo_slug',
        'content_id' => 'integer|required',
        'page_type' => 'string',
        'seo_title' => 'required|string',
        'seo_description' => 'required|string',
        'opengraph_title' => 'string',
        'opengraph_description' => 'string',
        'twitter_title' => 'string',
        'twitter_description' => 'string',
        'meta_image' => [
            'url',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ]
    ];

    public function __construct(Model $model, MenuRepository $menuRepo)
    {
        parent::__construct($model);
        $this->menuRepo = $menuRepo;
    }

    public function paginateQuery()
    {
        return $this->model->where('page_type', $this->model->setPageType());
    }
    
    protected function formRules()
    {
        if ($this->model->exists) {
            foreach (['seo_slug'] as $key) {
                $this->rules[$key] .= ',' . $this->model->getKey();
            }
        }
        parent::formRules();
    }

    protected function formData()
    {
        parent::formData();
        view()->share('staticPages', $this->menuRepo->staticPageList());
    }

    protected function doSave()
    {
        $this->model->page_type = $this->model->setPageType();
        parent::doSave();
    }

    public function getSlugUrl($key)
    {
        if ($menu = Menu::find($key)) {
            return $menu->url;
        }
        return '';
    }
}
