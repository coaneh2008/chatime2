<?php

namespace App\Http\Controllers\Admin;

use App\Model\InstagramPost as Model;

class InstagramPostController extends ResourceController
{

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
