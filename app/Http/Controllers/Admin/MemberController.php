<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Model\User as Model;

class MemberController extends ResourceController
{
    use ExportTrait;

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function paginateQuery()
    {
        return parent::paginateQuery()->where('group_type', Model::USER);
    }

    public function show($key)
    {
        $this->model = $this->find($key);

        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Not Found!');
            return $this->redirectIndex();
        }

        return view(suitViewName($this->getViewPrefix() . '.show'), [
            'model' => $this->model]);
    }

    protected function exportQuery()
    {
        $startDate = request()->get('start_date');
        $endDate = request()->get('end_date');
        $query = $this->model->newQuery()->where('group_type', Model::USER);
        if ($startDate && $endDate) {
            return $query->where('created_at', '>=', $startDate)
                ->where('created_at', '<=', $endDate);
        } elseif ($startDate) {
            return $query->where('created_at', '>=', $startDate);
        } elseif ($endDate) {
            return $query->where('created_at', '<=', $endDate);
        }
        return $query;
    }
}
