<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\ImportTrait;
use App\Model\MemberLeaderboard as Model;

class MemberLeaderboardController extends ResourceController
{
    use ExportTrait;
    use ImportTrait;
    
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function saveImport($data)
    {
        $data['amount'] = array_pull($data, 'amount_rp');
        $data['transaction'] = array_pull($data, 'transaction_frequency');
        $data['quantity'] = array_pull($data, 'quantity_pieces');
        $model = $this->model->newInstance();
        $model->fill($data);
        $model->save();
    }

    protected function beforeProceed()
    {
        \DB::table('member_leaderboards')->delete();
    }

    protected function deleteAll()
    {
        $this->beforeProceed();
        session()->flash(NOTIF_SUCCESS, 'All data ' . $this->getControllerName() . ' has been deleted.');
        return redirect()->back();
    }
}
