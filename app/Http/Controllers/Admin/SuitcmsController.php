<?php
namespace App\Http\Controllers\Admin;

use App\Model\FeaturedBrand;
use App\Model\InspirationArticle;
use App\Model\InspirationArticleCategory;

class SuitcmsController extends BaseController
{

    /**
     * Create a new Suitcms Controller
     *
     * @return void
     */
    public function __construct()
    {
        //Invoke Base Controller constructor
        parent::__construct();
    }

    public function index()
    {
        view()->share('navDashboard', true);
        $dashboardData = [];
        $dashboardData['inspiration-article-categories'] = [
            'name' => 'Article Categories',
            'count' => InspirationArticleCategory::count(),
            'color' => 'blue',
            'icon' => 'fa fa-newspaper-o',
        ];
        $dashboardData['inspiration-articles'] = [
            'name' => 'Articles',
            'count' => InspirationArticle::count(),
            'color' => 'red',
            'icon' => 'fa fa-file',
        ];
        $dashboardData['featured-brands'] = [
            'name' => 'Featured Brands',
            'count' => FeaturedBrand::count(),
            'color' => 'purple',
            'icon' => 'icon-star',
        ];
        $dashboardData['product-menus'] = [
            'name' => 'Product Menu',
            'count' => FeaturedBrand::count(),
            'color' => 'green',
            'icon' => 'icon-star',
        ];
        return view(suitViewName('index'), ['dashboardData' => $dashboardData]);
    }
}
