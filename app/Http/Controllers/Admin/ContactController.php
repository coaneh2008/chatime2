<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Model\Contact as Model;

class ContactController extends ResourceController
{
    use ExportTrait;

    public function __construct(Model $model)
    {
        $this->routePrefix = 'inbox';
        $this->pageName = 'Inbox';
        parent::__construct($model);
    }

    public function show($key)
    {
        $this->model = $this->find($key);
        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Not Found!');
            return $this->redirectIndex();
        }
        $this->formData();

        return view(suitViewName($this->getViewPrefix() . '.show'), ['model' => $this->model]);
    }
}
