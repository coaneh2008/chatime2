<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Model\Subscriber as Model;

class SubscriberController extends ResourceController
{
    use ExportTrait;

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function show($key)
    {
        $this->model = $this->find($key);

        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Not Found!');
            return $this->redirectIndex();
        }

        return view(suitViewName($this->getViewPrefix() . '.show'), [
            'model' => $this->model]);
    }
}
