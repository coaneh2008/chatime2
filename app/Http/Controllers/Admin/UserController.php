<?php

namespace App\Http\Controllers\Admin;

use App\Model\User as Model;

class UserController extends ResourceController
{
    protected $pageName = 'Admin';

    protected $rules = [
        'username' => 'required|alpha_dash|unique:users,username',
        'email' => 'required|email|unique:users,email',
        'name' => 'required',
        'password' => 'basic_password|confirmed',
        'password_confirmation' => 'string',
        'slug' => 'string|alpha_dash',
        'about' => 'string',
        'date_of_birth' => 'string',
        'active' => 'required|in:0,1',
        'group_type' => 'required'
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function formData()
    {
        view()->share('groups', $this->model->groups());
    }

    protected function beforeValidate()
    {
        parent::beforeValidate();
        $this->form->filterInput('trim', ['username', 'email', 'name', 'slug', 'about']);
        $this->form->filterInput('strtolower', ['username', 'email', 'slug']);
        $this->form->filterInput('e', ['about', 'name']);
    }

    protected function formRules()
    {
        $this->rules['group_type'] .= '|in:' . $this->implode(array_keys(Model::groups()));
        if ($this->model->exists) {
            foreach (['username', 'email'] as $key) {
                $this->rules[$key] .= ',' . $this->model->getKey();
            }
        } else {
            foreach (['password', 'password_confirmation'] as $key) {
                $this->rules[$key] .= '|required';
            }
        }
        parent::formRules();
    }

    protected function doDelete()
    {
        if (!$this->model->isMe()) {
            parent::doDelete();
        }
    }

    protected function paginateQuery()
    {
        return parent::paginateQuery()->where('group_type', Model::SUPER_ADMIN);
    }
}
