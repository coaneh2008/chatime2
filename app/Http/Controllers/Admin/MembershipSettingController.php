<?php

namespace App\Http\Controllers\Admin;

use App\Model\Setting as Model;

class MembershipSettingController extends ResourceController
{
    protected $rules = [
        'title' => 'required|string',
        'description' => 'string',
        'value' => 'string',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function paginateQuery()
    {
        return parent::paginateQuery()->orderBy('name')->where('type', Model::TYPE_MEMBERSHIP);
    }
}
