<?php

namespace App\Http\Controllers\Admin;

use App\Model\Promotion as Model;

class PromotionController extends ResourceController
{
    protected $rules = [
        'category_id' => 'required|integer',
        'content' => 'required|string',
        'start_date' => 'required',
        'end_date' => 'required',
        'file' => [
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF|pdf)$/'
        ],
        'type' => 'required|in:0,1',
        'btn_name' => [
            'required_if:type,==,1',
            'string'
        ],
        'url' => [
            'required_if:type,==,1',
            'string'
        ],
        'published' => 'required|in:0,1'
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function paginateQuery()
    {
        return $this->model->newQuery()->where('category_id', $this->categoryId);
    }

    protected function formData()
    {
        parent::formData();

        view()->share(['categoryId' => $this->categoryId, 'typeList' => Model::typeList()]);
    }

    protected function formRules()
    {
        parent::formRules();
    }
}
