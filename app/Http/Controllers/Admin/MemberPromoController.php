<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\MemberBenefit as Model;
use App\Model\MemberPromo;

class MemberPromoController extends MemberBenefitController
{
    use PublishToggleTrait;

    protected $type = Model::TYPE_MEMBER_PROMO;

    protected $additionalRules = [
        'description' => 'required|string',
        'banner_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'member_promos' => 'array',
        'member_promos.*.title' => 'required|string|max:255',
        // 'member_promos.*.description' => 'required|string',
        'member_promos.*.content' => 'required|string',
        'member_promos.*.thumb_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'member_promos.*.banner_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'member_promos.*.order' => 'integer',
        'member_promos.*.start_date' => 'required|date',
        'member_promos.*.end_date' => 'required|date',
        // 'member_promos.*.slug' => 'alpha_dash|unique:member_promos,slug',
        'member_promos.*.published' => 'required|in:0,1',
        'member_promos.*.is_highlighted' => 'required|in:0,1',
    ];

    protected function doSave()
    {
        parent::doSave();

        $existingIds = [];
        $existingMemberPromos = $this->model->memberPromos;
        if (isset($this->form->all()['member_promos'])) {
            $memberPromos = $this->form->all()['member_promos'];
            foreach ($memberPromos as $memberPromo) {
                $memberPromo['member_benefit_id'] = $this->model->id;
                if (isset($memberPromos['id'])) {
                    $instance = MemberPromo::find($memberPromos['id']);
                } else {
                    $instance = new MemberPromo();
                }
                $instance->fill($memberPromo);
                $instance->save();

                if (!empty($instance->getKey())) {
                    $existingIds[] = $instance->getKey();
                }
            }

            if ($existingMemberPromos) {
                $existingMemberPromos->map(function ($memberPromo) use ($existingIds) {
                    if (!in_array($memberPromo->getKey(), $existingIds)) {
                        $memberPromo->delete();
                    }
                });
            }
        }
    }
}
