<?php

namespace App\Http\Controllers\Admin;

use App\Model\Menu as Model;

class MenuController extends ResourceController
{
    protected $rules = [
        'title' => [
            'required_without:icon_image',
            'string'
        ],
        'order' => 'required|integer',
        'type' => 'required',
        'is_link' => 'required|in:0,1',
        'url' => 'required|max:255',
        'parent_id' => 'string',
        'icon_image' => [
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function formData()
    {
        parent::formData();
        view()->share('parents', ['' => 'No Parent'] + $this->parentList()->all());
        view()->share('types', $this->model->typeListName());
    }

    protected function beforeValidate()
    {
        parent::beforeValidate();
        $this->form->filterInput('strip_tags', ['title']);
        $this->form->filterInput('trim', ['title', 'url']);
    }

    protected function formRules()
    {
        $this->rules['type'] .= '|in:' . $this->implode(array_keys($this->model->typeListName()));
        $this->rules['parent_id'] .= '|in:' . $this->implode($this->parentList()->keys()->all());
        parent::formRules();
    }

    protected function parentList()
    {
        return $this->model->parentListById();
    }

    protected function doSave()
    {
        if (!empty($this->form->input('parent_id'))) {
            $this->model->parent()->associate($this->model->find($this->form->input('parent_id')));
        }
        return parent::doSave();
    }
}
