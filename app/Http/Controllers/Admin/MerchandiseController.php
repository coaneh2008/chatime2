<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\MemberMerchandise as Model;

class MerchandiseController extends ResourceController
{
    use ExportTrait;
    use PublishToggleTrait;
    
    /**
     * Form Rules
     * @var array
     */
    protected $rules = [
        'title' => 'required',
        'point' => 'integer',
        'thumb_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'order' => 'integer',
        'published' => 'required|in:0,1'
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
