<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\MemberBenefit as Model;
use App\Model\MerchantCategory as Category;
use App\Model\MerchantLocation as Location;
use App\Model\MerchantPromo;

class MemberMerchantPromoController extends MemberBenefitController
{
    use PublishToggleTrait;

    protected $type = Model::TYPE_MERCHANT_PROMO;

    protected $additionalRules = [
        'description' => 'required|string',
        'banner_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'merchant_promos' => 'array',
        'merchant_promos.*.title' => 'required|string|max:255',
        'merchant_promos.*.description' => 'required|string',
        'merchant_promos.*.content' => 'required|string',
        'merchant_promos.*.thumb_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'merchant_promos.*.banner_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'merchant_promos.*.order' => 'integer',
        'merchant_promos.*.start_date' => 'required|date',
        'merchant_promos.*.end_date' => 'required|date',
        // 'merchant_promos.*.slug' => 'alpha_dash|unique:merchant_promos,slug',
        'merchant_promos.*.published' => 'required|in:0,1',
        'merchant_promos.*.is_highlighted' => 'required|in:0,1',
        'merchant_promos.*.merchant_category_id' => 'required',
        'merchant_promos.*.merchant_name' => 'required',
        'merchant_promos.*.locations' => 'array',
    ];

    protected function formData()
    {
        parent::formData();
        view()->share('categories', Category::lists('title', 'id'));
        view()->share('locations', Location::lists('title', 'id'));
    }

    protected function doSave()
    {
        parent::doSave();
        $existingIds = [];
        $exsitingPromos = $this->model->merchantPromos;
        if (isset($this->form->all()['merchant_promos'])) {
            $merchantPromos = $this->form->all()['merchant_promos'];
            foreach ($merchantPromos as $merchantPromo) {
                $merchantPromo['member_benefit_id'] = $this->model->id;
                if (isset($merchantPromos['id'])) {
                    $instance = MerchantPromo::find($merchantPromos['id']);
                } else {
                    $instance = new MerchantPromo();
                }
                $instance->fill($merchantPromo);
                $instance->save();

                $locations = [];
                if (isset($merchantPromo['locations'])) {
                    $locations = $merchantPromo['locations'];
                }
                $instance->locations()->sync($locations);

                if (!empty($instance->getKey())) {
                    $existingIds[] = $instance->getKey();
                }
            }

            if ($exsitingPromos) {
                $exsitingPromos->map(function ($merchantPromo) use ($existingIds) {
                    if (!in_array($merchantPromo->getKey(), $existingIds)) {
                        $merchantPromo->delete();
                    }
                });
            }
        }
    }
}
