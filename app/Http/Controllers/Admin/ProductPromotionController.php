<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Jobs\GetProductData;
use App\Model\ProductPromotion as Model;
use App\Model\PromotionCategory;
use App\Repositories\ProductRepository;
use App\Repositories\PromotionCategoryRepository;

class ProductPromotionController extends ResourceController
{
    use ExportTrait;
    use PublishToggleTrait;

    protected $categoryId;
    protected $promoCatRepo;
    protected $type = PromotionCategory::TYPE_PRODUCT_PROMO;
    private $productRepo;

    protected $rules = [
        'category_id' => 'required|integer',
        'url_to_ace_online' => 'required|string',
        'start_date' => 'required',
        'end_date' => 'required',
        'order' => 'integer',
        'published' => 'required|in:0,1'
    ];

    public function __construct(
        Model $model,
        PromotionCategoryRepository $promoCatRepo,
        ProductRepository $productRepo
    ) {
        parent::__construct($model);
        $this->promoCatRepo = $promoCatRepo;
        $this->categoryId = $this->promoCatRepo->getPromotionCategoryIdByType($this->type);
        $this->productRepo = $productRepo;
    }

    protected function paginateQuery()
    {
        return $this->model->newQuery()->where('category_id', $this->categoryId);
    }

    protected function formData()
    {
        parent::formData();

        view()->share('categoryId', $this->categoryId);
        view()->share('urlToAceOnline', $this->model->product_id ? $this->model->product->url_to_ace_online : '');
    }

    protected function store()
    {
        $this->beforeValidate();
        if (!$this->form->validate()) {
            return redirect()->back();
        }
        $this->afterValidate();
        $this->model = $this->model->newInstance();

        $this->doSaveProductData();

        $this->doSave();

        session()->flash(NOTIF_SUCCESS, 'New ' . $this->getControllerName() . ' information created.');
        return $this->redirectIndex();
    }

    protected function update($key)
    {
        $this->model = $this->find($key);

        $this->beforeValidate();
        if (!$this->form->validate()) {
            return redirect()->back();
        }
        $this->afterValidate();

        $this->doSaveProductData();

        $this->doSave();

        session()->flash(NOTIF_SUCCESS, '' . $this->getControllerName() . ' information Updated.');
        return $this->redirectIndex();
    }

    public function doSaveProductData()
    {
        $productPromo = $this->form->all();
        $urlToAceOnline = $productPromo['url_to_ace_online'];
        $product = $this->productRepo->getProductByUrl($urlToAceOnline);
        $product = $product ? $product : $this->productRepo->addNewProduct($urlToAceOnline);
        $this->dispatch(new GetProductData($product));
        $this->model->product_id = $product->id;
        unset($productPromo['url_to_ace_online']);
        $this->model->fill($productPromo);
    }
}
