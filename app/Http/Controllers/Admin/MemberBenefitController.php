<?php

namespace App\Http\Controllers\Admin;

use App\Model\MemberBenefit as Model;

class MemberBenefitController extends ResourceController
{
    protected $rules = [
        'type' => 'required|integer',
        'title' => 'required|string|max:255',
        'thumb_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'order' => 'integer',
        'slug' => 'alpha_dash|unique:member_benefits,slug',
        'published' => 'required|in:0,1',
    ];

    protected function paginateQuery()
    {
        return $this->model->newQuery()->whereType($this->type);
    }

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function formRules()
    {
        $this->rules['type'] .= '|in:' . $this->implode(array_keys($this->model->typeListName()));
        if ($this->model->exists) {
            foreach (['slug'] as $key) {
                $this->rules[$key] .= ',' . $this->model->getKey();
            }
        }
        $this->rules = array_merge($this->rules, $this->additionalRules);
        parent::formRules();
    }

    protected function formData()
    {
        parent::formData();
        view()->share('type', $this->type);
    }
}
