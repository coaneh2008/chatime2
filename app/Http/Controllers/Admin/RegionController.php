<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\Region as Model;

class RegionController extends ResourceController
{
    use ExportTrait;
    use PublishToggleTrait;

    protected $rules = [
        'title' => 'required|string|max:255',
        'published' => 'required|in:0,1',
        'order' => 'integer',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
