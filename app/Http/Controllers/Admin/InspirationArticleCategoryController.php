<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\InspirationArticleCategory as Model;

class InspirationArticleCategoryController extends ResourceController
{
    use ExportTrait;
    use PublishToggleTrait;

    protected $rules = [
        'title' => 'required|string|max:255',
        'published' => 'required|in:0,1',
        'order' => 'integer',
        'slug' => 'alpha_dash|unique:inspiration_article_categories,slug',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function formRules()
    {
        if ($this->model->exists) {
            foreach (['slug'] as $key) {
                $this->rules[$key] .= ',' . $this->model->getKey();
            }
        }
        parent::formRules();
    }
}
