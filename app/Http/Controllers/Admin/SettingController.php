<?php

namespace App\Http\Controllers\Admin;

use App\Model\Setting as Model;
use Illuminate\Http\Request;

class SettingController extends ResourceController
{
    protected $rules = [
        'title' => 'required|string',
        'description' => 'string',
        'value' => 'string',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function paginateQuery()
    {
        return parent::paginateQuery()->orderBy('name')->where('type', Model::TYPE_GENERAL);
    }

    public function getValueByName(Request $request)
    {
        $data = Model::where('name', $request->name)->first();
        return $data ? $data->value : '';
    }
}
