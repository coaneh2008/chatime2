<?php

namespace App\Http\Controllers\Admin;

use App\Model\MerchantCategory as Model;

class MemberMerchantCategoryController extends ResourceController
{
    protected $rules = [
        'title' => 'required|string|max:255',
        'order' => 'integer',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
