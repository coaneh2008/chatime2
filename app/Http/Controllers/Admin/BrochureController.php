<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\Brochure as Model;

class BrochureController extends ResourceController
{
    use ExportTrait;
    use PublishToggleTrait;

    protected $rules = [
        'title' => 'required|string|max:255',
        'start_date' => 'required|date',
        'end_date' => 'date',
        'order' => 'integer',
        'thumb_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'file' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF|pdf)$/'
        ],
        'published' => 'required|in:0,1',
        'slug' => 'alpha_dash|unique:brochures,slug',
        'attachments' => 'array',
        'attachments.*.id' => 'integer',
        'attachments.*.image' => 'required',
        'attachments.*.order' => 'required',
        'page_type' => 'string',
        'seo_title' => 'required|string',
        'seo_description' => 'required|string',
        'opengraph_title' => 'string',
        'opengraph_description' => 'string',
        'twitter_title' => 'string',
        'twitter_description' => 'string',
        'meta_image' => [
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ]
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function formRules()
    {
        if ($this->model->exists) {
            foreach (['slug'] as $key) {
                $this->rules[$key] .= ',' . $this->model->getKey();
            }
        }
        parent::formRules();
    }

    protected function doSave()
    {
        $attachmentIds = [];
        $attachments = $this->model->attachments;
        if ($this->form->has('attachments')) {
            $formAttachments = $this->form->all()['attachments'];
            foreach ($formAttachments as $formAttachment) {
                $attachment = $this->model->setAttachment(
                    $formAttachment['image'],
                    $formAttachment['order']
                );
                if (!empty($attachment->getKey())) {
                    $attachmentIds[] = $attachment->getKey();
                }
            }
        }
        parent::doSave();

        $attachments->map(function ($attachment) use ($attachmentIds) {
            if (!in_array($attachment->getKey(), $attachmentIds)) {
                $attachment->delete();
            }
        });
    }
}
