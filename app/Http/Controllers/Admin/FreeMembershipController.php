<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\Event as Model;
use App\Repositories\EventParticipantRepository;

class FreeMembershipController extends EventController
{
    use ExportTrait;
    use PublishToggleTrait;

    protected $type = Model::TYPE_FREE_MEMBERSHIP;

    protected $participantRepo;

    protected $additionalRules = [];

    public function __construct(Model $model, EventParticipantRepository $participantRepo)
    {
        parent::__construct($model);
        $this->participantRepo = $participantRepo;
    }

    public function show($key)
    {
        $this->model = $this->find($key);
        $participants = $this->participantRepo->getFreeMembershipParticipantByEventId($this->model->id);

        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Not Found!');
            return $this->redirectIndex();
        }

        return view(suitViewName($this->getViewPrefix() . '.show'), [
            'model' => $this->model,
            'participants' => $participants
        ]);
    }

    protected function exportQuery()
    {
        return $this->model->newQuery()->whereType($this->type);
    }

    protected function exportEachQuery()
    {
        $model = $this->model->findByUrlKey($this->key);
        return $this->participantRepo->getFreeMembershipParticipantByEventId($model->id);
    }
}
