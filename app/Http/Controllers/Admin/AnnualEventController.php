<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\Event as Model;
use App\Repositories\EventParticipantRepository;

class AnnualEventController extends EventController
{
    use ExportTrait;
    use PublishToggleTrait;

    protected $type = Model::TYPE_ANNUAL_EVENT;

    protected $participantRepo;

    protected $additionalRules = [
        'total_participant' => 'required|integer'
    ];

    public function __construct(Model $model, EventParticipantRepository $participantRepo)
    {
        parent::__construct($model);
        $this->participantRepo = $participantRepo;
    }

    public function show($key)
    {
        $this->model = $this->find($key);
        $participants = $this->participantRepo->getParticipantByEventId($this->model->id);

        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Not Found!');
            return $this->redirectIndex();
        }

        return view(suitViewName($this->getViewPrefix() . '.show'), [
            'participants' => $participants,
            'model' => $this->model]);
    }

    protected function exportQuery()
    {
        return $this->model->newQuery()->whereType($this->type);
    }

    protected function exportEachQuery()
    {
        $model = $this->model->findByUrlKey($this->key);
        return $this->participantRepo->getParticipantByEventId($model->id);
    }
}
