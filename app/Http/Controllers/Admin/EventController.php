<?php

namespace App\Http\Controllers\Admin;

use App\Model\Event as Model;

class EventController extends ResourceController
{
    protected $rules = [
        'type' => 'required|integer',
        'title' => 'required|string|max:255',
        'description' => 'required|string',
        'content' => 'required|string',
        'thumb_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'banner_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'order' => 'integer',
        'slug' => 'alpha_dash|unique:events,slug',
        'published' => 'required|in:0,1',
        'start_date' => 'required|date',
        'end_date' => 'required|date'
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function paginateQuery()
    {
        return $this->model->newQuery()->whereType($this->type);
    }

    protected function formData()
    {
        parent::formData();
        view()->share('type', $this->type);
    }

    protected function formRules()
    {
        $this->rules['type'] .= '|in:' . $this->implode(array_keys($this->model->typeListName()));
        if ($this->model->exists) {
            foreach (['slug'] as $key) {
                $this->rules[$key] .= ',' . $this->model->getKey();
            }
        }
        $this->rules = array_merge($this->rules, $this->additionalRules);
        parent::formRules();
    }
}
