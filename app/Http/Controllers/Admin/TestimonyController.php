<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\Event as Model;
use App\Repositories\TestimonyRepository;

class TestimonyController extends EventController
{
    use ExportTrait;
    use PublishToggleTrait;

    protected $type = Model::TYPE_TESTIMONY;

    protected $testimonyRepo;

    protected $additionalRules = [];

    public function __construct(Model $model, TestimonyRepository $testimonyRepo)
    {
        parent::__construct($model);
        $this->testimonyRepo = $testimonyRepo;
    }

    public function show($key)
    {
        $this->model = $this->find($key);
        $testimonies = $this->testimonyRepo->getTestimonyByEventId($this->model->id);

        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Not Found!');
            return $this->redirectIndex();
        }

        return view(suitViewName($this->getViewPrefix() . '.show'), [
            'testimonies' => $testimonies,
            'model' => $this->model]);
    }

    protected function exportQuery()
    {
        return $this->model->newQuery()->whereType($this->type);
    }

    protected function exportEachQuery()
    {
        $model = $this->model->findByUrlKey($this->key);
        return $this->testimonyRepo->getTestimonyByEventId($model->id);
    }
}
