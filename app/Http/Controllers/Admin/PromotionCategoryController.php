<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\PromotionCategory as Model;

class PromotionCategoryController extends ResourceController
{
    use ExportTrait;
    use PublishToggleTrait;

    protected $rules = [
        'title' => 'required|string|max:255',
        'order' => 'required|integer',
        /*'type' => 'required',*/
        'slug' => 'alpha_dash|unique:promotion_categories,slug',
        'published' => 'required|in:0,1'
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function formData()
    {
        parent::formData();
        view()->share('typeList', $this->model->typeList());
    }

    protected function formRules()
    {
        if ($this->model->exists) {
            foreach (['slug'] as $key) {
                $this->rules[$key] .= ',' . $this->model->getKey();
            }
        }
        parent::formRules();
    }
    
}
