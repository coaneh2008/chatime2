<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\Page as Model;

class PageController extends ResourceController
{
    use PublishToggleTrait;

    protected $rules = [
        'parent_id' => 'integer',
        'layout' => 'required',
        'title' => 'required|string|max:255',
        'description' => 'string|max:255',
        'content' => 'required|string',
        'url_prefix' => 'string',
        'tags' => 'string',
        'slug' => 'alpha_dash|unique:pages,slug',
        'published' => 'required|in:0,1',
        'attachments' => 'array',
        'attachments.*.id' => 'integer',
        'attachments.*.name' => 'required|alpha_num',
        'attachments.*.uri' => 'required'
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function formData()
    {
        parent::formData();
        view()->share('layouts', $this->model->layoutList());
        view()->share('parents', ['' => 'No Parent'] + $this->parentList()->all());
    }

    protected function formRules()
    {
        if ($this->model->exists) {
            foreach (['slug'] as $key) {
                $this->rules[$key] .= ',' . $this->model->getKey();
            }
        }
        parent::formRules();
    }

    protected function parentList()
    {
        return $this->model->parentListById();
    }

    protected function doSave()
    {
        if (!empty($this->form->input('parent_id'))) {
            $this->model->parent()->associate($this->model->find($this->form->input('parent_id')));
        }

        $attachmentIds = [];
        $attachments = $this->model->attachments;
        if ($this->form->has('attachments')) {
            $formAttachments = $this->form->all()['attachments'];
            foreach ($formAttachments as $formAttachment) {
                $attachment = $this->model->setAttachment($formAttachment['name'], $formAttachment['uri']);
                if (!empty($attachment->getKey())) {
                    $attachmentIds[] = $attachment->getKey();
                }
            }
        }
        parent::doSave();

        $attachments->map(function ($attachment) use ($attachmentIds) {
            if (!in_array($attachment->getKey(), $attachmentIds)) {
                $attachment->delete();
            }
        });
    }
}
