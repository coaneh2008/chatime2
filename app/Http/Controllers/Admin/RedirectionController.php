<?php

namespace App\Http\Controllers\Admin;

use App\Model\Redirection as Model;

class RedirectionController extends ResourceController
{
    /**
     * Form Rules
     * @var array
     */
    protected $rules = [
        'old_path' => 'required|string',
        'new_url' => 'string',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
