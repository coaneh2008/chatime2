<?php

namespace App\Http\Controllers\Admin;

use App\Http\Form\Admin\Form;
use App\Model\BaseModel;
use App\Model\Translation\TranslateModel;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

abstract class ResourceController extends BaseController
{
    protected $defaultRow = 10;

    /**
     * Model instance
     * @var \App\Model\BaseModel
     */
    protected $model;

    /**
     * BaseForm instance
     * @var \App\Http\Form\Admin\BaseForm
     */
    protected $form;

    /**
     * Rules
     * @var array
     */
    protected $rules = [];

    public function __construct(BaseModel $model)
    {
        parent::__construct();

        $this->model = $model;
        $this->form = app(Form::class);

        Paginator::currentPageResolver(function () {
            return request('start', 0) / request('length', $this->defaultRow) + 1;
        });
    }

    /**
     * Index
     * @return \Illuminate\View\View
     */
    protected function index()
    {
        $models = $this->paginate();
        if (\Input::ajax()) {
            $total = $this->paginateQuery()->count();
            return response()->view(
                suitViewName($this->getViewPrefix() . '.index-ajax'),
                compact('models', 'total'),
                200,
                ["Content-Type" => "application/json"]
            );
        }
        return view(suitViewName($this->getViewPrefix() . '.index'), compact('models'));
    }

    /**
     * Get paginated model data
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Contracts\Pagination\Paginator
     */
    protected function paginate()
    {
        if (request()->ajax()) {
            $search = request('search.value');
            $orders = new Collection(request('order'));
            $orders = $orders->lists('dir', 'column');

            $columns = new Collection(request('columns'));
            $columns = $columns->lists('name', 'data');
            foreach ($orders as $key => $order) {
                if (isset($columns[$key])) {
                    $orders[$columns[$key]] = $order;
                }
                unset($orders[$key]);
            }

            $length = request('length', $this->defaultRow);
            if ($length < 0) {
                $length = $this->paginateQuery()->count();
            }
            $scopes = [];
            foreach (request()->query() as $key => $value) {
                if (starts_with($key, '__')) {
                    $key = ltrim($key, '_');
                    $scopes[$key] = $value;
                }
            }
            return $this->paginateQuery()
                        ->filter($search, $orders, $scopes)
                        ->newestCreated()
                        ->paginate($length, [$this->model->getTable() . '.*']);
        }
        return new Collection();
    }

    protected function paginateQuery()
    {
        return $this->model->newQuery();
    }

    /**
     * Create
     * @return \Illuminate\View\View
     */
    protected function create()
    {
        $this->formData();
        return view(suitViewName($this->getViewPrefix() . '.create'), ['model' => $this->model]);
    }
    /**
     * Store
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    protected function store()
    {
        $this->beforeValidate();
        if (!$this->form->validate()) {
            return redirect()->back();
        }
        $this->afterValidate();

        $this->model = $this->model->newInstance();
        $this->model->fill($this->form->all());

        $this->doSave();

        session()->flash(NOTIF_SUCCESS, 'New ' . $this->getControllerName() . ' information created.');
        return $this->redirectIndex();
    }

    protected function redirectIndex()
    {
        return redirect()->route(suitRouteName($this->getRoutePrefix() . '.index'));
    }

    /**
     * Edit
     * @param  string $key
     * @return \Illuminate\View\View|\Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function edit($key)
    {
        $this->model = $this->find($key);
        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Not Found!');
            return $this->redirectIndex();
        }
        $this->formData();

        return view(suitViewName($this->getViewPrefix() . '.edit'), ['model' => $this->model]);
    }

    /**
     * Update
     * @param  string $key
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    protected function update($key)
    {
        $this->model = $this->find($key);

        $this->beforeValidate();
        if (!$this->form->validate()) {
            return redirect()->back();
        }
        $this->afterValidate();

        $this->model->fill($this->form->all());
        $this->doSave();

        session()->flash(NOTIF_SUCCESS, '' . $this->getControllerName() . ' information Updated.');
        return $this->redirectIndex();
    }

    /**
     * Destory
     * @param  string $key
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    protected function destroy($key)
    {
        $this->model = $this->find($key);

        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Not Found!');
            return $this->redirectIndex();
        }

        $this->doDelete();
        session()->flash(NOTIF_SUCCESS, '' . $this->getControllerName() . ' information deleted.');
        return $this->redirectIndex();
    }

    /**
     * Process before validation is being done. Override this method
     * to pass data to baseForm for form requirement,
     * @return void
     */
    protected function beforeValidate()
    {
        $this->formRules();
    }

    protected function formRules()
    {
        $translateRules = [];
        if ($this->model instanceof TranslateModel) {
            $translateFields = $this->model->getTranslateField();
            foreach ($translateFields as $field) {
                if (isset($this->rules[$field])) {
                    $translateRules[$field] = $this->rules[$field];
                    unset($this->rules[$field]);
                }
            }
        }

        $arrayRules = [];
        foreach ($this->rules as $field => $rule) {
            $fieldSegments = explode('.*.', $field);
            if (count($fieldSegments) == 2) {
                $arrayFieldName = $fieldSegments[0];
                $itemFieldName = $fieldSegments[1];
                if (!isset($arrayRules[$arrayFieldName])) {
                    $arrayRules[$arrayFieldName] = [];
                }
                $arrayRules[$arrayFieldName][$itemFieldName] = $rule;
                unset($this->rules[$field]);
            }
        }

        $this->form->setRules($this->rules);
        if (!empty($translateRules)) {
            $this->form->addTranslateRules($translateRules);
        }
        foreach ($arrayRules as $fieldName => $rule) {
            $this->form->addArrayRules($fieldName, $rule);
        }
    }

    /**
     * Process after validation is being done. Override this method
     * for additional process from request input.
     * @return [type] [description]
     */
    protected function afterValidate()
    {
    }

    /**
     * Save Process
     * @return void
     */
    protected function doSave()
    {
        $this->model->save();
    }

    /**
     * Delete Process
     * @return void
     */
    protected function doDelete()
    {
        $this->model->delete();
    }

    /**
<<<<<<< HEAD
=======
     * Get Controller name without 'Controller' postfix
     * @return string
     */
    protected function getControllerName()
    {
        return preg_replace("/(.*)[\\\\](.*)(Controller)/", '$2', get_class($this));
    }

    /**
     * Get View Prefix. By default the value is plurar from and snake case of controller name
     * @return string
     */
    protected function getViewPrefix()
    {
        if ($this->viewPrefix === null) {
            return str_plural(snake_case($this->getControllerName()));
        }
        return $this->viewPrefix;
    }

    /**
     * Get Route Prefix. By default the value is plurar from and snake case of controller name
     * @return string
     */
    protected function getRoutePrefix()
    {
        if ($this->routePrefix === null) {
            return str_plural(snake_case($this->getControllerName(), '-'));
        }
        return $this->routePrefix;
    }

    /**
     * Get Page header for page title.  By default the value is uppercase word and snake case of controller name
     * @return [type] [description]
     */
    protected function getPageName()
    {
        if ($this->pageName === null) {
            $name = snake_case($this->getControllerName(), '-');
            $name = implode(' ', explode('-', $name));
            return ucwords($name);
        }
        return $this->pageName;
    }

    /**
>>>>>>> f1365957ba3f7fe5a99572d4b1c23c5b9d76d72d
     * Find operation
     * @param  string $key
     * @return void
     */
    protected function find($key)
    {
        return $this->paginateQuery()->findOrFailByUrlKey($key);
    }

    /**
     * Data which is needed to be used on form view
     * @return void
     */
    protected function formData()
    {
    }

    /**
     * implode specified data into coma-separated string
     * @param  array $data
     * @return string
     */
    protected function implode($attribute)
    {
        return implode(',', $attribute);
    }
}
