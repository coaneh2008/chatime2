<?php

namespace App\Http\Controllers\Admin;

use App\Model\PromotionCategory;
use App\Model\Promotion as Model;
use App\Repositories\PromotionCategoryRepository;

class MemberPromotionController extends PromotionController
{
    protected $categoryId;
    protected $promoCatRepo;
    protected $type = PromotionCategory::TYPE_MEMBER_PROMO;

    public function __construct(Model $model, PromotionCategoryRepository $promoCatRepo)
    {
        parent::__construct($model);
        $this->promoCatRepo = $promoCatRepo;
        $this->categoryId = $this->promoCatRepo->getPromotionCategoryIdByType($this->type);
    }
}
