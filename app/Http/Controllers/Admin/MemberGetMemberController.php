<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\Event as Model;
use App\Repositories\ReferalCodeRepository;

class MemberGetMemberController extends EventController
{
    use ExportTrait;
    use PublishToggleTrait;

    protected $type = Model::TYPE_MEMBER_GET_MEMBER;

    protected $referalCodeRepo;

    protected $additionalRules = [
        'prefix_referal_code' => 'required|string'
    ];

    public function __construct(Model $model, ReferalCodeRepository $referalCodeRepo)
    {
        parent::__construct($model);
        $this->referalCodeRepo = $referalCodeRepo;
    }

    public function show($key)
    {
        $this->model = $this->find($key);
        $referalCodes = $this->referalCodeRepo->getReferalCodeByEventId($this->model->id);

        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Not Found!');
            return $this->redirectIndex();
        }

        return view(suitViewName($this->getViewPrefix() . '.show'), [
            'referalCodes' => $referalCodes,
            'model' => $this->model]);
    }

    protected function exportQuery()
    {
        return $this->model->newQuery()->whereType($this->type);
    }

    protected function exportEachQuery()
    {
        $model = $this->model->findByUrlKey($this->key);
        return $this->referalCodeRepo->getReferalCodeByEventId($model->id);
    }
}
