<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\PromotionCategory;
use App\Model\Promotion as Model;
use App\Repositories\PromotionCategoryRepository;

class PromotionInformationController extends PromotionController
{
    use ExportTrait;
    use PublishToggleTrait;

    protected $categoryId;
    protected $promoCatRepo;
    protected $type = PromotionCategory::TYPE_PROMO_INFO;

    public function __construct(Model $model, PromotionCategoryRepository $promoCatRepo)
    {
        parent::__construct($model);
        $this->promoCatRepo = $promoCatRepo;
        $this->categoryId = $this->promoCatRepo->getPromotionCategoryIdByType($this->type);
    }

    protected function exportQuery()
    {
        return $this->model->newQuery()->where('category_id', $this->categoryId);
    }
}
