<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\MemberBenefit as Model;

class MemberFreeContentController extends MemberBenefitController
{
    use PublishToggleTrait;

    protected $type = Model::TYPE_FREE_CONTENT;

    protected $additionalRules = [
        'content' => 'required|string',
    ];
}
