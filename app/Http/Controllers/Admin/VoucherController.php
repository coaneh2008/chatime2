<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\MemberVoucher as Model;

class VoucherController extends ResourceController
{
    use ExportTrait;
    use PublishToggleTrait;
    
    /**
     * Form Rules
     * @var array
     */
    protected $rules = [
        'title' => 'required',
        'label' => 'string',
        'value' => 'string',
        'point' => 'integer',
        'content' => 'required',
        'order' => 'integer',
        'published' => 'required|in:0,1'
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
