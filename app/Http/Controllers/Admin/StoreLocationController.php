<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\StoreLocation as Model;

class StoreLocationController extends ResourceController
{
    use ExportTrait;
    use PublishToggleTrait;

    protected $rules = [
        'region_id' => 'required|integer',
        'type' => 'required|in:0,1,2',
        'title' => 'required|string|max:255',
        'address' => 'required|string|max:255',
        'location' => 'required',
        'published' => 'required|in:0,1',
        // 'order' => 'required|integer'
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function formData()
    {
        parent::formData();
        view()->share('regions', $this->model->region()->getRelated()->getRegions());
        view()->share('typeList', $this->model->typeList());
        if (isset($this->model->latitude) && isset($this->model->longitude)) {
            $this->model->location = implode(',', [$this->model->latitude, $this->model->longitude]);
        }
    }

    protected function doSave()
    {
        $data = $this->form->all();
        list($this->model->latitude, $this->model->longitude) = explode(',', $data['location']);
        unset($this->model->location);

        parent::doSave();
    }
}
