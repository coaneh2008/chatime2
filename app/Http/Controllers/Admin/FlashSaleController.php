<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\FlashSale as Model;
use App\Model\PromotionCategory;
use App\Repositories\PromotionCategoryRepository;

class FlashSaleController extends ResourceController
{
    use ExportTrait;
    use PublishToggleTrait;

    protected $categoryId;
    protected $promoCatRepo;
    protected $type = PromotionCategory::TYPE_FLASH_SALE;

    protected $rules = [
        'category_id' => 'required|integer',
        'url_to_ace_online' => 'string',
        'start_date' => 'required',
        'end_date' => 'required',
        'banner_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'published' => 'required|in:0,1'
    ];

    public function __construct(Model $model, PromotionCategoryRepository $promoCatRepo)
    {
        parent::__construct($model);
        $this->promoCatRepo = $promoCatRepo;
        $this->categoryId = $this->promoCatRepo->getPromotionCategoryIdByType($this->type);
    }

    protected function paginateQuery()
    {
        return $this->model->newQuery()->where('category_id', $this->categoryId);
    }

    protected function formData()
    {
        parent::formData();

        view()->share('categoryId', $this->categoryId);
    }
}
