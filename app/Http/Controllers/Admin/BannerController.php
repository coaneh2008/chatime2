<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\Banner as Model;

class BannerController extends ResourceController
{
    use ExportTrait;
    use PublishToggleTrait;

    /**
     * Form Rules
     * @var array
     */
    protected $rules = [
        'published' => 'required|in:0,1',
        'type' => 'in:0,1',
        'judul' => 'string',
        'deskripsi' => 'string',
        'page'=>'string',
        'device'=>'in:0,1',
        'video_url' => [
             'string',
             'url',
             'required_with:type'
        ],
        'image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'order' => 'integer',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function formData()
    {
        parent::formData();
        view()->share(
            'typeLists', ['' => 'None'] + $this->model->typeList()
        );

        view()->share(
            'typeDevice', $this->model->typeDevice()
        );

        view()->share(
            'typePage', $this->model->typePage()
        );
    }
}
