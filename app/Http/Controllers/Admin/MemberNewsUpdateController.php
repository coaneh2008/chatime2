<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\MemberBenefit as Model;
use App\Model\MemberNewsUpdate;

class MemberNewsUpdateController extends MemberBenefitController
{
    use PublishToggleTrait;

    protected $type = Model::TYPE_NEWS_UPDATES;

    protected $additionalRules = [
        'description' => 'required|string',
        'banner_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'member_news_updates' => 'array',
        'member_news_updates.*.title' => 'required|string|max:255',
        'member_news_updates.*.content' => 'required|string',
        'member_news_updates.*.thumb_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'member_news_updates.*.banner_image' => [
            'required',
            'regex:/(?i)\.(?:jpe?g|png|gif|JPE?G|PNG|GIF)$/'
        ],
        'member_news_updates.*.order' => 'integer',
        'member_news_updates.*.start_date' => 'required|date',
        'member_news_updates.*.end_date' => 'required|date',
        // 'member_news_updates.*.slug' => 'alpha_dash|unique:member_news_updates,slug',
        'member_news_updates.*.published' => 'required|in:0,1',
        'member_news_updates.*.is_highlighted' => 'required|in:0,1',
    ];

    protected function doSave()
    {
        parent::doSave();

        $existingIds = [];
        $existingNews = $this->model->newsUpdates;
        if (isset($this->form->all()['member_news_updates'])) {
            $memberNews = $this->form->all()['member_news_updates'];
            foreach ($memberNews as $memberNews) {
                $memberNews['member_benefit_id'] = $this->model->id;
                if (isset($memberNews['id'])) {
                    $instance = MemberNewsUpdate::find($memberNews['id']);
                } else {
                    $instance = new MemberNewsUpdate();
                }
                $instance->fill($memberNews);
                $instance->save();

                if (!empty($instance->getKey())) {
                    $existingIds[] = $instance->getKey();
                }
            }

            if ($existingNews) {
                $existingNews->map(function ($memberNews) use ($existingIds) {
                    if (!in_array($memberNews->getKey(), $existingIds)) {
                        $memberNews->delete();
                    }
                });
            }
        }
    }
}
