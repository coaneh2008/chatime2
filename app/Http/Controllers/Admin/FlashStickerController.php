<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Extension\ExportTrait;
use App\Http\Controllers\Extension\PublishToggleTrait;
use App\Model\FlashSticker as Model;

class FlashStickerController extends ResourceController
{
    use ExportTrait;
    use PublishToggleTrait;

    protected $rules = [
        'color' => 'required|string',
        'url' => 'required|string',
        'content' => 'required|string',
        'start_date' => 'required',
        'end_date' => 'required',
        'order' => 'integer',
        'published' => 'required|in:0,1'
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function formRules()
    {
        parent::formRules();
    }
}
