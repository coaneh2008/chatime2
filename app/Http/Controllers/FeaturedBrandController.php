<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Extension\SeoTagDetailPageTrait;
use App\Model\FeaturedBrand;

class FeaturedBrandController extends Controller
{
    use SeoTagDetailPageTrait;

    protected $brands;

    public function __construct(FeaturedBrand $brands)
    {
        $this->brands = $brands;
    }

    public function show($slug)
    {
        $brand = $this->brands->findOrFailByUrlKey($slug);

        view()->share($this->getSeoTags($brand));

        return view('featured_brands.show', [
            'brand' => $brand
        ]);
    }
}
