<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Extension\SeoTagDetailPageTrait;
use App\Model\InspirationArticle;
use App\Repositories\InspirationArticleCategoryRepository;
use App\Repositories\InspirationArticleRepository;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class InspirationArticleController extends Controller
{
    use SeoTagDetailPageTrait;

    const MAX_PER_PAGE = 3;
    protected $categories;
    protected $inspirations;
    protected $inspirationRepo;

    public function __construct(
        InspirationArticleRepository $inspirationRepo,
        InspirationArticle $inspirations
    ) {
        $this->categories = \Cacheable::build(InspirationArticleCategoryRepository::class);
        $this->inspirations = $inspirations;
        $this->inspirationRepo = $inspirationRepo;
    }

    public function index(Request $request)
    {
        $categories = $this->categories->getCategoryList();

        $currentSlug = $request->slug;
        $month = $request->month;
        $year = $request->year;

        if ($month & $year) {
            $articles = $this->inspirations->getByDateArchived($month, $year);
        } else {
            $articles = $this->inspirations;
        }

        if (!$currentSlug) {
            if (!empty($categories[0]->slug)) {
                $currentSlug = $categories[0]->slug;
            }
        }
        
        if ($currentSlug) {
            $category = $this->categories->getInspirationArticleCategory($currentSlug);
            $articles = $articles->where('category_id', $category->id)->orderBy('published_date', 'desc')->published();
        } else {
            $articles = null;
        }
        
        if ($articles) {
            $articles = $articles->paginate(static::MAX_PER_PAGE)->appends([
                'slug' => $currentSlug
            ]);
        }

        $popularArticles = $this->inspirations->orderBy('viewer', 'desc')
            ->take(5)->get();

        return view('inspirations.index', [
            'articles' => $articles,
            'categories' => $categories,
            'bigInspirationBanners' => $this->inspirations
                ->getInspirationArticleByImageType($this->inspirations::TYPE_BIG_IMAGE),
            'smallInspirationBanners' => $this->inspirations
                ->getInspirationArticleByImageType($this->inspirations::TYPE_SMALL_IMAGE),
            'archivedYear' => $this->inspirations->getArchivedYear(),
            'popularArticles' => $popularArticles
        ]);
    }

    public function paginate($items, $perPage = 15, $page = null)
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, [
            'path' => env('APP_URL') . str_replace('http://acehardware.growvalley.growmint.com', '', request()->url()),
            'query' => request()->query(),
        ]);
    }

    public function show($slug)
    {
        
        $article = $this->inspirations->published()->with(['inspirationArticleCategory', 'user', 'tags'])->findOrFailByUrlKey($slug);
        $articles = $this->inspirationRepo->getNextArticles($article);
        $this->inspirationRepo->setNextArticles($articles, $slug);

        \DB::table('inspiration_articles')->where('slug', $slug)->increment('viewer');

        $mostReadArticles = $this->inspirations->orderBy('viewer', 'desc')
            ->take(2)->get();
        $relatedArticles = $this->getRelatedArticles($article);
        view()->share($this->getSeoTags($article));
        return view('inspirations.show', [
            'article' => $article,
            'relatedArticles' => $relatedArticles,
            'mostReadArticles' => $mostReadArticles,
            'nextArticle' => count($articles) ? $articles[0] : null
        ]);
    }

    protected function getRelatedArticles($article)
    {
        $articleTagIds = $article->tags->pluck('id')->toArray();
        $relatedArticles = $this->inspirations
            ->where('id', '<>', $article->id)
            ->whereHas('tags', function ($q) use ($articleTagIds) {
                $q->whereIn('tags.id', $articleTagIds);
            })->take(2)->get();

        if (count($relatedArticles) == 0) {
            $relatedArticles = $this->inspirations
                ->where('id', '<>', $article->id)
                ->where('category_id', $article->category_id)
                ->take(2)->get();
        }
        return $relatedArticles;
    }

    public function nextArticleData($counter, $slug)
    {
        $articles = $this->inspirationRepo->getNextArticle($slug);
        $nextArticle = $articles[$counter];
        if ($counter == count($articles) - 1) {
            $next = null;
        } else {
            $next = route('frontend.inspiration.next-article-data', ['counter' => $counter + 1, 'slug' => $slug]);
        }
        $data = $this->transform($nextArticle, $next);

        return json_encode($data);
    }

    public function transform($article, $next)
    {
        $title = $article->title;
        $relatedProducts = \App\Model\InspirationArticle::getInspirationArticleAttachments($article->id);

        $view = \View::make('inspirations.data-content', [
            'article' => $article,
            'relatedProducts' => $relatedProducts
        ]);
        $content = $view->render();

        $data = [
            'title' => $title,
            'content' => $content,
            'next' => $next
        ];
        return $data;
    }
}
