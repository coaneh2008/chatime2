<?php

namespace App\Http\Controllers;

use App\Jobs\GetCustomerData;
use App\Model\CustomerPoint;
use App\Model\Event;
use App\Model\FreeMembershipUser;
use App\Model\User;
use App\Repositories\Api\CheckEmailAndCardApiRepository;
use App\Repositories\Api\LoginApiRepository;
use App\Repositories\Api\PaymentApiRepository;
use App\Repositories\Api\RegisterApiRepository;
use App\Repositories\CityRepository;
use App\Repositories\CustomerPointRepository;
use App\Repositories\EventRepository;
use App\Repositories\MemberBenefitRepository;
use App\Repositories\ReferalCodeRepository;
use App\Repositories\UserRepository;
use Auth;
use Illuminate\Http\Request;

class MembershipController extends Controller
{
    protected $cityRepo;
    protected $eventRepo;
    protected $userRepo;
    protected $paymentRepo;
    protected $pointRepo;
    protected $referalRepo;

    public function __construct(
        CityRepository $cityRepo,
        EventRepository $eventRepo,
        UserRepository $userRepo,
        MemberBenefitRepository $memberBenefitRepo,
        PaymentApiRepository $paymentRepo,
        CustomerPointRepository $pointRepo,
        ReferalCodeRepository $referalRepo
    ) {
        $this->cityRepo = $cityRepo;
        $this->eventRepo = $eventRepo;
        $this->userRepo = $userRepo;
        $this->memberBenefitRepo = $memberBenefitRepo;
        $this->paymentRepo = $paymentRepo;
        $this->pointRepo = $pointRepo;
        $this->referalRepo = $referalRepo;
    }
    public function index()
    {
        $data['benefits'] = $this->memberBenefitRepo->getBenefits();
        return view('memberships.index', $data);
    }

    public function voucher()
    {
        $data['vouchers'] = $this->memberBenefitRepo->getVoucherLists();
        return view('memberships.voucher-list', $data);
    }

    public function merchandises()
    {
        $data['merchandises'] = $this->memberBenefitRepo->getMerchandiseLists();
        return view('memberships.merchandise-list', $data);
    }

    public function memberBenefit($slug)
    {
        $data = $this->memberBenefitRepo->getMemberBenefitPromoLists(
            $slug,
            request()->get('category'),
            request()->get('location')
        );
        return view('memberships.member-benefit-list', $data);
    }

    public function memberBenefitDetail($slug, $detailSlug)
    {
        $data = $this->memberBenefitRepo->getMemberBenefitDetail($slug, $detailSlug);
        return view('memberships.member-benefit-detail', $data);
    }

    public function login()
    {
        return view('memberships.login');
    }

    public function register()
    {
        $shoppingPurpose = \Setting::get('membership-register-shopping-purpose');
        if(!is_array($shoppingPurpose)){
            $shoppingPurpose = explode(',', $shoppingPurpose);
        }

        if (!$shoppingPurpose) {
            $shoppingPurpose = $this->userRepo->getShoppingPurposeList();
        }
        return view('memberships.register', [
            'citizenships' => $this->userRepo->getCitizenships(),
            'cities' => $this->cityRepo->getCities(),
            'shoppingPurpose' => $shoppingPurpose,
            'occupations' => $this->userRepo->getOccupations(),
            'maritalStatus' => $this->userRepo->getMaritalStatus(),
            'religions' => $this->userRepo->getReligions(),
            'sexTypes' => $this->userRepo->getSexTypes()
            ]);
    }

    public function registerStep2()
    {
        if ($this->userRepo->getNewMemberDataStep1()) {
            $mGetMEvent = $this->eventRepo->getActiveFreeMembershipEvent();

            return view('memberships.register-step-2', [
                'administrationTypes' => $this->userRepo->getAdministrationTypes(),
                'renewalTypes' => $this->userRepo->getRenewalTypes(),
                'mGetMEvent' => $mGetMEvent
            ]);
        } else {
            return view('errors.404');
        }
    }

    public function doRegisterStep1(Request $request)
    {
        $email = $request->email;

        $formatedDate = \Carbon\Carbon::parse($request->date_of_birth)->format('m/d/Y');

        $newMemberData = new User;
        $newMemberData->name = $request->name;
        $newMemberData->email = $email;
        $newMemberData->current_address_city_id = $request->current_address_city_id;
        $newMemberData->ktp_address_city_id = $request->ktp_address_city_id;
        $newMemberData->place_of_birth = $request->place_of_birth;
        $newMemberData->date_of_birth = $formatedDate;
        $newMemberData->current_address_zip_code = $request->current_address_zip_code;
        $newMemberData->current_address = $request->current_address;
        $newMemberData->ktp_address_zip_code = $request->ktp_address_zip_code;
        $newMemberData->ktp_address = $request->ktp_address;
        $newMemberData->identity_card_id = $request->identity_card_id;
        $newMemberData->cellphone = $request->cellphone;
        $newMemberData->telephone = $request->telephone;
        $newMemberData->gender = $request->gender;
        $newMemberData->religion_id = $request->religion_id;
        $newMemberData->marital_status = $request->marital_status;
        $newMemberData->citizenship_id = $request->citizenship_id;
        $newMemberData->occupation_id = $request->occupation_id;
        $newMemberData->shopping_purpose = $request->shopping_purpose;

        $customMessages = [
            'required' => ':attribute tidak boleh kosong',
            'email' => ':attribute tidak valid',
            'unique' => "Email telah terdaftar",
            'max' => ":attribute tidak boleh melebihi :max karakter",
            "min" => ":attribute setidaknya :min karakter",
            'same' => ":attribute dan :other harus sama",
            "required_with" => ":attribute harus diisi sesuai dengan :values",
        ];

        $this->validate($request, [
            'name' => 'required|required|max:255',
            'email' => [
                'required',
                'email_ace',
                'email_dotco',
                'regex:/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.
                [\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.
                [a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.
                |[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.
                ){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/'
            ],
            'confirm_email' => 'required_with:email|same:email',
            'place_of_birth' => 'string|max:100',
            'date_of_birth' => 'required',
            'current_address_zip_code' => 'string|max:15',
            'current_address' => 'string|max:100',
            'ktp_address_zip_code' => 'string|max:15',
            'ktp_address' => 'string|max:100',
            'identity_card_id' => 'required|string|max:60',
            'cellphone' => [
                'required',
                'string',
                'min:9',
                'regex:/\(?(?:\+62|62|0)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}/'
            ],
            'telephone' => 'string|min:9',
            'gender' => 'integer|in:0,1',
            'religion_id' => 'integer|in:1,2,3,4,5,6,7',
            'marital_status' => 'integer|in:1,2,3',
            'citizenship_id' => 'integer|in:1,2',
            'occupation_id' => 'integer|in:1,2,3,4,5,6',
            'shopping_purpose' => 'string|max:100',
        ], $customMessages);

        $this->storeNewMemberDataStep1($newMemberData);
        return redirect()->route('frontend.membership.register-step-2');
    }

    public function storeNewMemberDataStep1($newMemberData)
    {
        $this->userRepo->setNewMemberDataStep1($newMemberData);
    }

    public function doRegisterStep2(Request $request)
    {
        $this->validate($request, [
            'referal_id' => 'string|referal_ace',
        ]);
        $eventId = $request->event_id;
        $newMemberData = $this->userRepo->getNewMemberDataStep1();
        $newMemberData->administration_type = $request->administration_type;
        $newMemberData->is_renewal = $request->is_renewal;
        $newMemberData->referal_id = $request->referal_id;
        $newMemberData->reference_date = \Carbon\Carbon::now()->format('m/d/Y');
        $response = $this->doSave($eventId, $newMemberData);
        if ($response) {
            if($response->is_ok == 'false'){
                return redirect()
                    ->back()
                    ->with(session()
                    ->flash(NOTIF_DANGER, $response->message));
            }
            return redirect()
                ->route('frontend.membership.register-success', $response->email)
                ->with(session()
                ->flash(NOTIF_SUCCESS, trans('Please check your email for account activation')));
        } else {
            return redirect()
                ->back()
                ->with(session()
                ->flash(NOTIF_DANGER, 'Sorry, something went wrong'));
        }
    }

    public function doSave($eventId, $data)
    {
        $formatedDate = \Carbon\Carbon::parse($data->date_of_birth)->format('Y-m-d');
        $city = $this->cityRepo->getCityById($data->current_address_city_id);
        if ($city) {
            $cityCode = $city->code;
        } else {
            $cityCode = '';
        }

        $freeMembershipFlag = ($data->administration_type == 2) ? 1 : 0;

        $message = "{'P_Company':'AHI',
            'P_Cust_Name':'" . $data->name . "',
            'P_Cust_Add':'" . $data->current_address . "',
            'P_Cust_Hp':'" . $data->cellphone . "',
            'P_Cust_Mail':'" . $data->email . "',
            'P_Cust_Birth_Date':'" . $data->date_of_birth . "',
            'P_Cust_Birth_Place':'" . $data->place_of_birth . "',
            'P_Cust_Sex':'" . $data->gender . "',
            'P_Cust_City':'" . $cityCode . "',
            'P_Cust_PosCode':'" . $data->current_address_zip_code . "',
            'P_Cust_Add_Ktp':'" . $data->ktp_address . "',
            'P_Cust_Phone':'" . $data->cellphone . "',
            'P_Cust_IdCard':'" . $data->identity_card_id . "',
            'P_Cust_Religion':'" . $data->religion_id . "',
            'P_Cust_Stat_Marital':'" . $data->marital_status . "',
            'P_Cust_citizenship':'" . $data->citizenship_id . "',
            'P_Cust_Employ':'" . $data->occupation_id . "',
            'P_Cust_Purpose':'" . $data->shopping_purpose . "',
            'P_Cust_AutoRen':'" . $data->is_renewal . "',
            'P_Card_Ref':'" . $data->referal_id . "',
            'P_Reference_Date':'" . $data->reference_date . "',
            'P_Cust_FreeF':'" . $freeMembershipFlag . "'}";

        $userApiRepo = new RegisterApiRepository();
        $response = $userApiRepo->register($message);
        if ($response) {
            if ($response->is_ok == 'true') {
                $password = $response->rows[0]->Passkey;
                $data->date_of_birth = $formatedDate;
                $data->member_id = $response->rows[0]->Cust_Id;
                $data->username = $response->rows[0]->Card;
                $data->password = $password;
                $data->save();
                if ($data->save()) {
                    if ($data->administration_type == 2) {
                        FreeMembershipUSer::create([
                            'user_id' => $data->id,
                            'event_id' => $eventId,
                            'status' => 0
                        ]);
                    }
                }

                $this->sendMailReply($data, $password);
                return $data;
            }else{
                return $response;
            }
        }
        return 0;
    }

    public function sendMailReply($user, $password)
    {
        \Mail::queue(
            'memberships.send-email-activation',
            [
                'user' => $user,
                'password' => $password
            ],
            function ($message) use ($user, $password) {
                $message->to($user->email);
                $message->subject('Account Activation | Ace Hardware Indonesia');
            }
        );
    }

    public function doLogin(Request $request)
    {
        $customMessages = [
            'required' => ':attribute tidak boleh kosong.'
        ];

        $this->validate($request, [
            'no_kartu_member' => 'required',
            'passkey' => 'required'
        ], $customMessages);

        $username = $request->no_kartu_member;
        $password = $request->passkey;

        $message = "{'P_Card_Id':'" . $username . "','P_Passkey':'" . $password . "'}";
        $loginApiRepo = new LoginApiRepository();
        $response = $loginApiRepo->login($message);
        if ($response) {
            if ($response->is_ok == 'true') {
                $data = $response->rows;
                $token = $data[0]->token;
                $memberId = $data[0]->cust_id;
                $userData = [
                    'username' => $username,
                    'password' => $password,
                    'member_id' => $memberId
                ];

                User::updateOrCreate(['member_id' => $memberId], $userData);

                Auth::attempt(['username' => $username, 'password' => $password]);

                $user = Auth::user();

                $this->userRepo->setUserToken($user->id, $token);
                
                $this->dispatch(new GetCustomerData($user));
                if (session()->has('REDIRECT_TO')) {
                    $redirectUrl = session()->pull('REDIRECT_TO');
                    return redirect()->away($redirectUrl);
                }
                return Redirect(route('frontend.membership.dashboard'));
            } else {
                return redirect()->back()
                    ->with(session()
                    ->flash(NOTIF_DANGER, $response->message));
            }
        } else {
            return redirect()->back()
                ->with(session()
                ->flash(NOTIF_DANGER, 'Sorry, something went wrong'));
        }
    }

    public function logout()
    {
        session()->flush();
        Auth::logout();
        return Redirect(route('frontend.membership.login'));
    }

    public function dashboard()
    {
        $user = Auth::user();
        $this->dispatch(new GetCustomerData($user));
        $customerPoint = $this->pointRepo->getCustomerPointByUserId($user->id);
        $cuttingPoint = \Setting::get('total-point-for-renewal-account');

        $now = \Carbon\Carbon::now();
        $addMonth = $now->addMonth();
        $exipiryDate = $user->expiry_date;

        $initialCounter = 0;
        $counter = $this->userRepo->getAccountStatus($user->id);
        $pointCounter = $this->userRepo->getPointStatus($user->id);

        $this->userRepo->setAccountStatus($user->id, $counter ? $counter : $initialCounter);
        $this->userRepo->setPointStatus($user->id, $pointCounter ? $pointCounter : $initialCounter);

        $accountStatus = $this->userRepo->getAccountStatus($user->id);
        $pointStatus = $this->userRepo->getPointStatus($user->id);
        
        $adminType = $user->administration_type;
        $tempAccount = str_contains($user->username, 'TAM');
        $followFreeMembership = (($adminType == 2) && $tempAccount);
        $isFreeMembership = ($followFreeMembership) ? true : false;
        $isRenewal = ($user->is_renewal == 1) ? true : false;
        $rawPoint = $customerPoint->total_point;
        $totalPoint = explode(' ', $rawPoint, 2)[0];
        $havePoint = ($totalPoint > $cuttingPoint) ? true : false;

        $isRenewalHavePoint = ($isRenewal && $havePoint) ? true: false;
        $isRenewalNoPoint = ($isRenewal && !$havePoint) ? true : false;
        $noRenewalHavePoint = (!$isRenewal && $havePoint) ? true : false;
        $noRenewalNoPoint = (!$isRenewal && !$havePoint) ? true : false;

        $btnPayIsShow = ($isRenewalHavePoint || !$isFreeMembership) ? true : false;
    
        if ($isFreeMembership) {
            $notifWarning = \Setting::get('member-account-renewal-alert-for-free-membership');
        } else {
            if ($isRenewalNoPoint) {
                $notifWarning = \Setting::get('member-account-auto-renewal-not-enough-point-alert');
            } elseif ($noRenewalHavePoint) {
                $notifWarning = \Setting::get('member-account-manual-renewal-enough-point-alert');
            } elseif ($noRenewalNoPoint) {
                $notifWarning = \Setting::get('member-account-manual-renewal-not-enough-point-alert');
            }
        }

        if ($accountStatus == 1) {
            if (($addMonth > $exipiryDate) && !$isRenewalHavePoint) {
                session()->flash(NOTIF_WARNING, $notifWarning);
            }
        }
        $expiryPointDate = $customerPoint->expiry_date;
        $expiryPoint = $customerPoint->expiry_point;
        $notifExpiryPoint = \Setting::get('point-expiry-alert');
        if ($pointStatus == 1) {
            if (($addMonth > $expiryPointDate) && ($expiryPoint > 0)) {
                session()->flash('NOTIF_WARNING_POINT_EXPIRED', $notifExpiryPoint);
            }
        }
        return view('memberships.dashboard', [
            'events' => $this->eventRepo->getActiveEvent(),
            'accountStatus' => $accountStatus,
            'btnPayIsShow' => $btnPayIsShow,
            'havePoint' => $havePoint,
            'followFreeMembership' => $followFreeMembership,
            'adminType' => $adminType,
            'tempAccount' => $tempAccount
        ]);
    }

    public function registerSuccess($key)
    {
        $user = User::where('email', $key)->first();
        if ($user) {
            $type = \App\Model\MemberBenefit::TYPE_MEMBER_PROMO;
            $benefit = $this->memberBenefitRepo->getBenefitsByType($type);

            return view('memberships.register-success', [
                'adminType' => $user->administration_type,
                'benefit' => $benefit,
                'isRenewal' => $user->is_renewal
            ]);
        } else {
            return view('errors.404');
        }
    }
}