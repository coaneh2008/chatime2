<?php

use Suitcms\Model\Menu;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/test-email', function () {
    \Artisan::call('test:email', ['email' => request()->get('email')]);
    return redirect()->back();
});
Route::group(['middleware' => 'seotag'], function () {
    Route::get('/renewal', [
            'uses' => 'RenewalController@renewAccount',
            'as' => 'frontend.membership.renew-account'
        ]);
    Route::get('/', function () {
        return View::make('menu', ['menus' => App\Model\Menu::getSecondaryMenu()]);
    });
    Route::get('/', [
        'uses' => 'HomeController@index',
        'as' => 'frontend.home.index'
    ]);
    Route::post('/subscribe', ['as' => 'frontend.subscribe', 'uses' => 'SubscriberController@store']);

    //Route::post('addActionPO','ProjectController@add_po_action');

    Route::get('/product-selection', [
        'uses' => 'HomeController@showProductSelection',
        'as' => 'frontend.menu.product-selection'
    ]);
    Route::get('/search', ['as' => 'search', 'uses' => 'SearchController@search']);
    Route::get('brands/{slug}', [
        'uses' => 'FeaturedBrandController@show',
        'as' => 'frontend.brand.show'
    ]);
    Route::group(['prefix' => 'promotions'], function () {
        Route::get('/', [
            'uses' => 'PromotionController@index',
            'as' => 'frontend.promotion.index'
        ]);

        Route::get('/{slug}/{mobile}', [
            'uses' => 'PromotionController@filter_sort_ajax',
            'as' => 'frontend.promotion.index'
        ]);

        Route::get('/promotion-brochure/download/{id}/{type}', [
            'uses' => 'PromotionController@downloadBrochure',
            'as' => 'frontend.promotion-brochure.download'
        ]);
    });
    Route::group(['prefix' => 'news'], function () {
        Route::get('/', [
            'uses' => 'EventNewsController@index',
            'as' => 'frontend.new.index'
        ]);

        Route::get('/category/{slug}', [
            'uses' => 'EventNewsController@filter_sort_ajax',
            'as' => 'frontend.new.index'
        ]);

        Route::get('/category/{slug}/{sort}', [
            'uses' => 'EventNewsController@filter_sort_ajax',
            'as' => 'frontend.new.index'
        ]);

        Route::get('/{slug}', [
            'uses' => 'EventNewsController@show',
            'as' => 'frontend.new.show'
        ]);

        Route::get('/promotion-brochure/download/{id}/{type}', [
            'uses' => 'EventNewsController@downloadBrochure',
            'as' => 'frontend.promotion-brochure.download'
        ]);
    });
    Route::group(['prefix' => 'thematic-promotion'], function () {
        Route::get('/', [
            'uses' => 'ThematicPromotionController@index',
            'as' => 'frontend.thematic-promotion.index'
        ]);
        Route::get('/{slug}', [
            'uses' => 'ThematicPromotionController@show',
            'as' => 'frontend.thematic-promotion.show'
        ]);
    });
    Route::get('home-promotion/{slug}', [
        'uses' => 'HomePromotionController@show',
        'as' => 'frontend.home-promotion.show'
    ]);
    Route::get('product-categories/{slug}', [
        'uses' => 'ProductCategoryController@show',
        'as' => 'frontend.product-category.show'
    ]);
    Route::group(['prefix' => 'inspirations'], function () {
        Route::get('/', [
            'uses' => 'InspirationArticleController@index',
            'as' => 'frontend.inspiration.index'
        ]);
        Route::get('/{slug}', [
            'uses' => 'InspirationArticleController@show',
            'as' => 'frontend.inspiration.show'
        ]);
        Route::get('/next-article-data/{counter}/{slug}', [
            'uses' => 'InspirationArticleController@nextArticleData',
            'as' => 'frontend.inspiration.next-article-data'
        ]);
    });
    Route::group(['prefix' => 'brochures'], function () {
        Route::get('/', [
            'uses' => 'BrochureController@index',
            'as' => 'frontend.brochure.index'
        ]);
        Route::get('/{slug}', [
            'uses' => 'BrochureController@show',
            'as' => 'frontend.brochure.show'
        ]);
        Route::get('download/{slug}', [
            'uses' => 'BrochureController@download',
            'as' => 'frontend.brochure.download'
        ]);
    });
    Route::group(['prefix' => 'contact-us'], function () {
        Route::get('/', [
            'uses' => 'ContactUsController@index',
            'as' => 'frontend.contact-us.index'
        ]);
        Route::post('/send-message', [
            'uses' => 'ContactUsController@store',
            'as' => 'frontend.contact-us.send-message'
        ]);
    });
    Route::group(['prefix' => 'social-wall'], function () {
        Route::get('/', [
            'uses' => 'SocialWallController@index',
            'as' => 'frontend.social-wall.index'
        ]);
    });

    Route::group(['prefix' => 'membership'], function () {
        Route::get('/', [
            'uses' => 'MembershipController@index',
            'as' => 'frontend.membership.index'
        ]);
        Route::get('/forgot-passkey', [
            'uses' => 'PasswordController@forgotPasskey',
            'as' => 'frontend.membership.forgot-passkey'
        ]);
        Route::post('/do-forgot-passkey', [
            'uses' => 'PasswordController@doForgotPasskey',
            'as' => 'frontend.membership.do-forgot-passkey'
        ]);
        Route::get('/voucher-list', [
            'uses' => 'MembershipController@voucher',
            'as' => 'frontend.membership.voucher'
        ]);
        Route::get('/merchandises', [
            'uses' => 'MembershipController@merchandises',
            'as' => 'frontend.membership.merchandise'
        ]);
        Route::get('/member-benefit/{slug}', [
            'uses' => 'MembershipController@memberBenefit',
            'as' => 'frontend.membership.benefit-index'
        ]);
        Route::get('/member-benefit/{slug}/{detailSlug}', [
            'uses' => 'MembershipController@memberBenefitDetail',
            'as' => 'frontend.membership.benefit-detail'
        ]);
        Route::group(['middleware' => 'guest'], function () {
            Route::get('/login', [
                'uses' => 'MembershipController@login',
                'as' => 'frontend.membership.login'
            ]);
            Route::post('/do-login', [
                'uses' => 'MembershipController@doLogin',
                'as' => 'frontend.membership.do-login'
            ]);
            Route::get('/register', [
                'uses' => 'MembershipController@register',
                'as' => 'frontend.membership.register'
            ]);
            Route::post('/do-register', [
                'uses' => 'MembershipController@doRegisterStep1',
                'as' => 'frontend.membership.do-register-step-1'
            ]);
            Route::get('/register-step-2', [
                'uses' => 'MembershipController@registerStep2',
                'as' => 'frontend.membership.register-step-2'
            ]);
            Route::post('/do-register-step-2', [
                'uses' => 'MembershipController@doRegisterStep2',
                'as' => 'frontend.membership.do-register-step-2'
            ]);
            Route::get('/register-success/{key}', [
                'uses' => 'MembershipController@registerSuccess',
                'as' => 'frontend.membership.register-success'
            ]);
        });
    });

    Route::group(['prefix' => 'membership'], function () {
        Route::get('verified-email/{id}/{email}', ['uses' => 'UserController@verified_email']);
        Route::group(['middleware' => 'membership.auth'], function () {
            Route::get('/pay-via-ace-online', [
                'uses' => 'PaymentController@payViaAceOnline',
                'as' => 'frontend.payment.pay-via-ace-online'
            ]);
            Route::group(['prefix' => 'auto-renewal'], function () {
                Route::get('/yes/{val}', [
                    'uses' => 'UserController@autoRenewal',
                    'as' => 'frontend.auto-renewal.yes'
                ]);
                Route::get('/no/{val}', [
                    'uses' => 'UserController@autoRenewal',
                    'as' => 'frontend.auto-renewal.no'
                ]);
            });
            Route::get('logout', [
                'uses' => 'MembershipController@logout',
                'as' => 'frontend.user.logout'
            ]);
            Route::get('/dashboard', [
                'uses' => 'MembershipController@dashboard',
                'as' => 'frontend.membership.dashboard'
            ]);
            Route::group(['prefix' => 'profile'], function () {
                Route::get('/edit', [
                    'uses' => 'UserController@editProfile',
                    'as' => 'frontend.membership.edit-profile'
                ]);
                Route::post('do-profile-edit', [
                    'uses' => 'UserController@doEditProfile',
                    'as' => 'frontend.membership.do-edit-profile'
                ]);
                Route::get('/edit-passkey', [
                    'uses' => 'UserController@editPasskey',
                    'as' => 'frontend.membership.edit-passkey'
                ]);
                Route::post('/do-edit-passkey', [
                    'uses' => 'UserController@doEditPasskey',
                    'as' => 'frontend.membership.do-edit-passkey'
                ]);
            });
            Route::group(['prefix' => 'events'], function () {
                Route::get('/', [
                    'uses' => 'EventController@index',
                    'as' => 'frontend.events.index'
                ]);
                Route::get('/{slug}', [
                    'uses' => 'EventController@show',
                    'as' => 'frontend.events.show'
                ]);
                Route::post('add-event-response', [
                    'uses' => 'EventController@addEventResponse',
                    'as' => 'frontend.events.add-event-response'
                ]);
                Route::get('/participation/confirm/{email}/{slug}', [
                    'uses' => 'EventController@confirmEventParticipation',
                    'as' => 'frontend.events.confirm-event-participation'
                ]);
                Route::get('{slug}/add-your-testimony', [
                    'uses' => 'EventController@showTestimony',
                    'as' => 'frontend.events.show-testimony'
                ]);
                Route::get('{slug}/detail', [
                    'uses' => 'EventController@showFreeMembership',
                    'as' => 'frontend.events.show-free-membership'
                ]);
                Route::get('{slug}/share-your-referal', [
                    'uses' => 'EventController@showMemberGetMember',
                    'as' => 'frontend.events.show-member-get-member'
                ]);
                Route::post('/add-testimony', [
                    'uses' => 'EventController@addTestimony',
                    'as' => 'frontend.events.add-testimony'
                ]);
            });
            Route::group(['prefix' => 'leaderboard'], function () {
                Route::get('/', [
                'uses' => 'LeaderboardController@index',
                'as' => 'frontend.leaderboard.index'
                ]);
                Route::get('/{code}/{name}', [
                    'uses' => 'LeaderboardController@show',
                    'as' => 'frontend.leaderboard.show'
                ]);
            });
            Route::group(['prefix' => '/followed-events'], function () {
                Route::get('/', [
                    'uses' => 'EventController@followedEvent',
                    'as' => 'frontend.followed-events.index'
                ]);
                Route::get('/{slug}', [
                    'uses' => 'EventController@showFollowedEvent',
                    'as' => 'frontend.followed-events.show-followed-event'
                ]);
                Route::post('add-followed-event-response', [
                    'uses' => 'EventController@addFollowedEventResponse',
                    'as' => 'frontend.followed-events.add-followed-event-response'
                ]);
                Route::get('{slug}/detail', [
                    'uses' => 'EventController@showFollowedFreeMembership',
                    'as' => 'frontend.followed-events.show-free-membership'
                ]);
                Route::get('{slug}/share-your-referal', [
                    'uses' => 'EventController@showFollowedMemberGetMember',
                    'as' => 'frontend.followed-events.show-member-get-member'
                ]);
            });
            Route::get('/transaction-&-redeemption', [
                'uses' => 'TransactionController@index',
                'as' => 'frontend.events.transaction'
            ]);
        });
    });

    Route::group(['prefix' => 'store-locations'], function () {
        Route::get('/', [
            'uses' => 'StoreLocationController@index',
            'as' => 'frontend.store-location.index'
        ]);
        Route::get('map-data', [
            'uses' => 'StoreLocationController@mapData',
            'as' => 'frontend.store-location.map-data'
        ]);
    });

    Route::group(['prefix' => 'aboutus'], function () {
        Route::get('/', [
            'uses' => 'HomeController@aboutus',
            'as' => 'frontend.home.aboutus'
        ]);
        Route::get('map-data', [
            'uses' => 'StoreLocationController@mapData',
            'as' => 'frontend.store-location.map-data'
        ]);
    });


    Route::group(['prefix' => 'diary'], function () {
        Route::get('/', [
            'uses' => 'HomeController@diary',
            'as' => 'frontend.home.diary'
        ]);

        Route::get('/{card_code}', [
            'uses' => 'HomeController@diary',
            'as' => 'frontend.home.diary'
        ]);

    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('/', [
            'uses' => 'ProductController@index',
            'as' => 'frontend.product.index'
        ]);

        Route::get('/{slug}', [
            'uses' => 'ProductController@filter_sort_ajax',
            'as' => 'frontend.product.index'
        ]);

        Route::get('map-data', [
            'uses' => 'StoreLocationController@mapData',
            'as' => 'frontend.store-location.map-data'
        ]);
    });

    Route::get('glide/{path}', function($path){
        $server = \League\Glide\ServerFactory::create([
            'source' => app('filesystem')->disk('public')->getDriver(),
        'cache' => storage_path('glide'),
        ]);
        return $server->getImageResponse($path, Input::query());
    })->where('path', '.+');
    
    /*Route::group(['prefix' => 'api', 'namespace' => 'Api'], function () {
        Route::resource('users', 'UserController', ['only' => ['show', 'index']]);
        Route::resource('menus', 'MenuController', ['only' => ['show', 'index']]);
        Route::resource('settings', 'SettingController', ['only' => ['show', 'index']]);
        Route::resource('pages', 'PageController', ['only' => ['show', 'index']]);
    });*/

    Route::get('{url}', ['as' => 'static.page', 'uses' => 'PageController@show'])->where('url', '.+');
});
