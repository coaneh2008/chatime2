<?php

\Route::match(
    ['GET', 'POST'],
    suitLoginUrl(),
    [
        'middleware' => 'admin.guest',
        'as' => suitRouteName('login'),
        'uses' => 'SessionController@login'
    ]
);

\Route::controller(suitPath('password'), 'RemindersController');

\Route::group(['prefix' => \Config::get('suitcms.prefix_url'), 'middleware' => 'admin.auth'], function () {
    // ElFinder
    \Route::get(
        'elfinder',
        [
            'as' => suitRouteName('elfinder'),
            'uses' => '\Barryvdh\Elfinder\ElfinderController@showTinyMCE4'
        ]
    );

    \Route::get('/', ['as' => suitRouteName('index'),
                      'uses' => 'SuitcmsController@index']);
    \Route::match(['GET', 'POST'], '/change-password', ['as' => suitRouteName('password'),
                                     'uses' => 'SessionController@changePassword']);
    \Route::get('/logout', ['as' => suitRouteName('logout'),
                                            'uses' => 'SessionController@logout']);
    \Route::get('/get-slug-menu/{id}', ['as' => suitRouteName('get-slug-menu'),
                                            'uses' => 'SeoTagController@getSlugUrl']);

    // Custom Route to change published in Index
    \Route::post('pages/publish/{key}/{field?}', [
        'as' => suitRouteName('pages.publish'),
        'uses' => 'PageController@publish'
    ]);
    \Route::post('member-free-contents/publish/{key}/{field?}', [
        'as' => suitRouteName('member-free-contents.publish'),
        'uses' => 'MemberFreeContentController@publish'
    ]);
    \Route::post('member-promos/publish/{key}/{field?}', [
        'as' => suitRouteName('member-promos.publish'),
        'uses' => 'MemberPromoController@publish'
    ]);
    \Route::post('member-news-updates/publish/{key}/{field?}', [
        'as' => suitRouteName('member-news-updates.publish'),
        'uses' => 'MemberNewsUpdateController@publish'
    ]);
    \Route::post('member-merchant-promos/publish/{key}/{field?}', [
        'as' => suitRouteName('member-merchant-promos.publish'),
        'uses' => 'MemberMerchantPromoController@publish'
    ]);

    //Import & export
    \Route::group(['prefix' => 'vouchers'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('vouchers.export'),
            'uses' => 'VoucherController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('vouchers.publish'),
            'uses' => 'VoucherController@publish'
        ]);
    });
    \Route::group(['prefix' => 'merchandises'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('merchandises.export'),
            'uses' => 'MerchandiseController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('merchandises.publish'),
            'uses' => 'MerchandiseController@publish'
        ]);
    });
    \Route::group(['prefix' => 'banners'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('banners.export'),
            'uses' => 'BannerController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('banners.publish'),
            'uses' => 'BannerController@publish'
        ]);
    });
    \Route::group(['prefix' => 'product-categories'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('product-categories.export'),
            'uses' => 'ProductCategoryController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('product-categories.publish'),
            'uses' => 'ProductCategoryController@publish'
        ]);
    });
    \Route::group(['prefix' => 'home-infos'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('home-infos.export'),
            'uses' => 'HomeInfoController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('home-infos.publish'),
            'uses' => 'HomeInfoController@publish'
        ]);
    });
    \Route::group(['prefix' => 'content-abouts'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('content-abouts.export'),
            'uses' => 'ContentAboutController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('content-abouts.publish'),
            'uses' => 'ContentAboutController@publish'
        ]);
    });
    \Route::group(['prefix' => 'new-events'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('new-events.export'),
            'uses' => 'NewEventController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('new-events.publish'),
            'uses' => 'NewEventController@publish'
        ]);
    });

    \Route::group(['prefix' => 'new-event-categories'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('new-event-categories.export'),
            'uses' => 'NewEventCategoryController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('new-event-categories.publish'),
            'uses' => 'NewEventCategoryController@publish'
        ]);
    });

    \Route::group(['prefix' => 'flash-stickers'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('flash-stickers.export'),
            'uses' => 'FlashStickerController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('flash-stickers.publish'),
            'uses' => 'FlashStickerController@publish'
        ]);
    });
    \Route::group(['prefix' => 'flash-sales'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('flash-sales.export'),
            'uses' => 'FlashSaleController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('flash-sales.publish'),
            'uses' => 'FlashSaleController@publish'
        ]);
    });
    \Route::group(['prefix' => 'holiday-promotions'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('holiday-promotions.export'),
            'uses' => 'HolidayPromotionController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('holiday-promotions.publish'),
            'uses' => 'HolidayPromotionController@publish'
        ]);
    });
    \Route::group(['prefix' => 'promotion-informations'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('promotion-informations.export'),
            'uses' => 'PromotionInformationController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('promotion-informations.publish'),
            'uses' => 'PromotionInformationController@publish'
        ]);
    });
    \Route::group(['prefix' => 'product-promotions'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('product-promotions.export'),
            'uses' => 'ProductPromotionController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('product-promotions.publish'),
            'uses' => 'ProductPromotionController@publish'
        ]);
    });
    \Route::group(['prefix' => 'home-promotions'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('home-promotions.export'),
            'uses' => 'HomePromotionController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('home-promotions.publish'),
            'uses' => 'HomePromotionController@publish'
        ]);
    });
    \Route::group(['prefix' => 'thematic-promotions'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('thematic-promotions.export'),
            'uses' => 'ThematicPromotionController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('thematic-promotions.publish'),
            'uses' => 'ThematicPromotionController@publish'
        ]);
    });
    \Route::group(['prefix' => 'featured-brands'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('featured-brands.export'),
            'uses' => 'FeaturedBrandController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('featured-brands.publish'),
            'uses' => 'FeaturedBrandController@publish'
        ]);
    });
    \Route::group(['prefix' => 'product-menus'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('product-menus.export'),
            'uses' => 'ProductMenuController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('product-menus.publish'),
            'uses' => 'ProductMenuController@publish'
        ]);
    });
    \Route::group(['prefix' => 'brochures'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('brochures.export'),
            'uses' => 'BrochureController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('brochures.publish'),
            'uses' => 'BrochureController@publish'
        ]);
    });
    \Route::group(['prefix' => 'promotion-categories'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('promotion-categories.export'),
            'uses' => 'PromotionCategoryController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('promotion-categories.publish'),
            'uses' => 'PromotionCategoryController@publish'
        ]);
    });
    \Route::group(['prefix' => 'inspiration-article-categories'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('inspiration-article-categories.export'),
            'uses' => 'InspirationArticleCategoryController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('inspiration-article-categories.publish'),
            'uses' => 'InspirationArticleCategoryController@publish'
        ]);
    });
    \Route::group(['prefix' => 'inspiration-article-categories'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('inspiration-article-categories.export'),
            'uses' => 'InspirationArticleCategoryController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('inspiration-article-categories.publish'),
            'uses' => 'InspirationArticleCategoryController@publish'
        ]);
    });
    \Route::group(['prefix' => 'inspiration-articles'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('inspiration-articles.export'),
            'uses' => 'InspirationArticleController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('inspiration-articles.publish'),
            'uses' => 'InspirationArticleController@publish'
        ]);
    });
    \Route::group(['prefix' => 'regions'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('regions.export'),
            'uses' => 'RegionController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('regions.publish'),
            'uses' => 'RegionController@publish'
        ]);
    });
    \Route::group(['prefix' => 'store-locations'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('store-locations.export'),
            'uses' => 'StoreLocationController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('store-locations.publish'),
            'uses' => 'StoreLocationController@publish'
        ]);
    });
    \Route::group(['prefix' => 'member-leaderboards'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('member-leaderboards.export'),
            'uses' => 'MemberLeaderboardController@export'
        ]);
        \Route::match(['GET', 'POST'], 'import', [
            'as' => suitRouteName('member-leaderboards.import'),
            'uses' => 'MemberLeaderboardController@import'
        ]);
        \Route::get('template', [
            'as' => suitRouteName('member-leaderboards.template'),
            'uses' => 'MemberLeaderboardController@template'
        ]);
        \Route::get('delete-all', [
            'as' => suitRouteName('member-leaderboards.delete-all'),
            'uses' => 'MemberLeaderboardController@deleteAll'
        ]);
    });
    \Route::group(['prefix' => 'inbox'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('inbox.export'),
            'uses' => 'ContactController@export'
        ]);
        Route::get('/{id}', [
            'uses' => 'ContactController@show',
            'as' => 'backend.inbox.view-message'
        ]);
    });
    \Route::group(['prefix' => 'subscribers'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('subscribers.export'),
            'uses' => 'SubscriberController@export'
        ]);
    });

    \Route::group(['prefix' => 'annual-events'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('annual-events.export'),
            'uses' => 'AnnualEventController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('annual-events.publish'),
            'uses' => 'AnnualEventController@publish'
        ]);
    });

    \Route::group(['prefix' => 'free-memberships'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('free-memberships.export'),
            'uses' => 'FreeMembershipController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('free-memberships.publish'),
            'uses' => 'FreeMembershipController@publish'
        ]);
    });

    \Route::group(['prefix' => 'member-get-members'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('member-get-members.export'),
            'uses' => 'MemberGetMemberController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('member-get-members.publish'),
            'uses' => 'MemberGetMemberController@publish'
        ]);
    });

    \Route::group(['prefix' => 'testimonies'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('testimonies.export'),
            'uses' => 'TestimonyController@export'
        ]);
        \Route::post('publish/{key}/{field?}', [
            'as' => suitRouteName('testimonies.publish'),
            'uses' => 'TestimonyController@publish'
        ]);
    });
    \Route::group(['prefix' => 'members'], function () {
        \Route::match(['GET', 'POST'], 'export', [
            'as' => suitRouteName('members.export'),
            'uses' => 'MemberController@export'
        ]);
    });

    \Route::resource('pages', 'PageController', ['except' => 'show']);
    \Route::resource('redirections', 'RedirectionController', ['except' => 'show']);
    \Route::resource('users', 'UserController', ['except' => 'show']);
    \Route::resource('members', 'MemberController', ['only' => ['index', 'show']]);
    \Route::resource('menus', 'MenuController', ['except' => 'show']);
    \Route::resource('settings', 'SettingController', ['except' => 'show']);
    \Route::get('settings/get-value-by-name', [
        'uses' => 'SettingController@getValueByName',
        'as' => 'backend.setting.value'
    ]);
    \Route::resource('membership-settings', 'MembershipSettingController', ['except' => 'show']);
    \Route::resource('html-templates', 'HtmlTemplateController', ['except' => 'show']);

    \Route::resource('brochures', 'BrochureController', ['except' => 'show']);
    \Route::resource('banners', 'BannerController', ['except' => 'show']);
    \Route::resource('featured-brands', 'FeaturedBrandController', ['except' => 'show']);
    \Route::resource('product-menus', 'ProductMenuController', ['except' => 'show']);
    \Route::resource('inbox', 'ContactController', ['except' => 'edit']);
    \Route::resource('subscribers', 'SubscriberController', ['only' => ['index', 'show']]);
    \Route::resource('inspiration-articles', 'InspirationArticleController', ['except' => 'show']);
    \Route::resource('inspiration-article-categories', 'InspirationArticleCategoryController', ['except' => 'show']);

    \Route::resource('instagram-posts', 'InstagramPostController', ['except' => 'show']);
    \Route::resource('products', 'ProductController', ['only' => ['index', 'show']]);

    \Route::resource('product-categories', 'ProductCategoryController', ['except' => 'show']);
    \Route::resource('home-infos', 'HomeInfoController', ['except' => 'show']);
    \Route::resource('content-abouts', 'ContentAboutController', ['except' => 'show']);
    \Route::resource('new-events', 'NewEventController', ['except' => 'show']);
    \Route::resource('new-event-categories', 'NewEventCategoryController', ['except' => 'show']);
    \Route::resource('regions', 'RegionController', ['except' => 'show']);
    \Route::resource('seo-tags', 'SeoTagController', ['except' => 'show']);
    \Route::resource('store-locations', 'StoreLocationController', ['except' => 'show']);
    
    // Event
    \Route::resource('annual-events', 'AnnualEventController');
    \Route::resource('testimonies', 'TestimonyController');
    \Route::resource('member-get-members', 'MemberGetMemberController');
    \Route::resource('free-memberships', 'FreeMembershipController');

    // Member Benefit
    \Route::resource('member-promos', 'MemberPromoController');
    \Route::resource('member-free-contents', 'MemberFreeContentController');
    \Route::resource('member-news-updates', 'MemberNewsUpdateController');
    \Route::resource('member-merchant-categories', 'MemberMerchantCategoryController');
    \Route::resource('member-merchant-locations', 'MemberMerchantLocationController');
    \Route::resource('member-merchant-promos', 'MemberMerchantPromoController');
    \Route::resource('vouchers', 'VoucherController');
    \Route::resource('merchandises', 'MerchandiseController');
    \Route::resource('photos', 'PhotoController');

    \Route::resource('member-leaderboards', 'MemberLeaderboardController', ['except' => 'show']);

    //Promotion
    \Route::resource('promotion-categories', 'PromotionCategoryController', ['except' => 'show']);
    \Route::resource('flash-sales', 'FlashSaleController', ['except' => 'show']);
    \Route::resource('holiday-promotions', 'HolidayPromotionController', ['except' => 'show']);
    \Route::resource('member-promotions', 'MemberPromotionController', ['except' => 'show']);
    \Route::resource('promotion-informations', 'PromotionInformationController', ['except' => 'show']);
    \Route::resource('product-promotions', 'ProductPromotionController', ['except' => 'show']);
    \Route::resource('home-promotions', 'HomePromotionController', ['except' => 'show']);
    \Route::resource('thematic-promotions', 'ThematicPromotionController', ['except' => 'show']);
    
    \Route::resource('flash-stickers', 'FlashStickerController', ['except' => 'show']);

    //Template Route
    \Route::get(
        'template/list',
        [
            'as' => suitRouteName('template.list'),
            'uses' => 'HtmlTemplateController@templateList'
        ]
    );
    \Route::get(
        'template/{key}',
        [
            'as' => suitRouteName('template.detail'),
            'uses' => 'HtmlTemplateController@template'
        ]
    );
});
