<?php

Form::macro('suitField', function ($for, $label, $input, $classes = '', array $option = []) {
    $errorName = str_replace('[', '.', $for);
    $errorName = str_replace(']', '', $errorName);
    $error = Session::has('error')?Session::get('error')->first($errorName):null;
    $isError = !empty($error);

    if (isset($option['info'])) {
        $info = $option['info'];
    }

    $labelId = generateId($for);
    $labelValue = ($label !== null) ? $label : '';
    $infoValue = isset($info) ? '<span class="help-block">' . $info . '</span>' : '';
    $errorValue = $isError ? '<span class="help-block">' . $error . '</span>' : '';
    $hasError = ($isError ? ' has-error':'');
    $result = "
        <div class=\"form-group $hasError\">
        <label class=\"control-label col-md-2\" for=\"$labelId\">$labelValue</label>
            <div class=\"col-md-10 $classes \">
            $input
              $infoValue
              $errorValue
            </div>
        </div>";

    if (isset($option['thumbnail'])) {
        $url = $option['thumbnail'];
        $result .= "
            <div class=\"row\">
                <div class=\"col-md-offset-2 col-xs-6 col-md-3\">
                    <a href=\"$url\" target=\"_blank\" class=\"thumbnail\">
                      <img src=\"$url\" alt=\"$url\">
                    </a>
                </div>
            </div>
        ";
    }

    if (isset($option['gmap'])) {
        $containerId = generateId($for) . '-map-canvas';
        $result .= "
            <div class=\"row\">
                <div class=\"col-md-offset-2 col-md-10\">
                  <div id=\"$containerId\" style=\"height:500px;\"> </div>
                </div>
            </div>
        ";
    }
    return $result;
});

Form::macro('suitSection', function ($label) {
    return "<h3 class=\"form-section\">$label</h3>";
});

Form::macro('suitText', function ($name, $label, $value = null, $attributes = []) {
    addStringToArray('id', generateID($name), $attributes, true);
    addStringToArray('class', 'form-control', $attributes);
    addStringToArray('placeholder', "Enter " . ucwords(implode(' ', explode('_', $name))), $attributes, true);

    return Form::suitField($name, $label, Form::text($name, $value, $attributes), '', $attributes);
});

Form::macro('suitNumber', function ($name, $label, $value = null, $attributes = []) {
    addStringToArray('id', generateID($name), $attributes, true);
    addStringToArray('class', 'form-control', $attributes);
    addStringToArray('placeholder', "Enter " . ucwords(implode(' ', explode('_', $name))), $attributes, true);

    return Form::suitField($name, $label, Form::input('number', $name, $value, $attributes), '', $attributes);
});

Form::macro('suitTextarea', function ($name, $label, $value = null, $attributes = []) {
    addStringToArray('id', generateID($name), $attributes, true);
    addStringToArray('class', 'form-control', $attributes);
    addStringToArray('placeholder', "Enter " . ucwords(implode(' ', explode('_', $name))), $attributes, true);
    addStringToArray('rows', 5, $attributes, true);

    return Form::suitField($name, $label, Form::textarea($name, $value, $attributes), '', $attributes);
});

Form::macro('suitPassword', function ($name, $label, $attributes = []) {
    addStringToArray('id', generateID($name), $attributes, true);
    addStringToArray('class', 'form-control', $attributes);
    addStringToArray('placeholder', "Enter $label", $attributes, true);

    return Form::suitField($name, $label, Form::password($name, $attributes), '', $attributes);
});

Form::macro('suitOpen', function (array $attributes = []) {
    addStringToArray('class', 'form-horizontal', $attributes);

    return Form::open($attributes);
});

Form::macro('suitModel', function ($model, array $attributes = []) {
    addStringToArray('class', 'form-horizontal', $attributes);

    if (isset($attributes['prefix'])) {
        $attributes['route'] = $model->exists
            ? [suitRouteName($attributes['prefix'] . '.update'), $model]
            : suitRouteName($attributes['prefix'] . '.store');
        unset($attributes['prefix']);
    }

    if (!isset($attributes['method'])) {
        $attributes['method'] = $model->exists ? 'PUT' : 'POST';
    }

    return Form::model($model, $attributes);
});

Form::macro('suitSelect', function ($name, $label, $lists = [], $placeholder = null, $value = null, $attributes = []) {
    addStringToArray('id', generateId($name), $attributes, true);
    addStringToArray('class', 'form-control select2me', $attributes);
    addStringToArray('data-placeholder', $placeholder, $attributes, true);

    if (!empty($attributes['data-placeholder'])) {
        $lists = ['' => ''] + $lists;
    }

    return Form::suitField($name, $label, Form::select($name, $lists, $value, $attributes), '', $attributes);
});

Form::macro(
    'suitMultiSelect',
    function ($name, $label, $lists = [], $placeholder = null, $value = null, $attributes = []) {
        $attributes['multiple'] = true;
        return \Form::suitSelect($name, $label, $lists, $placeholder, $value, $attributes);
    }
);

Form::macro(
    'suitMultiSelect2Window',
    function ($name, $label, $lists = [], $placeholder = null, $value = null, $attributes = []) {
        addStringToArray('id', generateId($name), $attributes, true);
        addStringToArray('class', 'form-control multi-select', $attributes);
        addStringToArray('data-placeholder', $placeholder, $attributes, true);
        $attributes['multiple'] = true;

        if (!empty($attributes['data-placeholder'])) {
            $lists = ['' => ''] + $lists;
        }

        return Form::suitField($name, $label, Form::select($name, $lists, $value, $attributes), '', $attributes);
    }
);

Form::macro(
    'suitMultiSelect2WindowValidation',
    function ($name, $label, $lists = [], $placeholder = null, $value = null, $attributes = []) {
        addStringToArray('id', generateId($name), $attributes, true);
        addStringToArray('class', 'form-control multi-select', $attributes);
        addStringToArray('data-placeholder', $placeholder, $attributes, true);
        $attributes['multiple'] = true;

        if (!empty($attributes['data-placeholder'])) {
            $lists = ['' => ''] + $lists;
        }
        $param = ['lists' => $lists, 'name' => $name, 'selected' => $value];
        $selectViewRender = \View::make('admins._partials.manual-multi-select', $param);
        $selectViewHtml = $selectViewRender->render();
        return Form::suitField($name, $label, $selectViewHtml, '', $attributes);
    }
);

Form::macro('suitTokenField', function ($name, $label, $value = null, $attributes = []) {
    addStringToArray('id', generateId($name), $attributes, true);
    addStringToArray('class', 'form-control select2tag', $attributes);
    addStringToArray(
        'data-placeholder',
        isset($attributes['placehoder'])?$attributes['placehoder']:"Enter $label",
        $attributes,
        true
    );

    return Form::suitField($name, $label, Form::hidden($name, $value, $attributes), '', $attributes);
});

Form::macro('suitSubmit', function ($value = null, $name = null, $attributess = []) {
    addStringToArray('class', 'btn green', $attributes);
    return Form::input('submit', $name, $value, $attributes);
});

\Form::macro('suitReset', function ($value = null, $attributess = []) {
    addStringToArray('class', 'btn yellow', $attributes);
    return Form::reset($value, $attributes);
});

Form::macro('suitFileInput', function ($name, $label) {
    $input = "
            <div class=\"fileinput fileinput-new\" data-provides=\"fileinput\">
                <div class=\"input-group input-large\">
                    <div class=\"form-control uneditable-input span3\" data-trigger=\"fileinput\">
                        <i class=\"fa fa-file fileinput-exists\"></i>&nbsp; <span class=\"fileinput-filename\">
                        </span>
                    </div>
                    <span class=\"input-group-addon btn default btn-file\">
                        <span class=\"fileinput-new\">Select file </span>
                        <span class=\"fileinput-exists\"> Change </span>
                        <input type=\"file\" name=\"$name\">
                    </span>
                    <a href=\"#\" class=\"input-group-addon btn default fileinput-exists\" data-dismiss=\"fileinput\">
                        Remove
                    </a>
                </div>
            </div>
    ";
    return Form::suitField($name, $label, $input);
});

\Form::macro('suitDate', function ($name, $label, $value = null, $attributes = []) {
    addStringToArray('id', generateId($name), $attributes, true);
    addStringToArray('class', 'form-control', $attributes);
    $inputText = Form::text($name, $value, $attributes);
    $input = "
        <div class=\"input-group date date-picker\" data-date-format=\"yyyy-mm-dd\">
            <span class=\"input-group-btn\">
                <button class=\"btn default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
            </span>
            $inputText
        </div>
    ";
    return Form::suitField($name, $label, $input);
});

\Form::macro('suitTimePicker', function ($name, $label, $value = null, $attributes = []) {
    addStringToArray('id', $name, $attributes, true);
    addStringToArray('class', 'form-control time-picker', $attributes);
    $inputText = Form::text($name, $value, $attributes);
    $input = "
        <div class=\"input-icon\">
            <i class=\"fa fa-clock-o\">
            </i>
            $inputText
        </div>
    ";
    return Form::suitField($name, $label, $input);
});

\Form::macro('suitDateTime', function ($name, $label, $value = null, $attributes = []) {
    addStringToArray('id', $name, $attributes, true);
    addStringToArray('class', 'form-control', $attributes);
    $inputText = Form::text($name, $value, $attributes);
    $input = "
        <div class=\"input-group date datetime-picker\">
            <span class=\"input-group-addon\">
                <span class=\"fa fa-calendar\"></span>
            </span>
            $inputText
        </div>
    ";
    return Form::suitField($name, $label, $input);
});

\Form::macro(
    'suitDateRange',
    function ($nameFrom, $nameTo, $label, $valueFrom = null, $valueTo = null, $attributes = []) {
        addStringToArray('class', 'form-control', $attributes);
        
        addStringToArray('id', $nameFrom, $attributes, true);
        $inputTextFrom = Form::text($nameFrom, $valueFrom, $attributes);

        addStringToArray('id', $nameTo, $attributes, false);
        $inputTextTo = Form::text($nameTo, $valueTo, $attributes);
        $input = "
            <div class=\"input-group date date-picker input-daterange\">
                $inputTextFrom
                <span class=\"input-group-addon\"> to
                </span>
                $inputTextTo
            </div>
        ";
        return Form::suitField($nameFrom, $label, $input);
    }
);

Form::macro('suitFileBrowser', function ($name, $label, $value = null, $attributes = []) {
    $key = generateId($name);
    addStringToArray('id', $key, $attributes, true);
    addStringToArray('class', 'form-control', $attributes);
    addStringToArray('placeholder', "Browse from server or Enter Url.", $attributes, true);

    $inputText = Form::text($name, $value, $attributes);
    $input = "
        <div class=\"input-group\">
            <span class=\"input-group-btn popup_selector\" data-inputid=\"$key\" style=\"cursor:pointer;\">
                <button class=\"btn default\" type=\"button\"><i class=\"fa fa-folder-open-o\"></i></button>
            </span>
            $inputText
        </div>
    ";
    $value = Form::getValueAttribute($name, $value);

    if (!empty($value)) {
        $url = asset($value);
        $attributes['thumbnail'] = $url;
    }
    return Form::suitField($name, $label, $input, '', $attributes);
});

Form::macro('suitWysiwyg', function ($name, $label, $value = null, $type = 'basic', $attributes = []) {
    addStringToArray('class', "editor", $attributes);
    if (!isset($attributes['rows'])) {
        $attributes['rows'] = 15;
    }
    return Form::suitTextarea($name, $label, $value, $attributes);
});

Form::macro('suitSingleCheckbox', function ($name, $label, $checked = false, $value = 1, $attributes = []) {
    addStringToArray('id', generateID($name), $attributes, true);
    addStringToArray('class', 'form-control', $attributes);
    return Form::suitField($name, $label, Form::checkbox($name, $value, $checked, $attributes), 'checkbox-inline');
});

Form::macro('suitSwitch', function ($name, $label, $text = ['No', 'Yes'], $checked = false, $attributes = []) {
    addStringToArray('id', generateID($name), $attributes, true);
    addStringToArray('class', 'form-control make-switch', $attributes);
    addStringToArray('data-on-text', $text[1], $attributes);
    addStringToArray('data-off-text', $text[0], $attributes);
    addStringToArray('data-off-color', 'danger', $attributes);
    $inputFalse = Form::hidden($name, 0);
    $inputTrue = Form::checkbox($name, 1, $checked, $attributes);
    $input = "
        <div class=\"input-group\">
            $inputFalse
            $inputTrue
        </div>
    ";
    return Form::suitField($name, $label, $input);
});

Form::macro('suitMap', function ($name, $label, $value = null, $attributes = []) {
    $attributes['data-inputid'] = $name;
    $attributes['data-location'] = Form::getValueAttribute($name, $value);
    $attributes['gmap'] = true;
    addStringToArray('class', 'google-map', $attributes);

    return Form::suitText($name, $label, $value, $attributes);
});

Form::macro('suitBack', function ($name = 'Back', $url = null) {
    if ($url === null) {
        $url = \URL::previous();
    }
    return "<a href=\"$url\" class=\"btn red\">$name</a>";
});

/**
 * Form for index
 */

Form::macro('suitIndexSwitch', function ($name, $checked, $url, $attributes = []) {
    $token = csrf_token();
    $onText = 'Yes';
    $offText = 'No';
    if (in_array('data-on-text', $attributes)) {
        $onText = $attributes['data-on-text'];
    }
    if (in_array('data-off-text', $attributes)) {
        $offText = $attributes['data-off-text'];
    }
    addStringToArray('id', generateID($name), $attributes, true);
    addStringToArray('class', 'form-control mini-switch', $attributes);
    addStringToArray('data-on-text', $onText, $attributes);
    addStringToArray('data-off-text', $offText, $attributes);
    addStringToArray('data-off-color', 'danger', $attributes);
    addStringToArray('data-size', 'mini', $attributes);
    addStringToArray('data-url', $url, $attributes);
    addStringToArray('data-token', $token, $attributes);
    $inputFalse = Form::hidden($name, 0);
    $inputTrue = Form::checkbox($name, 1, $checked, $attributes);
    $input = "
            <div class=\"text-center index-switch\">
                $inputFalse
                $inputTrue
            </div>
    ";
    return $input;
});

// View macro
Form::macro('suitView', function ($value, $label) {
    return "
        <div class=\"form-group\">
            <label class=\"control-label col-sm-2\">
                $label :
            </label>
            <div class=\"col-sm-10\">
                <div style=\"overflow: hidden; padding-top: 10px;  word-wrap: break-word\">
                    $value
                </div>
            </div>
        </div>";
});

Form::macro('suitViewThumb', function ($value, $label) {
    $asset = asset($value);
    return "
        <div class=\"form-group\">
            <label class=\"control-label col-sm-2\">
                $label :
            </label>
            <div class=\"col-xs-6 col-md-3\">
                <a href=\"$asset\" target=\"_blank\" class=\"thumbnail\">
                  <img src=\"$asset\" alt=\"$value\">
                </a>
            </div>
        </div>";
});
