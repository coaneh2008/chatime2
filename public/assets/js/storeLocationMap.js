import * as _ from './utils'

export default function () {
    const mapContainer = document.querySelector('.store-location-map')

    if ( !mapContainer ) return;

    const map = new google.maps.Map(mapContainer, {
        center: { lat: -6.155185, lng: 106.833115 },
        zoom: 7,
        clickableIcons: false,
        disableDefaultUI: true
    })

    const app = angular.module('StoreMap', [])
    app.controller('StoreMapController', ['$http', '$scope', function ($http, $scope) {
        const $el = $('#storeLocation')
        const dataUri = $el.data('stores')
        const infoWindow = new google.maps.InfoWindow()
        const searchInput = document.querySelector('#storeSearchInput')
        let searchInputEnhanced
        let markers = []
        this.results = []
        this.regionsListIsActive = false

        const setSelectedRegion = (region) => {
            $scope.$apply(() => {
                const bounds = new google.maps.LatLngBounds()

                if ( region === 'all' ) {
                    this.searchKeyword = 'you'
                    this.results = this.data
                        .map(d => d.stores)
                        .reduce((a, b) => a.concat(b), [])
                    searchInputEnhanced.list = this.STORES.map(_.get('name'))
                } else {
                    this.searchKeyword = region
                    this.results = this.data
                        .filter((d) => d.name === region)
                        .map(d => d.stores)
                        .reduce((a, b) => b)
                    searchInputEnhanced.list = _.pipe(
                        _.filter(a => a.name === region),
                        _.map(_.get('stores')),
                        _.get(0),
                        _.map(_.get('name'))
                    )(this.data)
                }

                clearMarkers()

                this.results.forEach((position) => {
                    const marker = new google.maps.Marker({
                        map,
                        position: { lat: position.lat, lng: position.lng },
                        icon: {
                            url: mapMarkerUrl,
                            scaledSize: new google.maps.Size(28, 45)
                        }
                    })

                    marker.addListener('click', () => {
                        infoWindow.setContent(`
                            <h3>${ position.name }</h3>
                            <p style="max-width: 240px;">${ position.address }</p>
                            <a href="https://www.google.com/maps/dir//${position.address}/@${position.lat},${position.lng},13z" target="_blank">Get direction</a>
                        `)
                        infoWindow.open(map, marker)
                        map.panTo(marker.position)
                        map.panBy(0, -100)
                        $scope.$apply(() => {
                            this.closeRegionsList()
                        })
                    })

                    markers.push(marker)
                    bounds.extend(marker.position)
                })

                map.fitBounds(bounds)
                map.setZoom(map.getZoom() - 1)
                searchInput.value = ''
            })
        }

        $http.get(dataUri)
            .then(_.get('data'))
            .then(_.set(this, 'data'))
            .then(_.get('data'))
            .then(transformData)
            .then(_.set(this, 'regions'))
            .then(() => {
                this.STORES = _.pipe(
                    _.map(_.get('stores')),
                    _.reduce((a,b) => a.concat(b), []),
                )(this.data)
                const regionSelect = document.querySelector('#setRegion')
                const setAllRegion = document.querySelector('#setAllRegion')
                searchInput.addEventListener('awesomplete-selectcomplete', (e) => {
                    const { text } = e
                    const selectedStore = _.pipe(
                        _.filter(a => a.name === text.value),
                        _.get(0),
                    )(this.STORES)
                    const marker = new google.maps.Marker({
                        map,
                        position: { lat: selectedStore.lat, lng: selectedStore.lng },
                        icon: {
                            url: mapMarkerUrl,
                            scaledSize: new google.maps.Size(28, 45)
                        }
                    })
                    markers.push(marker)
                    map.setCenter(marker.position)
                    map.panBy(0, -100)
                    map.setZoom(14)
                    infoWindow.setContent(`
                        <h3>${ selectedStore.name }</h3>
                        <p style="max-width: 240px;">${ selectedStore.address }</p>
                        <a href="#" data-direction="${ selectedStore.lat },${ selectedStore.lng }">Get direction</a>
                    `)
                    infoWindow.open(map, marker)
                })

                regionSelect.addEventListener('click', (e) => {
                    const region = $('input[name="region"]:checked').val()
                    setSelectedRegion(region)
                })

                setAllRegion.addEventListener('click', (e) => {
                    e.preventDefault()
                    setSelectedRegion('all')
                })

                searchInputEnhanced = new Awesomplete(searchInput, {
                    list: this.STORES.map(_.get('name'))
                })
            })
            .catch(_.noop)

        this.toggleRegionsList = () => {
            this.regionsListIsActive = !this.regionsListIsActive
        }

        this.closeRegionsList = () => {
            this.regionsListIsActive = false
        }

        function transformData(data) {
            return _.map((region) => {
                return {
                    name: region.name,
                    id: region.id
                }
            })(data)
        }

        function clearMarkers() {
            markers.forEach((marker) => {
                marker.setMap(null)
            })
        }
    }])
}
