# config valid only for current version of Capistrano
lock '3.9.1'

set :application, 'suitcms'
set :repo_url, 'git@gitlab.com:suitmedia/ace-hardware-products.git'

# Default branch is :master
ask :branch, `git tag`.split("\n").last

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('.env')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('public/files', 'storage/framework/cache', 'storage/logs')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 3

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute "touch #{release_path.join('storage/logs/laravel.log')}"
      execute "chmod a+w #{release_path.join('storage')} -R"
      execute "cd '#{release_path}'; composer install"
      execute "cd '#{release_path}'; php artisan migrate -n --force"
    end
  end

  before :publishing, :restart
end
