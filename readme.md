Ace Hardware Products
=======

Platform for standard web content management system based on Laravel 5.1. For more detail [read the documentation on wiki]

![Ace Hardware Product]

## Main Component
* Laravel 5.1
* [Metronic 3.5][1]

## Package Dependencies
* laravelcollective/html 5.1.*
* guzzlehttp/guzzle ~6.2.0
* barryvdh/laravel-debugbar 2.0.1
* barryvdh/laravel-elfinder 0.3.*
* bkwld/croppa ~4.2

## Requirement
* PHP >= 5.5.9
* MySQL >= 5.5

## Installation

1. Clone this repository
1. Create dan configure `.env` file based on `.env.example`
1. Run `composer install` in the root project to install all dependencies including develeopment requirement.
1. Run `php artisan key:generate` in the root project to generate new Key for new Application.
1. Run `php artisan cache:clear` in the root project to flush the application cache.
1. Run `php artisan config:cache` in the root project to create a cache file for faster configuration loading.
1. Run `php artisan migrate` in the root project to migrate main suitcms database.
1. Create username and password for admin `php artisan user:new-admin [your username] [your email] [your password]`
1. Try login in with url `/secret/login`
1. Done!

[1]: http://www.keenthemes.com/preview/index.php?theme=metronic   "Metronic Live Preview"
[2]: http://ckeditor.com/                                         "CKEditor"
[3]: http://elfinder.org/                                  "Elfinder"
