<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_benefits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->integer('type')->index();
            $table->string('slug')->nullable()->index();
            $table->text('description')->nullable();
            $table->mediumText('content')->nullable();
            $table->string('thumb_image', 500)->nullable();
            $table->text('thumb_image_info')->default('');
            $table->dateTime('published_at')->nullable();
            $table->integer('order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('member_benefits');
    }
}
