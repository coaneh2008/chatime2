<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion_categories', function ($table) {
            $table->increments('id');
            $table->string('title');
            $table->enum('type', [0, 1, 2, 3, 4])->index();
            $table->integer('order')->default(0);
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
        });

        Schema::create('promotions', function ($table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->mediumText('content')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->string('file', 500)->nullable();
            $table->text('file_info')->default('');
            $table->dateTime('published_at')->nullable();

            $table->timestamps();

            $table->foreign('category_id')
                  ->references('id')->on('promotion_categories')
                  ->onDelete('cascade');
        });

        Schema::create('product_promotions', function ($table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('discount');
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->string('file', 500)->nullable();
            $table->text('file_info')->default('');
            $table->integer('order')->default(0);
            $table->dateTime('published_at')->nullable();
            
            $table->timestamps();

            $table->foreign('category_id')
                  ->references('id')->on('promotion_categories')
                  ->onDelete('cascade');

            $table->foreign('product_id')
                  ->references('id')->on('products')
                  ->onDelete('cascade');
        });

        Schema::create('flash_sales', function ($table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->mediumText('content')->nullable();
            $table->string('url_to_ruparupa', 500)->nullable();
            $table->string('banner_image', 500)->nullable();
            $table->text('banner_image_info')->default('');
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->dateTime('published_at')->nullable();

            $table->timestamps();

            $table->foreign('category_id')
                  ->references('id')->on('promotion_categories')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flash_sales');
        Schema::dropIfExists('promotions');
        Schema::dropIfExists('product_promotions');
        Schema::dropIfExists('promotion_categories');
    }
}
