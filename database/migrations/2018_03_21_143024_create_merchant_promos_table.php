<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantPromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_promo_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('order')->default(0);
            $table->timestamps();
        });

        Schema::create('merchant_promos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('member_benefit_id')->unsigned();
            $table->integer('merchant_category_id')->unsigned();
            $table->string('title');
            $table->string('merchant_name');
            $table->string('slug')->nullable()->index();
            $table->text('description')->nullable();
            $table->mediumText('content')->nullable();
            $table->string('thumb_image', 500)->nullable();
            $table->text('thumb_image_info')->default('');
            $table->string('banner_image', 500)->nullable();
            $table->text('banner_image_info')->default('');
            $table->dateTime('published_at')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->enum('is_highlighted', [0, 1]);
            $table->integer('order')->default(0);

            $table->timestamps();

            $table->foreign('member_benefit_id')
                  ->references('id')->on('member_benefits')
                  ->onDelete('cascade');
            $table->foreign('merchant_category_id')
                  ->references('id')->on('merchant_promo_categories')
                  ->onDelete('cascade');
        });

        Schema::create('merchant_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('order')->default(0);
            $table->timestamps();
        });

        Schema::create('merchant_promo_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('merchant_promo_id')->unsigned();
            $table->integer('merchant_location_id')->unsigned();

            $table->timestamps();

            $table->foreign('merchant_promo_id')
                  ->references('id')->on('merchant_promos')
                  ->onDelete('cascade');

            $table->foreign('merchant_location_id')
                  ->references('id')->on('merchant_locations')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('merchant_promo_locations');
        Schema::drop('merchant_locations');
        Schema::drop('merchant_promos');
        Schema::drop('merchant_promo_categories');
    }
}
