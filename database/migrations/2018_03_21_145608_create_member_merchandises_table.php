<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberMerchandisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_merchandises', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('thumb_image', 500)->nullable();
            $table->text('thumb_image_info')->default('');
            $table->integer('point')->default(0);
            $table->integer('order')->default(0);
            $table->dateTime('published_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('member_merchandises');
    }
}
