<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberLeaderboardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_leaderboards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rank')->nullable();
            $table->string('member_card_id')->nullable();
            $table->string('name')->nullable();
            $table->string('amount')->nullable();
            $table->string('transaction')->nullable();
            $table->string('quantity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('member_leaderboards');
    }
}
