<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBannerImageInMemberBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member_benefits', function ($table) {
            $table->string('banner_image', 500)->nullable();
            $table->text('banner_image_info')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_benefits', function ($table) {
            $table->dropColumn('banner_image');
            $table->dropColumn('banner_image_info');
        });
    }
}
