<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInspirationArticlesChangeCategoryId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspiration_articles', function (Blueprint $table) {
            $table->dropForeign('inspiration_articles_category_id_foreign');
            $table->dropColumn('category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspiration_articles', function (Blueprint $table) {
            $table->integer('category_id')->after('id')->unsigned();
            $table->foreign('category_id')
                ->references('id')->on('product_categories')
                ->onDelete('cascade');
        });
    }
}
