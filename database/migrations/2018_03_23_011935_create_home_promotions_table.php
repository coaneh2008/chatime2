<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_promotions', function ($table) {
            $table->increments('id');
            $table->string('title');
            $table->mediumText('content')->nullable();
            $table->string('thumb_image', 500)->nullable();
            $table->text('thumb_image_info')->default('');
            $table->string('banner_image', 500)->nullable();
            $table->text('banner_image_info')->default('');
            $table->enum('type', [0, 1])->index();
            $table->enum('is_highlighted', [0, 1]);
            $table->integer('order')->default(0);
            $table->string('slug')->nullable()->index();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->dateTime('published_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_promotions');
    }
}
