<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspirationArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspiration_articles', function ($table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('author_id')->unsigned()->nullable();
            $table->string('title');
            $table->string('thumb_image', 500)->nullable();
            $table->text('thumb_image_info')->default('');
            $table->string('banner_image', 500)->nullable();
            $table->text('banner_image_info')->default('');
            $table->text('description')->nullable();
            $table->mediumText('content')->nullable();
            $table->enum('is_highlighted', [0, 1]);
            $table->string('slug')->nullable()->index();
            $table->integer('order')->default(0);
            $table->integer('viewer')->unsigned()->default(0);

            $table->dateTime('published_at')->nullable();
            $table->timestamps();

            $table->foreign('category_id')
                  ->references('id')->on('product_categories')
                  ->onDelete('cascade');

            $table->foreign('author_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspiration_articles');
    }
}
