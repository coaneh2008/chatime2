<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHomePromotionDropIsHighlighted extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('home_promotions', function (Blueprint $table) {
            $table->dropColumn('is_highlighted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('home_promotions', function (Blueprint $table) {
            $table->enum('is_highlighted', [0, 1]);
        });
    }
}
