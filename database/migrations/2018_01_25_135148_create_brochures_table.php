<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrochuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brochures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->nullable()->index();
            $table->string('thumb_image', 500)->nullable();
            $table->text('thumb_image_info')->default('');
            $table->string('file', 500)->nullable();
            $table->text('file_info')->default('');
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
        });

        Schema::create('brochure_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brochure_id')->unsigned();
            $table->string('image', 500)->nullable();
            $table->text('image_info')->default('');
            $table->integer('order')->default(0);
            $table->timestamps();

            $table->foreign('brochure_id')
                 ->references('id')->on('brochures')
                 ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brochure_attachments');
        Schema::dropIfExists('brochures');
    }
}
