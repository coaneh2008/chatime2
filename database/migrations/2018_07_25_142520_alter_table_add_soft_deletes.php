<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAddSoftDeletes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('product_categories', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('flash_stickers', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('promotion_categories', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('flash_sales', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('promotions', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('product_promotions', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('home_promotions', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('inspiration_article_categories', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('inspiration_articles', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('featured_brands', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('brochures', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('regions', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('store_locations', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('events', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('member_benefits', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('member_promos', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('member_news_updates', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('merchant_promo_categories', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('merchant_locations', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('merchant_promos', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('member_vouchers', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('member_merchandises', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('product_categories', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('flash_stickers', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('promotion_categories', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('flash_sales', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('promotions', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('product_promotions', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('home_promotions', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('inspiration_article_categories', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('inspiration_articles', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('featured_brands', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('brochures', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('regions', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('store_locations', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('events', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('member_benefits', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('member_promos', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('member_news_updates', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('merchant_promo_categories', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('merchant_locations', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('merchant_promos', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('member_vouchers', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('member_merchandises', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
