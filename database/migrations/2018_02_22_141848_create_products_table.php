<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brand_id')->unsigned()->nullable();
            $table->string('url_to_ruparupa');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('currency')->nullable();
            $table->double('price')->nullable();
            $table->enum('is_available', [0, 1])->nullable();
            $table->string('image', 500)->nullable();
            $table->text('image_info')->default('');
            $table->timestamps();

            $table->foreign('brand_id')
                  ->references('id')->on('featured_brands')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
