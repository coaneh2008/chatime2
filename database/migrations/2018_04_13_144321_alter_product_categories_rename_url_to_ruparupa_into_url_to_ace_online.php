<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductCategoriesRenameUrlToRuparupaIntoUrlToAceOnline extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_categories', function ($table) {
            $table->renameColumn('url_to_ruparupa', 'url_to_ace_online');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_categories', function ($table) {
            $table->renameColumn('url_to_ace_online', 'url_to_ruparupa');
        });
    }
}
