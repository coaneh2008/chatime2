<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlugInMerchantCategoriesAndMerchantLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_promo_categories', function ($table) {
            $table->string('slug')->nullable()->index();
        });
        Schema::table('merchant_locations', function ($table) {
            $table->string('slug')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_locations', function ($table) {
            $table->dropColumn('slug');
        });
        Schema::table('merchant_promo_categories', function ($table) {
            $table->dropColumn('slug');
        });
    }
}
