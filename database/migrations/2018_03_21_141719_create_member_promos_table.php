<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberPromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_promos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('member_benefit_id')->unsigned();
            $table->string('title');
            $table->string('slug')->nullable()->index();
            $table->text('description')->nullable();
            $table->mediumText('content')->nullable();
            $table->string('thumb_image', 500)->nullable();
            $table->text('thumb_image_info')->default('');
            $table->string('banner_image', 500)->nullable();
            $table->text('banner_image_info')->default('');
            $table->dateTime('published_at')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->enum('is_highlighted', [0, 1]);
            $table->integer('order')->default(0);

            $table->timestamps();

            $table->foreign('member_benefit_id')
                  ->references('id')->on('member_benefits')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('member_promos');
    }
}
