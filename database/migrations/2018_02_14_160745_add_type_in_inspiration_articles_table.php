<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeInInspirationArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspiration_articles', function (Blueprint $table) {
            $table->string('image_type')->index()->after('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspiration_articles', function (Blueprint $table) {
            $table->dropColumn('image_type');
        });
    }
}
