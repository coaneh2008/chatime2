<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPromotionsAddDinamycBtn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promotions', function ($table) {
            $table->enum('type', [0, 1])->after('category_id');
            $table->string('btn_name')->after('file_info');
            $table->string('url')->after('btn_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promotions', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('btn_name');
            $table->dropColumn('url');
        });
    }
}
