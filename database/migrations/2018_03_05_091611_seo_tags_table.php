<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeoTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('page_type')->length(1)->unsigned()->nullable();
            $table->integer('content_id')->unsigned()->nullable();
            $table->string('seo_slug')->nullable();
            $table->string('seo_title')->nullable();
            $table->text('seo_description')->nullable();
            $table->string('opengraph_title')->nullable();
            $table->string('opengraph_description')->nullable();
            $table->string('twitter_title')->nullable();
            $table->string('twitter_description')->nullable();
            $table->string('meta_image', 500)->nullable();
            $table->text('meta_image_info')->default('');

            $table->timestamps();

            $table->unique(['page_type', 'content_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seo_tags');
    }
}
