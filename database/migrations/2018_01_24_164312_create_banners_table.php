<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function ($table) {
            $table->increments('id');
            $table->string('video_url')->default('')->nullable();
            $table->string('desktop_image', 500)->nullable();
            $table->text('desktop_image_info')->default('');
            $table->string('mobile_image', 500)->nullable();
            $table->text('mobile_image_info')->default('');
            $table->integer('order')->default(0);
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
