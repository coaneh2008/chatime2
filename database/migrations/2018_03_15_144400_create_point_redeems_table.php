<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointRedeemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_redeems', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('redeem_no', 30);
            $table->string('company_name', 40);
            $table->integer('point')->default(0);
            $table->dateTime('date')->nullable();
            
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_redeems');
    }
}
