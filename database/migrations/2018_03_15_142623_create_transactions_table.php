<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('transactions', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('receive_no', 23);
            $table->string('coupon_no', 20);
            $table->string('company_name', 40);
            $table->double('amount');
            $table->integer('point')->default(0);
            $table->dateTime('date')->nullable();
            
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
