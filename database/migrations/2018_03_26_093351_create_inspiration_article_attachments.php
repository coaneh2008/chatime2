<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspirationArticleAttachments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspiration_article_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inspiration_article_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('order')->default(0);
            $table->timestamps();

            $table->foreign('inspiration_article_id')
                 ->references('id')->on('inspiration_articles')
                 ->onDelete('cascade');
                 
            $table->foreign('product_id')
                  ->references('id')->on('products')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspiration_article_attachments');
    }
}
