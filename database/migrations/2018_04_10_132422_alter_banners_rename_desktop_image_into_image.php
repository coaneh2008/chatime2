<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBannersRenameDesktopImageIntoImage extends Migration
{
    public function __construct()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banners', function ($table) {
            $table->renameColumn('desktop_image', 'image');
            $table->renameColumn('desktop_image_info', 'image_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banners', function ($table) {
            $table->renameColumn('image', 'desktop_image');
            $table->renameColumn('image_info', 'desktop_image_info');
        });
    }
}
