<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInspirationArticlesTableAddPublishedDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspiration_articles', function ($table) {
            $table->date('published_date')->after('published_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspiration_articles', function (Blueprint $table) {
            $table->dropColumn('published_date');
        });
    }
}
