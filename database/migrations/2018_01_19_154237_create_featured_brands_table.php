<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturedBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('featured_brands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->mediumText('content')->nullable();
            $table->string('slug')->nullable()->index();
            $table->string('thumb_image', 500)->nullable();
            $table->text('thumb_image_info')->default('')->nullable();
            $table->string('banner_image', 500)->nullable();
            $table->text('banner_image_info')->default('')->nullable();
            $table->integer('order')->default(0);
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('featured_brands');
    }
}
