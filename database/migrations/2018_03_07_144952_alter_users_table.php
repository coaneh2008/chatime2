<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('member_id')
                ->after('id')
                ->unique()
                ->nullable();
            $table->string('referal_id')
                ->after('member_id')
                ->nullable();
            $table->date('reference_date')
                ->after('referal_id')
                ->nullable();
            $table->integer('current_address_city_id')
                ->unsigned()
                ->after('member_id')
                ->nullable();
            $table->integer('ktp_address_city_id')
                ->unsigned()
                ->after('current_address_city_id')
                ->nullable();
            $table->string('place_of_birth', 100)
                ->after('about')
                ->nullable();
            $table->string('ktp_address_zip_code', 15)
                ->after('about')
                ->nullable();
            $table->string('ktp_address', 100)
                ->after('about')
                ->nullable();
            $table->string('current_address_zip_code', 15)
                ->after('about')
                ->nullable();
             $table->string('current_address', 100)
             ->after('about')
                ->nullable();
            $table->date('expiry_date')
                ->after('password')
                ->nullable();
            $table->enum('is_renewal', [0, 1])
                ->after('expiry_date')
                ->nullable();
            $table->string('profile_picture', 500)
                ->after('is_renewal')
                ->nullable();
            $table->text('profile_picture_info')
                ->after('profile_picture')
                ->default('');
            $table->string('identity_card_id', 60)
                ->after('date_of_birth')
                ->nullable();
            $table->string('cellphone', 15)
                ->after('identity_card_id')
                ->nullable();
            $table->string('telephone', 15)
                ->after('cellphone')
                ->nullable();
            $table->enum('gender', [0, 1])
                ->after('telephone')
                ->nullable();
            $table->enum('religion_id', [1, 2, 3, 4, 5, 6, 7])
                ->after('gender')
                ->nullable();
            $table->enum('marital_status', [1, 2, 3])
                ->after('religion_id')
                ->nullable();
            $table->enum('citizenship_id', [1, 2])
                ->after('marital_status')
                ->nullable();
            $table->enum('occupation_id', [1, 2, 3, 4, 5, 6])
                ->after('citizenship_id')
                ->nullable();
            $table->string('shopping_purpose', 100)->nullable();
            $table->enum('administration_type', [0, 1, 2])->nullable();
            

            $table->foreign('current_address_city_id')
                  ->references('id')->on('cities')
                  ->onDelete('cascade');
            $table->foreign('ktp_address_city_id')
                  ->references('id')->on('cities')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('member_id');
            $table->dropColumn('referal_id');
            $table->dropColumn('reference_date');
            $table->dropForeign('users_current_address_city_id_foreign');
            $table->dropColumn('current_address_city_id');
            $table->dropForeign('users_ktp_address_city_id_foreign');
            $table->dropColumn('ktp_address_city_id');
            $table->dropColumn('place_of_birth');
            $table->dropColumn('current_address_zip_code');
            $table->dropColumn('current_address');
            $table->dropColumn('ktp_address_zip_code');
            $table->dropColumn('ktp_address');
            $table->dropColumn('expiry_date');
            $table->dropColumn('is_renewal');
            $table->dropColumn('profile_picture');
            $table->dropColumn('profile_picture_info');
            $table->dropColumn('identity_card_id');
            $table->dropColumn('cellphone');
            $table->dropColumn('telephone');
            $table->dropColumn('gender');
            $table->dropColumn('religion_id');
            $table->dropColumn('marital_status');
            $table->dropColumn('citizenship_id');
            $table->dropColumn('occupation_id');
            $table->dropColumn('shopping_purpose');
            $table->dropColumn('administration_type');
        });
    }
}
