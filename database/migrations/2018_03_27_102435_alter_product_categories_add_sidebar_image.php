<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductCategoriesAddSidebarImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_categories', function ($table) {
            $table->string('sidebar_image', 500)->nullable();
            $table->text('sidebar_image_info')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_categories', function ($table) {
            $table->dropColumn('sidebar_image');
            $table->dropColumn('sidebar_image_info');
        });
    }
}
