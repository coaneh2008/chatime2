<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_categories', function ($table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('type')->index()->default('Primary');
            $table->string('title');
            $table->string('url_to_ruparupa')->default('')->nullable();
            $table->string('thumb_image', 500)->nullable();
            $table->text('thumb_image_info')->default('');
            $table->string('banner_image', 500)->nullable();
            $table->text('banner_image_info')->default('');
            $table->text('description')->nullable();
            $table->mediumText('content')->nullable();
            $table->string('slug')->nullable()->index();
            $table->integer('order')->default(0);

            $table->dateTime('published_at')->nullable();
            $table->timestamps();

            $table->foreign('parent_id')
                  ->references('id')->on('product_categories')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_categories');
    }
}
