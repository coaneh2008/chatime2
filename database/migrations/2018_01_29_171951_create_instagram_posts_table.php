<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstagramPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_posts', function ($table) {
            $table->increments('id');
            $table->string('code')->unique()->index();
            $table->string('image', 500)->nullable();
            $table->text('image_info')->default('');
            $table->string('video', 500)->nullable();
            $table->text('video_info')->default('');
            $table->text('caption')->nullable();
            $table->string('location')->nullable();
            $table->enum('type', [0, 1])->index();
            $table->string('url', 500)->nullable();
            $table->integer('total_like')->unsigned()->default(0);
            $table->integer('total_comment')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram_posts');
    }
}
