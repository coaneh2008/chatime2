<?php

use Illuminate\Database\Seeder;

class BrochuresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brochures')->delete();

        for ($i=0; $i<11; $i++) {
            $brochure1 = new \App\Model\Brochure();
            $brochure1->title = "ACE Cawang Holiday Seasons";
            $brochure1->thumb_image = asset('assets/img/catalogue.jpg');
            $brochure1->file = asset('assets/example.pdf');
            $brochure1->order = $i;
            $brochure1->published_at =  \Carbon\Carbon::now();
            $brochure1->save();
        }

        $brochureOldestId = DB::table('brochures')->first()->id;

        $brochure1Attachment1 = new \App\Model\BrochureAttachment();
        $brochure1Attachment1->brochure_id = $brochureOldestId;
        $brochure1Attachment1->image = asset('assets/img/catalogue.jpg');
        $brochure1Attachment1->order = 1;
        $brochure1Attachment1->save();

        $brochure1Attachment2 = new \App\Model\BrochureAttachment();
        $brochure1Attachment2->brochure_id = $brochureOldestId;
        $brochure1Attachment2->image = asset('assets/img/catalogue.jpg');
        $brochure1Attachment2->order = 2;
        $brochure1Attachment2->save();
    }
}
