<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->delete();

        $product = new \App\Model\Product();
        $product->url_to_ace_online = 'https://www.ruparupa.com/meja-lipat-persegi-panjang-4ft-putih.html';
        $product->save();
    }
}
