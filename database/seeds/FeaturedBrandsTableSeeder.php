<?php

use Illuminate\Database\Seeder;

class FeaturedBrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('featured_brands')->delete();

        for ($i=1; $i<=10; $i++) {
            $brand = new App\Model\FeaturedBrand();
            $brand->title = 'Krisbow';
            $brand->thumb_image = asset('assets/img/logo-brand-'. $i . '.jpg');
            $brand->banner_image = asset('assets/img/brand-hero.jpg');
            $brand->content = "
                <h1 class='h2'>THE #1 COMMERCIAL &amp; INDUSTRY SUPPLY COMPANY IN INDONESIA</h1>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem ullam vero eius similique ab pariatur officiis dolorum magnam recusandae. Omnis, dolorum voluptatum, quaerat aspernatur ipsum quisquam voluptates temporibus pariatur quasi commodi id rem ratione magnam facere modi, illum ut. Expedita voluptates commodi ipsum odit labore blanditiis temporibus dicta ab maiores deleniti cupiditate eaque nihil amet ipsam, velit suscipit eligendi facere. Officia delectus, asperiores. Aperiam voluptates, excepturi, aspernatur quis aliquid autem suscipit ut, deleniti velit provident rerum fuga eaque! At, eum.</p>

                <div class='responsive-media block'>
                    <iframe width='560' height='315' src='ttps://www.youtube.com/embed/WFWfYCz-scw?rel=0&amp;showinfo=0' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>
                </div>

                <p>Autem dicta eos porro atque id alias, fugiat deserunt ullam repellat consequatur. Quidem dolore, repellendus illo dolores obcaecati, accusamus quia atque minus quam, tenetur quo ut placeat eius perspiciatis, cum deserunt natus doloribus quibusdam recusandae maxime? Odio minus porro, aliquam illo earum cumque modi aperiam consequuntur obcaecati ea odit quaerat. Omnis culpa, aut similique facere corporis voluptates. Ipsam minus reiciendis temporibus perferendis dignissimos nobis molestias perspiciatis doloribus vitae, placeat sit ab deserunt quam deleniti autem nemo laudantium et. Reiciendis, commodi.</p>
            ";
            $brand->order = $i;
            $brand->published_at = \Carbon\Carbon::now();
            $brand->created_at = \Carbon\Carbon::now();
            $brand->updated_at = \Carbon\Carbon::now();
            $brand->save();
        }
    }
}
