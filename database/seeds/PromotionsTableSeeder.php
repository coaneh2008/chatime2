<?php

use Illuminate\Database\Seeder;

class PromotionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('promotions')->delete();

        $categoryId1 = App\Model\PromotionCategory::where('type', App\Model\PromotionCategory::TYPE_HOLIDAY_PROMO)
            ->first()->id;
        $categoryId2 = App\Model\PromotionCategory::where('type', App\Model\PromotionCategory::TYPE_PROMO_INFO)
            ->first()->id;
        $categoryId3 = App\Model\PromotionCategory::where('type', App\Model\PromotionCategory::TYPE_MEMBER_PROMO)
            ->first()->id;
        $categoryId4 = App\Model\PromotionCategory::where('type', App\Model\PromotionCategory::TYPE_PRODUCT_PROMO)
            ->first()->id;
        $categoryId5 = App\Model\PromotionCategory::where('type', App\Model\PromotionCategory::TYPE_FLASH_SALE)
            ->first()->id;

        $productId = App\Model\Product::first()->id;

        $promo1 = new App\Model\Promotion();
        $promo1->category_id = $categoryId1;
        $promo1->content = '
	        <h1>HOME FOR HOLIDAY INSPIRATIONS</h1>
	        <p><time>22 November &mdash; 25 Desember 2017</time></p>

	        <p><b>HOLIDAY IS COMING!!!</b></p>
	        <p>Memasuki bulan Desember artinya sebentar lagi Hari Raya Natal akan tiba. Bersiaplah untuk mendekorasi rumah dengan pernak pernik Natal. Kegiatan ini selalu menjadi aktivitas wajib di rumah ketika menyambut hari yang penuh damai dan suka cita. Tak ketinggalan, makan malam bersama keluarga dan orang yang dikasihi saat malam Natal juga melengkapi momen indah ini menjadi lebih bahagia.</p>
        ';
        $promo1->start_date = '2018-03-21 20:59:55';
        $promo1->end_date = '2018-05-21 20:59:55';
        // $promo1->file = asset('assets/img/promo-popup.jpg');
        $promo1->published_at = \Carbon\Carbon::now();
        $promo1->save();

        $promo2 = new App\Model\Promotion();
        $promo2->category_id = $categoryId2;
        $promo2->content = '
	        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, excepturi voluptates modi distinctio, enim porro consectetur accusantium vitae asperiores vero repellendus nihil quidem alias delectus aut labore praesentium doloribus deserunt ratione! Necessitatibus voluptates, eos repudiandae fuga ex maiores praesentium, magnam sapiente tempora aspernatur eveniet quam voluptas laudantium velit, facilis delectus.</p>
	        <p>Ab voluptatem aliquid facere error labore deserunt repudiandae eos ut neque, quos ducimus minus impedit excepturi, nesciunt perspiciatis architecto iusto dolor culpa consectetur, asperiores quisquam cumque temporibus? Est rem voluptatem ducimus adipisci officia totam aperiam deserunt, voluptates quae maxime natus dignissimos, similique, delectus? Nostrum inventore natus itaque vitae nam praesentium.</p>
	        <p>Quibusdam, nobis, non quo magnam id, veritatis architecto soluta libero similique beatae asperiores? Officiis, libero nesciunt sint? Odit recusandae totam molestiae, veniam quaerat odio ullam, officia praesentium voluptatem pariatur, error tempore quis possimus amet deserunt vel sed. Accusantium, omnis, officia! Sint recusandae quas quia alias, maxime iste optio reprehenderit unde.</p>
	        <p>Molestias assumenda atque rerum error fugiat mollitia sequi laborum, quasi velit minima autem numquam expedita quibusdam dignissimos aspernatur accusamus tenetur exercitationem harum, magni, commodi repellendus. Minus ex iure accusantium unde assumenda at nisi aperiam veritatis tenetur asperiores dignissimos, magni provident voluptate dolorum numquam quas labore cupiditate! In quasi libero sapiente.</p>
	        <p>Minima ducimus cumque voluptates quos itaque officia, doloribus dolore! Nesciunt suscipit corporis dolor mollitia a vel voluptatum veniam hic nostrum temporibus quisquam aliquam provident, molestias asperiores pariatur recusandae repellat! Perspiciatis hic rerum maiores adipisci optio alias praesentium, quisquam cumque libero aperiam error soluta quam ut provident, et cum voluptate aut.</p>
        ';
        $promo2->start_date = '2018-03-21 20:59:55';
        $promo2->end_date = '2018-05-21 20:59:55';
        // $promo2->file = asset('assets/img/promo-popup.jpg');
        $promo2->published_at = \Carbon\Carbon::now();
        $promo2->save();

        $promo3 = new App\Model\Promotion();
        $promo3->category_id = $categoryId3;
        $promo3->content = '
        <div class="member-promos">
            <div class="member-promos-item member-promos-item--big">
                <div class="member-promo member-promo--intro">
                    Nikmati potongan harga hingga 50% semua produk ACE berlaku untuk umum
                </div>
            </div>
            <div class="member-promos-item member-promos-item--small">
                <div class="member-promo member-promo--circle">
                    <img src="assets/img/member-promo-icon-1.png" alt="">
                </div>
                <h4 class="text-center">Penawaran spesial member &amp; bank partner</h4>
            </div>
            <div class="member-promos-item member-promos-item--small">
                <div class="member-promo member-promo--circle">
                    <img src="assets/img/member-promo-icon-2.png" alt="">
                </div>
                <h4 class="text-center">Gift with purchases</h4>
            </div>
            <div class="member-promos-item member-promos-item--small">
                <div class="member-promo member-promo--circle">
                    <img src="assets/img/member-promo-icon-3.png" alt="">
                </div>
                <h4 class="text-center">Tukar poin</h4>
            </div>
        </div>
        ';
        $promo3->start_date = '2018-03-21 20:59:55';
        $promo3->end_date = '2018-05-21 20:59:55';
        // $promo3->file = asset('assets/img/promo-popup.jpg');
        $promo3->published_at = \Carbon\Carbon::now();
        $promo3->save();

        $promo4 = new App\Model\ProductPromotion();
        $promo4->category_id = $categoryId4;
        $promo4->product_id = $productId;
        // $promo4->discount = 20;
        $promo4->order = 1;
        $promo4->start_date = '2018-03-21 20:59:55';
        $promo4->end_date = '2018-05-21 20:59:55';
        // $promo4->file = asset('assets/img/promo-popup.jpg');
        $promo4->published_at = \Carbon\Carbon::now();
        $promo4->save();

        $promo4 = new App\Model\ProductPromotion();
        $promo4->category_id = $categoryId4;
        $promo4->product_id = $productId;
        // $promo4->discount = 25;
        $promo4->order = 2;
        $promo4->start_date = '2018-03-21 20:59:55';
        $promo4->end_date = '2018-05-21 20:59:55';
        $promo4->published_at = \Carbon\Carbon::now();
        $promo4->save();

        $promo5 = new App\Model\FlashSale();
        $promo5->category_id = $categoryId5;
        $promo5->url_to_ace_online = 'https://www.ruparupa.com/promotions/online-exclusive.html?utm_source=acehardware.co.id&utm_medium=referral&utm_content=floating+ace&utm_campaign=online+exclusive+luggage';
        $promo5->banner_image = asset('assets/img/promo-popup.jpg');
        $promo5->start_date = '2018-03-21 20:59:55';
        $promo5->end_date = '2018-05-21 20:59:55';
        $promo5->published_at = \Carbon\Carbon::now();
        $promo5->save();
    }
}
