<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->delete();

        $region1 = new App\Model\Region;
        $region1->title = 'DKI Jakarta';
        $region1->order = 1;
        $region1->published_at = \Carbon\Carbon::now();
        $region1->created_at = \Carbon\Carbon::now();
        $region1->updated_at = \Carbon\Carbon::now();
        $region1->save();

        $region2 = new App\Model\Region;
        $region2->title = 'Medan';
        $region2->order = 2;
        $region2->published_at = \Carbon\Carbon::now();
        $region2->created_at = \Carbon\Carbon::now();
        $region2->updated_at = \Carbon\Carbon::now();
        $region2->save();
    }
}
