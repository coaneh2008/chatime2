<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(BannersTableSeeder::class);
        $this->call(BrochuresTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(FeaturedBrandsTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(ProductCategoriesTableSeeder::class);
        $this->call(InspirationArticlesTableSeeder::class);
        $this->call(RegionsTableSeeder::class);
        $this->call(StoreLocationsTableSeeder::class);
        $this->call(MemberBenefitSeeder::class);
        $this->call(PromotionCategoriesTableSeeder::class);
        $this->call(PromotionsTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(HomePromotionsTableSeeder::class);

        Model::reguard();
    }
}
