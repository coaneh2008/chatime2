<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contacts')->delete();

        DB::table('contacts')->insert([
            'name' => 'Example Name',
            'address' => 'Example Address',
            'phone' => '0123',
            'email' => 'example@example.com',
            'message' => 'Example question',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
