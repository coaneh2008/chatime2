<?php

use App\Model\MemberBenefit;
use App\Model\MemberNewsUpdate;
use App\Model\MemberPromo;
use App\Model\MerchantCategory;
use App\Model\MerchantLocation;
use App\Model\MerchantPromo;
use Illuminate\Database\Seeder;

class MemberBenefitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('member_benefits')->delete();
        DB::table('merchant_locations')->delete();
        DB::table('merchant_promo_categories')->delete();
        $faker = Faker\Factory::create();
        $type = [0, 1, 2, 3];
        for ($i = 0; $i < 10; $i++) { 
        	$benefit = MemberBenefit::create([
	            'title' => $faker->word(1),
	            'type' => $faker->numberBetween(0,3),
	            'description' => '<h1 class="text-caps">' . $faker->sentence() . '</h1>
                            <h2>' . $faker->paragraph() . '</h2>',
	            
	            'thumb_image' => 'http://frontend.suitdev.com/ace-hardware-product/assets/img/inspiration-small-1.jpg',
	            'banner_image' => 'http://frontend.suitdev.com/ace-hardware-product/assets/img/banner-1.jpg',
	            'published' => 1,
	            'order' => $i,
	        ]);
	        \Log::info('Create data -> ' . $i);

	        if ($benefit->type == 0) {
	        	$benefit->title = 'Free Content ' . $benefit->title; 
	        	$benefit->content = '<div class="fg fg-768-2">
	                    <div>
	                        <h3 class="text-caps">Use point</h3>

	                        <ol class="custom-ol">
	                            <li>Tukarkan point dengan voucher <a href="/membership/voucher-list">(lihat detail)</a></li>
	                            <li>Gunakan untuk berbelanja</li>
	                            <li>Tukarkan point dengan merchandise <a href="/membership/merchandises">(lihat detail)</a></li>
	                        </ol>
	                    </div>
	                    <div>
	                        <h3 class="text-caps">How to get point</h3>

	                        <ol class="custom-ol">
	                            <li>Belanja sebanyak-banyaknya</li>
	                            <li>Anda akan mendapatkan point setiap pembelian 100k</li>
	                        </ol>
	                    </div>
	                </div>';
	               $benefit->save();
	        } elseif ($benefit->type == 1) {
	        	$benefit->title = 'Member Promo ' . $benefit->title; 
	        	$benefit->save();
	        	for ($j = 0; $j < 10; $j++) {
	        		$promo = MemberPromo::create([
	        			'member_benefit_id' => $benefit->getKey(),
				        'title' => $faker->sentence(2),
				        'description' => $faker->paragraph(),
				        'content' => '<ol class="custom-ol">
                                <li>' . $faker->paragraph(6) .
                                '</li>
                                <li>' . $faker->paragraph(6) .
                                '</li>
                                <li>'
                                . $faker->paragraph(6) .
                                '</li>
                            </ol>',
				        'thumb_image' => 'http://frontend.suitdev.com/ace-hardware-product/assets/img/inspiration-small-1.jpg',
				        'banner_image' => 'http://frontend.suitdev.com/ace-hardware-product/assets/img/banner-1.jpg',
				        'published' => 1,
				        'start_date' => \Carbon\Carbon::create(2018,03,01),
				        'end_date' => \Carbon\Carbon::create(2018,04,30),
				        'order' => $j,
				        'is_highlighted' => $faker->randomElement([0,1]),
	        		]);
	        	}
	        } elseif ($benefit->type == 2) {
	        	$benefit->title = 'Merchant Promo ' . $benefit->title; 
	        	$benefit->save();
	        	for ($j = 0; $j < 10; $j++) { 
	        		MerchantCategory::create([
	        			'title' => $faker->sentence(1),
	        			'order' => $j,
	        		]);
	        	}
	        	for ($j = 0; $j < 10; $j++) { 
	        		MerchantLocation::create([
	        			'title' => $faker->city(),
	        			'order' => $j,
	        		]);
	        	}
	        	for ($j = 0; $j < 20; $j ++) {
	        		$categoryIds = MerchantCategory::lists('id')->toArray();
	        		$promo = MerchantPromo::create([
	        			'member_benefit_id' => $benefit->getKey(),
	        			'merchant_category_id' => $faker->randomElement($categoryIds),
	        			'merchant_name' => $faker->word(2),
				        'title' => $faker->sentence(2),
				        'description' => $faker->paragraph(1),
				        'content' => '<ol class="custom-ol">
                                <li>' . $faker->paragraph(6) .
                                '</li>
                                <li>' . $faker->paragraph(6) .
                                '</li>
                                <li>'
                                . $faker->paragraph(6) .
                                '</li>
                            </ol>',
				        'thumb_image' => 'http://frontend.suitdev.com/ace-hardware-product/assets/img/inspiration-small-1.jpg',
				        'banner_image' => 'http://frontend.suitdev.com/ace-hardware-product/assets/img/banner-1.jpg',
				        'published' => 1,
				        'start_date' => \Carbon\Carbon::create(2018,03,01),
				        'end_date' => \Carbon\Carbon::create(2018,04,30),
				        'order' => $j,
				        'is_highlighted' => $faker->randomElement([0,1]),
	        		]);
	        		$locationIds = MerchantLocation::lists('id')->toArray();
		        	$total = $faker->numberBetween(1,count($locationIds));
		        	$array = $faker->randomElements($array = $locationIds, $count = $total);
		        	$promo->locations()->sync($array);
	        	}
	        	
		    } elseif ($benefit->type == 3) {
		    	$benefit->title = 'News Update ' . $benefit->title; 
		    	$benefit->save();
	        	for ($j = 0; $j < 10; $j ++) {
	        		$promo = MemberNewsUpdate::create([
	        			'member_benefit_id' => $benefit->getKey(),
				        'title' => $faker->sentence(2),
				        'content' => '<ol class="custom-ol">
	                            <li>' . $faker->paragraph(6) .
	                            '</li>
	                            <li>' . $faker->paragraph(6) .
	                            '</li>
	                            <li>'
	                            . $faker->paragraph(6) .
	                            '</li>
	                        </ol>',
				        'thumb_image' => 'http://frontend.suitdev.com/ace-hardware-product/assets/img/inspiration-small-1.jpg',
				        'banner_image' => 'http://frontend.suitdev.com/ace-hardware-product/assets/img/banner-1.jpg',
				        'published' => 1,
				        'start_date' => \Carbon\Carbon::create(2018,03,01),
				        'end_date' => \Carbon\Carbon::create(2018,04,30),
				        'order' => $j,
				        'is_highlighted' => $faker->randomElement([0,1]),
	        		]);
	        	}
	        } else { dd ('dd');}
	    }  
    }
}
