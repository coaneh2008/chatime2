<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->delete();

        $event1 = new App\Model\Event();
        $event1->type = App\Model\Event::TYPE_ANNUAL_EVENT;
        $event1->title = 'Cozy Home';
        $event1->description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis labore ducimus unde laboriosam est qui earum!";
        $event1->content = "
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium iusto amet optio eum quam veniam aliquid molestiae nam fugiat quis nemo cupiditate libero, magni, enim, totam nulla ducimus quaerat assumenda sit magnam architecto quasi! Autem fugiat enim sint minima laudantium ab. Quo totam ipsa alias, perferendis nemo repudiandae fuga reprehenderit!
            </p>
            <p>Officiis laudantium consequuntur et dolorem explicabo nulla, ullam nesciunt quibusdam quae? Sed veniam, culpa est aperiam molestiae itaque ratione numquam quisquam vero. Ex quisquam velit minima quas nulla eum quaerat suscipit asperiores, nemo accusantium harum veniam aspernatur sunt unde, autem voluptatem quo officiis a placeat, cumque omnis labore repudiandae modi!
            </p>
        ";
        $event1->total_participant = 50;
        $event1->order = 1;
        $event1->start_date = '2018-03-01';
        $event1->end_date = '2018-03-31';
        $event1->published_at = \Carbon\Carbon::now();
        $event1->created_at = \Carbon\Carbon::now();
        $event1->updated_at = \Carbon\Carbon::now();
        $event1->save();

        $eventParticipant1 = new App\Model\EventParticipant();
        $eventParticipant1->user_id = \App\Model\User::first()->id;
        $eventParticipant1->event_id = $event1->id;
        $eventParticipant1->save();

        $event2 = new App\Model\Event();
        $event2->type = App\Model\Event::TYPE_TESTIMONY;
        $event2->title = 'Krisbow Testimony';
        $event2->description = "Give testimony about 'Krisbow' brand";
        $event2->content = "
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque mollitia ducimus laborum architecto, maiores quis recusandae molestiae ipsum, beatae, ipsa excepturi eius voluptatibus, placeat ut odit accusamus iste quae facilis.</p>
        ";
        $event2->order = 2;
        $event2->start_date = '2018-03-01';
        $event2->end_date = '2018-03-31';
        $event2->published_at = \Carbon\Carbon::now();
        $event2->created_at = \Carbon\Carbon::now();
        $event2->updated_at = \Carbon\Carbon::now();
        $event2->save();

        $testimony1 = new App\Model\Testimony();
        $testimony1->user_id = \App\Model\User::first()->id;
        $testimony1->event_id = $event2->id;
        $testimony1->comment = 'Really good brand';
        $testimony1->save();

        $event3 = new App\Model\Event();
        $event3->type = App\Model\Event::TYPE_MEMBER_GET_MEMBER;
        $event3->title = 'Member Get Member';
        $event3->description = "Invite your friend to join ACE Rewards";
        $event3->content = "";
        $event3->order = 3;
        $event3->prefix_referal_code = 'MAR';
        $event3->start_date = '2018-03-01';
        $event3->end_date = '2018-03-31';
        $event3->published_at = \Carbon\Carbon::now();
        $event3->created_at = \Carbon\Carbon::now();
        $event3->updated_at = \Carbon\Carbon::now();
        $event3->save();

        $event4 = new App\Model\Event();
        $event4->type = App\Model\Event::TYPE_FREE_MEMBERSHIP;
        $event4->title = 'Free Membership';
        $event4->description = "Get 6 month free membership";
        $event4->content = "";
        $event4->order = 4;
        $event4->start_date = '2018-03-01';
        $event4->end_date = '2018-03-31';
        $event4->published_at = \Carbon\Carbon::now();
        $event4->created_at = \Carbon\Carbon::now();
        $event4->updated_at = \Carbon\Carbon::now();
        $event4->save();
    }
}
