<?php

use Illuminate\Database\Seeder;

class StoreLocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('store_locations')->delete();

        $regionOldestId = DB::table('regions')->first()->id;

        $storeLocation1 = new App\Model\StoreLocation;
        $storeLocation1->title = 'Cempaka Putih';
        $storeLocation1->region_id = $regionOldestId;
        $storeLocation1->address = "Jl. Letjen. Suprapto No. 62, Cempaka Putih, RT08/RW03, Jakarta Pusat, DKI Jakarta";
        $storeLocation1->latitude = -6.164598;
        $storeLocation1->longitude = 106.878090;
        $storeLocation1->published_at = \Carbon\Carbon::now();
        $storeLocation1->created_at = \Carbon\Carbon::now();
        $storeLocation1->updated_at = \Carbon\Carbon::now();
        $storeLocation1->save();

        $storeLocation2 = new App\Model\StoreLocation;
        $storeLocation2->title = 'Emporium Pluit Mall';
        $storeLocation2->region_id = $regionOldestId;
        $storeLocation2->address = "Jl. Pluit Selatan Raya, RT23/RW08, Penjaringan, Jakarta Utara, DKI Jakarta";
        $storeLocation2->latitude = -6.127535;
        $storeLocation2->longitude = 106.791229;
        $storeLocation2->published_at = \Carbon\Carbon::now();
        $storeLocation2->created_at = \Carbon\Carbon::now();
        $storeLocation2->updated_at = \Carbon\Carbon::now();
        $storeLocation2->save();

        $storeLocation3 = new App\Model\StoreLocation;
        $storeLocation3->title = 'Fatmawati';
        $storeLocation3->region_id = $regionOldestId;
        $storeLocation3->address = "Jl. Fatmawati Raya Blok B1 No. 42, Cilandak Barat, RT05/RW03, Jakarta Selatan, DKI Jakarta";
        $storeLocation3->latitude = -6.264171;
        $storeLocation3->longitude = 106.798868;
        $storeLocation3->published_at = \Carbon\Carbon::now();
        $storeLocation3->created_at = \Carbon\Carbon::now();
        $storeLocation3->updated_at = \Carbon\Carbon::now();
        $storeLocation3->save();

        $storeLocation4 = new App\Model\StoreLocation;
        $storeLocation4->title = 'Sun Plaza';
        $storeLocation4->region_id = $regionOldestId+1;
        $storeLocation4->address = "Jl. KH. Zainul Arifin No. 7 (Jl. Pangeran Diponegoro), Medan, Sumatera Utara.";
        $storeLocation4->latitude = 3.581425;
        $storeLocation4->longitude = 98.671281;
        $storeLocation4->published_at = \Carbon\Carbon::now();
        $storeLocation4->created_at = \Carbon\Carbon::now();
        $storeLocation4->updated_at = \Carbon\Carbon::now();
        $storeLocation4->save();

        $storeLocation5 = new App\Model\StoreLocation;
        $storeLocation5->title = 'Centre Point';
        $storeLocation5->region_id = $regionOldestId+1;
        $storeLocation5->address = "Komplek Medan Centre Point, Jl. Timor Blok H - I, Medan, Sumatera Utara";
        $storeLocation5->latitude = 3.591796;
        $storeLocation5->longitude = 98.680948;
        $storeLocation5->published_at = \Carbon\Carbon::now();
        $storeLocation5->created_at = \Carbon\Carbon::now();
        $storeLocation5->updated_at = \Carbon\Carbon::now();
        $storeLocation5->save();

        $storeLocation6 = new App\Model\StoreLocation;
        $storeLocation6->title = 'Thamrin Plaza';
        $storeLocation6->region_id = $regionOldestId+1;
        $storeLocation6->address = "Jalan Thamrin No. 75R, Sei Rengas II, Medan Area, Kota Medan, Sumatera Utara";
        $storeLocation6->latitude = 3.586506;
        $storeLocation6->longitude = 98.691773;
        $storeLocation6->published_at = \Carbon\Carbon::now();
        $storeLocation6->created_at = \Carbon\Carbon::now();
        $storeLocation6->updated_at = \Carbon\Carbon::now();
        $storeLocation6->save();
    }
}
