<?php

use Illuminate\Database\Seeder;

class PromotionCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('promotion_categories')->delete();

        $promotionCategory1 = new \App\Model\PromotionCategory;
        $promotionCategory1->title = 'Holiday Promotion';
        $promotionCategory1->type = \App\Model\PromotionCategory::TYPE_HOLIDAY_PROMO;
        $promotionCategory1->order = 1;
        $promotionCategory1->published_at = \Carbon\Carbon::now();
        $promotionCategory1->save();

        $promotionCategory2 = new \App\Model\PromotionCategory;
        $promotionCategory2->title = 'Information';
        $promotionCategory2->type = \App\Model\PromotionCategory::TYPE_PROMO_INFO;
        $promotionCategory2->order = 2;
        $promotionCategory2->published_at = \Carbon\Carbon::now();
        $promotionCategory2->save();

        $promotionCategory3 = new \App\Model\PromotionCategory;
        $promotionCategory3->title = 'Member Promo';
        $promotionCategory3->type = \App\Model\PromotionCategory::TYPE_MEMBER_PROMO;
        $promotionCategory3->order = 3;
        $promotionCategory3->published_at = \Carbon\Carbon::now();
        $promotionCategory3->save();

        $promotionCategory4 = new \App\Model\PromotionCategory;
        $promotionCategory4->title = 'Product Promotion';
        $promotionCategory4->type = \App\Model\PromotionCategory::TYPE_PRODUCT_PROMO;
        $promotionCategory4->order = 4;
        $promotionCategory4->published_at = \Carbon\Carbon::now();
        $promotionCategory4->save();

        $promotionCategory5 = new \App\Model\PromotionCategory;
        $promotionCategory5->title = 'Flash Sale';
        $promotionCategory5->type = \App\Model\PromotionCategory::TYPE_FLASH_SALE;
        $promotionCategory5->order = 5;
        $promotionCategory5->published_at = \Carbon\Carbon::now();
        $promotionCategory5->save();
    }
}
