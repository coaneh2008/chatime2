<?php

use Illuminate\Database\Seeder;

class ProductCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_categories')->delete();
        
        $primaryProductCategories1 = new App\Model\ProductCategory();
        $primaryProductCategories1->title = 'Home and Living';
        $primaryProductCategories1->type = App\Model\ProductCategory::TYPE_PRIMARY;
        $primaryProductCategories1->thumb_image = asset('assets/img/icon-category-home-living.png');
        $primaryProductCategories1->banner_image = asset('assets/img/product-hero-banner.jpg');
        $primaryProductCategories1->description = "Find your dream living room";
        $primaryProductCategories1->content = "
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium iusto amet optio eum quam veniam aliquid molestiae nam fugiat quis nemo cupiditate libero, magni, enim, totam nulla ducimus quaerat assumenda sit magnam architecto quasi! Autem fugiat enim sint minima laudantium ab. Quo totam ipsa alias, perferendis nemo repudiandae fuga reprehenderit!
            </p>
            <p>Officiis laudantium consequuntur et dolorem explicabo nulla, ullam nesciunt quibusdam quae? Sed veniam, culpa est aperiam molestiae itaque ratione numquam quisquam vero. Ex quisquam velit minima quas nulla eum quaerat suscipit asperiores, nemo accusantium harum veniam aspernatur sunt unde, autem voluptatem quo officiis a placeat, cumque omnis labore repudiandae modi!
            </p>
        ";
        $primaryProductCategories1->order = 1;
        $primaryProductCategories1->published_at = \Carbon\Carbon::now();
        $primaryProductCategories1->created_at = \Carbon\Carbon::now();
        $primaryProductCategories1->updated_at = \Carbon\Carbon::now();
        $primaryProductCategories1->save();

        $primaryProductCategories2 = new App\Model\ProductCategory();
        $primaryProductCategories2->title = 'Furniture';
        $primaryProductCategories2->type = App\Model\ProductCategory::TYPE_PRIMARY;
        $primaryProductCategories2->thumb_image = asset('assets/img/icon-category-furniture.png');
        $primaryProductCategories2->description = "";
        $primaryProductCategories2->content = "";
        $primaryProductCategories2->order = 2;
        $primaryProductCategories2->published_at = \Carbon\Carbon::now();
        $primaryProductCategories2->created_at = \Carbon\Carbon::now();
        $primaryProductCategories2->updated_at = \Carbon\Carbon::now();
        $primaryProductCategories2->save();

        $primaryProductCategories3 = new App\Model\ProductCategory();
        $primaryProductCategories3->title = 'Home Improvement';
        $primaryProductCategories3->type = App\Model\ProductCategory::TYPE_PRIMARY;
        $primaryProductCategories3->thumb_image = asset('assets/img/icon-category-home-improvement.png');
        $primaryProductCategories3->description = "";
        $primaryProductCategories3->content = "";
        $primaryProductCategories3->order = 3;
        $primaryProductCategories3->published_at = \Carbon\Carbon::now();
        $primaryProductCategories3->created_at = \Carbon\Carbon::now();
        $primaryProductCategories3->updated_at = \Carbon\Carbon::now();
        $primaryProductCategories3->save();

        $primaryProductCategories4 = new App\Model\ProductCategory();
        $primaryProductCategories4->title = 'Hobbies and Lifestyle';
        $primaryProductCategories4->type = App\Model\ProductCategory::TYPE_PRIMARY;
        $primaryProductCategories4->thumb_image = asset('assets/img/icon-category-hobby-lifestyle.png');
        $primaryProductCategories4->description = "";
        $primaryProductCategories4->content = "";
        $primaryProductCategories4->order = 4;
        $primaryProductCategories4->published_at = \Carbon\Carbon::now();
        $primaryProductCategories4->created_at = \Carbon\Carbon::now();
        $primaryProductCategories4->updated_at = \Carbon\Carbon::now();
        $primaryProductCategories4->save();

        $primaryProductCategories5 = new App\Model\ProductCategory();
        $primaryProductCategories5->title = 'Electronic and Gadget';
        $primaryProductCategories5->type = App\Model\ProductCategory::TYPE_PRIMARY;
        $primaryProductCategories5->thumb_image = asset('assets/img/icon-category-electronic.png');
        $primaryProductCategories5->description = "";
        $primaryProductCategories5->content = "";
        $primaryProductCategories5->order = 5;
        $primaryProductCategories5->published_at = \Carbon\Carbon::now();
        $primaryProductCategories5->created_at = \Carbon\Carbon::now();
        $primaryProductCategories5->updated_at = \Carbon\Carbon::now();
        $primaryProductCategories5->save();

        $primaryProductCategories6 = new App\Model\ProductCategory();
        $primaryProductCategories6->title = 'Kitchen';
        $primaryProductCategories6->type = App\Model\ProductCategory::TYPE_PRIMARY;
        $primaryProductCategories6->thumb_image = asset('assets/img/icon-category-kitchen.png');
        $primaryProductCategories6->description = "";
        $primaryProductCategories6->content = "";
        $primaryProductCategories6->order = 6;
        $primaryProductCategories6->published_at = \Carbon\Carbon::now();
        $primaryProductCategories6->created_at = \Carbon\Carbon::now();
        $primaryProductCategories6->updated_at = \Carbon\Carbon::now();
        $primaryProductCategories6->save();

        $primaryProductCategories7 = new App\Model\ProductCategory();
        $primaryProductCategories7->title = 'Bed and Bath';
        $primaryProductCategories7->type = App\Model\ProductCategory::TYPE_PRIMARY;
        $primaryProductCategories7->thumb_image = asset('assets/img/icon-category-bed-bath.png');
        $primaryProductCategories7->description = "";
        $primaryProductCategories7->content = "";
        $primaryProductCategories7->order = 7;
        $primaryProductCategories7->published_at = \Carbon\Carbon::now();
        $primaryProductCategories7->created_at = \Carbon\Carbon::now();
        $primaryProductCategories7->updated_at = \Carbon\Carbon::now();
        $primaryProductCategories7->save();

        $primaryProductCategories8 = new App\Model\ProductCategory();
        $primaryProductCategories8->title = 'Automotive';
        $primaryProductCategories8->type = App\Model\ProductCategory::TYPE_PRIMARY;
        $primaryProductCategories8->thumb_image = asset('assets/img/icon-category-automotive.png');
        $primaryProductCategories8->description = "";
        $primaryProductCategories8->content = "";
        $primaryProductCategories8->order = 8;
        $primaryProductCategories8->published_at = \Carbon\Carbon::now();
        $primaryProductCategories8->created_at = \Carbon\Carbon::now();
        $primaryProductCategories8->updated_at = \Carbon\Carbon::now();
        $primaryProductCategories8->save();

        $primaryProductCategories9 = new App\Model\ProductCategory();
        $primaryProductCategories9->title = 'Health and Sport';
        $primaryProductCategories9->type = App\Model\ProductCategory::TYPE_PRIMARY;
        $primaryProductCategories9->thumb_image = asset('assets/img/icon-category-health-sport.png');
        $primaryProductCategories9->description = "";
        $primaryProductCategories9->content = "";
        $primaryProductCategories9->order = 9;
        $primaryProductCategories9->published_at = \Carbon\Carbon::now();
        $primaryProductCategories9->created_at = \Carbon\Carbon::now();
        $primaryProductCategories9->updated_at = \Carbon\Carbon::now();
        $primaryProductCategories9->save();

        $primaryProductCategories10 = new App\Model\ProductCategory();
        $primaryProductCategories10->title = 'Toy and Baby';
        $primaryProductCategories10->type = App\Model\ProductCategory::TYPE_PRIMARY;
        $primaryProductCategories10->thumb_image = asset('assets/img/icon-category-toy.png');
        $primaryProductCategories10->description = "";
        $primaryProductCategories10->content = "";
        $primaryProductCategories10->order = 10;
        $primaryProductCategories10->published_at = \Carbon\Carbon::now();
        $primaryProductCategories10->created_at = \Carbon\Carbon::now();
        $primaryProductCategories10->updated_at = \Carbon\Carbon::now();
        $primaryProductCategories10->save();

        $primaryProductCategories11 = new App\Model\ProductCategory();
        $primaryProductCategories11->title = 'Holiday Gift';
        $primaryProductCategories11->type = App\Model\ProductCategory::TYPE_PRIMARY;
        $primaryProductCategories11->thumb_image = asset('assets/img/icon-category-gift.png');
        $primaryProductCategories11->description = "";
        $primaryProductCategories11->content = "";
        $primaryProductCategories11->order = 11;
        $primaryProductCategories11->published_at = \Carbon\Carbon::now();
        $primaryProductCategories11->created_at = \Carbon\Carbon::now();
        $primaryProductCategories11->updated_at = \Carbon\Carbon::now();
        $primaryProductCategories11->save();

        $primaryProductCategories12 = new App\Model\ProductCategory();
        $primaryProductCategories12->title = 'Best Deal';
        $primaryProductCategories12->type = App\Model\ProductCategory::TYPE_PRIMARY;
        $primaryProductCategories12->thumb_image = asset('assets/img/icon-category-deal.png');
        $primaryProductCategories12->description = "";
        $primaryProductCategories12->content = "";
        $primaryProductCategories12->order = 12;
        $primaryProductCategories12->published_at = \Carbon\Carbon::now();
        $primaryProductCategories12->created_at = \Carbon\Carbon::now();
        $primaryProductCategories12->updated_at = \Carbon\Carbon::now();
        $primaryProductCategories12->save();

        $productCategoriesOldestId = DB::table('product_categories')->orderBy('created_at', 'asc')->first()->id;
        
        $secondaryProductCategories1 = new App\Model\ProductCategory();
        $secondaryProductCategories1->title = 'Home Decoration';
        $secondaryProductCategories1->url_to_ace_online = 'https://www.ruparupa.com/rumah-tangga/dekorasi-rumah.html';
        $secondaryProductCategories1->parent_id = $productCategoriesOldestId;
        $secondaryProductCategories1->type = App\Model\ProductCategory::TYPE_SECONDARY;
        $secondaryProductCategories1->thumb_image = asset('assets/img/product-desc-1.jpg');
        $secondaryProductCategories1->description = "Make a playfull statement in the living room with full decoration";
        $secondaryProductCategories1->content = "";
        $secondaryProductCategories1->order = 1;
        $secondaryProductCategories1->published_at = \Carbon\Carbon::now();
        $secondaryProductCategories1->created_at = \Carbon\Carbon::now();
        $secondaryProductCategories1->updated_at = \Carbon\Carbon::now();
        $secondaryProductCategories1->save();

        //Secondary menu 2
        $secondaryProductCategories1 = new App\Model\ProductCategory();
        $secondaryProductCategories1->title = 'Perlengkapan Menyuci dan Menyetrika';
        $secondaryProductCategories1->url_to_ace_online = 'https://www.ruparupa.com/rumah-tangga/dekorasi-rumah.html';
        $secondaryProductCategories1->parent_id = $productCategoriesOldestId;
        $secondaryProductCategories1->thumb_image = asset('assets/img/product-desc-1.jpg');
        $secondaryProductCategories1->type = App\Model\ProductCategory::TYPE_SECONDARY;
        $secondaryProductCategories1->description = "Make a playfull statement in the living room with full decoration";
        $secondaryProductCategories1->content = "";
        $secondaryProductCategories1->order = 1;
        $secondaryProductCategories1->published_at = \Carbon\Carbon::now();
        $secondaryProductCategories1->created_at = \Carbon\Carbon::now();
        $secondaryProductCategories1->updated_at = \Carbon\Carbon::now();
        $secondaryProductCategories1->save();
        
        $subproductCategories1OldestId = DB::table('product_categories')
            ->where('type', App\Model\ProductCategory::TYPE_SECONDARY)
            ->orderBy('created_at', 'asc')
            ->first()
            ->id;
        
        $thirdProductCategories1 = new App\Model\ProductCategory();
        $thirdProductCategories1->title = 'Wall Decoration';
        $thirdProductCategories1->url_to_ace_online = 'https://www.ruparupa.com/rumah-tangga/dekorasi-rumah.html';
        $thirdProductCategories1->parent_id = $subproductCategories1OldestId;
        $thirdProductCategories1->type = App\Model\ProductCategory::TYPE_TERTIARY;
        $thirdProductCategories1->description = "Make a playfull statement in the living room with full decoration";
        $thirdProductCategories1->content = "";
        $thirdProductCategories1->order = 1;
        $thirdProductCategories1->published_at = \Carbon\Carbon::now();
        $thirdProductCategories1->created_at = \Carbon\Carbon::now();
        $thirdProductCategories1->updated_at = \Carbon\Carbon::now();
        $thirdProductCategories1->save();

        $thirdProductCategories1 = new App\Model\ProductCategory();
        $thirdProductCategories1->title = 'Desk Decoration';
        $thirdProductCategories1->url_to_ace_online = 'https://www.ruparupa.com/rumah-tangga/dekorasi-rumah.html';
        $thirdProductCategories1->parent_id = $subproductCategories1OldestId;
        $thirdProductCategories1->type = App\Model\ProductCategory::TYPE_TERTIARY;
        $thirdProductCategories1->description = "Make a playfull statement in the living room with full decoration";
        $thirdProductCategories1->content = "";
        $thirdProductCategories1->order = 2;
        $thirdProductCategories1->published_at = \Carbon\Carbon::now();
        $thirdProductCategories1->created_at = \Carbon\Carbon::now();
        $thirdProductCategories1->updated_at = \Carbon\Carbon::now();
        $thirdProductCategories1->save();

        $thirdProductCategories1 = new App\Model\ProductCategory();
        $thirdProductCategories1->title = 'Watch';
        $thirdProductCategories1->url_to_ace_online = 'https://www.ruparupa.com/rumah-tangga/dekorasi-rumah.html';
        $thirdProductCategories1->parent_id = $subproductCategories1OldestId;
        $thirdProductCategories1->type = App\Model\ProductCategory::TYPE_TERTIARY;
        $thirdProductCategories1->description = "Make a playfull statement in the living room with full decoration";
        $thirdProductCategories1->content = "";
        $thirdProductCategories1->order = 3;
        $thirdProductCategories1->published_at = \Carbon\Carbon::now();
        $thirdProductCategories1->created_at = \Carbon\Carbon::now();
        $thirdProductCategories1->updated_at = \Carbon\Carbon::now();
        $thirdProductCategories1->save();


        $thirdProductCategories1 = new App\Model\ProductCategory();
        $thirdProductCategories1->title = 'Wall Decoration';
        $thirdProductCategories1->url_to_ace_online = 'https://www.ruparupa.com/rumah-tangga/dekorasi-rumah.html';
        $thirdProductCategories1->parent_id = $subproductCategories1OldestId+1;
        $thirdProductCategories1->type = App\Model\ProductCategory::TYPE_TERTIARY;
        $thirdProductCategories1->description = "Make a playfull statement in the living room with full decoration";
        $thirdProductCategories1->content = "";
        $thirdProductCategories1->order = 1;
        $thirdProductCategories1->published_at = \Carbon\Carbon::now();
        $thirdProductCategories1->created_at = \Carbon\Carbon::now();
        $thirdProductCategories1->updated_at = \Carbon\Carbon::now();
        $thirdProductCategories1->save();

        $thirdProductCategories1 = new App\Model\ProductCategory();
        $thirdProductCategories1->title = 'Desk Decoration';
        $thirdProductCategories1->url_to_ace_online = 'https://www.ruparupa.com/rumah-tangga/dekorasi-rumah.html';
        $thirdProductCategories1->parent_id = $subproductCategories1OldestId+1;
        $thirdProductCategories1->type = App\Model\ProductCategory::TYPE_TERTIARY;
        $thirdProductCategories1->description = "Make a playfull statement in the living room with full decoration";
        $thirdProductCategories1->content = "";
        $thirdProductCategories1->order = 2;
        $thirdProductCategories1->published_at = \Carbon\Carbon::now();
        $thirdProductCategories1->created_at = \Carbon\Carbon::now();
        $thirdProductCategories1->updated_at = \Carbon\Carbon::now();
        $thirdProductCategories1->save();

        $thirdProductCategories1 = new App\Model\ProductCategory();
        $thirdProductCategories1->title = 'Watch';
        $thirdProductCategories1->url_to_ace_online  = 'https://www.ruparupa.com/rumah-tangga/dekorasi-rumah.html';
        $thirdProductCategories1->parent_id = $subproductCategories1OldestId+1;
        $thirdProductCategories1->type = App\Model\ProductCategory::TYPE_TERTIARY;
        $thirdProductCategories1->description = "Make a playfull statement in the living room with full decoration";
        $thirdProductCategories1->content = "";
        $thirdProductCategories1->order = 3;
        $thirdProductCategories1->published_at = \Carbon\Carbon::now();
        $thirdProductCategories1->created_at = \Carbon\Carbon::now();
        $thirdProductCategories1->updated_at = \Carbon\Carbon::now();
        $thirdProductCategories1->save();
    }
}
