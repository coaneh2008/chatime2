<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->delete();

        DB::table('pages')->insert([
        [
            'slug' => 'term-and-condition',
            'layout' => 'pages.term-and-condition',
            'published_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ],
        [
            'slug' => 'privacy-policy',
            'layout' => 'pages.privacy-policy',
            'published_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ],
        [
            'slug' => 'free-membership-term-and-condition',
            'layout' => 'pages.free-membership-term-and-condition',
            'published_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ],
        ]);

        $pageOldestId = DB::table('pages')->first()->id;

        DB::table('pages_translate')->insert([
        [
            'page_id' => $pageOldestId,
            'lang' => 'en_US',
            'title' => 'Terms &amp; conditions',
            'description' => 'Terms &amp; conditions',
            'content'=> '
                <h2 class="home-section-heading">Terms &amp; conditions</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui in voluptatum ratione amet quo quis dolorum explicabTerms &amp; conditionso distinctio commodi cum tenetur debitis quas repellendus aspernatur neque magni hic suscipit nihil, est libero enim quod nulla esse id labore. Et, excepturi velit. Deserunt libero enim quidem nobis. Reiciendis saepe minus veritatis, tenetur exercitationem laboriosam, deserunt ipsa at qui fugiat minima maiores sit hic veniam laudantium natus quidem voluptatum est! Temporibus, explicabo?
                </p>
                <p>Ad quas consectetur possimus eligendi corporis, nostrum itaque quisquam debitis, inventore iusto dignissimos voluptate, sunt error consequatur quos omnis veritatis ratione cumque quod, eveniet optio voluptatum at aliquam! Sed eveniet qui, nihil libero eos est at repudiandae quaerat natus numquam rerum quis odit nam vel voluptas magni distinctio expedita veniam veritatis aspernatur, sint nesciunt quibusdam architecto deleniti! Velit quidem omnis, minima alias laboriosam quae consectetur, atque nemo, reiciendis cupiditate voluptas.
                </p>
                <p>Nesciunt impedit unde non perferendis possimus magni dicta enim saepe dolores ratione sapiente, necessitatibus, deleniti eligendi quo, explicabo laudantium tempore quae neque ex quibusdam iure aut aspernatur repudiandae pariatur. Sequi officia, repudiandae vitae asperiores temporibus. Ducimus explicabo, architecto eos nam culpa quos laudantium a eveniet officiis sequi praesentium asperiores quas dolores atque error obcaecati temporibus adipisci aut dolor recusandae. Est, maiores facere, velit porro voluptatem quaerat hic veritatis eligendi asperiores.
                </p>
                <p>Deleniti asperiores maiores pariatur mollitia, rem sequi quos quod, illum laboriosam et ut quas, ex impedit illo repudiandae molestias quam corrupti minus nesciunt cum a. Magnam laborum nihil labore doloremque commodi accusantium quam eos, vitae ab ducimus quas quae aperiam maxime autem repudiandae doloribus porro hic? Incidunt ad, asperiores! Quia fugit, repudiandae maiores esse eum omnis, harum ad ea ab voluptate obcaecati ratione consequuntur nostrum temporibus aut quos ut deserunt?
                </p>'
        ],
        [
            'page_id' => $pageOldestId+1,
            'lang' => 'en_US',
            'title' => 'Privacy Policy',
            'description' => 'Privacy Policy',
            'content'=> '
                <h2 class="home-section-heading">Privacy Policy</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui in voluptatum ratione amet quo quis dolorum explicabTerms &amp; conditionso distinctio commodi cum tenetur debitis quas repellendus aspernatur neque magni hic suscipit nihil, est libero enim quod nulla esse id labore. Et, excepturi velit. Deserunt libero enim quidem nobis. Reiciendis saepe minus veritatis, tenetur exercitationem laboriosam, deserunt ipsa at qui fugiat minima maiores sit hic veniam laudantium natus quidem voluptatum est! Temporibus, explicabo?
                </p>
                <p>Ad quas consectetur possimus eligendi corporis, nostrum itaque quisquam debitis, inventore iusto dignissimos voluptate, sunt error consequatur quos omnis veritatis ratione cumque quod, eveniet optio voluptatum at aliquam! Sed eveniet qui, nihil libero eos est at repudiandae quaerat natus numquam rerum quis odit nam vel voluptas magni distinctio expedita veniam veritatis aspernatur, sint nesciunt quibusdam architecto deleniti! Velit quidem omnis, minima alias laboriosam quae consectetur, atque nemo, reiciendis cupiditate voluptas.
                </p>
                <p>Nesciunt impedit unde non perferendis possimus magni dicta enim saepe dolores ratione sapiente, necessitatibus, deleniti eligendi quo, explicabo laudantium tempore quae neque ex quibusdam iure aut aspernatur repudiandae pariatur. Sequi officia, repudiandae vitae asperiores temporibus. Ducimus explicabo, architecto eos nam culpa quos laudantium a eveniet officiis sequi praesentium asperiores quas dolores atque error obcaecati temporibus adipisci aut dolor recusandae. Est, maiores facere, velit porro voluptatem quaerat hic veritatis eligendi asperiores.
                </p>
                <p>Deleniti asperiores maiores pariatur mollitia, rem sequi quos quod, illum laboriosam et ut quas, ex impedit illo repudiandae molestias quam corrupti minus nesciunt cum a. Magnam laborum nihil labore doloremque commodi accusantium quam eos, vitae ab ducimus quas quae aperiam maxime autem repudiandae doloribus porro hic? Incidunt ad, asperiores! Quia fugit, repudiandae maiores esse eum omnis, harum ad ea ab voluptate obcaecati ratione consequuntur nostrum temporibus aut quos ut deserunt?
                </p>'
        ],
        [
            'page_id' => $pageOldestId+2,
            'lang' => 'en_US',
            'title' => 'Free Membership',
            'description' => 'Free Membership',
            'content'=> '
                <h2 class="home-section-heading">Free Membership Term & Condition</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui in voluptatum ratione amet quo quis dolorum explicabTerms &amp; conditionso distinctio commodi cum tenetur debitis quas repellendus aspernatur neque magni hic suscipit nihil, est libero enim quod nulla esse id labore. Et, excepturi velit. Deserunt libero enim quidem nobis. Reiciendis saepe minus veritatis, tenetur exercitationem laboriosam, deserunt ipsa at qui fugiat minima maiores sit hic veniam laudantium natus quidem voluptatum est! Temporibus, explicabo?
                </p>
                <p>Ad quas consectetur possimus eligendi corporis, nostrum itaque quisquam debitis, inventore iusto dignissimos voluptate, sunt error consequatur quos omnis veritatis ratione cumque quod, eveniet optio voluptatum at aliquam! Sed eveniet qui, nihil libero eos est at repudiandae quaerat natus numquam rerum quis odit nam vel voluptas magni distinctio expedita veniam veritatis aspernatur, sint nesciunt quibusdam architecto deleniti! Velit quidem omnis, minima alias laboriosam quae consectetur, atque nemo, reiciendis cupiditate voluptas.
                </p>
                <p>Nesciunt impedit unde non perferendis possimus magni dicta enim saepe dolores ratione sapiente, necessitatibus, deleniti eligendi quo, explicabo laudantium tempore quae neque ex quibusdam iure aut aspernatur repudiandae pariatur. Sequi officia, repudiandae vitae asperiores temporibus. Ducimus explicabo, architecto eos nam culpa quos laudantium a eveniet officiis sequi praesentium asperiores quas dolores atque error obcaecati temporibus adipisci aut dolor recusandae. Est, maiores facere, velit porro voluptatem quaerat hic veritatis eligendi asperiores.
                </p>
                <p>Deleniti asperiores maiores pariatur mollitia, rem sequi quos quod, illum laboriosam et ut quas, ex impedit illo repudiandae molestias quam corrupti minus nesciunt cum a. Magnam laborum nihil labore doloremque commodi accusantium quam eos, vitae ab ducimus quas quae aperiam maxime autem repudiandae doloribus porro hic? Incidunt ad, asperiores! Quia fugit, repudiandae maiores esse eum omnis, harum ad ea ab voluptate obcaecati ratione consequuntur nostrum temporibus aut quos ut deserunt?
                </p>'
        ]
        ]);
    }
}
