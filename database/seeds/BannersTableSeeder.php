<?php

use Illuminate\Database\Seeder;

class BannersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banners')->delete();

        $banner1 = new App\Model\Banner;
        $banner1->image = asset('assets/img/banner-1.jpg');
        $banner1->video_url = 'https://www.youtube.com/embed/_WPsK5O0hKQ';
        $banner1->order = 1;
        $banner1->published_at = \Carbon\Carbon::now();
        $banner1->save();

        $banner2 = new App\Model\Banner;
        $banner2->image = asset('assets/img/banner-2.jpg');
        $banner2->video_url = 'https://www.youtube.com/embed/_WPsK5O0hKQ';
        $banner2->order = 2;
        $banner2->published_at = \Carbon\Carbon::now();
        $banner2->save();
    }
}
