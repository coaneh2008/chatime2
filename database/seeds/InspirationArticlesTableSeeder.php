<?php

use Illuminate\Database\Seeder;

class InspirationArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inspiration_articles')->delete();

        $productCategoryOldestId = DB::table('inspiration_article_categories')->first()->id;
        $userOldestId = DB::table('users')->where('group_type', 1)->first()->id;
        
        $inspirationArticle1 = new App\Model\InspirationArticle();
        $inspirationArticle1->category_id = $productCategoryOldestId;
        $inspirationArticle1->title = "How to start painting your wall";
        $inspirationArticle1->thumb_image = asset('assets/img/inspiration-detail-hero.jpg');
        $inspirationArticle1->banner_image = asset('assets/img/inspiration-big-1.jpg');
        $inspirationArticle1->image_type = \App\Model\InspirationArticle::TYPE_BIG_IMAGE;
        $inspirationArticle1->description = "Beauty your home with the right tools";
        $inspirationArticle1->content = "
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In, ut. Soluta, quod quam. Repellat illum saepe ex sed vel sequi, doloremque velit quibusdam eligendi, delectus omnis similique ratione eveniet earum magnam, soluta quod. Praesentium ut porro minima repudiandae in ipsum mollitia accusantium impedit nobis vel ab et, tempora tempore expedita aperiam quidem repellat itaque quasi earum cupiditate adipisci. Omnis, blanditiis.
        </p>
        ";
        $inspirationArticle1->tags = "home,living";
        $inspirationArticle1->order = 1;
        $inspirationArticle1->author_id = $userOldestId;
        $inspirationArticle1->is_highlighted = 1;
        $inspirationArticle1->published_at = \Carbon\Carbon::now();
        $inspirationArticle1->created_at = \Carbon\Carbon::now();
        $inspirationArticle1->updated_at = \Carbon\Carbon::now();
        $inspirationArticle1->save();

        $inspirationArticle2 = new App\Model\InspirationArticle();
        $inspirationArticle2->category_id = $productCategoryOldestId;
        $inspirationArticle2->title = "How to start painting your wall";
        $inspirationArticle2->thumb_image = asset('assets/img/inspiration-thumb-1.jpg');
        $inspirationArticle2->banner_image = asset('assets/img/inspiration-big-2.jpg');
        $inspirationArticle2->image_type = \App\Model\InspirationArticle::TYPE_BIG_IMAGE;
        $inspirationArticle2->description = "Beauty your home with the right tools";
        $inspirationArticle2->content = "
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In, ut. Soluta, quod quam. Repellat illum saepe ex sed vel sequi, doloremque velit quibusdam eligendi, delectus omnis similique ratione eveniet earum magnam, soluta quod. Praesentium ut porro minima repudiandae in ipsum mollitia accusantium impedit nobis vel ab et, tempora tempore expedita aperiam quidem repellat itaque quasi earum cupiditate adipisci. Omnis, blanditiis.
        </p>
        ";
        $inspirationArticle2->tags = "home,living";
        $inspirationArticle2->order = 2;
        $inspirationArticle2->author_id = $userOldestId;
        $inspirationArticle2->is_highlighted = 1;
        $inspirationArticle2->published_at = \Carbon\Carbon::now();
        $inspirationArticle2->created_at = \Carbon\Carbon::now();
        $inspirationArticle2->updated_at = \Carbon\Carbon::now();
        $inspirationArticle2->save();

        $inspirationArticle3 = new App\Model\InspirationArticle();
        $inspirationArticle3->category_id = $productCategoryOldestId;
        $inspirationArticle3->title = "How to start painting your wall";
        $inspirationArticle3->thumb_image = asset('assets/img/inspiration-thumb-2.jpg');
        $inspirationArticle3->banner_image = asset('assets/img/inspiration-small-1.jpg');
        $inspirationArticle3->image_type = \App\Model\InspirationArticle::TYPE_SMALL_IMAGE;
        $inspirationArticle3->description = "Beauty your home with the right tools";
        $inspirationArticle3->content = "
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In, ut. Soluta, quod quam. Repellat illum saepe ex sed vel sequi, doloremque velit quibusdam eligendi, delectus omnis similique ratione eveniet earum magnam, soluta quod. Praesentium ut porro minima repudiandae in ipsum mollitia accusantium impedit nobis vel ab et, tempora tempore expedita aperiam quidem repellat itaque quasi earum cupiditate adipisci. Omnis, blanditiis.
        </p>
        ";
        $inspirationArticle3->tags = "home,living";
        $inspirationArticle3->order = 1;
        $inspirationArticle3->author_id = $userOldestId;
        $inspirationArticle3->is_highlighted = 1;
        $inspirationArticle3->published_at = \Carbon\Carbon::now();
        $inspirationArticle3->created_at = \Carbon\Carbon::now();
        $inspirationArticle3->updated_at = \Carbon\Carbon::now();
        $inspirationArticle3->save();

        $inspirationArticle4 = new App\Model\InspirationArticle();
        $inspirationArticle4->category_id = $productCategoryOldestId;
        $inspirationArticle4->title = "How to start painting your wall";
        $inspirationArticle4->thumb_image = asset('assets/img/inspiration-thumb-1.jpg');
        $inspirationArticle4->banner_image = asset('assets/img/inspiration-small-2.jpg');
        $inspirationArticle4->image_type = \App\Model\InspirationArticle::TYPE_SMALL_IMAGE;
        $inspirationArticle4->description = "Beauty your home with the right tools";
        $inspirationArticle4->content = "
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In, ut. Soluta, quod quam. Repellat illum saepe ex sed vel sequi, doloremque velit quibusdam eligendi, delectus omnis similique ratione eveniet earum magnam, soluta quod. Praesentium ut porro minima repudiandae in ipsum mollitia accusantium impedit nobis vel ab et, tempora tempore expedita aperiam quidem repellat itaque quasi earum cupiditate adipisci. Omnis, blanditiis.
        </p>
        ";
        $inspirationArticle4->tags = "home,living";
        $inspirationArticle4->order = 2;
        $inspirationArticle4->author_id = $userOldestId;
        $inspirationArticle4->is_highlighted = 1;
        $inspirationArticle4->published_at = \Carbon\Carbon::now();
        $inspirationArticle4->created_at = \Carbon\Carbon::now();
        $inspirationArticle4->updated_at = \Carbon\Carbon::now();
        $inspirationArticle4->save();
    }
}
