<?php

use Illuminate\Database\Seeder;

class HomePromotionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('home_promotions')->delete();
        
        for ($i=1; $i<=2; $i++) {
            $promo = new \App\Model\HomePromotion();
            $promo->title = 'home promotion';
            $promo->content = 'This is content';
            $promo->thumb_image = asset('assets/img/home-promotion-big.jpg');
            $promo->banner_image = asset('assets/img/product-hero-banner.jpg');
            $promo->type = \App\Model\HomePromotion::TYPE_BIG_IMAGE;
            $promo->start_date = '2018-01-01 20:59:55';
            $promo->end_date = '2018-05-21 20:59:55';
            $promo->order = $i;
            $promo->published_at = \Carbon\Carbon::now();
            $promo->save();
        }

        for ($i=1; $i<=2; $i++) {
            $promo = new \App\Model\HomePromotion();
            $promo->title = 'home promotion';
            $promo->content = 'This is content';
            $promo->thumb_image = asset('assets/img/home-promotion-small-' . $i . '.jpg');
            $promo->banner_image = asset('assets/img/product-hero-banner.jpg');
            $promo->type = \App\Model\HomePromotion::TYPE_SMALL_IMAGE;
            $promo->start_date = '2018-01-01 20:59:55';
            $promo->end_date = '2018-05-21 20:59:55';
            $promo->order = $i;
            $promo->published_at = \Carbon\Carbon::now();
            $promo->save();
        }
    }
}
