<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->delete();

        //Root Menu
        DB::table('menus')->insert([
            [
                'is_link' => true,
                'type' => 'Main',
                'url' => 'membership',
                'order' => 2,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'is_link' => true,
                'type' => 'Main',
                'url' => 'inspirations',
                'order' => 3,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'is_link' => true,
                'type' => 'Main',
                'url' => 'https://www.ruparupa.com/',
                'order' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'is_link' => true,
                'type' => 'Main',
                'url' => 'http://www.acehardware.co.id/',
                'order' => 4,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'is_link' => false,
                'type' => 'Footer',
                'url' => '#',
                'order' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'is_link' => false,
                'type' => 'Footer',
                'url' => '#',
                'order' => 2,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'is_link' => false,
                'type' => 'Footer',
                'url' => '#',
                'order' => 3,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
        ]);

        $mainMenuOldestId = DB::table('menus')->where('type', 'Main')->first()->id;
        $footerMenuOldestId = DB::table('menus')->where('type', 'Footer')->first()->id;

        // Child menu
        DB::table('menus')->insert([
            [
                'parent_id' => $footerMenuOldestId,
                'is_link' => true,
                'type' => 'Footer',
                'url' => 'http://www.acehardware.co.id/',
                'order' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'parent_id' => $footerMenuOldestId,
                'is_link' => true,
                'type' => 'Footer',
                'url' => 'https://www.kawanlama.com/career/',
                'order' => 2,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'parent_id' => $footerMenuOldestId,
                'is_link' => true,
                'type' => 'Footer',
                'url' => 'promotions',
                'order' => 3,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'parent_id' => $footerMenuOldestId,
                'is_link' => true,
                'type' => 'Footer',
                'url' => 'inspirations',
                'order' => 4,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'parent_id' => $footerMenuOldestId+1,
                'is_link' => true,
                'type' => 'Footer',
                'url' => 'contact-us',
                'order' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'parent_id' => $footerMenuOldestId+1,
                'is_link' => true,
                'type' => 'Footer',
                'url' => 'privacy-policy',
                'order' => 2,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'parent_id' => $footerMenuOldestId+1,
                'is_link' => true,
                'type' => 'Footer',
                'url' => 'term-and-condition',
                'order' => 3,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'parent_id' => $footerMenuOldestId+1,
                'is_link' => true,
                'type' => 'Footer',
                'url' => 'store-locations',
                'order' => 4,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'parent_id' => $footerMenuOldestId+2,
                'is_link' => false,
                'type' => 'Footer',
                'url' => '#',
                'order' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
        ]);

        DB::table('menus_translate')->insert([
            [
                'menu_id' => $mainMenuOldestId,
                'lang' => 'en_US',
                'title' => 'Membership'
            ],
            [
                'menu_id' => $mainMenuOldestId+1,
                'lang' => 'en_US',
                'title' => 'Inspiration'
            ],
            [
                'menu_id' => $mainMenuOldestId+2,
                'lang' => 'en_US',
                'title' => 'Buy Online'
            ],
            [
                'menu_id' => $mainMenuOldestId+3,
                'lang' => 'en_US',
                'title' => 'Company'
            ],
            [
                'menu_id' => $footerMenuOldestId,
                'lang' => 'en_US',
                'title' => 'Ace hardware'
            ],
            [
                'menu_id' => $footerMenuOldestId+1,
                'lang' => 'en_US',
                'title' => 'Customer services'
            ],
            [
                'menu_id' => $footerMenuOldestId+2,
                'lang' => 'en_US',
                'title' => 'Call center'
            ],
            [
                'menu_id' => $footerMenuOldestId+3,
                'lang' => 'en_US',
                'title' => 'Company'
            ],
            [
                'menu_id' => $footerMenuOldestId+4,
                'lang' => 'en_US',
                'title' => 'Career'
            ],
            [
                'menu_id' => $footerMenuOldestId+5,
                'lang' => 'en_US',
                'title' => 'Promotion'
            ],
            [
                'menu_id' => $footerMenuOldestId+6,
                'lang' => 'en_US',
                'title' => 'Inspiration'
            ],
            [
                'menu_id' => $footerMenuOldestId+7,
                'lang' => 'en_US',
                'title' => 'Contact us'
            ],
            [
                'menu_id' => $footerMenuOldestId+8,
                'lang' => 'en_US',
                'title' => 'Privacy policy'
            ],
            [
                'menu_id' => $footerMenuOldestId+9,
                'lang' => 'en_US',
                'title' => 'Term & conditions'
            ],
            [
                'menu_id' => $footerMenuOldestId+10,
                'lang' => 'en_US',
                'title' => 'Store location'
            ],
            [
                'menu_id' => $footerMenuOldestId+11,
                'lang' => 'en_US',
                'title' => '021-5829100'
            ],
        ]);
        
        //Secondary Menu
        $menu1 = new \App\Model\Menu;
        $menu1->en_US = [
            'title' => 'Store Location'
        ];
        $menu1->is_link = true;
        $menu1->icon_image = asset('assets/img/icon-nav-store.png');
        $menu1->type = \App\Model\Menu::TYPE_SECONDARY;
        $menu1->url = 'store-locations';
        $menu1->order = 1;
        $menu1->save();

        $menu2 = new \App\Model\Menu;
        $menu2->en_US = [
            'title' => 'Contact Us'
        ];
        $menu2->icon_image = asset('assets/img/icon-nav-contact.png');
        $menu2->is_link = true;
        $menu2->type = \App\Model\Menu::TYPE_SECONDARY;
        $menu2->url = 'contact-us';
        $menu2->order = 2;
        $menu2->save();

        $menu3 = new \App\Model\Menu;
        $menu3->en_US = [
            'title' => 'Download App'
        ];
        $menu3->icon_image = asset('assets/img/icon-nav-download.png');
        $menu3->is_link = true;
        $menu3->type = \App\Model\Menu::TYPE_SECONDARY;
        $menu3->url = 'https://itunes.apple.com/us/app/ace-hardware-indonesia/id1056027974?ls=1&mt=8';
        $menu3->order = 3;
        $menu3->save();

        $menu4 = new \App\Model\Menu;
        $menu4->en_US = [
            'title' => 'Brochure'
        ];
        $menu4->icon_image = asset('assets/img/icon-nav-brochure.png');
        $menu4->is_link = true;
        $menu4->type = \App\Model\Menu::TYPE_SECONDARY;
        $menu4->url = 'brochures';
        $menu4->order = 4;
        $menu4->save();

        //Homepage
        $menu = new \App\Model\Menu;
        $menu->en_US = [
            'title' => 'Home Page'
        ];
        $menu->is_link = true;
        $menu->type = \App\Model\Menu::TYPE_HOMEPAGE;
        $menu->url = '';
        $menu->save();
    }
}
